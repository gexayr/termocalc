<?php

namespace Cms\Controller;

use Engine\Core\Template\User;
use Engine\Helper\Cookie;
use Data;
use Material;

class DataController extends CmsController
{
    public function getCities()
    {
        $cities='<option value="">Выберите город </option>';
        $params = $this->request->post;
        $result = Data::getCitiesByReg($params['region']);
        foreach ($result as $item){
            if($item != null){
                $cities.='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
        print_r($cities);
    }

    public function getCityParams()
    {
        $city='';
        $params = $this->request->post;

//        Cookie::set('city', $params['city']);

        $result = Data::getCity($params['city']);

        if(!isset($result) or $result == 0){
            die();
        }
        foreach ($result as $key=>$value){
//            if($item != null){
            $city.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>
                        </tr>';
//            }
        }
        print_r($city);
    }

    public function getBuildingParams()
    {
        $building='';
        $params = $this->request->post;

//        Cookie::set('building', $params['building']);

        $result = Data::getBuildingData($params['building']);
        if(!isset($result) or $result == 0){
            die();
        }
        foreach ($result[0] as $key=>$value){

            $building.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>';

            foreach ($result[1] as $key1=>$value1){
                if($key == $key1){
                    $building.='    <td>
                                '.$value1.'
                            </td>
                        </tr>';
                }
            }
        }
        print_r($building);
    }

    public function getRoomParams()
    {
        $room='';
        $params = $this->request->post;
        $result = Data::getRoom($params['room']);
        foreach ($result as $key=>$value){
//            if($item != null){
            $room.='<tr>
                            <th scope="row">
                                '.$key.'
                            </th>
                            <td>
                                '.$value.'
                            </td>
                        </tr>';
//            }
        }
        print_r($room);
    }
    public function getRoomType()
    {
        $room='';
        $params = $this->request->post;
        $result = Data::getRoom($params['room']);
        $room = json_encode(get_object_vars($result));
        print_r($room);
    }

    public function getCitiesBuildingsParams()
    {
        $params = $this->request->post;
        $city_building = Data::getCitiesBuildingsParams($params);


        print_r($city_building);
    }



    public function getMaterialById()
    {
        $params = $this->request->post;
        $result = Data::getMaterial($params['material_id']);
        print_r($result->name);
    }

    public function getSelfMaterialById()
    {
        $params = $this->request->post;
        $result = User::getSelfCreateData($params['material_id']);
        print_r($result->name);
    }

    public function getMatCat()
    {
        $materials = '';
        $params = $this->request->post;

        $result = Data::getMaterial($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<li><table class="table"><tbody class="tab-'.$params['type'].'"><tr>';
        foreach ($result as $key=>$value){
//            if($value != null){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
//            }
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
//            if($value != null){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
//            }
        }
        $materials.= '</tr></tbody></table></li>';

        print_r($materials);
    }


    public function getMatByCat()
    {
        $materials = '';
        $params = $this->request->post;
        $result = Data::getMaterialByParent($params['parent']);
        foreach ($result as $item){
            if($item != null){
                $materials .='<option value="'.$item->id.'">'.$item->name.'</option>';
            }
        }
        print_r($materials);
    }



    public function getCoefficient()
    {
        $coefficient = '';
        $params = $this->request->post;
        $result = Data::getCoefficient($params['coefficient']);
        if($result != null){
            $coefficient .=$result->coefficient;
        }
        print_r($coefficient);
    }




    public function getMatCatBlocks()
    {
        $materials = '';
        $params = $this->request->post;
//        print_r($params);exit;

        $result = Data::getMaterial($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<tbody class="tab-'.$params['type'].'"><tr>';
        foreach ($result as $key=>$value){
//            if($value != null){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='<th scope="row" '.$style.'>
                            '.$key.'
                         </th>';
//            }
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
//            if($value != null){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
//            }
        }
        $materials.= '</tr></tbody>';

        print_r($materials);
    }


    public function getDewData()
    {
        $params = $this->request->post;
        $result =Data::getDewData($params);
        if($result != null){
            print_r($result->per_55);
        }
    }


//    ====================
    public function calcPMU()
    {
        $params = $this->request->post;
        $result = Data::calcPMU($params);

        print_r($result);
    }

//    ====================
    public function calc_3_1()
    {
        $params = $this->request->post;
        $result = Data::calc_3_1($params);

        print_r($result);
    }

//    ====================
    public function calc_4_tab()
    {
        $params = $this->request->post;
        $result = Data::calc_4_tab($params);

        print_r($result);
    }


    public function calc()
    {
        $params = $this->request->post;
        $result = Data::calc($params);

        print_r($result);
    }

    public function writeCookieSimple()
    {
        $params = $this->request->post;
        $result = Data::writeCookieSimple($params);
        print_r($result);
    }
    public function writeCookie()
    {
        $params = $this->request->post;
        $result = Data::writeCookie($params);
//        $cookie_name = $params['cookie_name'];
//
//        var_dump(  Cookie::get($cookie_name));
//        var_dump( $_COOKIE[$cookie_name]);
        print_r($result);
    }
    public function writeCookieIndex()
    {
        $params = $this->request->post;
        Cookie::delete('city');
        Cookie::delete('building');
        Cookie::set('city', $params['city']);
        Cookie::set('building', $params['building']);
    }

    public function deleteCookie()
    {
        $params = $this->request->post;
        $result = Data::deleteCookie($params);
//        $cookie_name = $params['cookie_name'];
//
//        var_dump(  Cookie::get($cookie_name));
//        var_dump( $_COOKIE[$cookie_name]);
        print_r($result);
    }

    public function writeLayer()
    {
        $params = $this->request->post;
        $result = Data::addLayerData($params);
        print_r($result);
    }


    public function writeLayerName()
    {
        $params = $this->request->post;
        if($params['name'] == ''){
            die('вы не ввели имя');
        }


//        print_r($params);
        $result = Data::addLayerDataName($params);
        print_r($result);
    }
    public function editLayerName()
    {
        $params = $this->request->post;
        if($params['name'] == ''){
            die('вы не ввели имя');
        }
        $result = Data::editLayerDataName($params);
        print_r($result);
    }

    public function removeDataLayer()
    {
        $params = $this->request->post;

        $result = Data::removeLayerDataName($params);
        print_r($result);
    }

    public function calcHeating()
    {
        $params = $this->request->post;
        $result = Data::calcHeating($params);
        print_r($result);

    }


    public function calcWindow()
    {
        $params = $this->request->post;
        $result = Data::calcWindow($params);
        print_r($result);
    }


    public function forGraph()
    {
        $params = $this->request->post;
//dd($params);
        foreach ($params['image'] as $key=>$value){
            if(isset($params['hide_element']) && in_array($key, $params['hide_element']))continue;
            $result[] = $_SERVER['REQUEST_SCHEME'] . '://'.$_SERVER['SERVER_NAME']."/content/uploads/texture/$value";
//            $result[] = 'https://'.$_SERVER['SERVER_NAME']."/content/themes/default/img/texture/$value";
//            $result[] = "https://www.termocalc.ru/content/themes/default/img/texture/$value";
//            $result[] = "https://www.termocalc.ru/content/uploads/texture/$value";

        }

        print_r (json_encode($result));
//        return json_encode($result);
    }


//  =============  for self create materials =============

    public function addSelf(){
        $params = $this->request->post;
        User::add($params);

    }
    public function updateSelf(){
        $params = $this->request->post;
        User::updateSelf($params);

    }
    public function removeSelf(){
        $params = $this->request->post;
        $result = User::removeSelf($params);
        print_r($result);
    }

    public function editSelf($id){
        $selfMaterial = User::getSelfCreateData($id);
        $data = ['selfMaterial' => $selfMaterial];
        $this->view->render('edit_self_created',$data);
    }



    public function getMatCatSelf()
    {
        $materials = '';
        $params = $this->request->post;

        $result = User::getSelfCreateData($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<li><table class="table"><tbody class="tab-'.$params['type'].'"><tr>';
        foreach ($result as $key=>$value){
//            if($value != null){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
//            }
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
//            if($value != null){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
//            }
        }
        $materials.= '</tr></tbody></table></li>';

        print_r($materials);
    }

    public function getMatCatBlocksSelf()
    {
        $materials = '';
        $params = $this->request->post;
        $result = User::getSelfCreateData($params['material_id']);
        if(!isset($result)){
            die();
        }
        $materials.= '<tbody class="tab-'.$params['type'].'"><tr>';
        foreach ($result as $key=>$value){
            $style = '';
            if($key == 'id'){ $style = "style='width: 60px;'";}
            $materials.='<th scope="row" '.$style.'>
                            '.$key.'
                         </th>';
        }
        $materials.= '</tr><tr>';

        foreach ($result as $key=>$value){
            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
        }
        $materials.= '</tr></tbody>';
        print_r($materials);
    }

//    compare
    public function sendToCompare()
    {
        $params = $this->request->post;
        if(empty($params['id'])){
            die('error');
        }
        $result = Data::sendToCompare($params);
        print_r($result);
    }

    public function downloadPdfFile()
    {
        $params = $this->request->post;
        if(empty($params['id'])){
            die('error');
        }
        $com_time = User::getCookie('compare-time');
//        if($com_time){
//            if($com_time + 2*3600 > time())
//                print_r("файл можно сгенерировать 1 раз в течение 2-ух часов");
//            return;
//        }
//        $com_time = User::setCookie('compare-time', time());
//        header('Location: /test.php');
        print_r($com_time);
    }

//    estimate

    private function getHtml(
        $est_item,
        $size,
        $material_rate = 0,
        $work_rate = 0,
        $volume = 0,
        $construction_type = null,
        $frame_between = null,
        $frame_width = null,
        $frame_lay_num = null
    )
    {

        $multiple = $est_item->rate * $size * $volume;
        $all_multiple =  round($est_item->price * $multiple, 3);

        if($construction_type == 'frame'){
            if($frame_lay_num == 1){
                $number = round(($frame_width * 100) / ($frame_width + $frame_between), 3);
            }else{
                $number = round(($frame_between * 100 ) / ($frame_width + $frame_between), 3);
            }

            $multiple = $est_item->rate * $size * $number * 0.01 * $volume;
            $all_multiple =  round($est_item->price * $multiple, 3);
        }


        switch ($est_item->type){
            case 'material':
                $type = "<b>материал:</b>";
                break;
            default:
                $type = "<b>работа:</b>";
        }
        $inner = '';
        $inner .= "<tr>";
        $inner .= "<td>";
        $inner .= $type;
        $inner .= "</td>";
        $inner .= "<td>";
        $inner .= $est_item->name;
        $inner .= "</td>";
        $inner .= "<td>";
        $inner .= $est_item->unit;
        $inner .= "</td>";

        $inner .= "<td>";
        $inner .= $est_item->price;
        $inner .= "</td>";
        $inner .= "<td>";
        $inner .= $est_item->rate;
        $inner .= "</td>";
        if($est_item->unit == 'кв.м.'){
            $all_multiple = $est_item->rate * $volume * $est_item->price;
            $multiple = $est_item->rate * $volume;
        }
        $inner .= "<td>$multiple</td>";
        $inner .= "<td>$all_multiple</td>";

        if($est_item->type == 'material'){
            $material_rate += $all_multiple;
        } else {
            $work_rate += $all_multiple;
        }

        $result['content'] = $inner;
        $result['material_rate'] = $material_rate;
        $result['work_rate'] = $work_rate;
        return $result;
    }

    public function getEstimate()
    {
        $params = $this->request->post;

        $volume = ($params['estimate']['volume'] != "")?$params['estimate']['volume']:0;
        $estimate_frame = isset($params['estimate']['frame'])?($params['estimate']['frame']):[];
        $estimate_blocks = isset($params['estimate']['blocks'])?($params['estimate']['blocks']):[];
        $estimate_another = isset($params['estimate']['another'])?($params['estimate']['another']):[];
        $all_materials = ($estimate_another + $estimate_frame + $estimate_blocks);
        ksort($all_materials);

        $html = "<thead style='font-size: 12px'><tr>
<td></td>
<td>наименование </td>
<td>единица</td>
<td>цена единицы</td>
<td>расход ед.</td>
<td>кол-во</td>
<td>цена всего</td>
</tr></thead>";
        $params_estimate['col'] = "material_id";
        $params_estimate['operator'] = "=";
        $i = 1;
        $flag = 0;
        $material_rate = 0;
        $work_rate = 0;
        $num = 1;
        foreach ($all_materials as $key=>$id){
            $size = (isset($params['estimate']['size'][$i])) ? $params['estimate']['size'][$i] : '';

            $self = false;
            $frame_lay_num = null;

            if($id > 10000){
                $material = User::getSelfCreateData($id);
                $self = true;
            }else{
                $material = Data::getMaterial($id);
            }

            $params_estimate['val'] = $id;
            $estimate_result = User::getEstimateBy($params_estimate);

            if (array_key_exists($key, $estimate_frame)) {
                if($flag == 0){
                    $i--;
                    $flag = 1;
                    $frame_lay_num = 1;
                }else{
                    $flag = 0;
                    $num--;
                    $frame_lay_num = 2;
                }
                $type = 'frame';
                $html .= "<tr><td colspan=\"7\">$num - <b>$material->name</b> (каркас)</td></tr>";
                if($self){
                    $html .= "<tr>";
                    $html .= "<td colspan=\"5\" >собственный материал:</td>";
                    $html .= "<td colspan=\"2\" >$material->COL30</td>";
                    $html .= "</tr>";
                }

                foreach ($estimate_result as $est_item){
                    if($est_item->construction == 'frame'){
                        $result = $this->getHtml(
                            $est_item,
                            $size,
                            $material_rate,
                            $work_rate,
                            $volume,
                            $type,
                            $params['estimate']['frame-size']['frame-between'][$num],
                            $params['estimate']['frame-size']['frame-width'][$num],
                            $frame_lay_num
                        );

                        $html .= $result['content'];
                        $material_rate =  $result['material_rate'];
                        $work_rate =  $result['work_rate'];
                    }
                }
            } elseif(array_key_exists($key, $estimate_blocks)) {
                $type = 'blocks';
                if($flag == 0){
                    $i--;
                    $flag = 1;
                }else{
                    $flag = 0;
                    $num--;
                }
                $html .= "<tr><td colspan=\"7\">$num - <b>$material->name</b> (кладка)</td></tr>";
                if($self){
                    $html .= "<tr>";
                    $html .= "<td colspan=\"5\" >собственный материал:</td>";
                    $html .= "<td colspan=\"2\" >$material->COL30</td>";
                    $html .= "</tr>";
                }
                foreach ($estimate_result as $est_item) {
                    if ($est_item->construction == 'block') {
                        $result = $this->getHtml($est_item, $size, $material_rate, $work_rate, $volume, $type);
                        $html .= $result['content'];
                        $material_rate =  $result['material_rate'];
                        $work_rate =  $result['work_rate'];
                    }
                }
            } else {
                $html .= "<tr><th colspan=\"7\">$num - $material->name</th></tr>";
                if($self){
                    $html .= "<tr>";
                    $html .= "<td colspan=\"5\" >собственный материал:</td>";
                    $html .= "<td colspan=\"2\" >$material->COL30</td>";
                    $html .= "</tr>";
                    $material_rate +=$material->COL30;
                }
                foreach ($estimate_result as $est_item){
                    if($est_item->construction == 'simple'){
                        $result = $this->getHtml($est_item, $size, $material_rate, $work_rate, $volume);
                        $html .= $result['content'];
                        $material_rate =  $result['material_rate'];
                        $work_rate =  $result['work_rate'];
                    }
                }
            }
            $i++;
            $num++;
        }

        $html .= "<tr>";
        $html .= "<td colspan=\"7\" ></td>";
        $html .= "</tr>";

        $coefficient = '';
        if (isset($params['estimate']['coefficient'][2])) {
            switch ($params['estimate']['coefficient'][2]){
                case 0.75:
                    $coefficient = 'wall_1';
                    break;
                case 0.92:
                    $coefficient = 'wall_3';
                    break;
                case 0.87:
                    $coefficient = 'wall_2';
                    break;
                default:
                    $coefficient = '';
            }
        } elseif(isset($params['estimate']['coefficient'][3])) {
            switch ($params['estimate']['coefficient'][3]){
                case 0.96:
                    $coefficient = 'roof_1';
                    break;
                case 0.92:
                    $coefficient = 'roof_2';
                    break;
                case 0.82:
                    $coefficient = 'roof_3';
                    break;
                case 0.91:
                    $coefficient = 'roof_4';
                    break;
                default:
                    $coefficient = '';
            }
        } elseif(isset($params['estimate']['coefficient'][4])) {
            switch ($params['estimate']['coefficient'][4]){
                case 0.98:
                    $coefficient = 'floor_1';
                    break;
                case 0.96:
                    $coefficient = 'floor_2';
                    break;
                case 0.97:
                    $coefficient = 'floor_3';
                    break;
                case 0.9:
                    $coefficient = 'floor_4';
                    break;
                default:
                    $coefficient = '';
            }
        }

        if($coefficient != ''){
            $params_estimate['val'] = $coefficient;
            $estimate_coefficient = User::getEstimateBy($params_estimate);
            $estimate_material_rate = 0;
            $estimate_work_rate = 0;
            $coefficient_name = substr($params['estimate']['coefficient_name'], 3);

            $html .= "<tr>";
            $html .= "<td colspan=\"7\" ><b>Технология: </b> $coefficient_name</td>";
            $html .= "</tr>";

            foreach ($estimate_coefficient as $item){
                $html .= "<tr>";
                if($item->type == 'material'){
                    $html .= "<td>";
                    $html .= "<b>материал: </b>";
                    $html .= "</td>";
                } else {
                    $html .= "<td>";
                    $html .= "<b>работа: </b>";
                    $html .= "</td>";
                }
                $multiple = $item->rate * $volume;
                $all_multiple = $item->rate * $volume * $item->price;

                $html .= "<td>";
                $html .= $item->name;
                $html .= "</td>";
                $html .= "<td>";
                $html .= $item->unit;
                $html .= "</td>";
                $html .= "<td>";
                $html .= $item->price;
                $html .= "</td>";
                $html .= "<td>";
                $html .= $item->rate;
                $html .= "</td>";
                $html .= "<td>$multiple</td>";
                $html .= "<td>$all_multiple</td>";
                $html .= "</tr>";

                if($item->type == 'material'){
                    $estimate_material_rate += $all_multiple;
                } else {
                    $estimate_work_rate += $all_multiple;
                }
            }

            $html .= "<tr>";
            $html .= "<td colspan=\"7\" ></td>";
            $html .= "</tr>";

            $material_rate += $estimate_material_rate;
            $work_rate += $estimate_work_rate;
        }

        $all_material_rate = $material_rate;
        $all_work_rate = $work_rate;

        $res_price_mat = $all_material_rate;
        $res_price_work = $all_work_rate;

        $totalCost = $res_price_mat + $res_price_work;
        User::setCookie("total_cost_".$params['estimate']['tab_num'], $totalCost);

        $html .= "<tr>";
        $html .= "<td colspan=\"5\" ><b>Итого стоимость материалов:</b></td>";
        $html .= "<td colspan=\"2\" >$res_price_mat</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td colspan=\"5\" ><b>Итого стоимость выполнения работы:</b></td>";
        $html .= "<td colspan=\"2\" >$res_price_work</td>";
        $html .= "</tr>";
        $html .= "<tr>";
        $html .= "<td colspan=\"5\" ><b>Итого общая стоимость:</b></td>";
        $html .= "<td colspan=\"2\" >".$totalCost."</td>";
        $html .= "</tr>";

        $materialsArray = (array_unique($all_materials));
        array_push($materialsArray, $coefficient);
        $materialFiles = [];
        foreach ($materialsArray as $item) {
            $file = User::getFile($item);
            if(!empty($file)) {
                $materialFiles[] = $file[0]->id;
            }
        }

        User::setCookie("files", json_encode(array_filter($materialFiles)));

        echo ($html);
    }

    public function getEstimateWindow()
    {
        $params = $this->request->post;
        $result_mat = 0;
        $result_work = 0;
        $html = "<thead style='font-size: 12px'><tr>
                    <td>наименование </td>
                    <td>тип</td>
                    <td>Цена на 1 кв.м.</td>
                    <td>Цена всего</td>
                    </tr>
                 </thead>";
        $params_estimate['col'] = "material_id";
        $params_estimate['operator'] = "=";
        if($params['estimate']['volume']['win'] != ''){
            $volume = $params['estimate']['volume']['win'];
            $params_estimate['val'] = 'window';
            $result = 0;
            $estimate = User::getEstimateBy($params_estimate);
            $html .= '<tr>';
            $html .= '<td colspan="4"><b>Окна</b></td>';
            $html .= '</tr>';
            foreach ($estimate as $item) {
                if ($item->type == 'material') {
                    $type = "<b>материал:</b>";
                    $result_mat += $item->price * $volume;
                } else {
                    $type = "<b>работа:</b>";
                    $result_work += $item->price * $volume;
                }
                $priceAll = $item->price * $volume;
                $html .= '<tr>';
                $html .= "<td>$item->name</td>";
                $html .= "<td>$type</td>";
                $html .= "<td>$item->price</td>";
                $html .= "<td>$priceAll</td>";
                $html .= "</tr>";
                $result += $priceAll;
            }
            User::setCookie("total_cost_5_1", $result);
            $html .= '<tr>';
            $html .= '<td colspan="3"><b>Стоимость окон всего (рублей) </b></td>';
            $html .= "<td>$result</td>";
            $html .= '</tr>';
        }

        if($params['estimate']['volume']['lan'] != '') {
            $params_estimate['val'] = 'lantern';
            $volume = $params['estimate']['volume']['lan'];
            $result = 0;
            $estimate = User::getEstimateBy($params_estimate);
            $html .= '<tr>';
            $html .= '<td colspan="4"><b>Фонари</b></td>';
            $html .= '</tr>';
            foreach ($estimate as $item) {
                if ($item->type == 'material') {
                    $type = "<b>материал:</b>";
                    $result_mat += $item->price * $volume;
                } else {
                    $type = "<b>работа:</b>";
                    $result_work += $item->price * $volume;
                }
                $priceAll = $item->price * $volume;
                $html .= '<tr>';
                $html .= "<td>$item->name</td>";
                $html .= "<td>$type</td>";
                $html .= "<td>$item->price</td>";
                $html .= "<td>$priceAll</td>";
                $html .= "</tr>";
                $result += $priceAll;
            }
            User::setCookie("total_cost_5_2", $result);
            $html .= '<tr>';
            $html .= '<td colspan="3"><b>Стоимость фонарей всего(рублей) </b></td>';
            $html .= "<td>$result</td>";
            $html .= '</tr>';
        }

        $resultAll = $result_mat + $result_work;
        $html .= '<tr>';
        $html .= '<td colspan="4"></td>';
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3"><b>Общая стоимость материалов</b></td>';
        $html .= "<td>$result_mat</td>";
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3"><b>Общая стоимость работ</b></td>';
        $html .= "<td>$result_work</td>";
        $html .= '</tr>';
        $html .= '<tr>';
        $html .= '<td colspan="3"><b>Общая стоимость</b></td>';
        $html .= "<td>$resultAll</td>";
        $html .= '</tr>';

        echo ($html);
    }

    public function getEstimateHeating()
    {
        $params = $this->request->post;
        $energyArr = [
            'heating_1' => '- природный газ',
            'heating_2' => '- сжиженный газ',
            'heating_3' => '- дизтопливо',
            'heating_4' => '- электричество',
            'heating_5' => '- дрова',
            'heating_6' => '- уголь каменный',
            'heating_7' => '- пеллеты',
        ];
        $id = (array_search($params['energy_type'], $energyArr));
        $params_estimate['col'] = "material_id";
        $params_estimate['operator'] = "=";
        $params_estimate['val'] = $id;
        $estimate = User::getEstimateBy($params_estimate);
        $html = '';
        $heatingFile = null;

        foreach ($estimate as $item) {
            $rate = json_decode($item->rate)->rate;
            $coefficient_rise = json_decode($item->rate)->coefficient_rise;
            $year = 1.15 * $params['TD320']/$rate/0.85*$item->price;
            $TD313 = $params['TD313'] * 1.15;
            $newYear = $year;
            $year_25 = $newYear;
            for($i=2;$i<=25;$i++){
                $newYear = $coefficient_rise * $newYear;
                $year_25 += $newYear;
            }
            $year = round($year, 2);
            $year_25 = round($year_25, 2);
            $heatingSystemPrice = 0;

            $price_params_estimate['col'] = "material_id";
            $price_params_estimate['operator'] = "=";
            $price_params_estimate['val'] = 'price';
            $price_estimates = User::getEstimateBy($price_params_estimate);

            foreach ($price_estimates as $price_estimate){
                if(json_decode($price_estimate->rate)->more < $TD313){
                    if(json_decode($price_estimate->rate)->less >= $TD313){
                        $heatingSystemPrice = $price_estimate->price;
                        $heatingFile = json_decode($price_estimate->rate)->file;
                    }
                }
            }
            if($heatingSystemPrice == 0){
                foreach ($price_estimates as $price_estimate) {
                    if(json_decode($price_estimate->rate)->more == 70000){
                        $heatingSystemPrice = $price_estimate->price + (($TD313 - 70000) * json_decode($price_estimate->rate)->less);
                        $heatingFile = json_decode($price_estimate->rate)->file;
                    }
                }
            }

            $html .= '<tr>';
            $html .= '<td><b>Выбран вид топлива:</b></td>';
            $html .= '<td>' . substr($params['energy_type'], 2) . '</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= '<td><b>Мощность отопительного оборудования:</b></td>';
            $html .= '<td>' . $TD313 * 0.001 . ' кВт</td>';
            $html .= '</tr>';

            $html .= '<tr>';
            $html .= "<td><b>Стоимость топлива на сегодня:</b></td>";
            $html .= "<td>$item->price руб. за $item->unit</td>";
            $html .= "</tr>";

            $html .= '<tr>';
            $html .= "<td><b>Стоимость отопления за год:</b></td>";
            $html .= "<td>$year руб. при КПД отопительного котла 85%</td>";
            $html .= "</tr>";

            $html .= '<tr>';
            $html .= "<td><b>Стоимость отопления за 25 лет:</b></td>";
            $html .= "<td>$year_25 руб.</td>";
            $html .= "</tr>";

            $html .= '<tr>';
            $html .= "<td><b>Стоимость отопительного оборудования:</b></td>";
            $html .= "<td>$heatingSystemPrice руб.</td>";
            $html .= "</tr>";
        }
        $html .= "<input type='hidden' id='heating-file' value='$heatingFile'>";
        if(!empty($year)){
            User::setCookie("total_cost_6_1", $year);
        }
        if(!empty($year_25)) {
            User::setCookie("total_cost_6_2", $year_25);
        }
        if(!empty($heatingSystemPrice)) {
            User::setCookie("total_cost_6_3", $heatingSystemPrice);
        }

        echo ($html);
    }

    public function getStaticThickness(){
        $params = $this->request->post;
        $materials = Data::getStaticThickness($params);
        $result = [];
        foreach ($materials as $material){
            $result[] = [$material->id => $material->COL20];
        }
        print_r (json_encode($result));
    }

    public function showPreview(){
        $params = $this->request->get;

        Cookie::delete('dont_show_preview');
        if (!empty($params['show']) && $params['show'] == 'not-show') {
            Cookie::set('dont_show_preview', 1);
        }
    }
}
