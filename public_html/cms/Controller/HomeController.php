<?php

namespace Cms\Controller;

class HomeController extends CmsController
{
    public function index()
    {
        $data = ['name' => 'Artem'];
        $this->view->render('index', $data);
    }

    public function news($id)
    {
        echo $id;
    }

    public function walls()
    {
        $this->view->render('walls');
    }

    public function test()
    {
        $this->view->render('test');
    }
    public function walls_()
    {
        $this->view->render('walls_');
    }

    public function getLoaderHomogeneous()
    {
        $this->view->render('loader/homogeneous');
    }

    public function getLoaderThermal()
    {
        $this->view->render('loader/thermal');
    }

    public function getLoaderBlocksBlock()
    {
        $this->view->render('loader/blocks/block');
    }



    public function getLoaderBlocksSeam()
    {
        $this->view->render('loader/blocks/seam');
    }

    public function getLoaderFrameFrame()
    {
        $this->view->render('loader/frame/frame');
    }


    public function getLoaderFrameFilling()
    {
        $this->view->render('loader/frame/filling');
    }

    public function getLoaderHolder()
    {
        $this->view->render('loader/holder');
    }


    public function getLoaderMaterials()
    {
        $this->view->render('loader/load-materials');
    }



    public function getTableHomogeneousThermal()
    {
        $this->view->render('loader/table/homogeneous_thermal');
    }

    public function getTableBlockFilling()
    {
        $this->view->render('loader/table/block_filling');
    }



    public function signUp()
    {
        $this->view->render('/reg');
//        header('Location: /forum/index.php/register');
    }


    public function login()
    {
        $this->view->render('/login');
//        header('Location: /forum/index.php/login');

    }



    public function sameEmail()
    {
        $data = ['note' => 'Пользователь с таким почтовым адресом уже зарегистрирован !'];
        $this->view->render('note-page',$data);
    }

    public function regSuccess()
    {
        $data = ['note' => 'Вы зарегистрированы, осталось подтвердить свой адрес электронной почты!'];
        $this->view->render('note-page',$data);
    }

    public function forgot()
    {
        $this->view->render('forgot');
    }

    public function forgetSendPage()
    {
        $this->view->render('forget-page');
    }

    public function reset()
    {
        $this->view->render('reset');
    }


    public function activation()
    {
        $this->view->render('activation');
    }
    public function profile()
    {
        $this->view->render('profile');
    }



    public function roof()
    {
        $this->view->render('roof');
    }

    public function floor()
    {
        $this->view->render('floor');
    }
    public function heating()
    {
        $this->view->render('heating');
    }
    public function window()
    {
        $this->view->render('windows');
    }

    public function help()
    {
        $this->view->render('help');
    }
    public function examples()
    {
        $this->view->render('examples');
    }
    public function comparison()
    {
        $this->view->render('comparison');
    }
    public function catalog()
    {
        $this->view->render('catalog');
    }
    public function forum()
    {
        $this->view->render('forum');
    }
    public function thank()
    {
        $data = ['note' => '<div class="alert alert-success alert-dismissible" style="text-align: center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    Ваше сообщение успешно отправлено
                </div>'];
        $this->view->render('note-page',$data);
    }

    public function rules()
    {
        $this->view->render('rules');
    }
//    pdf

    public function pdf()
    {
        $this->view->render('pdf');
    }
}