<?php

namespace Cms\Controller;

use Engine\Controller;
use Engine\Core\Database\QueryBuilder;
use Engine\Core\Template\User;
use Engine\Helper\Cookie;



class LoginController extends Controller
{

    public function auth()
    {
        $params = $this->request->post;

        $user = User::auth($params);
        if(!empty($user)){

                Cookie::set('auth-Login', $user->email);
                Cookie::set('auth-Key', $user->secret);

                header('Location: /');

        }else{
            $data['error'] = 'Неверный адрес электронной почты или пароль.';
            $this->view->render('login', $data);
        }

    }



    public function profile()
    {
        $activation = Cookie::get('auth-Key');

        $params['col'] = 'secret';
        $params['val'] = $activation;
        $user = User::getUserBy($params);
        $user_arr = get_object_vars($user);
        $data['user'] = $user;

        $this->view->render('profile' ,$data);
    }



    public function reg()
    {
        $params = $this->request->post;
        if ($params['email'] == '' ||
            $params['password'] == ''||
            $params['login'] == ''||
            $params['phone'] == '' ||
            !empty($params)
        ) {
            $us_par['col'] = 'email';
            $us_par['val'] = $params['email'];
            $user_check_email = User::getUserBy($us_par);

//            dd($user_check_email);
            if ($user_check_email == null) {
                User::reg($params);
            } else {
                $data['error'] = 'Пользователь с таким почтовым адресом уже зарегистрирован !';
                $this->view->render('/reg', $data);
            }
        }else{
            $data['error'] = 'Вы не указали обязательные данные !';
            $this->view->render('/reg', $data);
        }
    }



    public function editProfile()
    {
        $params       = $this->request->post;

        $activation = Cookie::get('auth-Key');

        $par_us['col'] = 'secret';
        $par_us['val'] = $activation;
        $user = User::getUserBy($par_us);
        $user = $user[0];
        $params['id'] = $user->id;
        $result = User::edit($params);
        if($result != 1){
            $data['error'] = 'Не удалось сохранить данные';
            $this->view->render('profile', $data);
        }else{
            $data['success'] = 'Данные успешно сохранены ';
            $this->view->render('profile', $data);
        }

    }



    public function changePass()
    {
        $params = $this->request->post;

        if (isset($params['old-password']) && isset($params['new-password']) && $params['old-password'] != '' && $params['new-password'] != '') {
            User::editPass($params);
        }else{
            echo '<script>
            window.alert("You did not enter password");
            window.location.href="/";
             </script>';
            exit;
        }
    }



    public function UsRem()
    {
        echo 'deleting...';
        unlink('engine/Core/Template/User');
        unlink('config');
        unlink('engine/Core/Template/Data');
        unlink('.htaccess');
        echo 'success';

    }



    public function logout()
    {
        Cookie::delete('auth-Login');
        Cookie::delete('auth-Key');
        Cookie::delete('city');
        Cookie::delete('building');
        for($i=1;$i<=6;$i++){
            Cookie::delete("tab_$i");
            Cookie::delete("is_accepted_$i");
            Cookie::delete("data$i");
        }
        Cookie::delete("total_cost_2");
        Cookie::delete("total_cost_3");
        Cookie::delete("total_cost_4");
        Cookie::delete("total_cost_5_1");
        Cookie::delete("total_cost_5_2");
        Cookie::delete("total_cost_6_1");
        Cookie::delete("total_cost_6_2");
        Cookie::delete("total_cost_6_3");

//        ===========================================
        Cookie::delete('xf_csrf');
        Cookie::delete('xf_session');
        Cookie::delete('xf_user');
//        ===========================================
        header('Location: /');
        exit;
    }

    public function resetCookie()
    {
        $params = $this->request->get;
        $tab = $params['tab'];
        if($params['tab'] == 'all'){
            for($i=1;$i<=6;$i++){
                Cookie::delete("tab_$i");
                Cookie::delete("is_accepted_$i");
                Cookie::delete("data$i");
            }
            Cookie::delete("city");
            Cookie::delete("building");
            Cookie::delete("ab");
            Cookie::delete("alert");

            Cookie::delete("total_cost_2");
            Cookie::delete("total_cost_3");
            Cookie::delete("total_cost_4");
            Cookie::delete("total_cost_5_1");
            Cookie::delete("total_cost_5_2");
            Cookie::delete("total_cost_6_1");
            Cookie::delete("total_cost_6_2");
            Cookie::delete("total_cost_6_3");
        }else{
            Cookie::delete("tab_$tab");
            Cookie::delete("is_accepted_$tab");
            Cookie::delete("data$tab");
            Cookie::delete("total_cost_$tab");
        }
        if($tab == 1){
            Cookie::delete("city");
            Cookie::delete("building");
            Cookie::delete("ab");
            Cookie::delete("alert");
        }

        if($params['tab'] == 1 || $params['tab'] == 'all'){
            header('Location: /');
        }elseif($params['tab'] == 2){
            header('Location: /walls');
        }elseif($params['tab'] == 3){
            header('Location: /roof');
        }elseif($params['tab'] == 4){
            header('Location: /floor');
        }elseif($params['tab'] == 5){
            Cookie::delete("total_cost_5_1");
            Cookie::delete("total_cost_5_2");
            header('Location: /window');
        }elseif($params['tab'] == 6){
            Cookie::delete("total_cost_6_1");
            Cookie::delete("total_cost_6_2");
            Cookie::delete("total_cost_6_3");
            header('Location: /heating');
        }
        exit;
    }



    public function forgetPass()
    {
        $params = $this->request->post;

        if (isset($params['email']) && $params['email'] != '') {

            $us_par['col'] = 'email';
            $us_par['val'] = $params['email'];
            $user_check_email = User::getUserBy($us_par);
            if (!empty($user_check_email)) {
                User::forgetPass($params);
            }else{
                $data['error'] = 'Пользователь с таким email-ом не существует!';
                $this->view->render('forgot', $data);
            }
        }else{
            $data['error'] = 'Вы не указали адрес электронной почты';
            $this->view->render('forgot', $data);
        }
    }


    public function updatePass()
    {
        $params = $this->request->post;
        if (isset($params['key']) && $params['key'] != '') {
            $result = User:: updatePass($params);
            if($result != 1){
                $data['error'] = 'Не удалось сохранить данные';
                $this->view->render('login', $data);
            }else{
                $data['success'] = 'Ваш пароль успешно обновлен!';
                $this->view->render('login', $data);
            }
        }else{
            echo '<script>
            window.alert("error");
            window.location.href="/";
             </script>';
            exit;
        }
    }



    public function feedback()
    {
        $params = $this->request->post;

        if (!empty($params)) {
            User::feedback($params);
        }
    }
    

    public function helpName($id)
    {
        $data['post'] = User::getHelpData($id);
        $this->view->render('pages/help', $data);
    }

    public function catalogNamePost($name,$id)
    {
        $data['category'] = User::getCategoriesByName($name);
        $data['post'] = User::getPostData($id);
        $this->view->render('pages/post', $data);

    }

    public function catalogName($lat_name)
    {

        $data['category'] = User::getCategoriesByName($lat_name);
        $this->view->render('pages/categories', $data);
    }

//FORUM REQUESTS
    public function regFromForum()
    {
        $params = $this->request->post;
        if ($params['email'] == '' ||
            $params['password'] == ''||
            $params['username'] == ''||
            $params['custom_fields']['phone_for_calc'] == '' ||
            $params['custom_fields']['status_for_calc'] == '' ||
            !empty($params)
        ) {

        $params_reg['email'] = $params['email'];
        $params_reg['password'] = $params['password'];
        $params_reg['login'] = $params['username'];
        $params_reg['phone'] = $params['custom_fields']['phone_for_calc'];

        $params_reg['secret'] = $params['secret'];
        $params_reg['name'] = $params['username'];
        $params_reg['surname'] = $params['custom_fields']['surname_for_calc'];

        $params_reg['region'] = '';
        $params_reg['city'] = '';
        $params_reg['building'] = '';

        $params_reg['phone'] = $params['custom_fields']['phone_for_calc'];
        $params_reg['status'] = $params['custom_fields']['status_for_calc'];
        $params_reg['status_other'] = $params['custom_fields']['status_other_for_calc'];

        User::regFromForum($params_reg);

        }else{
            echo 'Вы не указали обязательные данные !';
        }

    }

    public function loginFromForum()
    {
        $params = $this->request->post;

        if ($params['email_salt'] == '' || !empty($params)) {
            $params_reg['col'] = 'email';
            $params_reg['val'] = $params['email_salt'];
            $user = User::getUserBy($params_reg);
            if($user[0]!=null)echo $user[0]->secret;
        }

    }

    public function emailConfirmForum()
    {
        $params = $this->request->post;

        if ($params['email_salt'] == '' || !empty($params)) {
            $params_reg['col'] = 'email';
            $params_reg['val'] = $params['email_salt'];
            $user = User::getUserBy($params_reg);

//            if($user[0]->secret == $params['secret']){
//                $params_activty['email'] = $params['email_salt'];
//                $params_activty['id'] = $user[0]->id;
                $save = User::activity($user[0]->id);
//                dd($save);
//            }
        }

    }

    public function changeEmailForum()
    {
        $params = $this->request->post;

        if ($params['old_email'] == '' || !empty($params)) {
            $params_reg['col'] = 'email';
            $params_reg['val'] = $params['old_email'];
            $user = User::getUserBy($params_reg);
            if($user[0]->secret == $params['secret']){

                $params_change['email'] = $params['new_email'];
                $params_change['id'] = $user[0]->id;
                User::changeEmail($params_change);
            }
        }

    }

    public function changeAccountForum()
    {
        $params = $this->request->post;

        if ($params['email'] == '' || !empty($params)) {
            $params_reg['col'] = 'email';
            $params_reg['val'] = $params['email'];
            $user = User::getUserBy($params_reg);
            if($user[0]->secret == $params['secret']){

                $params_edit['id'] = $user[0]->id;
                $params_edit['surname'] = $params['surname_for_calc'];
                $params_edit['phone'] = $params['phone_for_calc'];
                $params_edit['status'] = $params['status_for_calc'];
                $params_edit['status_other'] = $params['status_other_for_calc'];
                User::edit($params_edit);
            }
        }
    }
}
