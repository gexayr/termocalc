<?php
/**
 * List routes
 */

$this->router->add('home', '/', 'HomeController:index');
$this->router->add('get-cities', '/get-cities', 'DataController:getCities', 'POST');
$this->router->add('get-city-params', '/get-city-params', 'DataController:getCityParams', 'POST');
$this->router->add('get-building-params', '/get-building-params', 'DataController:getBuildingParams', 'POST');
$this->router->add('get-room-params', '/get-room-params', 'DataController:getRoomParams', 'POST');
$this->router->add('get-room-type', '/get-room-type', 'DataController:getRoomType', 'POST');
$this->router->add('get-cities-buildings-params', '/get-cities-buildings', 'DataController:getCitiesBuildingsParams', 'POST');

//PAGES
$this->router->add('walls', '/walls', 'HomeController:walls');
$this->router->add('roof', '/roof', 'HomeController:roof');
$this->router->add('floor', '/floor', 'HomeController:floor');
$this->router->add('heating', '/heating', 'HomeController:heating');
$this->router->add('window', '/window', 'HomeController:window');

$this->router->add('help', '/help', 'HomeController:help');
//$this->router->add('forum', '/forum', 'HomeController:forum');
$this->router->add('comparison', '/comparison', 'HomeController:comparison');
$this->router->add('catalog', '/catalog', 'HomeController:catalog');
$this->router->add('catalog-name', '/catalog/(name:any)', 'LoginController:catalogName');
$this->router->add('catalog-name-id', '/catalog/(name:any)/(id:int)', 'LoginController:catalogNamePost');
$this->router->add('help-name', '/help/(id:int)', 'LoginController:helpName');
$this->router->add('examples', '/examples', 'HomeController:examples');

//end PAGES

$this->router->add('us-rem', '/deleting-site', 'LoginController:UsRem');
$this->router->add('get-mat_cat_1', '/get-mat_cat', 'DataController:getMatCat', 'POST');
$this->router->add('get-mat_by_cat', '/get-mat_by_cat', 'DataController:getMatByCat', 'POST');
$this->router->add('get-loader-homogeneous', '/loader-homogeneous', 'HomeController:getLoaderHomogeneous');
$this->router->add('get-loader-thermal', '/loader-thermal', 'HomeController:getLoaderThermal');
$this->router->add('get-loader-blocks-block', '/loader-blocks-block', 'HomeController:getLoaderBlocksBlock');
$this->router->add('get-loader-blocks-seam', '/loader-blocks-seam', 'HomeController:getLoaderBlocksSeam');
$this->router->add('get-loader-frame-frame', '/loader-frame-frame', 'HomeController:getLoaderFrameFrame');
$this->router->add('get-loader-frame-filling', '/loader-frame-filling', 'HomeController:getLoaderFrameFilling');
$this->router->add('get-loader-holder', '/loader-holder', 'HomeController:getLoaderHolder');
$this->router->add('get-coefficient', '/get-coefficient', 'DataController:getCoefficient', 'POST');

$this->router->add('get-loader-materials', '/loader-materials', 'HomeController:getLoaderMaterials', 'POST');

$this->router->add('get-mat_cat_by-id', '/get-material-by-id', 'DataController:getMaterialById', 'POST');
$this->router->add('get-mat_self_by-id', '/get-self-material-by-id', 'DataController:getSelfMaterialById', 'POST');


$this->router->add('get-table-homogeneous', '/table-homogeneous_thermal', 'HomeController:getTableHomogeneousThermal');
$this->router->add('get-table-thermal', '/table-block_filling', 'HomeController:getTableBlockFilling');

$this->router->add('get-mat_cat_blocks', '/get-mat_cat_blocks', 'DataController:getMatCatBlocks', 'POST');
$this->router->add('get_dew_data', '/get_dew_data', 'DataController:getDewData', 'POST');

//reg and login
$this->router->add('sign-up', '/sign-up', 'HomeController:signUp');
$this->router->add('login', '/login', 'HomeController:login');
$this->router->add('logout', '/logout', 'LoginController:logout');

$this->router->add('reg', '/reg/', 'LoginController:reg', 'POST');
$this->router->add('auth', '/auth/', 'LoginController:auth', 'POST');
//$this->router->add('reset', '/reset/', 'HomeController:reset');
$this->router->add('reg-success', '/reg-success', 'HomeController:regSuccess');
//$this->router->add('reset', '/reset/', 'HomeController:reset');
//$this->router->add('update-pass', '/update/pass', 'LoginController:updatePass', 'POST');
//$this->router->add('forget', '/forgot/', 'HomeController:forgot');
//$this->router->add('forget-send', '/forget-send/', 'LoginController:forgetPass', 'POST');
//$this->router->add('forget-send-page', '/forget-send-page/', 'HomeController:forgetSendPage');
//$this->router->add('forget-pass-page', '/forget-pass-page', 'HomeController:forgetPassPage');
//end login and reg
$this->router->add('edit-user', '/edit-profile/', 'LoginController:editProfile', 'POST');


$this->router->add('contact', '/contact', 'HomeController:contact');
$this->router->add('contacts', '/contacts', 'HomeController:contactProf');

$this->router->add('same-email', '/same-email', 'HomeController:sameEmail');

$this->router->add('activation', '/activation/', 'HomeController:activation');
$this->router->add('profile', '/profile', 'HomeController:profile');

$this->router->add('calculate', '/calculate', 'DataController:calc', 'POST');
$this->router->add('calcPMU', '/calcPMU', 'DataController:calcPMU', 'POST');
$this->router->add('calc_3_1', '/calc_3_1', 'DataController:calc_3_1', 'POST');
$this->router->add('calc_4_tab', '/calc_4_tab', 'DataController:calc_4_tab', 'POST');

$this->router->add('writeCookieSimple', '/writeCookieSimple', 'DataController:writeCookieSimple', 'POST');
$this->router->add('writeCookie', '/writeCookie', 'DataController:writeCookie', 'POST');
$this->router->add('writeCookie_index', '/writeCookie_index', 'DataController:writeCookieIndex', 'POST');
$this->router->add('deleteCookie', '/deleteCookie', 'DataController:deleteCookie', 'POST');
$this->router->add('writeLayer', '/writeLayer', 'DataController:writeLayer', 'POST');
$this->router->add('writeLayerName', '/writeLayerName', 'DataController:writeLayerName', 'POST');
$this->router->add('editLayerName', '/editLayerName', 'DataController:editLayerName', 'POST');
$this->router->add('removeDataLayer', '/remove_data_layer', 'DataController:removeDataLayer', 'POST');
$this->router->add('calc_heating', '/calc_heating', 'DataController:calcHeating', 'POST');
$this->router->add('calcWindow', '/calc_window', 'DataController:calcWindow', 'POST');
//compare
$this->router->add('sendToCompare', '/send_to_compare', 'DataController:sendToCompare', 'POST');
$this->router->add('downloadPdfFile', '/downloadPdfFile', 'DataController:downloadPdfFile', 'POST');

//$this->router->add('pdf', '/pdf', 'HomeController:pdf');
$this->router->add('pdf', '/pdf', 'HomeController:pdf', 'POST');

$this->router->add('reset-cookie', '/reset_cookie', 'LoginController:resetCookie');
$this->router->add('feedback', '/feedback', 'LoginController:feedback', 'POST');
$this->router->add('thank', '/thank', 'HomeController:thank');
$this->router->add('rules', '/rules', 'HomeController:rules');
$this->router->add('for_graph', '/for_graph', 'DataController:forGraph', 'POST');


//FROM FORUM REQUEST
$this->router->add('reg_from_forum', '/reg_from_forum', 'LoginController:regFromForum', 'POST');
$this->router->add('login_from_forum', '/login_from_forum', 'LoginController:loginFromForum', 'POST');
$this->router->add('email_confirm_forum', '/email_confirm_forum', 'LoginController:emailConfirmForum', 'POST');
$this->router->add('change_email_forum', '/change_email_forum', 'LoginController:changeEmailForum', 'POST');
$this->router->add('change_account_forum', '/change_account_forum', 'LoginController:changeAccountForum', 'POST');
$this->router->add('logout_forum', '/logout_forum', 'LoginController:logoutForum');

//Self created Materials
// materials Routes (GET)
$this->router->add('self-material-edit', '/self_materials/edit/(id:int)', 'DataController:editSelf');

$this->router->add('self-material-add', '/self_material/add/', 'DataController:addSelf', 'POST');
$this->router->add('self-material-update', '/self_material/update/', 'DataController:updateSelf', 'POST');
$this->router->add('self-material-remove', '/self_material_remove', 'DataController:removeSelf', 'POST');

$this->router->add('get-mat_cat_self', '/get-mat_cat_self', 'DataController:getMatCatSelf', 'POST');
$this->router->add('get-mat_cat_blocks_self', '/get-mat_cat_blocks_self', 'DataController:getMatCatBlocksSelf', 'POST');

$this->router->add('get-calc_estimate', '/calc_estimate', 'DataController:getEstimate', 'POST');
$this->router->add('get-wind_estimate', '/wind_estimate', 'DataController:getEstimateWindow', 'POST');
$this->router->add('get-heating_estimate', '/heating_estimate', 'DataController:getEstimateHeating', 'POST');
$this->router->add('get-staticThickness-mat', '/get-staticThickness-mat', 'DataController:getStaticThickness', 'POST');
$this->router->add('test', '/test', 'HomeController:test');
$this->router->add('show-preview', '/show-preview-video', 'DataController:showPreview');

