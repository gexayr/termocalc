<?php

return [
    'baseUrl'        => $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'],
    'defaultLang'     => 'english',
    'defaultTimezone' => 'America/Chicago',
    'defaultTheme'    => 'default'
];