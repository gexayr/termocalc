<?php

namespace Cms\Model\Layer;
use Engine\Core\Database\ActiveRecord;

class Layer
{
    use ActiveRecord;

    protected $table = 'layer_data';

    /**
     * @var
     */
    public $id;
    /**
     * @var
     */
    public $user_id;
    /**
     * @var
     */
    public $layer_data_1;
    public $layer_data_2;
    public $layer_data_3;
    public $layer_data_4;
    public $layer_data_5;
    public $layer_data_6;


    /**
     * @var
     */
    public $layer_link;
    /**
     * @var
     */
    public $data;
    public $name;
    public $compare;
    public $compare_data;

    /**
     * @return mixed
     */
    public function getCompare()
    {
        return $this->compare;
    }

    /**
     * @param mixed $compare
     */
    public function setCompare($compare)
    {
        $this->compare = $compare;
    }

    /**
     * @return mixed
     */
    public function getCompareData()
    {
        return $this->compare_data;
    }

    /**
     * @param mixed $compare_data
     */
    public function setCompareData($compare_data)
    {
        $this->compare_data = $compare_data;
    }


    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->user_id;
    }

    /**
     * @param mixed $user_id
     */
    public function setUserId($user_id)
    {
        $this->user_id = $user_id;
    }

    /**
     * @return mixed
     */
    public function getLayerData1()
    {
        return $this->layer_data_1;
    }

    /**
     * @param mixed $layer_data_1
     */
    public function setLayerData1($layer_data_1)
    {
        $this->layer_data_1 = $layer_data_1;
    }

    /**
     * @return mixed
     */
    public function getLayerData2()
    {
        return $this->layer_data_2;
    }

    /**
     * @param mixed $layer_data_2
     */
    public function setLayerData2($layer_data_2)
    {
        $this->layer_data_2 = $layer_data_2;
    }

    /**
     * @return mixed
     */
    public function getLayerData3()
    {
        return $this->layer_data_3;
    }

    /**
     * @param mixed $layer_data_3
     */
    public function setLayerData3($layer_data_3)
    {
        $this->layer_data_3 = $layer_data_3;
    }

    /**
     * @return mixed
     */
    public function getLayerData4()
    {
        return $this->layer_data_4;
    }

    /**
     * @param mixed $layer_data_4
     */
    public function setLayerData4($layer_data_4)
    {
        $this->layer_data_4 = $layer_data_4;
    }

    /**
     * @return mixed
     */
    public function getLayerData5()
    {
        return $this->layer_data_5;
    }

    /**
     * @param mixed $layer_data_5
     */
    public function setLayerData5($layer_data_5)
    {
        $this->layer_data_5 = $layer_data_5;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getLayerLink()
    {
        return $this->layer_link;
    }

    /**
     * @param mixed $layer_link
     */
    public function setLayerLink($layer_link)
    {
        $this->layer_link = $layer_link;
    }

    /**
     * @return mixed
     */
    public function getLayerData6()
    {
        return $this->layer_data_6;
    }

    /**
     * @param mixed $layer_data_6
     */
    public function setLayerData6($layer_data_6)
    {
        $this->layer_data_6 = $layer_data_6;
    }
}