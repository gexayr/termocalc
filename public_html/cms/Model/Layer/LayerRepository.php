<?php

namespace Cms\Model\Layer;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;


class LayerRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {

        if (empty($params)) {
            return 0;
        }
        $layer = new Layer;
        $layer->setUserId($params['user_id']);
        $layer->setName($params['name']);
        $layer->setLayerData1($params['data_layer_1']);
        $layer->setLayerData2($params['data_layer_2']);
        $layer->setLayerData3($params['data_layer_3']);
        $layer->setLayerData4($params['data_layer_4']);
        $layer->setLayerData5($params['data_layer_5']);
        $layer->setLayerData6($params['data_layer_6']);
        $layer->setCompareData($params['compare']);
        $layer->setLayerLink($params['layer_link']);
        $layerId = $layer->save();

        return $layerId;
    }

    public function getList()
    {
//        dd(111);

        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('layer_data')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }


    public function getLayer($id)
    {
        $region = new Layer($id);

        return $region->findOne();
    }

    public function getLayerDataByParams($params)
    {
        if (empty($params)) {
            return 0;
        }
        $sql = $this->queryBuilder->select()
            ->from('layer_data')
            ->where($params['col'], $params['val'], $params['operator'])
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }

    public function updateLayerData($params)
    {
        if (isset($params['id'])) {
            $id = $params['id'];
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('layer_data')
                ->set([$params['col'] => $params['val']])
                ->where('id', $id)->sql();
            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;
        }
    }

    public function remove($id)
    {
//dd($id);
        $sql = $this->queryBuilder->delete()
            ->from('layer_data')
            ->where('id', $id)
            ->sql();

        $result = $this->db->query($sql, $this->queryBuilder->values);

    }
}