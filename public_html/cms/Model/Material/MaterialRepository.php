<?php

namespace Cms\Model\Material;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;


class MaterialRepository extends Model
{
    /**
     * @param array $params
     * @return int
     */
    public function add($params = [])
    {
        if (empty($params)) {
            return 0;
        }

        $menu = new Material;
        $menu->setName($params['name']);
        $menuId = $menu->save();

        return $menuId;
    }

    public function getList()
    {
        $query = $this->db->query(
            $this->queryBuilder
                ->select()
                ->from('material')
                ->orderBy('id', 'ASC')
                ->sql()
        );

        return $query;
    }

    public function getMaterial($id)
    {
        $region = new Material($id);

        return $region->findOne();
    }


    public function getMaterialCategory()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->where('parent', 0)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialCategoryByParent($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->where('parent', $id)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialByParent($id)
    {
        if (empty($id)) {
            return 0;
        }

        $sql = $this->queryBuilder->select()
            ->from('material')
            ->where('category', $id)
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialsByParams($params)
    {
        if (empty($params)) {
            return 0;
        }
        $sql = $this->queryBuilder->select()
            ->from('material_1')
            ->where($params['col'], $params['val'], $params['operator'])
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }


    public function getMaterialsByParams1($params)
    {
        $sql = "SELECT * FROM material_1 WHERE COL3 = '".$params['val']."'";
        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }
    public function getMaterialsByParams2($params)
    {
        $sql = "SELECT * FROM material_1 WHERE (COL3 IS NULL OR COL3 = '') AND category = '".$params['val']."'";

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }

    public function updateMaterial($params)
    {

        if (isset($params['material_id'])) {
            $id = $params['material_id'];
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('material_1')
                ->set([$params['col'] => $params['val']])
                ->where('id', $id)->sql();
            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;

        }
    }

    public function getMaterialsByParamsNull($params)
    {
        if (empty($params)) {
            return 0;
        }
        $sql = 'SELECT * FROM material_1 WHERE (' . $params['col'] . ' IS NULL OR ' . $params['col'] . ' = ""';
        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query) ? $query : null;
    }


    public function getMaterialsByParamsNullHomo($params)
    {
        if (empty($params)) {
            return 0;
        }
//        $sql = 'SELECT * FROM material_1 WHERE ' . $params['col'] . ' <> "Т" OR ' . $params['col'] . ' = "Т" AND COL17 = "H"';
        $sql = 'SELECT * FROM material_1 WHERE ' . $params['col'] . ' IS NULL OR '
            . $params['col'] . ' = "" OR ' . $params['col'] . ' = "Т" AND COL17 = "H"';
        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query) ? $query : null;
    }


    public function getStaticThickness($params)
    {
        if (empty($params)) {
            return 0;
        }
        $sql = 'SELECT * FROM material_1 WHERE ' . $params['col'] . ' IS NOT NULL AND '
            . $params['col'] . ' != ""';
        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query) ? $query : null;
    }


    public function remove($id)
    {

        $sql = $this->queryBuilder->delete()
            ->from('material_1')
            ->where('id', $id)
            ->sql();

        $this->db->query($sql, $this->queryBuilder->values);
    }
}
