<?php

namespace Admin\Controller;

class PostController extends AdminController
{
    public function listing()
    {
        $this->load->model('Post');

        $this->data['posts'] = $this->model->post->getPosts();

        $this->view->render('posts/list', $this->data);
    }

    public function create()
    {
        $this->load->model('Postcategory');
        $this->data['categories'] = $this->model->postcategory->getPostCategories();
        $this->view->render('posts/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Post');
        $this->load->model('Postcategory');

        $this->data['post'] = $this->model->post->getPostData($id);
        $this->data['categories'] = $this->model->postcategory->getPostCategories();
        $this->view->render('posts/edit', $this->data);
    }

    public function add()
    {
        $this->load->model('Post');

        $params = $this->request->post;

        if (isset($params['title'])) {
            $postId = $this->model->post->createPost($params);
            echo $postId;
        }
    }

    public function update()
    {
        $this->load->model('Post');

        $params = $this->request->post;

        if (isset($params['title'])) {
            $postId = $this->model->post->updatePost($params);
            echo $postId;
        }
    }

    public function remove($id)
    {
//        dd($id);
        $this->load->model('Post');

        $postId = $this->model->post->remove($id);

        header('Location: /admin/posts/');
    }


    public function listingPostCategories()
    {
        $this->load->model('Postcategory');
//        echo "<pre>";
//dd( $this->load->model('Postcategory'));


        $this->data['categories'] = $this->model->postcategory->getPostCategories();

        $this->view->render('postcategory/list', $this->data);
    }

    public function createPostCategory()
    {
        $this->load->model('Postcategory');
        $this->data['categories'] = $this->model->postcategory->getChild('-');

        $this->view->render('postcategory/create', $this->data);
    }


    public function editPostCategory($id)
    {
        $this->load->model('Postcategory');

        $category = $this->model->postcategory->getPostCategoryData($id);
        $this->data['category'] = $category;
        $this->data['categories'] = $this->model->postcategory->getChild('-');

        $this->view->render('postcategory/edit', $this->data);
    }

    public function addPostCategory()
    {
        $this->load->model('Postcategory');

        $params = $this->request->post;

        if (isset($params['name']) && $params['name'] != '') {
            $params['lat_name'] = self::transliterate($params['name']);
            $categoryId = $this->model->postcategory->createPostCategory($params);
            echo $categoryId;
        }else{
            echo ' Вы не ввели имя категории ';
        }
    }


    public static function transliterate($text) {
        $cyr = [
            'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п',
            'р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
            'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П',
            'Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я', ' '
        ];
        $lat = [
            'a','b','v','g','d','e','io','zh','z','i','y','k','l','m','n','o','p',
            'r','s','t','u','f','h','ts','ch','sh','sht','a','i','y','e','yu','ya',
            'A','B','V','G','D','E','Io','Zh','Z','I','Y','K','L','M','N','O','P',
            'R','S','T','U','F','H','Ts','Ch','Sh','Sht','A','I','Y','e','Yu','Ya', '_'
        ];
        return str_replace($cyr, $lat, $text);
    }


    public function updatePostCategory()
    {
        $this->load->model('Postcategory');

        $params = $this->request->post;
        if (isset($params['name']) && $params['name'] != '') {

            $params['lat_name'] = self::transliterate($params['name']);
            $categoryId = $this->model->postcategory->updatePostCategory($params);
            echo $categoryId;
            if($categoryId != 0){
                header('Location: /admin/post_categories/');
            }
        }else{
            echo 'Вы не ввели имя категории ';
        }
    }

    public function getChildCategories($id)
    {
        $this->load->model('Postcategory');
        $categories = $this->model->postcategory->getChild($id);
        return $categories;
    }

    public function getChildPosts($id)
    {

        $this->load->model('post');
        $post = $this->model->post->getChildPosts($id);

        return $post;
    }
    public function removePostCategory($id)
    {
        $this->load->model('Post');
        $this->load->model('Postcategory');
        $categories = self::getChildCategories($id);


        $posts = self::getChildPosts($id);

        if($categories != NULL) {
            foreach ($categories as $item) {
                $this->model->postcategory->remove($item->id);
            }
        }
        if($posts != NULL) {
            foreach ($posts as $item) {
                $this->model->post->remove($item->id);
            }
        }
        $postId = $this->model->postcategory->remove($id);

        header('Location: /admin/post_categories/');
    }


    public function getCategories()
    {
        $params = $this->request->post;

        $categories = self::getChildCategories($params['id']);
        $html = '<option value=""></option>';
        if($categories != NULL) {
            foreach ($categories as $item) {
                $html .= "<option value='$item->id'>$item->name</option>";
            }
        }
        echo $html;
    }






########################################################
//    help section   //
########################################################


    public function listingHelp()
    {
        $this->load->model('Help');

        $this->data['posts'] = $this->model->help->getHelps();

        $this->view->render('helps/list', $this->data);
    }

    public function createHelp()
    {
        $this->view->render('helps/create');
    }

    public function editHelp($id)
    {
        $this->load->model('Help');
        $this->data['post'] = $this->model->help->getHelpData($id);
        $this->view->render('helps/edit', $this->data);
    }

    public function addHelp()
    {
        $this->load->model('Help');

        $params = $this->request->post;

        if (isset($params['title'])) {
            $postId = $this->model->help->createHelp($params);
            echo $postId;
        }
    }

    public function updateHelp()
    {
        $this->load->model('Help');

        $params = $this->request->post;
        if (isset($params['title'])) {
            $postId = $this->model->help->updateHelp($params);
            echo $postId;
        }
    }

    public function removeHelp($id)
    {
//        dd($id);
        $this->load->model('Help');
        $postId = $this->model->help->remove($id);
        header('Location: /admin/helps/');
    }
}