<?php

namespace Admin\Controller;

use Engine\Core\Template\Data;

class EstimateController extends AdminController
{
    private function getName($key)
    {
        switch ($key) {
            case 'floor_1':
                $result = 'Пол над проездом или над холодным открытым вентилируемым подвалом';
                break;
            case 'floor_2':
                $result = 'Пол над неотапливаемым закрытым подвалом со световыми проёмами';
                break;
            case 'floor_3':
                $result = 'Пол над неотапливаемыми закрытыми  подвалами без световых проёмов';
                break;
            case 'floor_4':
                $result = 'УШП или ПОЛЫ в подвале по грунту';
                break;
            case 'roof_1':
                $result = 'Скатная кровля';
                break;
            case 'roof_2':
                $result = 'Плоская кровля';
                break;
            case 'roof_3':
                $result = 'Перекрытие на вентилируемом чердаке';
                break;
            case 'roof_4':
                $result = 'Перекрытие на закрытом чердаке';
                break;
            case 'wall_1':
                $result = 'Стена с навесным фасадом';
                break;
            case 'wall_2':
                $result = 'Трёхслойная стена (кладка с утеплением)';
                break;
            case 'wall_3':
                $result = 'Стена с "мокрым фасадом" (СФТК)';
                break;
            case 'window':
                $result = 'Окна';
                break;
            case 'lantern':
                $result = 'Фонари';
                break;
            case 'heating_1':
                $result = 'Природный газ';
                break;
            case 'heating_2':
                $result = 'Сжиженный газ';
                break;
            case 'heating_3':
                $result = 'Дизтопливо';
                break;
            case 'heating_4':
                $result = 'Злектричество';
                break;
            case 'heating_5':
                $result = 'Дрова';
                break;
            case 'heating_6':
                $result = 'Уголь каменный';
                break;
            case 'heating_7':
                $result = 'Пеллеты';
                break;
            case 'price':
                $result = 'Отопительное оборудование';
                break;
            default:
                $result = 'Unexpected value';
        }

        return $result;
    }

    public function listing()
    {
        $this->load->model('Estimate');
        $this->load->model('Material');
        $this->data['estimate'] = $this->model->estimate->getEstimates();
        $this->data['materials'] = $this->model->material->getMaterials();
        $this->view->render('estimate/list', $this->data);
    }

    public function listingId($id)
    {
        $this->load->model('Material');
        $this->load->model('Estimate');
        $params['col'] = "material_id";
        $params['val'] = $id;
        $params['operator'] = "=";
        if((int) $id == 0){
            $this->data['material']['name'] = $this->getName($id);
            $this->data['type'] = 'condition';
            if(explode('_', $id)[0] == 'heating')$this->data['type'] = 'heating';
            if(explode('_', $id)[0] == 'price')$this->data['type'] = 'price';
        } else {
            $this->data['type'] = 'material';
            $this->data['material'] = (array) $this->model->material->getMaterialData($id);
        }
        $this->data['estimate'] = $this->model->estimate->getEstimatesByParams($params);
        $uploadedFiles = $this->model->estimate->getUploadedFile($id);
        if(!empty($uploadedFiles))
            $this->data['file'] = $uploadedFiles[0];

        $this->view->render('estimate/list_estimate', $this->data);
    }

    public function create()
    {
        $this->load->model('Material');
        $this->data['materials'] = $this->model->material->getMaterials();
        $this->view->render('estimate/create', $this->data);
    }

    public function createId($id)
    {
        $this->load->model('Material');
        if((int) $id == 0) {
            $this->data['material']['name'] = $this->getName($id);
            $this->data['material']['id'] = $id;
            $this->data['type'] = 'condition';
            if(explode('_', $id)[0] == 'heating')$this->data['type'] = 'heating';
            if(explode('_', $id)[0] == 'price')$this->data['type'] = 'price';
        } else {
            $this->data['type'] = 'material';
            $this->data['material'] = (array) $this->model->material->getMaterialData($id);
        }

        $this->view->render('estimate/create_estimate', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Estimate');
        $this->load->model('Material');
        $estimate = $this->model->estimate->getEstimateData($id);

        if((int) $estimate->material_id == 0) {
            $materialsObj = [];
            $materialsArr = [
                ['id' => 'wall_1', 'name' => 'Стена с навесным фасадом'],
                ['id' => 'wall_2', 'name' => 'Трёхслойная стена (кладка с утеплением)'],
                ['id' => 'wall_3', 'name' => 'Стена с "мокрым фасадом" (СФТК)'],
                ['id' => 'roof_1', 'name' => 'Скатная кровля'],
                ['id' => 'roof_2', 'name' => 'Плоская кровля'],
                ['id' => 'roof_3', 'name' => 'Перекрытие на вентилируемом чердаке'],
                ['id' => 'roof_4', 'name' => 'Перекрытие на закрытом чердаке'],
                ['id' => 'floor_1', 'name' => 'Пол над проездом или над холодным открытым вентилируемым подвалом'],
                ['id' => 'floor_2', 'name' => 'Пол над неотапливаемым закрытым подвалом со световыми проёмами'],
                ['id' => 'floor_3', 'name' => 'Пол над неотапливаемыми закрытыми  подвалами без световых проёмов'],
                ['id' => 'floor_4', 'name' => 'УШП или ПОЛЫ в подвале по грунту'],
                ['id' => 'window', 'name' => 'Окна'],
                ['id' => 'lantern', 'name' => 'Фонари'],
            ];

            $this->data['type'] = 'condition';
            if(explode('_', $estimate->material_id)[0] == 'heating') {
                $materialsArr = [
                    ['id' => 'heating_1', 'name' => 'Природный газ'],
                    ['id' => 'heating_2', 'name' => 'Сжиженный газ'],
                    ['id' => 'heating_3', 'name' => 'Дизтопливо'],
                    ['id' => 'heating_4', 'name' => 'Злектричество'],
                    ['id' => 'heating_5', 'name' => 'Дрова'],
                    ['id' => 'heating_6', 'name' => 'Уголь каменный'],
                    ['id' => 'heating_7', 'name' => 'Пеллеты'],
                ];
                $this->data['type'] = 'heating';
                $estimate_arr = (array) $estimate;
                $estimate_arr['coefficient_rise'] = json_decode($estimate->rate)->coefficient_rise;
                $estimate_arr['rate'] = json_decode($estimate->rate)->rate;
                $estimate = (object) $estimate_arr;
            }

            foreach ($materialsArr as $item) {
                $materialsObj[] = (object) $item;
            }

            if(explode('_', $estimate->material_id)[0] == 'price') {
                $this->data['type'] = 'price';
                $estimate_arr = (array) $estimate;
                $estimate_arr['more'] = json_decode($estimate->rate)->more;
                $estimate_arr['less'] = json_decode($estimate->rate)->less;
                $estimate_arr['file'] = json_decode($estimate->rate)->file;
                $estimate = (object) $estimate_arr;
            }

            $this->data['materials'] = $materialsObj;
            $this->data['estimate'] = $estimate;

        } else {
            $this->data['estimate'] = $estimate;
            $this->data['type'] = 'material';
            $this->data['materials'] = $this->model->material->getMaterials();
        }
        $this->view->render('estimate/edit', $this->data);
    }

    public function add()
    {
        $this->load->model('Estimate');
        $this->load->model('Material');

        $params = $this->request->post;

        if (isset($params['name']) && $params['name'] != null) {
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = null;
                }
            }

            if(explode('_', $params['material_id'])[0] == 'heating'){
                $params['rate'] = json_encode(
                    [
                        'rate' => $params['rate'],
                        'coefficient_rise' => $params['coefficient_rise']
                    ]
                );
                $params['type'] = 'material';
            }

            if(explode('_', $params['material_id'])[0] == 'price'){
                $this->data['type'] = 'price';
                $file = $this->request->files;

                if (is_uploaded_file($file['file']['tmp_name'])) {
                    if($file['file']['type'] != 'application/pdf'){
                        $this->data['material']['name'] = $this->getName($params['material_id']);
                        $this->data['material']['id'] = $params['material_id'];
                        $this->data['note'] = 'Можно загрузить только PDF файл';
                        $this->data['type'] = 'price';
                        $this->view->render('estimate/create_estimate', $this->data);
                        return false;
                    }
//                    $randomName = Data::generateRandomString();
                    $randomName = $file['file']['name'];
                    move_uploaded_file($file['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']
                        . "/content/themes/default/files/" . $randomName . "");
                }

                $params['rate'] = json_encode(
                    [
                        'more' => $params['more'],
                        'less' => $params['less'],
                        'file' => $randomName,
                    ]
                );
                $params['type'] = 'material';
                $params['unit'] = '-';
            }

            $estimateId = $this->model->estimate->createEstimate($params);
            if($estimateId != false){
                if((int) $params['material_id'] == 0) {
                    $this->data['material']['name'] = $this->getName($params['material_id']);
                    $this->data['material']['id'] = $params['material_id'];
                    $this->data['type'] = 'condition';
                    if(explode('_', $params['material_id'])[0] == 'heating')$this->data['type'] = 'heating';
                    if(explode('_', $params['material_id'])[0] == 'price')$this->data['type'] = 'price';
                } else {
                    $this->data['type'] = 'material';
                    $this->data['material'] = (array) $this->model->material->getMaterialData($params['material_id']);
                }
                $this->data['note'] = 'Смета успешносоздана, можете создать другую смету';
                $this->view->render('estimate/create_estimate', $this->data);
            }
        }else{
            die('empty name');
        }

    }

    public function update()
    {
        $this->load->model('Estimate');
        $params = $this->request->post;
        if (isset($params['name']) && $params['name'] != null) {
            $id = $params['estimate_id'];

            if(explode('_', $params['material_id'])[0] == 'heating'){
                $params['rate'] = json_encode(
                    [
                        'rate' => $params['rate'],
                        'coefficient_rise' => $params['coefficient_rise']
                    ]
                );
                $params['type'] = 'material';
                unset($params['coefficient_rise']);
            }
            if(explode('_', $params['material_id'])[0] == 'price'){
                $file = $this->request->files;
                if (!empty($file) && is_uploaded_file($file['file']['tmp_name'])) {
                    if($file['file']['type'] != 'application/pdf'){
                        echo 'Можно загрузить только PDF файл';
                        return false;
                    }
//                    $randomName = Data::generateRandomString();
                    $randomName = $file['file']['name'];
                    move_uploaded_file($file['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']
                        . "/content/themes/default/files/" . $randomName . "");
                } else {
                    $randomName = $params['old_filename'];
                }

                $params['rate'] = json_encode(
                    [
                        'more' => $params['more'],
                        'less' => $params['less'],
                        'file' => $randomName,
                    ]
                );
                $params['type'] = 'material';
                $params['unit'] = '-';
                unset($params['more']);
                unset($params['less']);
                unset($params['file']);
                unset($params['old_filename']);
            }

            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                if($key == 'estimate_id')
                    continue;
                $estimateId = $this->model->estimate->updateEstimate($key,$value,$id);
                if(!$estimateId)
                    die('error ' . $key);
            }

            header('Location: /admin/estimate/list_estimate/'.$params['material_id']);
        }else{
            die('empty name');
        }
    }

    public function remove()
    {
        $this->load->model('Estimate');
        $params = $this->request->post;

        $estimateData = $this->model->estimate->getEstimateData($params['id']);
        if($estimateData->material_id == 'price') {
            $estimateRate = (json_decode($estimateData->rate));
            if(!empty($estimateRate->file)){
                $fileName = $estimateRate->file;
                $filePath = $_SERVER['DOCUMENT_ROOT']
                    . "/content/themes/default/files/";
                if(file_exists($filePath . $fileName . ".pdf")){
                    unlink($filePath . $fileName . ".pdf");
                }
            }
        }
        $this->model->estimate->remove($params['id']);
    }

    public function removeFile()
    {
        $this->load->model('Estimate');
        $params = $this->request->post;
        $uploadedFile = $this->model->estimate->getUploadedFile($params['material_id']);
        if(!empty($uploadedFile)){
            $this->data['file'] = $uploadedFile[0];
            $filePath = $_SERVER['DOCUMENT_ROOT']
                . "/content/themes/default/files/";
            if(file_exists($filePath . $uploadedFile[0]->name . ".pdf")){
                unlink($filePath . $uploadedFile[0]->name . ".pdf");
            }
        }
        $this->model->estimate->removeFile($params);
    }

    public function uploadFile()
    {
        $this->load->model('Estimate');
        $params = $this->request->post;
        $file = $this->request->files;
        $file_params=[];
        if (!empty($file) && is_uploaded_file($file['file']['tmp_name'])) {
            if($file['file']['type'] != 'application/pdf'){
                echo 'Можно загрузить только PDF файл';
                return false;
            }
//            $file_params['name'] = Data::generateRandomString(7);
            $file_params['name'] = $file['file']['name'];
            move_uploaded_file($file['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']
                . "/content/themes/default/files/" . $file_params['name'] . "");
        } else {
            header('Location: /admin/estimate/list_estimate/'.$params['material_id']);
            return;
        }

        $file_params['material_id'] = $params['material_id'];
        $uploadedFile = $this->model->estimate->getUploadedFile($file_params['material_id']);
        if (empty($uploadedFile)) {
            $fileId = $this->model->estimate->uploadFile($file_params);
            if(!$fileId)
                die('error ');
        } else {
            $filePath = $_SERVER['DOCUMENT_ROOT']
                . "/content/themes/default/files/";
            if(file_exists($filePath . $uploadedFile[0]->name . ".pdf")){
                unlink($filePath . $uploadedFile[0]->name . ".pdf");
            }
            foreach($file_params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                $fileId = $this->model->estimate->updateUploadFile($key,$value,$uploadedFile[0]->id);
                if(!$fileId)
                    die('error ' . $key);
            }
        }

        header('Location: /admin/estimate/list_estimate/'.$params['material_id']);
    }

    public function uploadImage()
    {
        $file = $this->request->files;
        $ext_arr = explode("/", $file['file']['type']);
        if (!empty($file) && is_uploaded_file($file['file']['tmp_name'])) {
            $file_params['name'] = $file['file']['name'];
            move_uploaded_file($file['file']['tmp_name'], $_SERVER['DOCUMENT_ROOT']
                . "/content/uploads/" . $file_params['name']);
        }
        header('Location: /admin/helps/');

    }
}
