<?php

namespace Admin\Controller;

class MaterialController extends AdminController
{
    public function listing()
    {
        $this->load->model('Material');
        $this->data['materials'] = $this->model->material->getMaterials();
        $this->view->render('materials/list', $this->data);
    }

    public function create()
    {
        $this->load->model('Material');
        $this->load->model('Category');
        $par['col'] = 'parent';
        $par['val'] = '0';
        $this->data['category'] = $this->model->category->getCategoriesByParams($par);

        $this->view->render('materials/create', $this->data);
    }

    public function edit($id)
    {
        $this->load->model('Material');
        $this->load->model('Category');
        $par['col'] = 'parent';
        $par['val'] = '0';
        $this->data['category'] = $this->model->category->getCategoriesByParams($par);
        $this->data['material'] = $this->model->material->getMaterialData($id);
        $this->view->render('materials/edit', $this->data);
    }

    public function add()
    {
        $this->load->model('Material');
        $this->load->model('Category');

        $params = $this->request->post;

        if (isset($params['name']) && $params['name'] != null) {
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = null;
                }
            }
            if(!is_null($params['category'])){
                $category = $this->model->category->getCategoryData($params['category']);
                $params['category'] = $category->name;
            }
            if(!is_null($params['COL3'])){
                $category = $this->model->category->getCategoryData($params['COL3']);
                $params['COL3'] = $category->name;
            }
            if(!is_null($params['COL4'])){
                $category = $this->model->category->getCategoryData($params['COL4']);
                $params['COL4'] = $category->name;
            }
            $materialId = $this->model->material->createMaterial($params);
            if($materialId != false){
                header('Location: /admin/materials/');
            }
        }else{
            die('empty name');
        }
    }

    public function update()
    {
        $this->load->model('Material');
        $this->load->model('Category');
        $params = $this->request->post;
        if (isset($params['name']) && $params['name'] != null) {
            $id = $params['material_id'];

            if($params['category'] != ''){
                $category = $this->model->category->getCategoryData($params['category']);
                $params['category'] = $category->name;
            }
            if($params['COL3'] != ''){
                $category = $this->model->category->getCategoryData($params['COL3']);
                $params['COL3'] = $category->name;
            }
            if($params['COL4'] != ''){
                $category = $this->model->category->getCategoryData($params['COL4']);
                $params['COL4'] = $category->name;
            }
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                if($key == 'material_id')
                    continue;
                $materialId = $this->model->material->updateMaterialNew($key,$value,$id);
                if(!$materialId)
                    die('error' . $key);
            }
            header('Location: /admin/materials/edit/'.$params['material_id']);
        }else{
            die('empty name');
        }
    }

    public function remove()
    {
        $this->load->model('Material');
        $params = $this->request->post;
        $this->model->material->remove($params['id']);
    }

    public function getMaterialsByParams()
    {

        $this->load->model('Material');
        $params = $this->request->post;
        if (empty($params)) {
            return 0;
        }
        $materials = $this->model->material->getMaterialsByParams($params);

        $html = "";
        if($params['action'] == "create"){
            foreach ($materials as $material){
                $html .= "<h5><a href='/admin/estimate/create_estimate/$material->id'>$material->name</a></h5>";
            }
        }elseif($params['action'] == "list"){
            foreach ($materials as $material){
                $html .= "<h4><a href='/admin/estimate/list_estimate/$material->id'>$material->name</a></h4>";
            }
        }
        print_r($html);
    }


//    ==== category ====

    public function listingCategories()
    {
        $this->load->model('Category');
        $this->data['categories'] = $this->model->category->getCategories();
        $this->view->render('categories/list', $this->data);
    }

    private function getPretty($categories){
        $this->load->model('Category');
        $par['col'] = 'parent';

        $new_cat = [];
        foreach ($categories as $item){
            $new_cat[] = $item;
            $par['val'] = $item->id;
            $child_cat = $this->model->category->getCategoriesByParams($par);
            foreach ($child_cat as $child){
                $child->name = '- '.$child->name;
                $new_cat[] = $child;

                $par['val'] = $child->id;
                $subchild_cat = $this->model->category->getCategoriesByParams($par);
                foreach ($subchild_cat as $subchild){
                    $subchild->name = '-- '.$subchild->name;
                    $new_cat[] = $subchild;
                }
            }
        }
        return $new_cat;
    }
    public function createCategory()
    {

        $this->load->model('Material');
        $this->load->model('Category');
//        $this->data['categories'] = $this->model->category->getCategories();

        $par['col'] = 'parent';
        $par['val'] = 0;
        $categories = $this->model->category->getCategoriesByParams($par);
        $new_cat = $this->getPretty($categories);
        $this->data['categories'] = $new_cat;
        $this->data['category'] = $this->model->material->getMaterialCategory();
        $this->view->render('categories/create', $this->data);
    }

    public function editCategory($id)
    {
        $this->load->model('Category');
        $par['col'] = 'parent';
        $par['val'] = 0;
        $categories = $this->model->category->getCategoriesByParams($par);
        $new_cat = $this->getPretty($categories);
        $this->data['categories'] = $new_cat;
        $this->data['category'] = $this->model->category->getCategoryData($id);
        $this->view->render('categories/edit', $this->data);
    }

    public function addCategory()
    {
        $this->load->model('Category');
        $params = $this->request->post;
        if (isset($params['name']) && $params['name'] != null) {
            $materialId = $this->model->category->createCategory($params);
            if($materialId != false){
                header('Location: /admin/categories/');
            }
        }else{
            die('empty name');
        }
    }

    public function updateCategory()
    {
        $this->load->model('Category');
        $params = $this->request->post;
//        dd($params);
        if (isset($params['name']) && $params['name'] != null) {
            $id = $params['category_id'];
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                if($key == 'category_id')
                    continue;
                $materialId = $this->model->category->updateCategory($key,$value,$id);
                if(!$materialId)
                    die('error' . $key);
            }
            if($materialId)
                header('Location: /admin/category/edit/'.$params['category_id']);
        }else{
            die('empty name');
        }
    }

    public function removeCategory()
    {
        $this->load->model('Category');
        $this->load->model('Material');
        $params = $this->request->post;
        $category =   $this->model->category->getCategoryData($params['id']);
        $params['name'] = $category->name;
        $this->model->material->removeFromCat($params['name']);
        $this->model->category->remove($params['id']);
    }

    public function uploadFile()
    {
        function generateRandomString($length = 10) {
            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
            $charactersLength = strlen($characters);
            $randomString = '';
            for ($i = 0; $i < $length; $i++) {
                $randomString .= $characters[rand(0, $charactersLength - 1)];
            }
            return $randomString;
        }
        $files = $this->request->files;
        $name = generateRandomString(8);
        if (is_uploaded_file($files['file']['tmp_name'])) {
            $type_arr = explode('/', $files['file']['type']);
            $type = $type_arr[1];
            $orig_name =  $files['file']['name'];
            move_uploaded_file($files['file']['tmp_name'], ROOT_DIR . "/content/uploads/$name.$type");
            $json = file_get_contents(ROOT_DIR . '/Assets/js/plugins/redactor/core/uploadImage/images.json');
            $str_set = mb_substr($json, 0, -2);
            $str = ",\n
    {
        \"thumb\": \"https://termocalc.ru/content/uploads/$name.$type\",
        \"image\": \"https://termocalc.ru/content/uploads/$name.$type\",
        \"id\": \"$name\",
        \"title\": \"$orig_name\"
    }";
            file_put_contents(ROOT_DIR . "/Assets/js/plugins/redactor/core/uploadImage/images.json", $str_set.$str . "\n]");
        }
    }


    public function getSubcategoriesByParams()
    {

        $this->load->model('Category');
        $params = $this->request->post;
        $par['col'] = 'parent';
        $par['val'] = $params['id'];
        $subcategories = $this->model->category->getCategoriesByParams($par);
        $html = '<option value=""></option>';
        foreach ($subcategories as $subcategory){
            $html .= "<option value='$subcategory->id'>$subcategory->name</option>";
        }
        print_r($html);
    }




//    ======= Self Material =======

    public function listingSelf()
    {
        $this->load->model('SelfCreate');
        $this->data['materials'] = $this->model->selfCreate->getSelfCreates();
        $this->data['users'] = $this->model->selfCreate->getUsers();

        $this->view->render('selfMaterials/list', $this->data);
    }

    public function createSelf()
    {
        $this->load->model('SelfCreate');
        $this->data['users'] = $this->model->selfCreate->getUsers();
        $this->view->render('selfMaterials/create', $this->data);
    }

    public function editSelf($id)
    {
        $this->load->model('SelfCreate');
        $this->data['material'] = $this->model->selfCreate->getSelfCreateData($id);
        $this->data['users'] = $this->model->selfCreate->getUsers();
        $this->view->render('selfMaterials/edit', $this->data);
    }

    public function addSelf()
    {
        $this->load->model('SelfCreate');
        $params = $this->request->post;
        $params['category'] = 'Свои материалы';
        if (isset($params['name']) && $params['name'] != null) {
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = null;
                }
            }
            $materialId = $this->model->selfCreate->createSelfCreate($params);
            if($materialId != false){
                header('Location: /admin/self_materials/');
            }
        }else{
            die('empty name');
        }
    }

    public function updateSelf()
    {
        $this->load->model('SelfCreate');
        $params = $this->request->post;
        if (isset($params['name']) && $params['name'] != null) {
            $id = $params['material_id'];
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                if($key == 'material_id')
                    continue;
                $materialId = $this->model->selfCreate->updateSelfCreateNew($key,$value,$id);
                if(!$materialId)
                    die('error' . $key);
            }
            header('Location: /admin/self_materials/edit/'.$params['material_id']);
        }else{
            die('empty name');
        }
    }

    public function removeSelf()
    {
        $this->load->model('SelfCreate');
        $params = $this->request->post;
        $this->model->selfCreate->remove($params['id']);
        header('Location: /admin/self_materials/');

    }
}
