<?php
/**
 * List of routes in the Admin environment
 */

$this->router->add('login', '/admin/login/', 'LoginController:form');
$this->router->add('auth-admin', '/admin/auth/', 'LoginController:authAdmin', 'POST');
$this->router->add('dashboard', '/admin/', 'DashboardController:index');
$this->router->add('logout', '/admin/logout/', 'AdminController:logout');


$this->router->add('material', '/admin/materials/', 'MaterialController:listing');
$this->router->add('material-create', '/admin/materials/create/', 'MaterialController:create');
$this->router->add('material-edit', '/admin/materials/edit/(id:int)', 'MaterialController:edit');

$this->router->add('material-add', '/admin/material/add/', 'MaterialController:add', 'POST');
$this->router->add('material-update', '/admin/material/update/', 'MaterialController:update', 'POST');
$this->router->add('material-remove', '/admin/material/remove/', 'MaterialController:remove', 'POST');

$this->router->add('self-material', '/admin/self_materials/', 'MaterialController:listingSelf');
$this->router->add('self-material-create', '/admin/self_materials/create/', 'MaterialController:createSelf');
$this->router->add('self-material-edit', '/admin/self_materials/edit/(id:int)', 'MaterialController:editSelf');

$this->router->add('self-material-add', '/admin/self_material/add/', 'MaterialController:addSelf', 'POST');
$this->router->add('self-material-update', '/admin/self_material/update/', 'MaterialController:updateSelf', 'POST');
$this->router->add('self-material-remove', '/admin/self_material/remove/', 'MaterialController:removeSelf', 'POST');


$this->router->add('settings-general', '/admin/settings/general/', 'SettingController:general');
$this->router->add('settings-menus', '/admin/settings/appearance/menus/', 'SettingController:menus');

$this->router->add('setting-update', '/admin/settings/update/', 'SettingController:updateSetting', 'POST');
$this->router->add('setting-add-menu', '/admin/setting/ajaxMenuAdd/', 'SettingController:ajaxMenuAdd', 'POST');
$this->router->add('setting-add-menu-item', '/admin/setting/ajaxMenuAddItem/', 'SettingController:ajaxMenuAddItem', 'POST');
$this->router->add('setting-sort-menu-item', '/admin/setting/ajaxMenuSortItems/', 'SettingController:ajaxMenuSortItems', 'POST');
$this->router->add('setting-remove-menu-item', '/admin/setting/ajaxMenuRemoveItem/', 'SettingController:ajaxMenuRemoveItem', 'POST');
$this->router->add('setting-update-menu-item', '/admin/setting/ajaxMenuUpdateItem/', 'SettingController:ajaxMenuUpdateItem', 'POST');



$this->router->add('category', '/admin/categories/', 'MaterialController:listingCategories');
$this->router->add('category-create', '/admin/category/create/', 'MaterialController:createCategory');
$this->router->add('category-edit', '/admin/category/edit/(id:int)', 'MaterialController:editCategory');


$this->router->add('category-add', '/admin/category/add/', 'MaterialController:addCategory', 'POST');
$this->router->add('category-update', '/admin/category/update/', 'MaterialController:updateCategory', 'POST');
$this->router->add('category-remove', '/admin/category/remove/', 'MaterialController:removeCategory', 'POST');

$this->router->add('uploadImage', '/admin/uploadImage', 'MaterialController:uploadFile', 'POST');

$this->router->add('posts', '/admin/posts/', 'PostController:listing');
$this->router->add('post-create', '/admin/posts/create/', 'PostController:create');
$this->router->add('post-edit', '/admin/posts/edit/(id:int)', 'PostController:edit');

$this->router->add('post-add', '/admin/post/add/', 'PostController:add', 'POST');
$this->router->add('post-update', '/admin/post/update/', 'PostController:update', 'POST');
$this->router->add('post-remove', '/admin/posts/remove/(id:int)', 'PostController:remove');

$this->router->add('post-get-categories', '/admin/post/get_categories/', 'PostController:getCategories', 'POST');

$this->router->add('post-category', '/admin/post_categories/', 'PostController:listingPostCategories');
$this->router->add('post-category-create', '/admin/post_category/create/', 'PostController:createPostCategory');
$this->router->add('post-category-edit', '/admin/post_category/edit/(id:int)', 'PostController:editPostCategory');

$this->router->add('post-category-add', '/admin/post_category/add/', 'PostController:addPostCategory', 'POST');
$this->router->add('post-category-update', '/admin/post_category/update/', 'PostController:updatePostCategory', 'POST');
$this->router->add('post-category-remove', '/admin/post_category/remove/(id:int)', 'PostController:removePostCategory');

$this->router->add('helps', '/admin/helps/', 'PostController:listingHelp');
$this->router->add('help-create', '/admin/helps/create/', 'PostController:createHelp');
$this->router->add('help-edit', '/admin/helps/edit/(id:int)', 'PostController:editHelp');

$this->router->add('help-add', '/admin/help/add/', 'PostController:addHelp', 'POST');
$this->router->add('help-update', '/admin/help/update/', 'PostController:updateHelp', 'POST');
$this->router->add('help-remove', '/admin/helps/remove/(id:int)', 'PostController:removeHelp');

$this->router->add('estimate', '/admin/estimate/', 'EstimateController:listing');
$this->router->add('estimate-create', '/admin/estimate/create/', 'EstimateController:create');
$this->router->add('estimate-edit', '/admin/estimate/edit/(id:any)', 'EstimateController:edit');

$this->router->add('estimate-create-id', '/admin/estimate/create_estimate/(id:any)', 'EstimateController:createId');
$this->router->add('estimate-listing-id', '/admin/estimate/list_estimate/(id:any)', 'EstimateController:listingId');

$this->router->add('estimate-add', '/admin/estimate/add/', 'EstimateController:add', 'POST');
$this->router->add('estimate-update', '/admin/estimate/update/', 'EstimateController:update', 'POST');
$this->router->add('estimate-remove', '/admin/estimate/remove/', 'EstimateController:remove', 'POST');

$this->router->add('get-materials-by-params', '/admin/materials/get-materials-by-params/', 'MaterialController:getMaterialsByParams', 'POST');
$this->router->add('get-subcategories-by-params', '/admin/materials/get-subcategories-by-params/', 'MaterialController:getSubcategoriesByParams', 'POST');

$this->router->add('upload-file', '/admin/upload-file/(id:any)', 'EstimateController:uploadFile', 'POST');
$this->router->add('remove-file', '/admin/remove-file/(id:any)', 'EstimateController:removeFile', 'POST');
$this->router->add('upload-image', '/admin/upload-image', 'EstimateController:uploadImage', 'POST');
$this->router->add('preview_video_link', '/admin/preview_video_link', 'SettingController:updateSetting', 'POST');
$this->router->add('general-data', '/admin/general-data', 'SettingController:updateSetting', 'POST');
