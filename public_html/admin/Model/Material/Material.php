<?php

namespace Admin\Model\Material;
use Engine\Core\Database\ActiveRecord;

class Material
{
    use ActiveRecord;

    protected $table = 'material_1';

    public $id;

    public $category;
    public $COL3;
    public $COL4;
    public $name;
    public $COL6;
    public $COL7;
    public $COL8;
    public $COL9;
    public $COL10;
    public $COL11;
    public $COL12;
    public $COL13;
    public $COL14;
    public $COL15;
    public $COL16;
    public $COL17;
    public $COL18;
    public $COL19;
    public $COL20;
    public $COL21;
    public $COL22;
    public $COL23;
    public $COL24;
    public $COL25;
    public $COL26;
    public $COL27;
    public $COL28;
    public $COL29;
    public $COL30;
    public $COL31;
    public $COL32;
    public $COL33;
    public $COL34;
    public $COL35;
    public $COL36;
    public $COL37;
    public $COL38;


    /**
     * @return mixed
     */
    public function getCOL38()
    {
        return $this->COL38;
    }

    /**
     * @param mixed $COL38
     */
    public function setCOL38($COL38)
    {
        $this->COL38 = $COL38;
    }

    /**
     * @return mixed
     */
    public function getCOL30()
    {
        return $this->COL30;
    }

    /**
     * @param mixed $COL30
     */
    public function setCOL30($COL30)
    {
        $this->COL30 = $COL30;
    }

    /**
     * @return mixed
     */
    public function getCOL31()
    {
        return $this->COL31;
    }

    /**
     * @param mixed $COL31
     */
    public function setCOL31($COL31)
    {
        $this->COL31 = $COL31;
    }

    /**
     * @return mixed
     */
    public function getCOL32()
    {
        return $this->COL32;
    }

    /**
     * @param mixed $COL32
     */
    public function setCOL32($COL32)
    {
        $this->COL32 = $COL32;
    }

    /**
     * @return mixed
     */
    public function getCOL33()
    {
        return $this->COL33;
    }

    /**
     * @param mixed $COL33
     */
    public function setCOL33($COL33)
    {
        $this->COL33 = $COL33;
    }

    /**
     * @return mixed
     */
    public function getCOL24()
    {
        return $this->COL24;
    }

    /**
     * @param mixed $COL24
     */
    public function setCOL24($COL24)
    {
        $this->COL24 = $COL24;
    }

    /**
     * @return mixed
     */
    public function getCOL19()
    {
        return $this->COL19;
    }

    /**
     * @param mixed $COL19
     */
    public function setCOL19($COL19)
    {
        $this->COL19 = $COL19;
    }

    /**
     * @return mixed
     */
    public function getCOL23()
    {
        return $this->COL23;
    }

    /**
     * @param mixed $COL23
     */
    public function setCOL23($COL23)
    {
        $this->COL23 = $COL23;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getCOL3()
    {
        return $this->COL3;
    }

    /**
     * @param mixed $COL3
     */
    public function setCOL3($COL3)
    {
        $this->COL3 = $COL3;
    }

    /**
     * @return mixed
     */
    public function getCOL4()
    {
        return $this->COL4;
    }

    /**
     * @param mixed $COL4
     */
    public function setCOL4($COL4)
    {
        $this->COL4 = $COL4;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getCOL6()
    {
        return $this->COL6;
    }

    /**
     * @param mixed $COL6
     */
    public function setCOL6($COL6)
    {
        $this->COL6 = $COL6;
    }

    /**
     * @return mixed
     */
    public function getCOL7()
    {
        return $this->COL7;
    }

    /**
     * @param mixed $COL7
     */
    public function setCOL7($COL7)
    {
        $this->COL7 = $COL7;
    }

    /**
     * @return mixed
     */
    public function getCOL8()
    {
        return $this->COL8;
    }

    /**
     * @param mixed $COL8
     */
    public function setCOL8($COL8)
    {
        $this->COL8 = $COL8;
    }

    /**
     * @return mixed
     */
    public function getCOL9()
    {
        return $this->COL9;
    }

    /**
     * @param mixed $COL9
     */
    public function setCOL9($COL9)
    {
        $this->COL9 = $COL9;
    }

    /**
     * @return mixed
     */
    public function getCOL10()
    {
        return $this->COL10;
    }

    /**
     * @param mixed $COL10
     */
    public function setCOL10($COL10)
    {
        $this->COL10 = $COL10;
    }

    /**
     * @return mixed
     */
    public function getCOL11()
    {
        return $this->COL11;
    }

    /**
     * @param mixed $COL11
     */
    public function setCOL11($COL11)
    {
        $this->COL11 = $COL11;
    }

    /**
     * @return mixed
     */
    public function getCOL12()
    {
        return $this->COL12;
    }

    /**
     * @param mixed $COL12
     */
    public function setCOL12($COL12)
    {
        $this->COL12 = $COL12;
    }

    /**
     * @return mixed
     */
    public function getCOL13()
    {
        return $this->COL13;
    }

    /**
     * @param mixed $COL13
     */
    public function setCOL13($COL13)
    {
        $this->COL13 = $COL13;
    }

    /**
     * @return mixed
     */
    public function getCOL14()
    {
        return $this->COL14;
    }

    /**
     * @param mixed $COL14
     */
    public function setCOL14($COL14)
    {
        $this->COL14 = $COL14;
    }

    /**
     * @return mixed
     */
    public function getCOL15()
    {
        return $this->COL15;
    }

    /**
     * @param mixed $COL15
     */
    public function setCOL15($COL15)
    {
        $this->COL15 = $COL15;
    }

    /**
     * @return mixed
     */
    public function getCOL16()
    {
        return $this->COL16;
    }

    /**
     * @param mixed $COL16
     */
    public function setCOL16($COL16)
    {
        $this->COL16 = $COL16;
    }

    /**
     * @return mixed
     */
    public function getCOL17()
    {
        return $this->COL17;
    }

    /**
     * @param mixed $COL17
     */
    public function setCOL17($COL17)
    {
        $this->COL17 = $COL17;
    }

    /**
     * @return mixed
     */
    public function getCOL18()
    {
        return $this->COL18;
    }

    /**
     * @param mixed $COL18
     */
    public function setCOL18($COL18)
    {
        $this->COL18 = $COL18;
    }

    /**
     * @return mixed
     */
    public function getCOL20()
    {
        return $this->COL20;
    }

    /**
     * @param mixed $COL20
     */
    public function setCOL20($COL20)
    {
        $this->COL20 = $COL20;
    }

    /**
     * @return mixed
     */
    public function getCOL21()
    {
        return $this->COL21;
    }

    /**
     * @param mixed $COL21
     */
    public function setCOL21($COL21)
    {
        $this->COL21 = $COL21;
    }


    /**
     * @return mixed
     */
    public function getCOL22()
    {
        return $this->COL22;
    }

    /**
     * @param mixed $COL22
     */
    public function setCOL22($COL22)
    {
        $this->COL22 = $COL22;
    }


    /**
     * @return mixed
     */
    public function getCOL25()
    {
        return $this->COL25;
    }

    /**
     * @param mixed $COL25
     */
    public function setCOL25($COL25)
    {
        $this->COL25 = $COL25;
    }

    /**
     * @return mixed
     */
    public function getCOL26()
    {
        return $this->COL26;
    }

    /**
     * @param mixed $COL26
     */
    public function setCOL26($COL26)
    {
        $this->COL26 = $COL26;
    }

    /**
     * @return mixed
     */
    public function getCOL27()
    {
        return $this->COL27;
    }

    /**
     * @param mixed $COL27
     */
    public function setCOL27($COL27)
    {
        $this->COL27 = $COL27;
    }

    /**
     * @return mixed
     */
    public function getCOL28()
    {
        return $this->COL28;
    }

    /**
     * @param mixed $COL28
     */
    public function setCOL28($COL28)
    {
        $this->COL28 = $COL28;
    }

    /**
     * @return mixed
     */
    public function getCOL29()
    {
        return $this->COL29;
    }

    /**
     * @param mixed $COL29
     */
    public function setCOL29($COL29)
    {
        $this->COL29 = $COL29;
    }

    /**
     * @return mixed
     */
    public function getCOL34()
    {
        return $this->COL34;
    }

    /**
     * @param mixed $COL34
     */
    public function setCOL34($COL34)
    {
        $this->COL34 = $COL34;
    }

    /**
     * @return mixed
     */
    public function getCOL35()
    {
        return $this->COL35;
    }

    /**
     * @param mixed $COL35
     */
    public function setCOL35($COL35)
    {
        $this->COL35 = $COL35;
    }

    /**
     * @return mixed
     */
    public function getCOL36()
    {
        return $this->COL36;
    }

    /**
     * @param mixed $COL36
     */
    public function setCOL36($COL36)
    {
        $this->COL36 = $COL36;
    }

    /**
     * @return mixed
     */
    public function getCOL37()
    {
        return $this->COL37;
    }

    /**
     * @param mixed $COL37
     */
    public function setCOL37($COL37)
    {
        $this->COL37 = $COL37;
    }

}