<?php

namespace Admin\Model\Material;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class MaterialRepository extends Model
{
    /**
     * @return mixed
     */
    public function getMaterials()
    {
        $sql = $this->queryBuilder->select()
            ->from('material_1')
            ->orderBy('id', 'DESC')
            ->sql();
        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null|\stdClass
     */
    public function getMaterialData($id)
    {
        $Material = new Material($id);

        return $Material->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createMaterial($params)
    {

        $Material = new Material;
        $Material->setCategory($params['category']);
        $Material->setName($params['name']);
        for ($i=3;$i<=38;$i++){
            if($i == 5)continue;
            $Material->{"setCOL$i"}($params["COL$i"]);
        }
        $MaterialId = $Material->save();
        return $MaterialId;
    }

    /**
     * @param $params
     */
    public function updateMaterialNew($key,$value,$id)
    {
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('material_1')
                ->set([
                    $key => $value,
                ])
                ->where('id', $id)->sql();
            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;
    }


    public function updateMaterial($params)
    {
        if (isset($params['material_id'])) {
            $id = $params['material_id'];
            $queryBuilder = new QueryBuilder();
            $res = ['category' => $params['category'],
                    'name' => $params['name'],
                ];
            for ($i=3;$i<=38;$i++){
                if($i == 5)continue;
                $res ["COL$i"] = $params["COL$i"];
                }
            $sql = $queryBuilder
                ->update('material_1')
                ->set($res)
                ->where('id', $id)->sql();
            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;
        }
    }

    public function remove($id)
    {
        $sql = $this->queryBuilder->delete()
            ->from('material_1')
            ->where('id', $id)
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
        header('Location: /admin/materials/');
    }

    public function removeFromCat($cat_name)
    {

        $sql = $this->queryBuilder->delete()
            ->from('material_1')
            ->where('category', $cat_name)
            ->sql();

        $result = $this->db->query($sql, $this->queryBuilder->values);

    }


    /**
     * @return object
     */
    public function getMaterialCategory()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }


    public function getMaterialsByParams($params)
    {

        $sql = $this->queryBuilder->select()
            ->from('material_1')
            ->where($params['col'], $params['val'], $params['operator'])
            ->orderBy('id', 'ASC')
            ->sql();
//        $sql = 'SELECT * FROM material_1 WHERE ' . $params['col'] . ' LIKE %' . $params['val'] . '%';

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }
}