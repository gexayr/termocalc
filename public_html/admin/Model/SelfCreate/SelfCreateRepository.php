<?php

namespace Admin\Model\SelfCreate;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class SelfCreateRepository extends Model
{
    /**
     * @return mixed
     */
    public function getSelfCreates()
    {
        $sql = $this->queryBuilder->select()
            ->from('self_create')
//            ->orderBy('id', 'DESC')
            ->sql();
        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null | stdClass
     */
    public function getSelfCreateData($id)
    {
        $SelfCreate = new SelfCreate($id);
        return $SelfCreate->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createSelfCreate($params)
    {
        $SelfCreate = new SelfCreate;
        $SelfCreate->setUserId($params['user_id']);
        $SelfCreate->setCategory($params['category']);
        $SelfCreate->setName($params['name']);
        for ($i=3;$i<=38;$i++){
            if($i == 5)continue;
            $SelfCreate->{"setCOL$i"}($params["COL$i"]);
        }
        $SelfCreateId = $SelfCreate->save();

        return $SelfCreateId;
    }


    public function getSelfCreateBy($params)
    {


        $col = $params['col'];
        $val = $params['val'];
        $sql = $this->queryBuilder->select()
            ->from('self_create')
            ->where($col, $val)
            ->sql();
        $query = $this->db->query($sql, $this->queryBuilder->values);

        $result = null;
        if(isset($query[0])){
            $result = $query;
        }
        return $result;
    }

    /**
     * @param $key
     * @param $value
     * @param $id
     * @return mixed
     */
    public function updateSelfCreateNew($key, $value, $id)
    {
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('self_create')
                ->set([
                    $key => $value,
                ])
                ->where('id', $id)->sql();
            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;
    }

    /**
     * @param $id
     */
    public function remove($id)
    {
        $sql = $this->queryBuilder->delete()
            ->from('self_create')
            ->where('id', $id)
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
    }

    public function getUsers()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->sql();
        return $this->db->query($sql);
    }


}