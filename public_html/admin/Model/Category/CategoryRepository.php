<?php

namespace Admin\Model\Category;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class CategoryRepository extends Model
{
    /**
     * @return mixed
     */
    public function getCategories()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null|\stdClass
     */
    public function getCategoryData($id)
    {
        $Material = new Category($id);

        return $Material->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createCategory($params)
    {

        $Material = new Category;
        $Material->setName($params['name']);
        $Material->setParent($params['parent']);
        $MaterialId = $Material->save();
        return $MaterialId;
    }

    /**
     * @param $params
     */
    public function updateCategory($key,$value,$id)
    {

            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('material_category')
                ->set([
                    $key => $value,
                ])
                ->where('id', $id)->sql();

            $query = $this->db->execute($sql, $queryBuilder->values);
            return $query;
    }


    public function remove($id)
    {

        $sql = $this->queryBuilder->delete()
            ->from('material_category')
            ->where('id', $id)
            ->sql();

        $result = $this->db->query($sql, $this->queryBuilder->values);

        header('Location: /admin/categories/');


    }


    /**
     * @return object
     */
    public function getMaterialCategory()
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }


    public function getCategoriesByParams($params)
    {

        $sql = $this->queryBuilder->select()
            ->from('material_category')
            ->where($params['col'], $params['val'])
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }
}