<?php
namespace Admin\Model\Setting;

use Engine\Model;

class SettingRepository extends Model
{

    public function createSetting($params)
    {
        $setting = new Setting;
        $setting->setKeyField($params['key_field']);
        $setting->setValue($params['value']);
        $setting->setName($params['name']);
        $setting = $setting->save();

        return $setting;
    }

    public function getSettings()
    {
        $sql = $this->queryBuilder->select()
            ->from('setting')
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql);
    }

    /**
     * @param string $keyField
     * @return null|string
     */
    public function getSettingValue($keyField)
    {
        $sql = $this->queryBuilder->select('value')
            ->from('setting')
            ->where('key_field', $keyField)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query[0]) ? $query[0]->value : null;
    }

    public function getSettingName($keyField)
    {
        $sql = $this->queryBuilder->select('name')
            ->from('setting')
            ->where('key_field', $keyField)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);

        return isset($query[0]) ? $query[0]->name : null;
    }

    public function update(array $params)
    {
        if (!empty($params)) {
            foreach ($params as $key => $value) {
                if($key == 'general_data') {
                    $sql = $this->queryBuilder
                        ->update('setting')
                        ->set(['value' => $value['value'], 'name' => $value['name']])
                        ->where('key_field', $key)
                        ->sql();
                } else {
                    $sql = $this->queryBuilder
                        ->update('setting')
                        ->set(['value' => $value, 'name' => $key])
                        ->where('key_field', $key)
                        ->sql();
                }
                $result = $this->db->execute($sql, $this->queryBuilder->values);
            }
            return $result;
        }
    }

}