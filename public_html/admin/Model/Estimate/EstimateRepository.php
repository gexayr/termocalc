<?php

namespace Admin\Model\Estimate;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class EstimateRepository extends Model
{
    public function getEstimates()
    {
        $sql = $this->queryBuilder->select()
            ->from('estimate')
            ->orderBy('id', 'DESC')
            ->sql();
        return $this->db->query($sql);
    }

    public function getEstimateData($id)
    {
        $Estimate = new Estimate($id);

        return $Estimate->findOne();
    }

    public function createEstimate($params)
    {
        $Estimate = new Estimate;
        $Estimate->setMaterialId($params['material_id']);
        $Estimate->setConstruction($params['construction']);
        $Estimate->setType($params['type']);
        $Estimate->setName($params['name']);
        $Estimate->setUnit($params['unit']);
        $Estimate->setPrice($params['price']);
        $Estimate->setRate($params['rate']);
        $EstimateId = $Estimate->save();
        return $EstimateId;
    }

    public function updateEstimate($key,$value,$id)
    {
        $queryBuilder = new QueryBuilder();
        $sql = $queryBuilder
            ->update('estimate')
            ->set([
                $key => $value,
            ])
            ->where('id', $id)->sql();
        $query = $this->db->execute($sql, $queryBuilder->values);
        return $query;
    }

    public function remove($id)
    {
        $sql = $this->queryBuilder->delete()
            ->from('estimate')
            ->where('id', $id)
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
        header('Location: /admin/estimate/');
    }


    public function getEstimatesByParams($params)
    {

        $sql = $this->queryBuilder->select()
            ->from('estimate')
            ->where($params['col'], $params['val'], $params['operator'])
            ->orderBy('id', 'ASC')
            ->sql();
//        $sql = 'SELECT * FROM material_1 WHERE ' . $params['col'] . ' LIKE %' . $params['val'] . '%';

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }

//    Files
    public function updateUploadFile($key, $value, $id)
    {
        $queryBuilder = new QueryBuilder();
        $sql = $queryBuilder
            ->update('files')
            ->set([
                $key => $value,
            ])
            ->where('id', $id)->sql();
        $query = $this->db->execute($sql, $queryBuilder->values);
        return $query;
    }

    public function getUploadedFile($id)
    {
        $sql = $this->queryBuilder->select()
            ->from('files')
            ->where('material_id', $id)
            ->sql();
        return $this->db->query($sql, $this->queryBuilder->values);
    }

    public function uploadFile($params)
    {
        $sql = $this->queryBuilder->insert('files')
                ->set($params)
                ->sql();
        return $this->db->execute($sql, $this->queryBuilder->values);
    }


    public function removeFile($params)
    {
        $sql = $this->queryBuilder->delete()
            ->from('files')
            ->where('id', $params['id'])
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
        header('Location: /admin/estimate/list_estimate/'.$params['material_id']);
    }

    public function getFileBy($params)
    {
        $sql = $this->queryBuilder->select()
            ->from('files')
            ->where($params['col'], $params['val'])
            ->orderBy('id', 'ASC')
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return isset($query) ? $query : null;
    }
}