<?php

namespace Admin\Model\User;

use Engine\Core\Database\QueryBuilder;
use Engine\Model;

class UserRepository extends Model
{
    public function getUsers()
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
//            ->orderBy('id', 'DESC')
            ->sql();
        return $this->db->query($sql);
    }

    public function test()
    {
        $user = new User;
        $user->setEmail('test@admin.com');
        $user->setPassword(md5(rand(1, 10)));
        $user->setRole('user');
        $user->setHash('new');
        $user->save();
    }


    public function getUser($id)
    {
        $post = new User($id);

        return $post->findOne();
    }


    public function getUsersCount()
    {

        $sql = $this->queryBuilder->select('id')
            ->from('user')
            ->orderBy('id', 'DESC')
            ->sql();

        return $this->db->query($sql);
    }


    public function getUsersBy($params)
    {
        if(empty($params)) {
            return 0;
        }

        $page = ($params['page'] - 1)*20;
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->orderBy('id', 'DESC')
            ->limit($params['limit'])
            ->offset($page)
            ->sql();
        return $this->db->query($sql);
    }

    public function getUserBy($params)
    {

        $col = $params['col'];
        $val = $params['val'];
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->where($col, $val)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);


        $result = null;
        if(isset($query[0])){
            $result = $query;
        }

        return $result;
    }


    public function auth($params)
    {
        $sql = $this->queryBuilder->select()
            ->from('user')
            ->where('email', $params['email'])
            ->where('password', md5($params['password']))
            ->limit(1)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return !empty($query) ? $query[0] : null;

    }


    public function add($params)
    {
//        echo "<pre>";
//        debug_die($params);
        $params['date_reg'] = time();
        $user = new User;
        $user->setEmail($params['email']);
        $user->setName($params['name']);
        $user->setSurname($params['surname']);
        $user->setLogin($params['login']);
        $user->setPassword(md5($params['password']));
        $user->setRegionId(50);
        $user->setCityId(317);
        $user->setBuildingId($params['building']);
        $user->setPhone($params['phone']);
        $user->setStatus('developer');
        $user->setRole('user');
        $user->setSecret($params['secret']);
        $userId = $user->save();
        return $userId;

    }

    public function editPass($params)
    {
        if(empty($params)) {
            return 0;
        }

        $pass = (md5($params['new-password']));
        $old_pass = (md5($params['old-password']));
        $queryBuilder = new QueryBuilder();


        $sql = $queryBuilder
            ->update('user')
            ->set(['password' => $pass])
            ->where('password', $old_pass)->sql();
        $this->db->execute($sql, $queryBuilder->values);
        echo '<script>
            window.alert("Ваш пароль успешно обновлен!");
            window.location.href="/profile";
             </script>';
    }


    public function updatePass($params)
    {

        if(empty($params)) {
            return 0;
        }
        $pass = (md5($params['password']));
        $key = ($params['key']);
        $queryBuilder = new QueryBuilder();


        $sql = $queryBuilder
            ->update('user')
            ->set(['password' => $pass])
            ->where('secret', $key)->sql();

        $result = $this->db->execute($sql, $queryBuilder->values);
        return $result;

    }

    public function activity($id)
    {
        if(empty($id)) {
            return 0;
        }

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set(['verified' => 1])
            ->where('id', $id)->sql();

        $result = $this->db->execute($sql, $queryBuilder->values);
        return $result;
    }

    public function edit($params)
    {
        $id = $params['id'];
        if(empty($id)) {
            return 0;
        }

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set([
                'surname' => $params['surname'],
//                'name' => $params['name'],
//                'login' => $params['login'],
                'status' => $params['status'],
//                'region_id' => $params['region'],
//                'city_id' => $params['city'],
//                'building_id' => $params['building'],
                'phone' => $params['phone'],
                ])
            ->where('id', $id)->sql();

        $query = $this->db->execute($sql, $queryBuilder->values);
        return $query;
    }

    public function changeEmail($params)
    {
        $id = $params['id'];
        if(empty($id)) {
            return 0;
        }

        $queryBuilder = new QueryBuilder();

        $sql = $queryBuilder
            ->update('user')
            ->set([
                'email' => $params['email'],
                'verified' => "(NULL)",
                ])
            ->where('id', $id)->sql();

        $query = $this->db->execute($sql, $queryBuilder->values);
        return $query;
    }
}