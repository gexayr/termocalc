<?php

namespace Admin\Model\Help;

use Engine\Model;

class HelpRepository extends Model
{
    /**
     * @return mixed
     */
    public function getHelps()
    {
        $sql = $this->queryBuilder->select()
            ->from('help')
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null|\stdClass
     */
    public function getHelpData($id)
    {
        $post = new Help($id);

        return $post->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createHelp($params)
    {
        $post = new Help;
        $post->setTitle($params['title']);
        $post->setContent($params['content']);
        $postId = $post->save();

        return $postId;
    }

    /**
     * @param $params
     */
    public function updateHelp($params)
    {
        if (isset($params['post_id'])) {
            $post = new Help($params['post_id']);
            $post->setTitle($params['title']);
            $post->setContent($params['content']);
            $post->save();
        }
    }

    /**
     * @param $params
     */

    public function remove($id)
    {
        $sql = $this->queryBuilder->delete()
            ->from('help')
            ->where('id', $id)
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
    }

}