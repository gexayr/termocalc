<?php

namespace Admin\Model\Postcategory;

use Engine\Model;
use Engine\Core\Database\QueryBuilder;

class PostcategoryRepository extends Model
{
    /**
     * @return mixed
     */
    public function getPostCategories()
    {

        $sql = $this->queryBuilder->select()
            ->from('post_category')
            ->orderBy('id', 'ASC')
            ->sql();

        return $this->db->query($sql);
    }

    /**
     * @param $id
     * @return null|\stdClass
     */
    public function getPostCategoryData($id)
    {
        $Material = new Postcategory($id);

        return $Material->findOne();
    }

    /**
     * @param $params
     * @return mixed
     */
    public function createPostCategory($params)
    {
//dd($params);
        $Material = new Postcategory;
        $Material->setName($params['name']);
        $Material->setLatName($params['lat_name']);
        $Material->setParent($params['parent']);
        $Material->setStatus(1);
        $MaterialId = $Material->save();
        if($MaterialId != 0) {
            header('Location: /admin/post_categories/');
        }

    }

    /**
     * @param $params
     *  @return mixed
     */
    public function updatePostCategory($params)
    {
//        dd($params);

        if (isset($params['category_id'])) {
            $id = $params['category_id'];
            $queryBuilder = new QueryBuilder();
            $sql = $queryBuilder
                ->update('post_category')
                ->set([
                    'name' => $params['name'],
                    'lat_name' => $params['lat_name'],
                    'parent' => $params['parent'],
                    'status' => $params['status'],
                ])
                ->where('id', $id)->sql();

            $query = $this->db->execute($sql, $queryBuilder->values);
            if($query != 0) {
                header('Location: /admin/post_categories/');
            }
        }
    }

    public function getChild($id)
    {
        $sql = $this->queryBuilder->select()
            ->from('post_category')
            ->where('parent', $id)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return !empty($query) ? $query : null;

    }


    public function getCategoriesByLatName($lat_name)
    {
        $sql = $this->queryBuilder->select()
            ->from('post_category')
            ->where('lat_name', $lat_name)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return !empty($query) ? $query[0] : null;

    }

    public function getCategoriesByParams($name)
    {
        $name = urldecode($name);
        $sql = $this->queryBuilder->select()
            ->from('post_category')
            ->where('name', $name)
            ->sql();

        $query = $this->db->query($sql, $this->queryBuilder->values);
        return !empty($query) ? $query[0] : null;

    }


    public function remove($id)
    {

        $sql = $this->queryBuilder->delete()
            ->from('post_category')
            ->where('id', $id)
            ->sql();
        $result = $this->db->query($sql, $this->queryBuilder->values);
        header('Location: /admin/post_categories/');
    }
}