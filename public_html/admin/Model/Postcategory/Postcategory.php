<?php

namespace Admin\Model\Postcategory;

use Engine\Core\Database\ActiveRecord;

class Postcategory
{
    use ActiveRecord;

    protected $table = 'post_category';

    public $id;
    public $parent;
    public $name;
    public $status;
    public $lat_name;

    /**
     * @return mixed
     */
    public function getLatName()
    {
        return $this->lat_name;
    }

    /**
     * @param mixed $lat_name
     */
    public function setLatName($lat_name)
    {
        $this->lat_name = $lat_name;
    }

    /**
     * @return mixed
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

}