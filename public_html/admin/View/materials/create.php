<?php $this->theme->header(); ?>
<?php
$colName = [
    'COL6' => "Плотность кг/куб.м.",
    'COL7' => "Удельная теплоёмкость кДж/(кг·°С)",
    'COL8' => "Теплопроводность Вт/(м·°С)",
    'COL9' => "Влажность % A",
    'COL10' => "Влажность % B",
    'COL11' => "Теплопроводность Вт/(м·°С) A",
    'COL12' => "Теплопроводность Вт/(м·°С) B",
    'COL13' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) A",
    'COL14' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) B",
    'COL15' => "Паропроницаемость мг/(м·ч·Па)",
    'COL16' => "Предельно допустимое приращение влаги, %",
    'COL20' => "Фиксированная толщина слоя в мм",
    'COL21' => "Сопротивление паропроницанию , м ·ч·Па/мг",
    'COL25' => "Положительной",
    'COL26' => "Отрицательной",
    'COL27' => "Положительной",
    'COL28' => "Отрицательной",
];
?>
    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            Создать Материал
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">

                <div class="four wide column">
                    <form id="formPage" class="ui form" action="/admin/material/add/" method="post">

                        <div class="field">
                            <label>Название </label>
                            <input type="text" name="name" class="form-control" id="name" value="" required>
                        </div>

                        <div class="field">
                            <label>Категория</label>
                            <select name="category" id="material_category" onchange="material.change('sub1')">
                                <?php foreach($category as $item){ ?>
                                    <option value="<?=$item->id;?>"><?=$item->name;?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="field">
                            <label>1 подкатегория</label>(не обязательно)
                            <select name="COL3" id="COL3_sub" onchange="material.change('sub2')">
                                <option value=""></option>
                                <option value="13">Минеральные</option>
                                <option value="14">Полимерные</option>
                            </select>
                        </div>

                        <div class="field">
                            <label>2 подкатегория</label>(не обязательно)
                            <select name="COL4" id="COL4_sub">
                                <option value=""></option>
                            </select>
                        </div>

                        <?php
                        for($i=6;$i<=38;$i++){
                            $colLabel = "";
                            $res = "COL$i";
                            if(isset($colName["COL$i"]))
                                $colLabel = $colName["COL$i"];
                            ?>
                            <div class="field">
                                <label><?=$colLabel?> &nbsp; &nbsp;(<?=$res?>)</label>
                                <input type="text" name="COL<?=$i?>" class="form-control" id="COL<?=$i?>">
                            </div>
                        <?php } ?>
                        <button type="submit" class="ui primary button"> Добавить </button>

                    </form>
                </div>
            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>

