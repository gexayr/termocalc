<?php $this->theme->header(); ?>
<?php
//
//
//echo "<pre>";
//print_r($materials);
//echo "</pre>";
?>
    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Материалы
                        <a href="/admin/materials/create/" class="ui primary button right floated item">
                            Создать Материал
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Категория</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($materials as $material): ?>
                    <tr>
                        <th scope="row">
                            <?= $material->id ?>
                        </th>
                        <td>
                            <a href="/admin/materials/edit/<?= $material->id ?>">
                                <?= $material->name ?>
                            </a>
                        </td>
                        <td><?=$material->category;?></td>

                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?php $this->theme->footer(); ?>