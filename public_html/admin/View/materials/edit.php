<?php $this->theme->header(); ?>

<?php
$colName = [
    'COL6' => "Плотность кг/куб.м.",
    'COL7' => "Удельная теплоёмкость кДж/(кг·°С)",
    'COL8' => "Теплопроводность Вт/(м·°С)",
    'COL9' => "Влажность % A",
    'COL10' => "Влажность % B",
    'COL11' => "Теплопроводность Вт/(м·°С) A",
    'COL12' => "Теплопроводность Вт/(м·°С) B",
    'COL13' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) A",
    'COL14' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) B",
    'COL15' => "Паропроницаемость мг/(м·ч·Па)",
    'COL16' => "Предельно допустимое приращение влаги, %",
    'COL20' => "Фиксированная толщина слоя в мм",
    'COL21' => "Сопротивление паропроницанию , м ·ч·Па/мг",
    'COL25' => "Положительной",
    'COL26' => "Отрицательной",
    'COL27' => "Положительной",
    'COL28' => "Отрицательной",
];

$flag = true;
$flag1 = true;
$category_id = 999;
$subcategory_id = 999;
?>
    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $material->name ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/material/update/" method="post">
                        <input type="hidden" name="material_id" id="material_id" value="<?= $material->id ?>"/>

                        <div class="field">
                            <label>Название</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?= $material->name;?>" required>
                        </div>
                        <div class="field">
                            <label>Категория</label>
                            <select name="category" id="material_category" onchange="material.change('sub1')">
                                <?php foreach($category as $item){ ?>
                                <option
                                <?php if($material->category == $item->name) {
                                        echo "selected";
                                        $category_id = $item->id;
                                    } ?>
                                        value="<?=$item->id?>">
                                        <?=$item->name;?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>
                        <?php
                            $params['col'] = 'parent';
                            $params['val'] = $category_id;
                            $subcategory1 = Data::getCategoriesByParams($params);
                        ?>
                        <div class="field">
                            <label>1 подкатегория</label>(не обязательно)
                            <select name="COL3" id="COL3_sub" onchange="material.change('sub2')">
                                <option value=""></option>
                                <?php foreach($subcategory1 as $item){ ?>
                                <option
                                <?php if($material->COL3 == $item->name) {
                                        echo "selected";
                                        $subcategory_id = $item->id;
                                    } ?>
                                        value="<?=$item->id?>">
                                        <?=$item->name;?>
                                </option>
                                <?php } ?>
                            </select>
                        </div>

                        <?php
                        $params['col'] = 'parent';
                        $params['val'] = $subcategory_id;
                        $subcategory2 = Data::getCategoriesByParams($params);
                        ?>
                        <div class="field">
                            <label>2 подкатегория</label>(не обязательно)
                            <select name="COL4" id="COL4_sub">
                                <option value=""></option>
                                <?php foreach($subcategory2 as $item){ ?>
                                    <option <?php if($material->COL4 == $item->name)echo "selected";?>><?=$item->name;?></option>
                                    <?php } ?>
                            </select>
                        </div>
                        <?php
                        for($i=6;$i<=38;$i++){
                            $colLabel = "";
                            $res = "COL$i";
                            if(isset($colName["COL$i"]))
                                $colLabel = $colName["COL$i"];
                            ?>
                            <div class="field">
                                <label for="<?=$res?>"><?=$colLabel?> &nbsp; &nbsp;(<?=$res?>)</label>
                                <input type="text" name="<?=$res?>" class="form-control" id="<?=$res?>" value="<?=$material->$res;?>">
                            </div>
                        <?php } ?>

                </div>
                <div class="twelve wide column">
                    <div class="four wide column">
                        <div>
                            <button type="submit" class="ui primary button"> Обновить </button>

                        </div>
                    </div>
                    </form>
                    <form action="/admin/material/remove/" method="post">
                        <input type="hidden" name="id" value="<?= $material->id ?>">

                        <div style="position: relative">
                            <button type="submit" class="ui red button" style="float: right; margin-top: -36px;">
                                Удалить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>