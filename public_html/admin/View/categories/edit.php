<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $category->name ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/category/update/" method="post">
                        <input type="hidden" name="category_id" id="category_id" value="<?= $category->id ?>"/>
                        <div class="field">
                            <label>Название </label>
                            <input type="text" name="name" class="form-control" id="name" value="<?= $category->name;?>">
                        </div>
                        <div class="field">
                            <label>Родитель </label>
                            <select name="parent" id="">
                                <option value="-"></option>
                                <?php foreach($categories as $item){ ?>
                                    <option value="<?= $item->id ?>" <?=($category->parent == $item->id)?'selected':''?>><?= $item->name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                </div>
                <div class="twelve wide column">
                    <div class="four wide column">
                        <div>
                            <button type="submit" class="ui primary button"> Обновить </button>

                        </div>
                    </div>
                    </form>
                    <form action="/admin/category/remove/" method="post">
                        <input type="hidden" name="id" value="<?= $category->id ?>">
                        <div style="position: relative">
                            <button type="submit" class="ui red button" style="float: right; margin-top: -36px;">
                                Удалить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>