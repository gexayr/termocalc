<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            Создать категорию
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">

                <div class="four wide column">
                    <form id="formPage" class="ui form" action="/admin/category/add/" method="post">
                        <div class="field">
                            <label>Название </label>
                            <input type="text" name="name" class="form-control" value="">
                        </div>
                        <div class="field">
                            <label>Родитель </label>
                            <select name="parent" id="">
                                <option value="-"></option>
                                <?php foreach($categories as $item): ?>
                                    <option value="<?= $item->id ?>"><?= $item->name ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <button type="submit" class="ui primary button"> Добавить </button>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>

