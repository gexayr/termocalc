<div id="waiting"></div>
<!-- Bootstrap core JavaScript-->
<!-- Placed at the end of the document so the pages load faster -->
<div style="height: 10vh;"></div>
<script src="/admin/Assets/js/jquery-2.0.3.min.js"></script>
<script src="/admin/Assets/semantic/semantic.min.js"></script>
<script src="/admin/Assets/semantic/components/transition.min.js"></script>
<script src="/admin/Assets/semantic/components/dropdown.min.js"></script>
<script src="/admin/Assets/semantic/components/sidebar.min.js"></script>
<script src="/admin/Assets/js/jquery-sortable.js"></script>
<script src="/admin/Assets/js/material.js"></script>
<script src="/admin/Assets/js/post.js"></script>
<script src="/admin/Assets/js/setting.js?v=<?=time()?>"></script>
<script src="/admin/Assets/js/menu.js"></script>
<?php //Asset::render('js'); ?>


<script src="/admin/Assets/js/plugins/redactor/redactor.js"></script>

<!-- plugin js -->
<script src="/admin/Assets/js/plugins/redactor/plugins/video/video.js"></script>
<script src="/admin/Assets/js/plugins/redactor/plugins/imagemanager/imagemanager.js"></script>
<!--<script src="/admin/Assets/js/plugins/redactor/plugins/filemanager/filemanager.js"></script>-->
<script src="/admin/Assets/js/plugins/redactor/plugins/table/table.js"></script>
<script src="/admin/Assets/js/init.js"></script>


</body>
</html>