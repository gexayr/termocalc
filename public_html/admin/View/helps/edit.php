<?php $this->theme->header(); ?>
<!--    <script src="https://cdn.ckeditor.com/4.12.1/full-all/ckeditor.js"></script>-->
    <script src="https://cdn.ckeditor.com/4.12.1/standard/ckeditor.js"></script>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $post->title ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form">
                        <input type="hidden" name="post_id" id="formPostId" value="<?= $post->id ?>" />
                        <div class="field">
                            <label>Заголовок</label>
                            <input type="text" name="title" class="form-control" id="formTitle" value="<?= htmlspecialchars($post->title) ?>" placeholder="Title post...">
                        </div>
                        <div class="field">
                            <label>Контент</label>
                            <textarea name="content"><?=$post->content?></textarea>
                        </div>
                    </form>
                </div>
                <div class="four wide column">
                    <div>
<!--                        <p>Обновить справку </p>-->
                        <button type="submit" class="ui primary button" onclick="post.updateHelp()">
                            Обновить
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </main>


    <script>
        // Enable local "abbr" plugin from /myplugins/abbr/ folder.
        CKEDITOR.plugins.addExternal( 'youtube', '/admin/Assets/js/plugins/youtube/', 'plugin.js' );

        // extraPlugins needs to be set too.
        CKEDITOR.replace( 'content', {
            extraPlugins: 'youtube'
        } );
    </script>
<?php $this->theme->footer(); ?>