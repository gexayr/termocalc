<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Помощь
                        <a href="/admin/helps/create/" class="ui blue button right floated item">
                            Создать Помощь
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Заголовок</th>
                    <th>Дата</th>
                    <th>Удалить</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($posts as $post): ?>
                    <tr>
                        <th scope="row">
                            <?= $post->id ?>
                        </th>
                        <td>
                            <a href="/admin/helps/edit/<?= $post->id ?>">
                                <?= $post->title ?>
                            </a>
                        </td>
                        <td>
                            <?= $post->date ?>
                        </td>

                        <td>
                            <a href="/admin/helps/remove/<?= $post->id ?>" class="ui red button" onclick="return post.confirmDelete('пост помощи')">
                                Удалить
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            <form id="formPage" class="ui form" action="/admin/upload-image" method="post" enctype="multipart/form-data">
                <div class="field">
                    <p>ссылка на изображения после загрузки</p>
                    <label><?=$_SERVER['SERVER_NAME']?>/content/uploads/имя_файла</label>
                    <input type="file" name="file" class="form-control">
                </div>
                <div class="four wide column">
                    <div>
                        <button type="submit" class="ui primary button"> Сохранить </button>
                    </div>
                </div>
            </form>
        </div>
    </main>

<?php $this->theme->footer(); ?>