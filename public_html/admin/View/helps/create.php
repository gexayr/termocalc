<?php $this->theme->header(); ?>
    <script src="https://cdn.ckeditor.com/4.12.1/full-all/ckeditor.js"></script>
    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            Создать Помощь
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form">
                        <div class="field">
                            <label>Заголовок</label>
                            <input type="text" name="title" class="form-control" id="formTitle" placeholder="Title post...">
                        </div>
                        <div class="field">
                            <label>Контент </label>
<!--                            <textarea name="content" id="redactor"></textarea>-->
                            <textarea name="content"></textarea>
                        </div>
                    </form>
                </div>
                <div class="four wide column">
                    <div>
<!--                        <p>Publish this help</p>-->
                        <button type="submit" class="ui primary button" onclick="post.addHelp()">
                            Публиковать
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <script>
        // Enable local "abbr" plugin from /myplugins/abbr/ folder.
        CKEDITOR.plugins.addExternal( 'youtube', '/admin/Assets/js/plugins/youtube/', 'plugin.js' );

        // extraPlugins needs to be set too.
        CKEDITOR.replace( 'content', {
            extraPlugins: 'youtube'
        } );
    </script>
<?php $this->theme->footer(); ?>