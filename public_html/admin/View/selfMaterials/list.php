<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Собственные материалы
                        <a href="/admin/self_materials/create/" class="ui primary button right floated item">
                            Создать Материал
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Пользователь</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($materials as $material): ?>
                    <tr>
                        <th scope="row">
                            <?= $material->id - 10000?>
                        </th>
                        <td>
                            <a href="/admin/self_materials/edit/<?= $material->id ?>">
                                <?= $material->name ?>
                            </a>
                        </td>
                        <td>
                            <?php
                            foreach ($users as $user) {
                                if($user->id == $material->user_id)echo $user->email;
                            }
                            ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?php $this->theme->footer(); ?>