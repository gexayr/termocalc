<?php $this->theme->header(); ?>

<?php
$colName = [
    'COL6' => "Плотность кг/куб.м.",
    'COL7' => "Удельная теплоёмкость кДж/(кг·°С)",
    'COL8' => "Теплопроводность Вт/(м·°С)",
    'COL9' => "Влажность % A",
    'COL10' => "Влажность % B",
    'COL11' => "Теплопроводность Вт/(м·°С) A",
    'COL12' => "Теплопроводность Вт/(м·°С) B",
    'COL13' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) A",
    'COL14' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) B",
    'COL15' => "Паропроницаемость мг/(м·ч·Па)",
    'COL16' => "Предельно допустимое приращение влаги, %",
    'COL20' => "Фиксированная толщина слоя в мм",
    'COL21' => "Сопротивление паропроницанию , м ·ч·Па/мг",
    'COL25' => "Положительной",
    'COL26' => "Отрицательной",
    'COL27' => "Положительной",
    'COL28' => "Отрицательной",
];
?>
    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $material->name ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/self_material/update/" method="post">
                        <input type="hidden" name="material_id" id="material_id" value="<?=$material->id ?>"/>
                        <div class="field">
                            <label>Пользователь</label>
                            <select name="user_id">
                                <?php foreach($users as $user){ ?>
                                    <option value="<?=$user->id;?>" <?php if($user->id == $material->user_id)echo "selected" ?>><?=$user->email;?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Название</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?=$material->name;?>" required>
                        </div>
                        <?php
                        for($i=6;$i<=38;$i++){
                            $colLabel = "";
                            $res = "COL$i";
                            if(isset($colName["COL$i"]))
                                $colLabel = $colName["COL$i"];
                            ?>
                            <div class="field">
                                <label for="<?=$res?>"><?=$colLabel?> &nbsp; &nbsp;(<?=$res?>)</label>
                                <input type="text" name="<?=$res?>" class="form-control" id="<?=$res?>" value="<?=$material->$res;?>">
                            </div>
                        <?php } ?>

                </div>
                <div class="twelve wide column">
                    <div class="four wide column">
                        <div>
                            <button type="submit" class="ui primary button"> Обновить </button>

                        </div>
                    </div>
                    </form>
                    <form action="/admin/self_material/remove/" method="post">
                        <input type="hidden" name="id" value="<?= $material->id ?>">

                        <div style="position: relative">
                            <button type="submit" class="ui red button" style="float: right; margin-top: -36px;">
                                Удалить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>