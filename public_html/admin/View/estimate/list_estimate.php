<?php $this->theme->header();
$url = $_SERVER['REQUEST_URI'];
$material_id = substr($url, strrpos($url,'/') + 1);
?>
    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Смета <b><?=$material['name']?></b>
                    </h2>
                </div>
            </div>

        <?php if($type != 'price') { ?>
            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Наименование</th>
                    <th>Тип</th>
                    <th>Тип конструкции</th>
                    <th>Единица</th>
                    <th>Цена единицы</th>
                    <?=($type == 'heating') ? '<th>Теплотворная способн. КВТ</th>' : '<th>Расход единицы</th>'?>
                    <?=($type == 'heating') ? '<th>Коэффициент удорожания за год</th>' : ''?>
                </tr>
                </thead>
                <tbody>
                <?php foreach($estimate as $item): ?>
                    <?php
                    $construction = "Каркасная конструкция:";
                    if($item->construction == "simple")
                        $construction = "Обычный материал";
                    elseif($item->construction == "block")
                        $construction = "Кладка из блоков";
                    ?>
                    <tr>
                        <th scope="row">
                            <?= $item->id ?>
                        </th>
                        <td>
                            <a href="/admin/estimate/edit/<?= $item->id ?>">
                                <?= $item->name ?>
                            </a>
                        </td>
                        <td><?=($item->type == "material")?"материал":"работа"?></td>
                        <td><?=$construction?></td>
                        <td><?=$item->unit?></td>
                        <td><?=$item->price?></td>
                        <td><?=($type == 'heating') ? json_decode($item->rate)->rate : $item->rate?></td>
                        <?=($type == 'heating') ? '<td>' . json_decode($item->rate)->coefficient_rise . '</td>' : ''?>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        <?php } else { ?>
            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Наименование</th>
                    <th>Стоимость</th>
                    <th>ТД313 больше</th>
                    <th>ТД313 меньше или равно</th>
                    <th>Файл</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($estimate as $item): ?>
                    <?php
                        $obj = json_decode($item->rate)
                    ?>
                    <tr>
                        <th scope="row">
                            <?= $item->id ?>
                        </th>
                        <td>
                            <a href="/admin/estimate/edit/<?= $item->id ?>">
                                <?= $item->name ?>
                            </a>
                        </td>
                        <td><?=$item->price?></td>
                        <td><?=$obj->more?></td>
                        <td><?=$obj->less?></td>
                        <td><?=$obj->file?></td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>

        <?php } ?>
        <?php
            if (explode('_', $material_id)[0] == 'heating'
            || explode('_', $material_id)[0] == 'price'
            || explode('_', $material_id)[0] == 'window'
            || explode('_', $material_id)[0] == 'lantern'
            ) {
            } else {
        ?>
            <form id="formPage" class="ui form" action="/admin/upload-file/<?=$material_id?>" method="post" enctype="multipart/form-data">
                <div class="field">
                    <label>Загрузить дополнительный файл</label>
                    <input type="hidden" name="material_id" value="<?=$material_id;?>">
                    <input type="text" name="" class="form-control" value="<?=isset($file) && $file->name ? $file->name.'': '';?>" disabled>
                    <input type="file" name="file" class="form-control">
                </div>
                <div class="four wide column">
                    <div>
                        <button type="submit" class="ui primary button"> Сохранить </button>
                    </div>
                </div>
            </form><form action="/admin/remove-file/<?=$material_id?>" method="post">
                <input type="hidden" name="material_id" value="<?=$material_id;?>">
                <input type="hidden" name="id" value="<?=isset($file) && $file->id ? $file->id : '';?>">
                <div style="position: relative">
                    <button type="submit" class="ui red button" style="float: right; margin-top: -36px;">
                        Удалить
                    </button>
                </div>
            </form>
            <?php } ?>
        </div>
    </main>


<?php $this->theme->footer(); ?>
