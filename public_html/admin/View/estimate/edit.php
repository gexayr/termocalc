<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <?php if(isset($note)) {?>
                            <div class="ui floating message">
                                <p><?=$note?></p>
                            </div>
                        <?php } ?>
                        <h2 class="ui header">
                            <?= $estimate->name ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/estimate/update/" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="estimate_id" id="estimate_id" value="<?= $estimate->id ?>"/>

                        <?php
                            if($type == 'price') {
                        ?>
                                <input type="hidden" name="material_id" value="price"/>

                                <div class="field">
                                    <label>Наименование</label>
                                    <input type="text" name="name" class="form-control" id="name" value="<?= $estimate->name;?>" required>
                                </div>
                                <div class="field">
                                    <label>Стоимость</label>
                                    <input type="text" name="price" class="form-control" id="price" value="<?= $estimate->price;?>" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>ТД313 больше</label>
                                    <input type="text" name="more" class="form-control" value="<?= $estimate->more;?>" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>ТД313 меньше или равно  (если ТД313 больше 70000 то тут нужно вводить базовую цену за 1Вт)</label>
                                    <input type="text" name="less" class="form-control" value="<?= $estimate->less;?>" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>Загрузить дополнительный файл</label>
                                    <input type="hidden" name="old_filename" class="form-control" value="<?= $estimate->file;?>">
                                    <input type="text" name="" class="form-control" value="<?= $estimate->file;?>" disabled>
                                    <input type="file" name="file" class="form-control">
                                </div>
                        <?php
                            } else {
                        ?>
                        <div class="field">
                            <label>Материал</label>
                            <select name="material_id" class="disabled">
                                <?php foreach($materials as $item){ ?>
                                    <option <?php if($estimate->material_id == $item->id)echo "selected";?>
                                            value="<?=$item->id?>"><?=$item->name;?></option>
                                    <?php } ?>
                            </select>
                        </div>


                        <div class="field">
                            <label>Тип конструкции </label>
                            <select name="construction">
                                <option value="simple" <?=($estimate->construction == "simple")?"selected":"";?>>Обычный материал:</option>
                                <?php if ($type == 'material'): ?>
                                    <option value="block" <?=($estimate->construction == "block")?"selected":"";?>>Кладка из блоков:</option>
                                    <option value="frame" <?=($estimate->construction == "frame")?"selected":"";?>>Каркасная конструкция:</option>
                                <?php endif; ?>
                            </select>
                        </div>

                        <?php if ($type != 'heating'): ?>
                        <div class="field">
                            <label>Тип</label>
                            <select name="type">
                                <option value="material" <?=($estimate->type == "material")?"selected":"";?>>материал:</option>
                                <option value="work" <?=($estimate->type == "work")?"selected":"";?>>работа:</option>
                            </select>
                        </div>
                        <?php endif; ?>


                        <div class="field">
                            <label>Наименование</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?= $estimate->name;?>" required>
                        </div>

                        <div class="field">
                            <label>Единица</label>
                            <select name="unit" id="unit">
                                <option <?=($estimate->unit == 'куб.м.')?'selected':''?>>куб.м.</option>
                                <option <?=($estimate->unit == 'кг.')?'selected':''?>>кг.</option>
                                <?php if($type == 'heating'): ?>
                                    <option <?=($estimate->unit == 'квт')?'selected':''?>>квт</option>
                                    <option <?=($estimate->unit == 'л')?'selected':''?>>л</option>
                                <?php else: ?>
                                    <option <?=($estimate->unit == 'кв.м.')?'selected':''?>>кв.м.</option>
                                    <option <?=($estimate->unit == 'штука')?'selected':''?>>штука</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Цена единицы</label>
                            <input type="text" name="price" class="form-control" id="price" value="<?= $estimate->price;?>" required oninput="this.value=proverka(this.value)">
                        </div>

                        <div class="field">
                            <label><?=($type == 'heating')?'Теплотворная способность, кВт' : 'Расход единицы'?></label>
                            <input type="text" name="rate" class="form-control" id="rate" value="<?= $estimate->rate;?>" required oninput="this.value=proverka(this.value)">
                        </div>
                        <?php if($type == 'heating'): ?>
                            <div class="field">
                                <label>Коэффициент удорожания за год</label>
                                <input type="text" name="coefficient_rise" class="form-control" value="<?= $estimate->coefficient_rise;?>" required oninput="this.value=proverka(this.value)">
                            </div>
                        <?php endif; ?>

                        <?php
                            }
                        ?>

                </div>
                <div class="twelve wide column">

                <div class="four wide column">
                        <div>
                            <button type="submit" class="ui primary button"> Сохранить </button>
                        </div>
                    </div>
                    </form>
                <form action="/admin/estimate/remove/" method="post">
                        <input type="hidden" name="id" value="<?= $estimate->id ?>">
                        <div style="position: relative">
                            <button type="submit" class="ui red button" style="float: right; margin-top: -36px;">
                                Удалить
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>