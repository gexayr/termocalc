<?php $this->theme->header(); ?>
<?php
//
//
//echo "<pre>";
//print_r($materials);
//echo "</pre>";
?>

    <div id="modaldiv" class="ui modal">

        <i class="close icon"></i>
        <div class="header">
            Список материалов
        </div>
        <div class="scrolling content" id="content">

        </div>
        <div class="actions">
        </div>
    </div>


    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            Смета
                            <a href="/admin/estimate/create/" class="ui primary button right floated item">
                                Создать смету
                            </a>
                        </h2>
                    </div>
                </div>
            </div>
                <br>
                <div class="ui styled fluid accordion">
                    <?php
                    $material_category = [];
                    $material_subcategory = [];
                    foreach ($materials as $item) {
                        $material_category[] = $item->category;
                        $material_subcategory[$item->category][] = $item->COL3;
                    }
                    $material_category = array_unique($material_category);
                    foreach ($material_category as $item){
                        $material_subcategory[$item] = array_unique($material_subcategory[$item]);
                        foreach($material_subcategory[$item] as $key => $link){
                            if(empty($link)){
                                unset($material_subcategory[$item][$key]);
                            }
                        }
                    }

                    foreach ($material_category as $category) {
                    ?>
                    <div class="title">
                        <i class="dropdown icon"></i>
                        <?=$category?>
                    </div>
                    <div class="content">
                        <div class="accordion">
                            <?php
                            $subcategories1 = $material_subcategory[$category];
                            foreach ($subcategories1 as $subcategory1) {
                                ?>
                                <div class="title">
                                    <i class="dropdown icon"></i>
                                    <?= $subcategory1 ?>
                                </div>
                                <div class="content">
                                    <?php
                                    $par_materials1['val'] = $subcategory1;
                                    $materials1_data = Data::getMaterialsByParams1($par_materials1);
                                    ?>
                                    <?php foreach($materials1_data as $materials1_datum){
                                        echo "<h5><a href='/admin/estimate/list_estimate/$materials1_datum->id'>$materials1_datum->name</a></h5>";
                                        } ?>
                                </div>
                            <?php
                            }
                            ?>
                        </div>

                        <?php
                        $par_materials2['val'] = $category;
                        $materials2_data = Data::getMaterialsByParams2($par_materials2);
                        ?>
                        <?php foreach($materials2_data as $materials2_datum){
                            echo "<h5><a href='/admin/estimate/list_estimate/$materials2_datum->id'>$materials2_datum->name</a></h5>";
                        } ?>
                    </div>
                    <?php
                    }
                    ?>

                    <div class="title">
                        <i class="dropdown icon"></i>
                        Стены
                    </div>
                    <div class="content">
                        <h5><a href='/admin/estimate/list_estimate/wall_1'>Стена с навесным фасадом</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/wall_2'>Трёхслойная стена (кладка с утеплением)</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/wall_3'>Стена с "мокрым фасадом" (СФТК)</a></h5>
                    </div>

                    <div class="title">
                        <i class="dropdown icon"></i>
                        Кровля
                    </div>
                    <div class="content">
                        <h5><a href='/admin/estimate/list_estimate/roof_1'>Скатная кровля</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/roof_2'>Плоская кровля</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/roof_3'>Перекрытие на вентилируемом чердаке</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/roof_4'>Перекрытие на закрытом чердаке</a></h5>
                    </div>

                    <div class="title">
                        <i class="dropdown icon"></i>
                        Пол
                    </div>
                    <div class="content">
                        <h5><a href='/admin/estimate/list_estimate/floor_1'>Пол над проездом или над холодным открытым вентилируемым подвалом</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/floor_2'>Пол над неотапливаемым закрытым подвалом со световыми проёмами</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/floor_3'>Пол над неотапливаемыми закрытыми  подвалами без световых проёмов</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/floor_4'>УШП или ПОЛЫ в подвале по грунту</a></h5>
                    </div>
                    <div class="title">
                        <i class="dropdown icon"></i>
                        Окна и Фонари
                    </div>
                    <div class="content">
                        <h5><a href='/admin/estimate/list_estimate/window'>Окна</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/lantern'>Фонари</a></h5>
                    </div>

                    <div class="title">
                        <i class="dropdown icon"></i>
                        Отопление
                    </div>
                    <div class="content">
                        <h5><a href='/admin/estimate/list_estimate/heating_1'>Природный газ</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_2'>Сжиженный газ</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_3'>Дизтопливо</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_4'>Злектричество</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_5'>Дрова</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_6'>Уголь каменный</a></h5>
                        <h5><a href='/admin/estimate/list_estimate/heating_7'>Пеллеты</a></h5>
                        <h5>----------------</h5>
                        <h5><a href='/admin/estimate/list_estimate/price'>Стоимость отопительного оборудования</a></h5>

                    </div>

                </div>

        </div>
    </main>
    <input type="hidden" id="act" value="list">

<?php $this->theme->footer(); ?>