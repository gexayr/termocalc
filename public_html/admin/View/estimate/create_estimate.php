<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <?php if(isset($note)) {?>
                            <div class="ui floating message">
                                <p><?=$note?></p>
                            </div>
                        <?php } ?>
                        <h2 class="ui header">
                            Создать смету
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">

                <div class="ten wide column">
                    <form id="formPage" class="ui form" action="/admin/estimate/add/" method="post" enctype="multipart/form-data">

                        <div class="field">
                            <label>Материал</label>
                            <input type="hidden" name="material_id" value="<?=$material['id'];?>">
                            <input type="text" class="form-control" value="<?=$material['name'];?>" disabled>
                        </div>

                        <div class="field" <?=($type == 'price')?'style="display: none"':''?>>
                            <label>Тип конструкции </label>
                            <select name="construction">
                                <option value="simple">Обычный материал:</option>
                                <?php if ($type == 'material'): ?>
                                    <option value="block">Кладка из блоков:</option>
                                    <option value="frame">Каркасная конструкция:</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Наименование </label>
                            <input type="text" name="name" class="form-control" id="name" value="" required>
                        </div>
                        <?php
                            if($type == 'price') {
                        ?>
                                <div class="field">
                                    <label>Стоимость</label>
                                    <input type="text" name="price" class="form-control" id="price" value="" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>ТД313 больше</label>
                                    <input type="text" name="more" class="form-control" value="" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>ТД313 меньше или равно (если ТД313 больше 70000 то тут нужно вводить базовую цену за 1Вт)</label>
                                    <input type="text" name="less" class="form-control" value="" required oninput="this.value=proverka(this.value)">
                                </div>
                                <div class="field">
                                    <label>Загрузить дополнительный файл</label>
                                    <input type="file" name="file" class="form-control">
                                </div>
                        <?php
                            } else {
                        ?>

                        <?php if ($type != 'heating'): ?>
                        <div class="field">
                            <label>Тип</label>
                            <select name="type">
                                    <option value="material">материал:</option>
                                    <option value="work">работа:</option>
                            </select>
                        </div>
                        <?php endif; ?>
                        <div class="field">
                            <label>Единица</label>
                            <select name="unit" id="unit">
                                <option>куб.м.</option>
                                <option>кг.</option>
                                <?php if($type == 'heating'): ?>
                                    <option>квт</option>
                                    <option>л</option>
                                <?php else: ?>
                                    <option>кв.м.</option>
                                    <option>штука</option>
                                <?php endif; ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Цена единицы</label>
                            <input type="text" name="price" class="form-control" id="price" value="" required oninput="this.value=proverka(this.value)">
                        </div>
                        <div class="field">
                            <label><?=($type == 'heating')?'Теплотворная способность, кВт' : 'Расход единицы'?></label>
                            <input type="text" name="rate" class="form-control" id="rate" value="" required oninput="this.value=proverka(this.value)">
                        </div>
                        <?php if($type == 'heating'): ?>
                        <div class="field">
                            <label>Коэффициент удорожания за год</label>
                            <input type="text" name="coefficient_rise" class="form-control" required oninput="this.value=proverka(this.value)">
                        </div>
                        <?php endif; ?>
                        <?php
                            }
                        ?>
                        <button type="submit" class="ui primary button"> Добавить </button>
                    </form>
                </div>
            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>

