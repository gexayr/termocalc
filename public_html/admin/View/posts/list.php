<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Посты
                        <a href="/admin/posts/create/" class="ui blue button right floated item">
                            Создать Пост
                        </a>
                        <a href="/admin/post_categories/" class="ui inverted blue button right floated item">
                            Категории
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Название</th>
                    <th>Категории</th>
                    <th>Дата</th>
                    <th>Удалить </th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($posts as $post): ?>
                    <tr>
                        <th scope="row">
                            <?= $post->id ?>
                        </th>
                        <td>
                            <a href="/admin/posts/edit/<?= $post->id ?>">
                                <?= $post->title ?>
                            </a>
                        </td>
                        <td>
                            <?= $post->category_id ?>
                        </td>
                        <td>
                            <?= $post->date ?>
                        </td>

                        <td>
                            <a href="/admin/posts/remove/<?= $post->id ?>" class="ui red button" onclick="return post.confirmDelete('пост')">
                                Удалить
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?php $this->theme->footer(); ?>