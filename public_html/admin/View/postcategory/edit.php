<?php $this->theme->header(); ?>
<?php
//dd($category);
?>
    <main>
        <div class="ui container">
            <div class="ui grid">
                <div class="sixteen wide column">
                    <div class="col page-title">
                        <h2 class="ui header">
                            <?= $category->name ?>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="twelve wide column">
                    <form id="formPage" class="ui form" action="/admin/post_category/update/" method="post">
                        <input type="hidden" name="category_id" id="category_id" value="<?= $category->id ?>"/>
                        <div class="field">
                            <label>Название</label>
                            <input type="text" name="name" class="form-control" id="name" value="<?= htmlspecialchars($category->name);?>">
                        </div>
                        <div class="field">
                            <label>Родитель</label>
                            <select name="parent" id="">
                                <option value="-"></option>
                                <?php foreach($categories as $item): ?>
                                    <?php
                                if($item->id != $category->id){
                                    ?>
                                    <option value="<?= $item->id ?>" <?php if($category->parent == $item->id)echo 'selected'?>><?= $item->name ?></option>
                                <?php } endforeach; ?>
                            </select>
                        </div>
                        <div class="field">
                            <label>Статус </label>
                            <select name="status" id="">
                                <option value="1" <?php if($category->status == 1)echo 'selected'?>>on</option>
                                <option value="-" <?php if($category->status != 1)echo 'selected'?>>off</option>
                            </select>
                        </div>
                    <div class="four wide column">
                            <button type="submit" class="ui primary button"> Обовит </button>

                        <a href="/admin/post_category/remove/<?= $category->id ?>" class="ui red button right floated item" onclick="return post.confirmDelete('категорию ')">
                            Удалить
                        </a>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>