<?php $this->theme->header(); ?>

    <main>
        <div class="ui container">
            <div class="row">
                <div class="col page-title">
                    <h2 class="ui header">
                        Категории Поста
                        <a href="/admin/post_category/create/" class="ui inverted blue button right floated item">
                            Создать Категории Поста
                        </a>
                    </h2>
                </div>
            </div>

            <table class="ui very basic table">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Родитель</th>
                    <th>Название</th>
                    <th>Название латинскими буквами</th>
                    <th>Статус</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($categories as $category): ?>
                    <tr>
                        <td scope="row">
                            <?= $category->id ?>
                        </td>
                        <td>
                                <?= $category->parent ?>
                        </td>
                        <td>
                            <a href="/admin/post_category/edit/<?= $category->id ?>">
                                <?= $category->name ?>
                            </a>
                        </td>
                        <td>
                            <?= $category->lat_name ?>
                        </td>
                        <td>
                            <?= $category->status ?>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </main>

<?php $this->theme->footer(); ?>