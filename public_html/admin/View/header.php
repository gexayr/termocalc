<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Админ-панель</title>

    <link href="/admin/Assets/semantic/semantic.min.css" rel="stylesheet">
    <link rel="stylesheet" href="/admin/Assets/semantic/components/dropdown.min.css">
    <link rel="stylesheet" href="/admin/Assets/semantic/components/transition.min.css">
    <link rel="stylesheet" href="/admin/Assets/semantic/components/icon.min.css">
    <link rel="stylesheet" href="/admin/Assets/semantic/components/segment.min.css">
    <link rel="stylesheet" href="/admin/Assets/semantic/components/sidebar.min.css">


    <!-- Custom styles for this template -->
    <link href="/admin/Assets/css/dashboard.css?v=?v=<?=time()?>" rel="stylesheet">

    <!-- simplelineicons for this template -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/simple-line-icons/2.4.1/css/simple-line-icons.css">

    <!-- Redactor CSS -->
    <link rel="stylesheet" href="/admin/Assets/js/plugins/redactor/redactor.css">
</head>
<body>
<header>
    <div class="ui borderless main menu top-header">
        <div class="ui container">
            <div href="/admin/" class="header item logo-item">
            </div>
                <a class="item" href="/admin/">
                    <i class="icon-speedometer"></i>
                    Главная
                </a>
                <a class="item" href="/admin/categories/">
                    <i class="icon-layers"></i>
                    Категории Материалов
                </a>
                <a class="item" href="/admin/materials/">
                    <i class="icon-doc"></i>
                    Материалы
                </a>
                <a class="item" href="/admin/posts/">
                    <i class="icon-book-open"></i>
                    Блог
                </a>
                <a class="item" href="/admin/helps/">
                    <i class="icon-notebook"></i>
                    Раздел Помощь
                </a>
                <a class="item" href="/admin/self_materials/">
                    <i class="icon-login"></i>
                    Собственные материалы
                </a>
                <a class="item" href="/admin/estimate/">
                    <i class="icon-speech"></i>
                    Смета
                </a>
        </div>
    </div>
</header>