<?php $this->theme->header(); ?>

<!--=================================-->
<?php
$videoLink = \Engine\Core\Template\Setting::get('preview_video_link');
$generalData = \Engine\Core\Template\Setting::get('general_data');
$generalDataName = \Engine\Core\Template\Setting::getName('general_data');
//dd($generalDataName[1]);
?>

<!--=================================-->
    <main>
        <div class="container">
            <br><br><br>
            <div style="text-align:center;">
                <h1><i class="icon-heart icons"></i> Добро пожаловать в termocalc</h1>
                <i class="icon-social-youtube icons"></i>

            <div class="ui container">
                <div class="settings-block">
                    <div class="preview-video-block ui grid">
                        <div class="sixteen wide column">
                            <form id="formPage" class="ui form" action="/admin/preview_video_link" method="post">
                                <div class="field">
                                    <label>Ссылка ознокомительного видео</label>
                                    <span>тут вставляется весь iframe. Пр. <br>< iframe id="previewVideo" width="80%" height="315" src="//www.youtube.com/embed/sMzgonIEq7A" frameborder="0" allowfullscreen>< /iframe></span>
                                    <input type="text" name="preview_video_link" class="form-control" value="<?= htmlspecialchars($videoLink)?>" required>
                                </div>
                                <div class="four wide column">
                                    <button type="submit" class="ui primary button"> Обновить </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="preview-video-block ui grid">
                        <div class="sixteen wide column">
                            <form id="formPage" class="ui form" action="/admin/general-data" method="post">
                                <div class="field" id="general-fields">
                                    <label>Сохранённые расчёты, видимые для всех</label>
                                    <?php
                                        if (empty($generalData)) {
                                    ?>
                                    <div class="general_data_block">
                                        <label for="">расчёт</label>
                                        <input type="text" name="general_data[name][]" class="form-control" value="" placeholder="название расчёта" required>
                                        <input type="text" name="general_data[link][]" class="form-control" value="" placeholder="ссылка расчёта" required>
                                        <input type="checkbox" class="form-control" name="show">
                                    </div>
                                    <?php
                                        } else {
                                            $arrData = json_decode($generalData);
                                            $arrDataLink = $arrData->link;
                                            $arrDatashow = (array) $arrData->show;
                                            foreach ($arrDataLink as $key=>$value) {
                                            ?>

                                    <div class="general_data_block">
                                        <label for="">расчёт</label>
                                        <input type="text" name="general_data[name][]" class="form-control" value="<?=$arrData->name[$key]?>" placeholder="название расчёта" required>
                                        <input type="text" name="general_data[link][]" class="form-control" placeholder="ссылка расчёта" value="<?=$value?>" required>
                                        <div class="checkbox-block"><input type="checkbox" class="form-control" name="general_data[show][<?=$key?>]"
                                            <?=(!empty($arrDatashow[$key])) ? 'checked' : ''?>><label for="">неактивен для незарегистрированных </label></div>
                                    </div>
                                    <?php
                                            }
                                        }
                                    ?>
                                    </div>
                                <div class="four wide column">
                                    <button type="submit" class="ui primary button"> Обновить </button>
                                    &nbsp;<a href="javascript: void(0)" id="add-general-field">+ Добавить</a>
                                    &nbsp;<a href="javascript: void(0)" id="remove-general-field">֊ Удалить</a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>

            </div>
        </div>
    </main>

<?php $this->theme->footer(); ?>