var setting = {
    ajaxMethod: 'POST',

    update: function(element) {
        var formData = $('#settingForm').serialize();
        var button = $(element);

        $.ajax({
            url: '/admin/settings/update/',
            type: this.ajaxMethod,
            data: formData,
            beforeSend: function(){
                button.addClass('loading');
            },
            success: function(result){
                console.log(result);
                button.removeClass('loading');
            }
        });
    }
};

removeLink();
function removeLink() {
    if ($('#general-fields').find('.general_data_block').length == 1) {
        $('#remove-general-field').css('display', 'none')
    } else {
        $('#remove-general-field').css('display', 'inline-grid')
    }
}

$('#add-general-field').on('click', function() {
    $('#general-fields').append(
    '<div class="general_data_block">'
     + '<label for="">расчёт</label>'
     +   '<input type="text" name="general_data[name][]" placeholder="название расчёта" class="form-control" value="" required="">'
     +   '<input type="text" name="general_data[link][]" placeholder="ссылка расчёта" class="form-control" value="" required="">'
     +   '<div class="checkbox-block"><input type="checkbox" class="form-control" name="general_data[show][]"><label for="">неактивен для незарегистрированных </label></div>'
    + '</div>'
        );
    removeLink();
})

$('#remove-general-field').on('click', function() {
    let blockCount = $('#general-fields').find('.general_data_block').length;
    $('#general-fields').find('.general_data_block')[blockCount-1].remove();
    removeLink();
})