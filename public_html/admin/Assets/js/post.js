var post = {
    ajaxMethod: 'POST',

    add: function() {
        if($('#formTitle').val() == ''){
            alert('не ввели имя поста');
            return false;
        }
        if($('.redactor-editor').html() == '<p>​</p>'){
            alert('Пост не может быть пустым ');
            return false;
        }

        if($('#post_subcategory').val() == ''){
            var categoryId= $('#post_category').val();
        }else{
            var categoryId= $('#post_subcategory').val();
        }

        // console.log($('#post_subcategory').val());
        // console.log(categoryId);
        //
        //
        // return false;
        var formData = new FormData();

        formData.append('title', $('#formTitle').val());
        formData.append('content', $('.redactor-editor').html());

        formData.append('category_id', categoryId);

        $.ajax({
            url: '/admin/post/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                console.log(result);
                window.location = '/admin/posts/';
                // window.location = '/admin/posts/edit/' + result;
            }
        });
    },

    addSimple: function() {
        if($('#formTitle').val() == ''){
            alert('не ввели имя поста');
            return false;
        }
        if($('.redactor-editor').html() == '<p>​</p>'){
            alert('Пост не может быть пустым ');
            return false;
        }

        if($('#post_subcategory').val() == ''){
            var categoryId= $('#post_category').val();
        }else{
            var categoryId= $('#post_subcategory').val();
        }

        // console.log($('#post_subcategory').val());
        // console.log(categoryId);
        //
        //
        // return false;
        var formData = new FormData();

        formData.append('title', $('#formTitle').val());
        // formData.append('content', $('.cke_wysiwyg_frame .cke_editable').html());
        formData.append('content', $('.cke_wysiwyg_frame').contents().find('.cke_editable').html());

        formData.append('category_id', categoryId);

        // console.log($('.cke_wysiwyg_frame').contents().find('.cke_editable').html());
        // return false;
        $.ajax({
            url: '/admin/post/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                console.log(result);
                window.location = '/admin/posts/';
                // window.location = '/admin/posts/edit/' + result;
            }
        });
    },

    update: function() {
        if($('#formTitle').val() == ''){
            alert('не ввели имя поста');
            return false;
        }
        if($('.redactor-editor').html() == '<p>​</p>'){
            alert('Пост не может быть пустым ');
            return false;
        }

        if($('#post_subcategory').val() == ''){
            var categoryId= $('#post_category').val();
        }else{
            var categoryId= $('#post_subcategory').val();
        }

        var formData = new FormData();

        formData.append('post_id', $('#formPostId').val());
        formData.append('title', $('#formTitle').val());
        // formData.append('content', $('.redactor-editor').html());
        formData.append('content', $('.cke_wysiwyg_frame').contents().find('.cke_editable').html());

        formData.append('category_id', categoryId);


        $.ajax({
            url: '/admin/post/update/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');

            },
            success: function(result){
                console.log(result);
                $('#waiting').css('display', 'none');

            }
        });
    },


    changePostCategory: function() {
        // alert('123')
        var formData = new FormData();
        var id = $('#post_category').val();

        // console.log(id);

        formData.append('id', id);

        $.ajax({
            url: '/admin/post/get_categories/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // $('#waiting').css('display', 'block');
            },
            success: function(result){
                console.log(result);
                $('#post_subcategory').html(result);
            }
        });
    },

    confirmDelete: function (element) {
        if (!confirm("Вы уверены что хотите удалить " + element + " ?")) {
            return false
        }
    },


// #####################################################
// //////////////////// help section ///////////////////
// #####################################################


    addHelp: function() {
        if($('#formTitle').val() == ''){
            alert('не ввели имя поста');
            return false;
        }
        if($('.redactor-editor').html() == '<p>​</p>'){
            alert('Пост не может быть пустым ');
            return false;
        }

        var formData = new FormData();

        formData.append('title', $('#formTitle').val());
        // formData.append('content', $('.redactor-editor').html());
        formData.append('content', $('.cke_wysiwyg_frame').contents().find('.cke_editable').html());


        $.ajax({
            url: '/admin/help/add/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');
            },
            success: function(result){
                console.log(result);
                window.location = '/admin/helps/';
                // window.location = '/admin/posts/edit/' + result;
            }
        });
    },

    updateHelp: function() {
        if($('#formTitle').val() == ''){
            alert('не ввели имя поста');
            return false;
        }
        if($('.redactor-editor').html() == '<p>​</p>'){
            alert('Пост не может быть пустым ');
            return false;
        }


        var formData = new FormData();

        formData.append('post_id', $('#formPostId').val());
        formData.append('title', $('#formTitle').val());
        // formData.append('content', $('.redactor-editor').html());
        formData.append('content', $('.cke_wysiwyg_frame').contents().find('.cke_editable').html());


        $.ajax({
            url: '/admin/help/update/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                $('#waiting').css('display', 'block');

            },
            success: function(result){
                // console.log(result);
                $('#waiting').css('display', 'none');

            }
        });
    },
};