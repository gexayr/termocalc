var material = {
    ajaxMethod: 'POST',
    change: function(sub) {
        id = $('#COL3_sub').val();
        if(sub === 'sub1') {
            $("#COL4_sub").html('');
            id = $('#material_category').val();
        }
        // console.log(id);
        if(id == ''){
            $("#COL4_sub").html('');
            return false;
        }
        let formData = new FormData();
        formData.append('id', id);

        $.ajax({
            url: '/admin/materials/get-subcategories-by-params/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                // console.log(result);
                if(sub === 'sub1'){
                    $("#COL3_sub").html(result);
                }else if(sub === 'sub2'){
                    $("#COL4_sub").html(result);
                }
                // window.location = '/admin/materials/edit/' + result;
            }
        });
    },

    update: function() {

        // var formData = new FormData();
        //
        // formData.append('material_id', $('#material_id').val());
        // formData.append('category', $('#category').val());
        // formData.append('name', $('#name').val());
        // formData.append('COL3', $('#COL3').val());
        // formData.append('COL4', $('#COL4').val());
        // formData.append('COL6', $('#COL6').val());
        // formData.append('COL7', $('#COL7').val());
        // formData.append('COL8', $('#COL8').val());
        // formData.append('COL9', $('#COL9').val());
        // formData.append('COL10', $('#COL10').val());
        // formData.append('COL11', $('#COL11').val());
        // formData.append('COL12', $('#COL12').val());
        // formData.append('COL13', $('#COL13').val());
        // formData.append('COL14', $('#COL14').val());
        // formData.append('COL15', $('#COL15').val());
        // formData.append('COL16', $('#COL16').val());
        // formData.append('COL17', $('#COL17').val());
        // formData.append('COL18', $('#COL18').val());
        // formData.append('COL19', $('#COL19').val());
        // formData.append('COL20', $('#COL20').val());
        // formData.append('COL21', $('#COL21').val());
        // formData.append('COL22', $('#COL22').val());
        // formData.append('COL23', $('#COL23').val());
        // formData.append('COL24', $('#COL24').val());
        // formData.append('COL25', $('#COL25').val());
        // formData.append('COL26', $('#COL26').val());
        // formData.append('COL27', $('#COL27').val());
        // formData.append('COL28', $('#COL28').val());
        // formData.append('COL29', $('#COL29').val());
        // formData.append('COL30', $('#COL30').val());
        // formData.append('COL31', $('#COL31').val());
        // formData.append('COL32', $('#COL32').val());
        // formData.append('COL33', $('#COL33').val());
        // formData.append('COL34', $('#COL34').val());
        // formData.append('COL35', $('#COL35').val());
        // formData.append('COL36', $('#COL36').val());
        // formData.append('COL37', $('#COL37').val());
        //
        // $.ajax({
        //     url: '/admin/material/update/',
        //     type: this.ajaxMethod,
        //     data: formData,
        //     cache: false,
        //     processData: false,
        //     contentType: false,
        //     beforeSend: function(){
        //
        //     },
        //     success: function(result){
        //         console.log(result);
        //     }
        // });
    }
};
