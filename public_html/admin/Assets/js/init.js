$('#redactor').redactor({
    imageUpload: '/admin/uploadImage',
    imageManagerJson: '/admin/Assets/js/plugins/redactor/core/uploadImage/images.json',
    // fileUpload: '/admin/Assets/js/plugins/redactor/core/uploadFile/uploadFile.php',
    // fileManagerJson: '/admin/Assets/js/plugins/redactor/core/files.json',
    plugins: ['table', 'video', 'source','imagemanager', 'filemanager'],
    imagePosition: true,
    imageResizable: true

});
$(document)
    .ready(function() {
        // show dropdown on hover
        $('.ui.dropdown').dropdown({
            on: 'hover'
        });

        $('.btn-create-menu').on('click', function(){
            $('.mini.modal')
                .modal('show')
            ;
        });
    })
;

$(function() {
    var group = $("ol.edit-menu").sortable({
        group: 'edit-menu',
        handle: 'i.icon-cursor-move',
        onDragStart: function ($item, container, _super) {
            // Duplicate items of the no drop area
            if(!container.options.drop)
                $item.clone().insertAfter($item);
            _super($item, container);
        },
        onDrop: function ($item, container, _super) {
            var data = group.sortable("serialize").get();
            var jsonString = JSON.stringify(data, null, ' ');
            var formData = new FormData();

            console.log(data);

            formData.append('data', jsonString);
            formData.append('menu_id', $('#sortMenuId').val());

            $.ajax({
                url: '/admin/setting/ajaxMenuSortItems/',
                type: 'POST',
                data: formData,
                processData: false,
                contentType: false,
                beforeSend: function(){

                },
                success: function(result){

                }
            });

            _super($item, container);
        }
    });
});



$('.ui.accordion')
    .accordion()
;

// $(document).ready(function(){
    $('.t_action').click(function(){
        // $('#modaldiv').modal('show');

            $('.modal')
                .modal('show')
            ;

    var category = $(this).text().trim();
    var act = $("#act").val();
    // console.log(category);
        var formData = new FormData();
        formData.append('col', "category");
        formData.append('val', category);
        formData.append('operator', "=");
        formData.append('action', act);
        $.ajax({
            url: '/admin/materials/get-materials-by-params/',
            type: 'POST',
            data: formData,
            processData: false,
            contentType: false,
            beforeSend: function(){

            },
            success: function(result){
                // console.log(result);
                $("#content").html(result);
            }
        });

    });
// });


function proverka(val) {
    var reg = [/^\D+/, /[^.,\d]+/g, /[\.,]+/, /(\d+\.\d{2}).*$/],
        ch = val.replace(reg[0], '').replace(reg[1], '').replace(reg[2], '.').replace(reg[3], '$1');
    return ch;
};