<?php
require_once __DIR__ . '/../vendor/autoload.php';
require_once __DIR__ . '/Function.php';

class_alias('Engine\\Core\\Template\\Asset', 'Asset');
class_alias('Engine\\Core\\Template\\Theme', 'Theme');
class_alias('Engine\\Core\\Template\\Setting', 'Setting');
class_alias('Engine\\Core\\Template\\User', 'User');
class_alias('Engine\\Core\\Customize\\Customize', 'Customize');
class_alias('Engine\\Core\\Template\\Data', 'Data');


use Engine\Cms;
use Engine\DI\DI;

try{
    // Dependency injection
    $di = new DI();

    $services = require __DIR__ . '/Config/Service.php';
    // Init services
    foreach ($services as $service) {
        $provider = new $service($di);
        $provider->init();
    }

    // Init models
    $di->set('model', []);

    $cms = new Cms($di);
    $cms->run();

}catch (\ErrorException $e) {
    echo $e->getMessage();
}

function debug($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function debug_die($array){
    echo "<pre>";
    print_r($array);
    exit;
}


function dd($array){
    var_dump($array);
    exit;
}
