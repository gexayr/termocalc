<?php

namespace Engine\Core\Template;

use Engine\DI\DI;
use Cms\Model\Region\RegionRepository;
use Cms\Model\Room\RoomRepository;
use Cms\Model\City\CityRepository;
use Cms\Model\Building\BuildingRepository;
use Cms\Model\Material\MaterialRepository;
use Admin\Model\Category\CategoryRepository;
use Cms\Model\Coefficient\CoefficientRepository;
use Cms\Model\Layer\LayerRepository;
use Engine\Helper\Cookie;


/**
 * Class Menu
 * @package Engine\Core\Template
 */
class Data
{
    /**
     * @var DI
     */
    protected static $di;

    /**
     * @var $regionRepository
     */
    protected static $regionRepository;

    /**
     * @var $roomRepository
     */
    protected static $roomRepository;

    protected static $cityRepository;

    protected static $buildingRepository;

    protected static $materialRepository;
    protected static $categoryRepository;

    protected static $coefficientRepository;

    protected static $layerRepository;


    /**
     * Menu constructor.
     * @param $di
     */
    public function __construct($di)
    {
        self::$di = $di;
        self::$regionRepository = new RegionRepository(self::$di);
        self::$roomRepository = new RoomRepository(self::$di);
        self::$cityRepository = new CityRepository(self::$di);
        self::$buildingRepository = new BuildingRepository(self::$di);
        self::$materialRepository = new MaterialRepository(self::$di);
        self::$categoryRepository = new CategoryRepository(self::$di);
        self::$coefficientRepository = new CoefficientRepository(self::$di);
        self::$layerRepository = new LayerRepository(self::$di);

    }

//categories
    /**
     * @return object
     */
    public static function getCategories()
    {
        return self::$categoryRepository->getCategories();
    }

    public static function getCategory($id)
    {
        return self::$categoryRepository->getCategoryData($id);
    }

    public static function getCategoriesByParams($params)
    {
        return self::$categoryRepository->getCategoriesByParams($params);
    }

    public static function getCategoriesLine($category, $line = "")
    {
        if($category->parent != 0){
            $cat = self::getCategory($category->parent);
            $line .= "_";
            $line .= self::getCategoriesLine($cat, $line);
        }
        return $line;
    }

//    regions

    /**
     * @return object
     */
    public static function getRegions()
    {
        return self::$regionRepository->getList();
    }

    /**
     * @param int $regionId
     * @return mixed
     */
    public static function getRegion($regionId)
    {
        return self::$regionRepository->getRegion($regionId);
    }
//end regions
//buildings
    public static function getBuildings()
    {
        return self::$buildingRepository->getList();
    }

    public static function getBuildingData($id)
    {
        return self::$buildingRepository->getBuildingData($id);
    }

    public static function getBuilding($id)
    {
        return self::$buildingRepository->getBuilding($id);
    }

//    end buildings

//city
    public static function getCitiesByReg($id)
    {
        return self::$cityRepository->getCitiesByReg($id);
    }

    public static function getCity($id)
    {
        return self::$cityRepository->getCity($id);
    }
//end city

//rooms
    public static function getRooms()
    {
        return self::$roomRepository->getList();
    }


    public static function getRoom($id)
    {
        return self::$roomRepository->getRoom($id);
    }
//end rooms

//materials

    public static function getMaterials()
    {
        return self::$materialRepository->getList();
    }


    public static function getMaterial($id)
    {
        return self::$materialRepository->getMaterial($id);
    }

    public static function getMaterialCategory()
    {
        return self::$materialRepository->getMaterialCategory();
    }

    public static function getMaterialCategoryByParent($id)
    {
        return self::$materialRepository->getMaterialCategoryByParent($id);
    }

    public static function getMaterialByParent($id)
    {
        return self::$materialRepository->getMaterialByParent($id);
    }

    public static function getMaterialsByParams($params)
    {
        return self::$materialRepository->getMaterialsByParams($params);
    }

    public static function getMaterialsByParams1($params)
    {
        return self::$materialRepository->getMaterialsByParams1($params);
    }

    public static function getMaterialsByParams2($params)
    {
        return self::$materialRepository->getMaterialsByParams2($params);
    }

    public static function getMaterialsByParamsNull($params)
    {
        return self::$materialRepository->getMaterialsByParamsNull($params);
    }
    public static function getMaterialsByParamsNullHomo($params)
    {
        return self::$materialRepository->getMaterialsByParamsNullHomo($params);
    }

    public static function getStaticThickness($params)
    {
        $materials = self::$materialRepository->getStaticThickness($params);
        return $materials;
    }

    public static function updateMaterial($params)
    {
        return self::$materialRepository->updateMaterial($params);
    }
//end materials

//coefficient

    public static function getCoefficientList()
    {
        return self::$coefficientRepository->getList();
    }

    public static function getCoefficient($id)
    {
        return self::$coefficientRepository->getCoefficient($id);
    }

//end coefficient


    public static function getDewData($params)
    {
        return self::$coefficientRepository->getDewData($params);
    }

//    ======for admin====

    public static function remMat($id)
    {
//        echo $id;
        return self::$materialRepository->remove($id);
    }

//    ==========
    public static function getCitiesBuildingsParams($params)
    {

        $city_building = '';

        $city = Data::getCity($params['city']);

        $building = Data::getBuilding($params['building']);
        $building_data = Data::getBuildingData($params['building']);

        $rooms = Data::getRooms();

        $td01 = '';
        $td02 = '';
        $td03 = '';
        $td04 = '';
        $td05 = '';
        $td06 = '';
        $td07 = '';
        $td08 = '';
        $td09 = '';
        $td10 = '';
        $td11 = '';
        $td12 = '';
        $td13 = '';
        $td14 = '';
        $td15 = '';
        $td16 = '';
        $td17 = '';
        $td18 = '';
        $td19 = '';
        $td20 = '';
        $td21 = '';
        $td22 = '';
        $td23 = '';
        $td24 = '';
        $td25 = '';
        $td26 = '';
        $td27 = '';
        $td28 = '';
        $td29 = '';
        $td30 = '';
        $td31 = '';
        $td32 = '';
        $td33 = '';
        $td34 = '';
        $td35 = '';
        $td36 = '';
        $td37 = '';
        $td38 = '';
        $td39 = '';
        $td40 = '';
        $td41 = '';
        $td42 = '';
        $td43 = '';
        $td44 = '';
        $td45 = '';
        $td46 = '';
        $td47 = '';
        $td48 = '';
        $td49 = '';
        $td50 = '';
        $td51 = '';
        $td52 = '';
        $td53 = '';
        $td54 = '';
        $td55 = '';
        $td56 = '';
        $td57 = '';
        $td58 = '';
        $td59 = '';
        $td60 = '';
        $td61 = '';
        
        $td70 = '';
        $td71 = '';
        $td72 = '';
        $td73 = '';
        $td74 = '';
        $td75 = '';
        $td76 = '';
        $td77 = '';
        $td78 = '';
        $td79 = '';
        $td70 = '';
        $td81 = '';
        $td82 = '';
        $td83 = '';
        $td84 = '';
        $td85 = '';
        $td86 = '';
        $td87 = '';
        $td88 = '';
        $td89 = '';
        $td90 = '';

        $td82_1 = '';
        $td83_1 = '';
        $td84_1 = '';
        $td85_1 = '';
        $td86_1 = '';
        $td87_1 = '';
        $td88_1 = '';

        $td82_2 = '';
        $td83_2 = '';
        $td84_2 = '';
        $td85_2 = '';
        $td86_2 = '';
        $td87_2 = '';
        $td88_2 = '';

        if (isset($rooms)) {
            $td06 = $rooms[0]->stp15;
            $td07 = $rooms[0]->stp16;
            $td10 = $rooms[1]->stp15;
            $td11 = $rooms[1]->stp16;
            $td14 = $rooms[2]->stp15;
            $td15 = $rooms[2]->stp16;
            $td18 = $rooms[3]->stp15;
            $td19 = $rooms[3]->stp16;
            $td22 = $rooms[4]->stp15;
            $td23 = $rooms[4]->stp16;
            $td26 = $rooms[5]->stp15;
            $td27 = $rooms[5]->stp16;
            $td30 = $rooms[6]->stp15;
            $td31 = $rooms[6]->stp16;
            $td34 = $rooms[7]->stp15;
            $td35 = $rooms[7]->stp16;
        }

        if (isset($city)) {
            $td01 = $city->name;
            $td03 = $city->st024;
            $td04 = $city->st004;
            if (
                isset($city->st008) &&
                isset($building->id) &&
                isset($rooms[0]->stp02)
            ) {
                $td05 = $rooms[0]->stp02;
                if ($city->st008 <= -31) {
                    $rooms[0]->stp02 = 21;
                    $rooms[0]->stp03 = 23;
                    $rooms[0]->stp04 = 20;
                    $rooms[0]->stp06 = 20;
                    $rooms[0]->stp07 = 22;
                    $rooms[0]->stp08 = 19;
                } else {
                    $rooms[0]->stp02 = 20;
                    $rooms[0]->stp03 = 22;
                    $rooms[0]->stp04 = 18;
                    $rooms[0]->stp06 = 19;
                    $rooms[0]->stp07 = 20;
                    $rooms[0]->stp08 = 17;
                }
                if ($building != null) {
                    //                    echo "<pre>"''
                    //                    print_r($rooms);exit;
                    if ($building->id < 3) {
                        if ($rooms[0]->stp02 < 20) {
                            $td05 = 20;
                        } elseif ($rooms[0]->stp02 > 22) {
                            $td05 = 22;
                        } else {
                            $td05 = $rooms[0]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[0]->stp02 < 16) {
                            $td05 = 16;
                        } elseif ($rooms[0]->stp02 > 21) {
                            $td05 = 21;
                        } else {
                            $td05 = $rooms[0]->stp02;
                        }
                    }
                }

            }

            if (isset($city->st004) && $td07 != '') {
                $room_data = $td07;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td08 = 'b';
                    } else {
                        $td08 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td08 = 'a';
                    } else {
                        $td08 = 'b';

                    }
                } else {
                    $td08 = 'b';
                }
            }


            if (
                isset($building->id) &&
                isset($rooms[1]->stp02)
            ) {
                $td09 = $rooms[1]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {

                        if ($rooms[1]->stp02 < 20) {
                            $td09 = 20;
                        } elseif ($rooms[1]->stp02 > 22) {
                            $td09 = 22;
                        } else {
                            $td09 = $rooms[1]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[1]->stp02 < 16) {
                            $td09 = 16;
                        } elseif ($rooms[1]->stp02 > 21) {
                            $td09 = 21;
                        } else {
                            $td09 = $rooms[1]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td11 != '') {
                $room_data = $td11;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td12 = 'b';
                    } else {
                        $td12 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td12 = 'a';
                    } else {
                        $td12 = 'b';

                    }
                } else {
                    $td12 = 'b';
                }
            }


            if (
                isset($building->id) &&
                isset($rooms[2]->stp02)
            ) {
                $td13 = $rooms[2]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[2]->stp02 < 20) {
                            $td13 = 20;
                        } elseif ($rooms[2]->stp02 > 22) {
                            $td13 = 22;
                        } else {
                            $td13 = $rooms[2]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[2]->stp02 < 16) {
                            $td13 = 16;
                        } elseif ($rooms[2]->stp02 > 21) {
                            $td13 = 21;
                        } else {
                            $td13 = $rooms[2]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td15 != '') {
                $room_data = $td15;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td16 = 'b';
                    } else {
                        $td16 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td16 = 'a';
                    } else {
                        $td16 = 'b';

                    }
                } else {
                    $td16 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[3]->stp02)
            ) {
                $td17 = $rooms[3]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[3]->stp02 < 20) {
                            $td17 = 20;
                        } elseif ($rooms[3]->stp02 > 22) {
                            $td17 = 22;
                        } else {
                            $td17 = $rooms[3]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[3]->stp02 < 16) {
                            $td17 = 16;
                        } elseif ($rooms[3]->stp02 > 21) {
                            $td17 = 21;
                        } else {
                            $td17 = $rooms[3]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td19 != '') {
                $room_data = $td19;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td20 = 'b';
                    } else {
                        $td20 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td20 = 'a';
                    } else {
                        $td20 = 'b';

                    }
                } else {
                    $td20 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[4]->stp02)
            ) {
                $td21 = $rooms[4]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[4]->stp02 < 20) {
                            $td21 = 20;
                        } elseif ($rooms[4]->stp02 > 22) {
                            $td21 = 22;
                        } else {
                            $td21 = $rooms[4]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[4]->stp02 < 16) {
                            $td21 = 16;
                        } elseif ($rooms[4]->stp02 > 21) {
                            $td21 = 21;
                        } else {
                            $td21 = $rooms[4]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td23 != '') {
                $room_data = $td23;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td24 = 'b';
                    } else {
                        $td24 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td24 = 'a';
                    } else {
                        $td24 = 'b';

                    }
                } else {
                    $td24 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[5]->stp02)
            ) {
                $td25 = $rooms[5]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[5]->stp02 < 20) {
                            $td25 = 20;
                        } elseif ($rooms[5]->stp02 > 22) {
                            $td25 = 22;
                        } else {
                            $td25 = $rooms[5]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[5]->stp02 < 16) {
                            $td25 = 16;
                        } elseif ($rooms[5]->stp02 > 21) {
                            $td25 = 21;
                        } else {
                            $td25 = $rooms[5]->stp02;
                        }
                    }
                }


            }

            if (isset($city->st004) && $td27 != '') {
                $room_data = $td27;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td28 = 'b';
                    } else {
                        $td28 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td28 = 'a';
                    } else {
                        $td28 = 'b';

                    }
                } else {
                    $td28 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[6]->stp02)
            ) {
                $td29 = $rooms[6]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[6]->stp02 < 20) {
                            $td29 = 20;
                        } elseif ($rooms[6]->stp02 > 22) {
                            $td29 = 22;
                        } else {
                            $td29 = $rooms[6]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[6]->stp02 < 16) {
                            $td29 = 16;
                        } elseif ($rooms[6]->stp02 > 21) {
                            $td29 = 21;
                        } else {
                            $td29 = $rooms[6]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td31 != '') {
                $room_data = $td31;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td32 = 'b';
                    } else {
                        $td32 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td32 = 'a';
                    } else {
                        $td32 = 'b';

                    }
                } else {
                    $td32 = 'b';
                }
            }

            if (
                isset($building->id) &&
                isset($rooms[7]->stp02)
            ) {
                $td33 = $rooms[7]->stp02;

                if ($building != null) {
                    if ($building->id < 3) {
                        if ($rooms[7]->stp02 < 20) {
                            $td33 = 20;
                        } elseif ($rooms[7]->stp02 > 22) {
                            $td33 = 22;
                        } else {
                            $td33 = $rooms[7]->stp02;
                        }
                    }
                    if ($building->id == 3) {
                        if ($rooms[7]->stp02 < 16) {
                            $td33 = 16;
                        } elseif ($rooms[7]->stp02 > 21) {
                            $td33 = 21;
                        } else {
                            $td33 = $rooms[7]->stp02;
                        }
                    }
                }


            }


            if (isset($city->st004) && $td35 != '') {
                $room_data = $td35;
                $city_data = $city->st004;
                if ($room_data == 'сухой') {
                    if ($city_data == 'влажная') {
                        $td36 = 'b';
                    } else {
                        $td36 = 'a';
                    }
                } elseif ($room_data == 'нормальный') {
                    if ($city_data == 'сухая') {
                        $td36 = 'a';
                    } else {
                        $td36 = 'b';

                    }
                } else {
                    $td36 = 'b';
                }
            }


            $td37 = $city->st008;

            if (
                isset($building->id) &&
                isset($city->st015) &&
                isset($city->st017)
            ) {

                if ($building != null) {
                    if ($building->id == 2) {
                        $td38 = $city->st017;
                    } else {
                        $td38 = $city->st015;
                    }
                }
            }


            if (
                isset($building->id) &&
                isset($city->st014) &&
                isset($city->st016)
            ) {

                if ($building != null) {
                    if ($building->id == 2) {
                        $td39 = $city->st016;
                    } else {
                        $td39 = $city->st014;
                    }
                }
            }


            if (isset($city->st013)) {
                $td40 = $city->st013;
            }


            if (isset($city->st012)) {
                $td41 = $city->st012;
            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );

                foreach ($temperatureByMonth as $key => $value) {
                    if ($value < 0) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0) {
                    $td42 = '0';
                } else {
                    $td42 = round($sumPressure / $months, 6);
                }

            }


            if (
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );


                foreach ($pressure as $key => $value) {
                    $sumPressure += $value;
                    $months++;
                }
                if ($months == 0 || $sumPressure == 0) {
                    $td43 = '0';
                } else {
                    $td43 = round($sumPressure / $months, 6);
                }

            }


            if (
                $td05 != '' &&
                $td38 != '' &&
                $td39 != ''
            ) {
                $td44 = round((
                        $td05 -
                        $td38
                    ) * $td39, 6);
            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {


                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );

                foreach ($temperatureByMonth as $key => $value) {
                    if ($value < -5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }

                if ($months == 0 || $sumNegativeTemperatures == 0) {
                    $td45 = '0';
                } else {
                    $td45 = round($sumNegativeTemperatures / $months, 6);
                }

            }

            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach ($temperatureByMonth as $key => $value) {
                    if ($value < -5) $day += $dayInMount[$key];
                }

                if ($day == 0) {
                    $td46 = '0';
                } else {
                    $td46 = round($day / 30, 0);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );

                foreach ($temperatureByMonth as $key => $value) {
                    if ($value < -5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0) {
                    $td47 = '0';
                } else {
                    $td47 = round($sumPressure / $months, 6);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {


                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                foreach ($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }

                if ($months == 0 || $sumNegativeTemperatures == 0) {
                    $td48 = '0';
                } else {
                    $td48 = round($sumNegativeTemperatures / $months, 6);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach ($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) $day += $dayInMount[$key];
                }
                if ($day == 0) {
                    $td49 = '0';
                } else {
                    $td49 = round($day / 30, 0);

                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );
                foreach ($temperatureByMonth as $key => $value) {
                    if ($value >= -5 && $value <= 5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0) {
                    $td50 = '0';
                } else {
                    $td50 = round($sumPressure / $months, 6);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $sumNegativeTemperatures = 0;
                $months = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );

                foreach ($temperatureByMonth as $key => $value) {
                    if ($value > 5) {
                        $sumNegativeTemperatures += $value;
                        $months++;
                    }
                }


                if ($months == 0 || $sumNegativeTemperatures == 0) {
                    $td51 = '0';
                } else {
                    $td51 = round($sumNegativeTemperatures / $months, 6);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050)
            ) {

                $day = 0;
                $dayInMount = array(31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31);
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );


                foreach ($temperatureByMonth as $key => $value) {
                    if ($value > 5) $day += $dayInMount[$key];
                }
                if ($day == 0) {
                    $td52 = '0';
                } else {
                    $td52 = round($day / 30, 0);
                }

            }


            if (
                isset($city->st039) &&
                isset($city->st040) &&
                isset($city->st041) &&
                isset($city->st042) &&
                isset($city->st043) &&
                isset($city->st044) &&
                isset($city->st045) &&
                isset($city->st046) &&
                isset($city->st047) &&
                isset($city->st048) &&
                isset($city->st049) &&
                isset($city->st050) &&
                isset($city->st051) &&
                isset($city->st052) &&
                isset($city->st053) &&
                isset($city->st054) &&
                isset($city->st055) &&
                isset($city->st056) &&
                isset($city->st057) &&
                isset($city->st058) &&
                isset($city->st059) &&
                isset($city->st060) &&
                isset($city->st061) &&
                isset($city->st062) &&
                isset($city->st063)
            ) {

                $months = 0;
                $sumPressure = 0;
                $temperatureByMonth = array(
                    $city->st039,
                    $city->st040,
                    $city->st041,
                    $city->st042,
                    $city->st043,
                    $city->st044,
                    $city->st045,
                    $city->st046,
                    $city->st047,
                    $city->st048,
                    $city->st049,
                    $city->st050,
                );
                $pressure = array(
                    $city->st052,
                    $city->st053,
                    $city->st054,
                    $city->st055,
                    $city->st056,
                    $city->st057,
                    $city->st058,
                    $city->st059,
                    $city->st060,
                    $city->st061,
                    $city->st062,
                    $city->st063,
                );
                foreach ($temperatureByMonth as $key => $value) {
                    if ($value > 5) {
                        $sumPressure += $pressure[$key];
                        $months++;
                    }
                }
                if ($months == 0 || $sumPressure == 0) {
                    $td53 = '0';
                } else {
                    $td53 = round($sumPressure / $months, 6);
                }

            }


//                        function td54()
//                        {
//                            global $td;
//                            if (
//                                !isset($_SESSION['data']['typeBuilding']['data']['a']['stg03']) ||
//                                !isset($_SESSION['data']['typeBuilding']['data']['b']['stg03']) ||
//                                empty($td['td03']) ||
//                                empty($td['td44']) ||
//                                empty($td['td59'])
//                            ) {
//                                $td['error'] .= 'Ошибка в получении данных td54;' . '<br/><br/>';
//                                return '';
//                            }
//                            $td['error'] .= 'td54 = ([data][typeBuilding][data][a][stg03] * [td44] + [data][typeBuilding][data][a][stg03]) * [td59] * [td03];' . '<br/><br/>';
//
//                            return round((
//                                    $_SESSION['data']['typeBuilding']['data']['a']['stg03'] *
//                                    $td['td44'] +
//                                    $_SESSION['data']['typeBuilding']['data']['b']['stg03']
//                                ) * $td['td59'] * $td['td03'], 6);
//                        }


            $td59 = '1';
            $td60 = '1';
            $td61 = '1';

            if (isset($building_data)) {
                if (
                    isset($building_data[0]->stg03) &&
                    isset($building_data[1]->stg03) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td59 != '')
                ) {


                    $td54 = round((
                            $building_data[0]->stg03 *
                            $td44 +
                            $building_data[1]->stg03
                        ) * $td59 * $td03, 6);
                }
            }


            if (isset($building_data)) {
                if (
                    isset($building_data[0]->stg04) &&
                    isset($building_data[1]->stg04) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td55 = round((
                            $building_data[0]->stg04 *
                            $td44 +
                            $building_data[1]->stg04
                        ) * $td61 * $td03, 6);
                }
            }

            if (isset($building_data)) {
                if (
                    isset($building_data[0]->stg05) &&
                    isset($building_data[1]->stg05) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td56 = round((
                            $building_data[0]->stg05 *
                            $td44 +
                            $building_data[1]->stg05
                        ) * $td61 * $td03, 6);
                }
            }

            if (isset($building)) {
                if (
                    isset($building->id) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td60 != '')
                ) {

                    if ($building->id < 3) {
                        if ($td44 < 6000) {
                            $td57_1 = 0.000075;
                        } elseif ($td44 >= 6000 && $td44 <= 8000) {
                            $td57_1 = 0.00005;
                        } else {
                            $td57_1 = 0.000025;
                        }
                    } elseif ($building->id == 3) {
                        $td57_1 = 0.00005;
                    } else {
                        $td57_1 = 0.000025;
                    }

                    if ($building->id < 3) {
                        if ($td44 < 6000) {
                            $td57_2 = 0.15;
                        } elseif ($td44 >= 6000 && $td44 <= 8000) {
                            $td57_2 = 0.3;
                        } else {
                            $td57_2 = 0.5;
                        }
                    } elseif ($building->id == 3) {
                        $td57_2 = 0.2;
                    } else {
                        $td57_2 = 0.2;
                    }


                    $td57 = round((
                            $td57_1 *
                            $td44 +
                            $td57_2
                        ) * $td60 * $td03, 6);
                }
            }


            if (isset($building_data)) {
                if (
                    isset($building_data[0]->stg07) &&
                    isset($building_data[1]->stg07) &&
                    ($td03 != '') &&
                    ($td44 != '') &&
                    ($td61 != '')
                ) {


                    $td58 = round((
                            $building_data[0]->stg07 *
                            $td44 +
                            $building_data[1]->stg07
                        ) * $td61 * $td03, 6);
                }
            }

        }

        if (isset($building) && $building != null) {
            $td02 = $building->name;
        }
//============

        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td72 = round((
                    $td09 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td73 = round((
                    $td13 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td74 = round((
                    $td17 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td75 = round((
                    $td21 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td76 = round((
                    $td25 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td77 = round((
                    $td29 -
                    $td38
                ) * $td39, 6);
        }
//------------------------------
        if (
            $td05 != '' &&
            $td38 != '' &&
            $td39 != ''
        ) {
            $td78 = round((
                    $td33 -
                    $td38
                ) * $td39, 6);
        }
//============

        if (isset($building_data)) {
            if (
                isset($building_data[0]->stg03) &&
                isset($building_data[1]->stg03) &&
                ($td03 != '') &&
                ($td44 != '') &&
                ($td59 != '')
            ) {


                $td82 = round((
                        $building_data[0]->stg03 *
                        $td72 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td83 = round((
                        $building_data[0]->stg03 *
                        $td73 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td84 = round((
                        $building_data[0]->stg03 *
                        $td74 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td85 = round((
                        $building_data[0]->stg03 *
                        $td75 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td86 = round((
                        $building_data[0]->stg03 *
                        $td76 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td87 = round((
                        $building_data[0]->stg03 *
                        $td77 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);

                $td88 = round((
                        $building_data[0]->stg03 *
                        $td78 +
                        $building_data[1]->stg03
                    ) * $td59 * $td03, 6);
            }
        }


        if (isset($building_data)) {
            if (
                isset($building_data[0]->stg04) &&
                isset($building_data[1]->stg04) &&
                ($td03 != '') &&
                ($td44 != '') &&
                ($td61 != '')
            ) {


                $td82_1 = round((
                        $building_data[0]->stg04 *
                        $td72 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td83_1 = round((
                        $building_data[0]->stg04 *
                        $td73 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td84_1 = round((
                        $building_data[0]->stg04 *
                        $td74 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td85_1 = round((
                        $building_data[0]->stg04 *
                        $td75 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td86_1 = round((
                        $building_data[0]->stg04 *
                        $td76 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td87_1 = round((
                        $building_data[0]->stg04 *
                        $td77 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);

                $td88_1 = round((
                        $building_data[0]->stg04 *
                        $td78 +
                        $building_data[1]->stg04
                    ) * $td61 * $td03, 6);
            }
        }

        if (isset($building_data)) {
            if (
                isset($building_data[0]->stg04) &&
                isset($building_data[1]->stg04) &&
                ($td03 != '') &&
                ($td44 != '') &&
                ($td61 != '')
            ) {


                $td82_2 = round((
                        $building_data[0]->stg05 *
                        $td72 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td83_2 = round((
                        $building_data[0]->stg05 *
                        $td73 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td84_2 = round((
                        $building_data[0]->stg05 *
                        $td74 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td85_2 = round((
                        $building_data[0]->stg05 *
                        $td75 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td86_2 = round((
                        $building_data[0]->stg05 *
                        $td76 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td87_2 = round((
                        $building_data[0]->stg05 *
                        $td77 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);

                $td88_2 = round((
                        $building_data[0]->stg05 *
                        $td78 +
                        $building_data[1]->stg05
                    ) * $td61 * $td03, 6);
            }
        }

        
//============

        $coocie_params['cookie_name'] = 'ab';
        $coocie_params['living'] = $td08;
        $coocie_params['kitchen'] = $td12;
        $coocie_params['toilet'] = $td16;
        $coocie_params['bathroom'] = $td20;
        $coocie_params['rest_room'] = $td24;
        $coocie_params['hall'] = $td28;
        $coocie_params['vestibule'] = $td32;
        $coocie_params['pantry'] = $td36;
        self::writeCookie($coocie_params);
        
        
        $city_building .= "
            <tr><td>Город (td01)</td><td class='td01'>$td01</td></tr>
            <tr><td>Выбранный тип здания (td02)</td><td class='td02'>$td02</td></tr>
            <tr><td>Назначаемый региональный коэффициент m р (td03)</td><td class='td03'>$td03</td></tr>
            <tr><td>Зона влажности в которой находится населённый пункт (td04)</td><td class='td04'>$td04</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_1'>Жилая комната</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td05)</td><td class='td05'>$td05</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td06)</td><td class='td06'>$td06</td></tr>
            <tr><td>Влажностный режим помещения (td07)</td><td class='td07'>$td07</td></tr>
            <tr><td>Условия эксплуатации конструкций (td08)</td><td class='td08'>$td08</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_2'>Кухня</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td09)</td><td class='td09'>$td09</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td10)</td><td class='td10'>$td10</td></tr>
            <tr><td>Влажностный режим помещения (td11)</td><td class='td11'>$td11</td></tr>
            <tr><td>Условия эксплуатации конструкций (td12)</td><td class='td12'>$td12</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_3'>Туалет</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td13)</td><td class='td13'>$td13</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td14)</td><td class='td14'>$td14</td></tr>
            <tr><td>Влажностный режим помещения (td15)</td><td class='td15'>$td15</td></tr>
            <tr><td>Условия эксплуатации конструкций (td16)</td><td class='td16'>$td16</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_4'>Ванная или Совмещённый санузел</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td17)</td><td class='td17'>$td17</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td18)</td><td class='td18'>$td18</td></tr>
            <tr><td>Влажностный режим помещения (td19)</td><td class='td19'>$td19</td></tr>
            <tr><td>Условия эксплуатации конструкций (td20)</td><td class='td20'>$td20</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_5'>Помещения для отдыха и учебных занятий</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td21)</td><td class='td21'>$td21</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td22)</td><td class='td22'>$td22</td></tr>
            <tr><td>Влажностный режим помещения (td23)</td><td class='td23'>$td23</td></tr>
            <tr><td>Условия эксплуатации конструкций (td24)</td><td class='td24'>$td24</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_6'>Межквартирный коридор</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td25)</td><td class='td25'>$td25</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td26)</td><td class='td26'>$td26</td></tr>
            <tr><td>Влажностный режим помещения (td27)</td><td class='td27'>$td27</td></tr>
            <tr><td>Условия эксплуатации конструкций (td28)</td><td class='td28'>$td28</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_7'>Вестибюль, лестничная клетка</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td29)</td><td class='td29'>$td29</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td30)</td><td class='td30'>$td30</td></tr>
            <tr><td>Влажностный режим помещения (td31)</td><td class='td31'>$td31</td></tr>
            <tr><td>Условия эксплуатации конструкций (td32)</td><td class='td32'>$td32</td></tr>
            
            
            <tr class='h2_gray'><td class='roomType_8'>Кладовые</td><td></td></tr>
            
            <tr><td>Внутренняя температура помещения [t в] °С (td33)</td><td class='td33'>$td33</td></tr>
            <tr><td>Максимальная влажность помещения [ϕ в] % (td34)</td><td class='td34'>$td34</td></tr>
            <tr><td>Влажностный режим помещения (td35)</td><td class='td35'>$td35</td></tr>
            <tr><td>Условия эксплуатации конструкций (td36)</td><td class='td36'>$td36</td></tr>
            
            
            <tr class='h2_gray'><td>Прочие параметры</td><td></td></tr>
            
            <tr><td>Наружная температура (наиболее холодной пятидневки обеспеченностью 0,92) [t н] °С (td37)</td><td class='td37'>$td37</td></tr>
            <tr><td>Средняя температура наружного воздуха, °С, отопительного периода [t от] °С (td38)</td><td class='td38'>$td38</td></tr>
            <tr><td>Количество суток отопительного периода [z от] суток (td39)</td><td class='td39'>$td39</td></tr>
            <tr><td>Средняя температура периода с среднемесячными отрицательными температурами [t н отр] °С (td40)</td><td class='td40'>$td40</td></tr>
            <tr><td>Количество суток периода со среднемесячными отрицательными температурами [z o] суток (td41)</td><td class='td41'>$td41</td></tr>
            <tr><td>Среднее парциальное давление наружного воздуха за период отрицательных температур [е н отр] Па (td42)</td><td class='td42'>$td42</td></tr>
            <tr><td>Среднее парциальное давления наружного воздуха за годовой период [е н] Па (td43)</td><td class='td43'>$td43</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (td44)</td><td class='td44'>$td44</td></tr>
            <tr><td>Средняя наружная температура зимнего периода [t зима] °С (td45)</td><td class='td45'>$td45</td></tr>
            <tr><td>Количество месяцев зимнего периода [Z зима] суток (td46)</td><td class='td46'>$td46</td></tr>
            <tr><td>Среднее парциальное давление зимнего периода [е зима] Па (td47)</td><td class='td47'>$td47</td></tr>
            <tr><td>Средняя наружная температура весенне-осеннего периода [t весна-осень] °С (td48)</td><td class='td48'>$td48</td></tr>
            <tr><td>Количество месяцев весенне-осеннего периода [Z весна-осень] суток (td49)</td><td class='td49'>$td49</td></tr>
            <tr><td>Среднее парциальное давление весенне-осеннего периода [е весна-осень] Па (td50)</td><td class='td50'>$td50</td></tr>
            <tr><td>Средняя наружная температура летнего периода [t лето] °С (td51)</td><td class='td51'>$td51</td></tr>
            <tr><td>Количество месяцев летнего периода [Z лето] суток (td52)</td><td class='td52'>$td52</td></tr>
            <tr><td>Среднее парциальное давление летнего периода [е лето] Па (td53)</td><td class='td53'>$td53</td></tr>
            
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (td54)</td><td class='td54'>$td54</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий) [R o норм кровля] (м2 * °С) / Вт (td55)</td><td class='td55'>$td55</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче конструкций полов [R o норм пол] (м2 * °С) / Вт (td56)</td><td class='td56'>$td56</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче оконных конструкций [R o норм окна] (м2 * °С) / Вт (td57)</td><td class='td57'>$td57</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче фонарей [R o норм фонари] (м2 * °С) / Вт (td58)</td><td class='td58'>$td58</td></tr>
            <tr><td>Понижающий коэффициент при превышении удельного расхода отопления стен (td59)</td><td class='td59'>$td59</td></tr>
            <tr><td>Понижающий коэффициент при превышении удельного расхода отопления окон (td60)</td><td class='td60'>$td60</td></tr>
            <tr><td>Понижающий коэффициент при превышении удельного расхода отопления прочее (td61)</td><td class='td61'>$td61</td></tr>
            <tr><td></td><td class=''></td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Кухня) (td72)</td><td class='td72'>$td72</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Туалет) (td73)</td><td class='td73'>$td73</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Ванная или Совмещённый санузел) (td74)</td><td class='td74'>$td74</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Помещения для отдыха и учебных занятий) (td75)</td><td class='td75'>$td75</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Межквартирный коридор) (td76)</td><td class='td76'>$td76</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Вестибюль, лестничная клетка) (td77)</td><td class='td77'>$td77</td></tr>
            <tr><td>ГСОП  (градусо-сутки отопительного периода) [ГСОП] °С * сутки (Кладовые) (td78)</td><td class='td78'>$td78</td></tr>
            <tr><td></td><td class=''></td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Кухня) (td82)</td><td class='td82'>$td82</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Туалет) (td83)</td><td class='td83'>$td83</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Ванная или Совмещённый санузел) (td84)</td><td class='td84'>$td84</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Помещения для отдыха и учебных занятий) (td85)</td><td class='td85'>$td85</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Межквартирный коридор) (td86)</td><td class='td86'>$td86</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Вестибюль, лестничная клетка) (td87)</td><td class='td87'>$td87</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм стена] (м2 * °С) / Вт (Кладовые) (td88)</td><td class='td88'>$td88</td></tr>
            <tr><td></td><td class=''></td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Кухня) (кровля)(td82_1)</td><td class='td82_1'>$td82_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Туалет) (кровля)(td83_1)</td><td class='td83_1'>$td83_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Ванная или Совмещённый санузел) (кровля)(td84_1)</td><td class='td84_1'>$td84_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Помещения для отдыха и учебных занятий) (кровля)(td85_1)</td><td class='td85_1'>$td85_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Межквартирный коридор) (кровля)(td86_1)</td><td class='td86_1'>$td86_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Вестибюль, лестничная клетка) (кровля)(td87_1)</td><td class='td87_1'>$td87_1</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм кровля] (м2 * °С) / Вт (Кладовые) (кровля)(td88_1)</td><td class='td88_1'>$td88_1</td></tr>
            <tr><td></td><td class=''></td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Кухня) (пол)(td82_2)</td><td class='td82_2'>$td82_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Туалет) (пол)(td83_2)</td><td class='td83_2'>$td83_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Ванная или Совмещённый санузел) (пол)(td84_2)</td><td class='td84_2'>$td84_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Помещения для отдыха и учебных занятий) (пол)(td85_2)</td><td class='td85_2'>$td85_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Межквартирный коридор) (пол)(td86_2)</td><td class='td86_2'>$td86_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Вестибюль, лестничная клетка) (пол)(td87_2)</td><td class='td87_2'>$td87_2</td></tr>
            <tr><td>ТРЕБУЕМОЕ значение приведенного сопротивления теплопередаче стеновых конструкций [R o норм полов] (м2 * °С) / Вт (Кладовые) (пол)(td88_2)</td><td class='td88_2'>$td88_2</td></tr>
            ";

        return $city_building;
    }


    public static function calc($params)
    {

        $map = array(
            '2539' => '-40',
            '2322' => '-39',
            '2126' => '-38',
            '1947' => '-37',
            '1785' => '-36',
            '1638' => '-35',
            '1504' => '-34',
            '1382' => '-33',
            '1271' => '-32',
            '1170' => '-31',
            '1077' => '-30',
            '992.7' => '-29',
            '915.5' => '-28',
            '844.8' => '-27',
            '780.2' => '-26',
            '721' => '-25',
            '666.7' => '-24',
            '616.9' => '-23',
            '571.2' => '-22',
            '520.2' => '-21',
            '490.7' => '-20',
            '455.2' => '-19',
            '422.5' => '-18',
            '392.5' => '-17',
            '364.8' => '-16',
            '339.2' => '-15',
            '315.6' => '-14',
            '293.9' => '-13',
            '273.8' => '-12',
            '255.2' => '-11',
            '238' => '-10',
            '222.1' => '-9',
            '207.4' => '-8',
            '193.7' => '-7',
            '181.1' => '-6',
            '169.3' => '-5',
            '158.4' => '-4',
            '148.3' => '-3',
            '138.9' => '-2',
            '130.2' => '-1',
            '122.1' => '0',
            '114.5' => '1',
            '107.5' => '2',
            '100.9' => '3',
            '94.8' => '4',
            '89.1' => '5',
            '83.8' => '6',
            '78.8' => '7',
            '74.2' => '8',
            '69.9' => '9',
            '65.8' => '10',
            '62' => '11',
            '58.5' => '12',
            '55.2' => '13',
            '52.1' => '14',
            '49.1' => '15',
            '46.4' => '16',
            '43.9' => '17',
            '41.5' => '18',
            '39.2' => '19',
            '37.1' => '20',
            '35.1' => '21',
            '33.2' => '22',
            '31.5' => '23',
            '29.8' => '24',
            '28.3' => '25',
            '26.8' => '26',
            '25.4' => '27',
        );
//++++++++++++++++++++++++++++++++++++++++++


        $rooms = [
          'living',
          'kitchen',
          'toilet',
          'bathroom',
          'rest_room',
          'hall',
          'vestibule',
          'pantry'
        ];

$tab_num = $params['tab_num'];

$tab_cookie = User::getCookie('data1');
$ab_json = User::getCookie('ab');
//dd($ab_json);
        $result = [];
foreach($rooms as $room) {
    $html = '';


    if ($tab_cookie != null) {
        $data = (json_decode($tab_cookie));
        if($params['tab_num'] == 2) {
            if (isset($data->living_room_w) && $room == 'living') {
                continue;
            }
            if (isset($data->kitchen_w) && $room == 'kitchen') {
                continue;
            }
            if (isset($data->toilet_w) && $room == 'toilet') {
                continue;
            }
            if (isset($data->bathroom_w) && $room == 'bathroom') {
                continue;
            }
            if (isset($data->rest_room_w) && $room == 'rest_room') {
                continue;
            }
            if (isset($data->hall_w) && $room == 'hall') {
                continue;
            }
            if (isset($data->vestibule_w) && $room == 'vestibule') {
                continue;
            }
            if (isset($data->vestibule_w) && $room == 'pantry') {
                continue;
            }

        }elseif($params['tab_num'] == 3){
            if (isset($data->living_room_r) && $room == 'living') {
                continue;
            }
            if (isset($data->kitchen_r) && $room == 'kitchen') {
                continue;
            }
            if (isset($data->toilet_r) && $room == 'toilet') {
                continue;
            }
            if (isset($data->bathroom_r) && $room == 'bathroom') {
                continue;
            }
            if (isset($data->rest_room_r) && $room == 'rest_room') {
                continue;
            }
            if (isset($data->hall_r) && $room == 'hall') {
                continue;
            }
            if (isset($data->vestibule_r) && $room == 'vestibule') {
                continue;
            }
            if (isset($data->vestibule_r) && $room == 'pantry') {
                continue;
            }
        }elseif($params['tab_num'] == 4){
            if (isset($data->living_room_b) && $room == 'living') {
                continue;
            }
            if (isset($data->kitchen_b) && $room == 'kitchen') {
                continue;
            }
            if (isset($data->toilet_b) && $room == 'toilet') {
                continue;
            }
            if (isset($data->bathroom_b) && $room == 'bathroom') {
                continue;
            }
            if (isset($data->rest_room_b) && $room == 'rest_room') {
                continue;
            }
            if (isset($data->hall_b) && $room == 'hall') {
                continue;
            }
            if (isset($data->vestibule_b) && $room == 'vestibule') {
                continue;
            }
            if (isset($data->vestibule_b) && $room == 'pantry') {
                continue;
            }
        }


        if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'rest_room'){
            continue;
        }
        if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'hall'){
            continue;
        }
        if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'vestibule'){
            continue;
        }
        if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'pantry'){
            continue;
        }
    }

    $selectCoefficient = $params['selectCoefficient'];

    $F230 = $params['F230'];
    $AR49 = $params['AR49'];

    $AP40 = $params['AP40'];
    $AR58 = $params['AR58'];
    $AP33 = $params['AP33'];

    if($room == 'kitchen'){
        $F230 = $params['F230_2'];
        $AR49 = $params['AR49_2'];
//        $if_x_lay = $params['val_td_CTM38_x_2'];
    }
    if($room == 'toilet'){
        $F230 = $params['F230_3'];
        $AR49 = $params['AR49_3'];
//        $if_x_lay = $params['val_td_CTM38_x_3'];
    }
    if($room == 'bathroom'){
        $F230 = $params['F230_4'];
        $AR49 = $params['AR49_4'];
//        $if_x_lay = $params['val_td_CTM38_x_4'];
    }
    if($room == 'rest_room'){
        $F230 = $params['F230_5'];
        $AR49 = $params['AR49_5'];
//        $if_x_lay = $params['val_td_CTM38_x_5'];
    }
    if($room == 'hall'){
        $F230 = $params['F230_6'];
        $AR49 = $params['AR49_6'];
//        $if_x_lay = $params['val_td_CTM38_x_6'];
    }
    if($room == 'vestibule'){
        $F230 = $params['F230_7'];
        $AR49 = $params['AR49_7'];
//        $if_x_lay = $params['val_td_CTM38_x_7'];
    }
    if($room == 'pantry'){
        $F230 = $params['F230_8'];
        $AR49 = $params['AR49_8'];
//        $if_x_lay = $params['val_td_CTM38_x_8'];
    }

    if($tab_num == 4) {
        if($selectCoefficient == 0.96) {
            $AP40 = ($AP40 + $AR49)/3;
            $AR58 = ($AR58 + $AR49)/5;
        }elseif($selectCoefficient == 0.97){
            $AP40 = ($AP40 + $AR49)/4;
            $AR58 = ($AR58 + $AR49)/7;
        }elseif($selectCoefficient == 0.9){
            $AP40 = 4;
            $AR58 = 4;
        }
    }

    $cond_Ventilated = $params['cond_Ventilated'];
    $before_bearing = $params['before_bearing'];
    $after_bearing = $params['after_bearing'];
    $bearing = $params['bearing'];
    $cond_aerial = $params['cond_aerial'];


    $sum_td_CTM36 = 0;
    $sum_td_CTM37 = 0;
    $sum_td_CTM38 = 0;
    foreach ($params['numb'] as $item => $val) {
        $td_CTM07 = trim($params['td_CTM07'][$item]);
        $td_CTM08 = trim($params['td_CTM08'][$item]);
        $td_CTM11 = trim($params['td_CTM11'][$item]);
        $td_CTM17 = trim($params['td_CTM17'][$item]);
        $td_CTM20 = trim($params['td_CTM20'][$item]);
        $td_CTM21 = trim($params['td_CTM21'][$item]);
        $td_CTM22 = trim($params['td_CTM22'][$item]);
        $td_CTM23 = trim($params['td_CTM23'][$item]);
        $td_CTM24 = trim($params['td_CTM24'][$item]);

// ==================cond td_ctm36===================

        $params['td_CTM36'][$item] = 0;
        if ($td_CTM17 != "") {
            $params['td_CTM36'][$item] = $td_CTM17;
        } elseif ($td_CTM11 == '-' || $td_CTM11 == null || $td_CTM11 == '' || $td_CTM11 == 0) {
            $params['td_CTM36'][$item] = 0;
        } else {
            $params['td_CTM36'][$item] = $td_CTM20 / $td_CTM11;
        }

        if ($cond_Ventilated != 'false') {
            if ($before_bearing != 'false' && $item < $before_bearing) {
                $params['td_CTM36'][$item] = 0;
            }
            if ($after_bearing != 'false' && $after_bearing <= $item) {
                $params['td_CTM36'][$item] = 0;
            }
        }

// ==================end cond td_ctm36===================

// ==================cond td_ctm37===================

        if ($td_CTM07 != null && $td_CTM20 != null && $td_CTM07 != 0) {
            $params['td_CTM37'][$item] = $td_CTM20 / $td_CTM07;
        } else {
            $params['td_CTM37'][$item] = $td_CTM22;

            if($tab_num == 4){
                $params['td_CTM37'][$item] = $td_CTM24;
                if($cond_aerial != 'false'){
                    if($cond_aerial < $bearing){
                        $params['td_CTM37'][$item] = $td_CTM23;
                    }
                }
            }else{
                if($cond_aerial != 'false'){
                    if($cond_aerial < $bearing){
                        $params['td_CTM37'][$item] = $td_CTM21;
                    }
                }
            }
            if ($params['condFoil'] != 'false') {
                $params['td_CTM37'][$item] *= 2;
            };
        }
//=====

        if ($tab_cookie != null) {
            $ab = (json_decode($ab_json));
            if($ab->$room == 'b') {
//                echo $room;
//                dd($ab->$room);
                if ($td_CTM08 != null && $td_CTM20 != null && $td_CTM08 != 0) {
                    $params['td_CTM37'][$item] = $td_CTM20 / $td_CTM08;
                } else {
                    $params['td_CTM37'][$item] = $td_CTM22;

                    if($tab_num == 4){
                        $params['td_CTM37'][$item] = $td_CTM24;
                        if($cond_aerial != 'false'){
                            if($cond_aerial < $bearing){
                                $params['td_CTM37'][$item] = $td_CTM23;
                            }
                        }
                    }else {
                        if ($cond_aerial != 'false') {
                            if ($cond_aerial < $bearing) {
                                $params['td_CTM37'][$item] = $td_CTM21;
                            }
                        }
                    }
                    if ($params['condFoil'] != 'false') {
                        $params['td_CTM37'][$item] *= 2;
                    };
                }
            }
        }

//=====
        if ($cond_Ventilated != 'false') {
            if ($before_bearing != 'false') {
                if ($before_bearing == $item) {
                    $params['td_CTM37'][$item] = 0.1149425;

                    if ($tab_num == 3) {
                        $roof_type = $params['roof_type'];

                        if ($roof_type == 1) {
                            $params['td_CTM37'][$item] = 0.114942528735632;
                        } else {
                            $params['td_CTM37'][$item] = 0.131578947;
                        }
//                        if ($selectCoefficient == 0.96 || $selectCoefficient == 0.92 || $selectCoefficient == 0.9) {
//                            $params['td_CTM37'][$item] = 0.0434782608695652;
//                        } else {
//                            $params['td_CTM37'][$item] = 0.083333333;
//                        }
                    }
                } elseif ($item < $before_bearing) {
                    $params['td_CTM37'][$item] = 0;
                }


            }
            if ($after_bearing != 'false') {
                if ($item == $after_bearing) {
                    $params['td_CTM37'][$item] = 0.083333333;
                    if ($tab_num == 3) {
                        $roof_type = $params['roof_type'];

                        if ($roof_type == 1) {
                            $params['td_CTM37'][$item] = 0.114942528735632;
                        } else {
                            $params['td_CTM37'][$item] = 0.131578947;
                        }
                        if ($selectCoefficient == 0.96 || $selectCoefficient == 0.92 || $selectCoefficient == 0.9) {
                            $params['td_CTM37'][$item] = 0.0434782608695652;
                        } else {
                            $params['td_CTM37'][$item] = 0.083333333;
                        }
                    }
                } elseif ($item > $after_bearing) {
                    $params['td_CTM37'][$item] = 0;
                }

            }

        }


// ==================end cond td_ctm37===================


        if ($td_CTM20 == null) {
            $td_CTM20 = 0;
        }
        $sum_td_CTM36 += (double) $params['td_CTM36'][$item];
        $sum_td_CTM37 += (double) $params['td_CTM37'][$item];
        $params['td_CTM38'][$item] = $sum_td_CTM38;
        $sum_td_CTM38 += (double) $td_CTM20;
//        echo "\n". $params['td_CTM37'][$item]."\n";

    }
    $AP49 = 0.1149425;
    $AP58_1 = 0.0434782608695652;
    $AP58_2 = $AP49;
    if ($after_bearing != 'false') {
        $AP58_1 = 0;
    }
    if ($before_bearing != 'false') {
        $AP58_2 = 0;
    }
    $AP58 = $sum_td_CTM37 + $AP58_1 + $AP58_2;

    $AO58 = $sum_td_CTM36;

    $params['td_CTM37']['null'] = $AP49;
    $params['td_CTM37']['x'] = 0.0434782608695652;


//###############
    if ($after_bearing != 'false') {
        $params['td_CTM37']['x'] = 0.083333333;
    }
//###############

    if ($tab_num == 3) {

        $roof_type = $params['roof_type'];

        if ($selectCoefficient == 0.96 || $selectCoefficient == 0.92 || $selectCoefficient == 0.9) {
            $params['td_CTM37']['x'] = 0.0434782608695652;
        } else {
            $params['td_CTM37']['x'] = 0.083333333;
        }

        if ($roof_type == 1) {
            $params['td_CTM37']['null'] = 0.114942528735632;
        } else {
            $params['td_CTM37']['null'] = 0.131578947;
            $AP49 = 0.131578947;
        }

        $AP58_2 = $AP49;
        $AP58 = $sum_td_CTM37 + $AP58_1 + $AP58_2;

    }elseif($tab_num == 4){
//        dd($selectCoefficient);
        if($selectCoefficient == 0.98 || $selectCoefficient == 0.92){
            $params['td_CTM37']['x'] = 0.0588235294117647;
        }elseif($selectCoefficient == 0.96){
            $params['td_CTM37']['x'] = 0.0833333333333333;
        }elseif($selectCoefficient == 0.97 || $selectCoefficient == 0.9){
            $params['td_CTM37']['x'] = 0.166666666666666;
        }
        $AP58_2 = $AP49;
        $AP58 = $sum_td_CTM37 + $params['td_CTM37']['x'] + $AP58_2;
    }

    if ($cond_Ventilated != 'false') {
        if ($before_bearing != 'false') {
            $params['td_CTM37']['null'] = 0;
        }
        if ($after_bearing != 'false') {
            $params['td_CTM37']['x'] = 0;
        }
    }


    $params['td_CTM39']['null'] = $AR49;
    $params['td_CTM40']['null'] = $AR49;
    $params['td_CTM39'][0] = $AR49 - (($AR49 - $AR58) / $AP58) * $AP49;
    $half_CTM41 = (5330 * ($AO58 * ($AR49 - $AR58)) / ($AP58 * ($F230 - $AP33)));

    foreach ($params['numb'] as $key => $val) {

        $td_CTM07 = trim($params['td_CTM07'][$key]);
        $td_CTM08 = trim($params['td_CTM08'][$key]);
        $td_CTM11 = trim($params['td_CTM11'][$key]);
        $data_td_39 = $AR49 - (($AR49 - $AP40) / $AP58) * $AP49;
        $data_td_40 = $AR49 - (($AR49 - $AR58) / $AP58) * $AP49;
        if ($cond_Ventilated != 'false') {
            if ($before_bearing != 'false' && $key < $before_bearing) {
                $data_td_39 = $AR49;
                $data_td_40 = $AR49;
            }
        }
        $params['td_CTM39'][$key] = $data_td_39;
        $params['td_CTM40'][$key] = $data_td_40;

        $AP49 += (double) $params['td_CTM37'][$key];

        if ($td_CTM07 != 0 && $td_CTM11 != '-') {

            if($ab->$room == 'b') {
                $params['td_CTM41'][$key] = $half_CTM41 * ($td_CTM11 / $td_CTM08);
            }else{
                $params['td_CTM41'][$key] = $half_CTM41 * ($td_CTM11 / $td_CTM07);
            }

            if ($params['td_CTM41'][$key] < 1) {
                $params['td_CTM41'][$key] = 118.5;
            }
            if ($params['td_CTM41'][$key] < 25.4) {
                $params['td_CTM42'][$key] = -17.4806 * (log(1.018 * $params['td_CTM41'][$key] - 0.886)) + 83.124;

            } else if ($params['td_CTM41'][$key] > 2539) {
                $params['td_CTM42'][$key] = -11.498 * (log($params['td_CTM41'][$key] - 0.32)) + 50.116;
            } else {

                $test_ind1 = '-';
                $test_ind2 = '-';
                $test_val = 0;

                foreach ($map as $k => $v) {

                    if (($params['td_CTM41'][$key] < $k) && ($test_ind1 == '-')) {
                        $test_ind2 = $k;
                    }
                    if (($params['td_CTM41'][$key] > $k) && ($test_ind1 == '-')) {
                        $test_ind1 = $k;
                        $test_val = $v;
                    }

                }
                $params['td_CTM42'][$key] = ($test_ind1 - $params['td_CTM41'][$key]) / ($test_ind2 - $test_ind1) + ($test_val);
            }
        } else {
            $params['td_CTM41'][$key] = '';
            $params['td_CTM42'][$key] = '';
        }


        if ($cond_Ventilated != 'false') {

            if ($before_bearing != 'false' && $key < $before_bearing) {
                $params['td_CTM41'][$key] = '';
                $params['td_CTM42'][$key] = '';
            }
            if ($after_bearing != 'false' && $after_bearing <= $key) {
                $params['td_CTM41'][$key] = '';
                $params['td_CTM42'][$key] = '';
            }
        }

    }


    $params['td_CTM38']['x'] = $sum_td_CTM38;
    $params['td_CTM39']['x'] = $AR49 - (($AR49 - $AP40) / $AP58) * $AP49;
    $params['td_CTM40']['x'] = $AR49 - (($AR49 - $AR58) / $AP58) * $AP49;

    $params['td_CTM36']['last'] = $AO58;
    $params['td_CTM37']['last'] = $AP58;
    $params['td_CTM39']['last'] = $AP40;
    $params['td_CTM40']['last'] = $AR58;

//        --------------------------------------


//        --------------------------------------
    $html .= '
               <tr class="tr_0">
                   <td class="td_CTM00">0</td>
                   <td class="td_CTM01"></td>
                   <td class="td_CTM02"></td>
                   <td class="td_CTM03"></td>
                   <td class="td_CTM04"></td>
                   <td class="td_CTM05"></td>
                   <td class="td_CTM07"></td>
                   <td class="td_CTM09"></td>
                   <td class="td_CTM11"></td>
                   <td class="td_CTM12"></td>
                   <td class="td_CTM13"></td>
                   <td class="td_CTM14"></td>
                   <td class="td_CTM15"></td>
                   <td class="td_CTM16"></td>
                   <td class="td_CTM17"></td>
                   <td class="td_CTM18"></td>
                   <td class="td_CTM19"></td>
                   <td class="td_CTM20"></td>
                   <td class="td_CTM21"></td>
                   <td class="td_CTM22"></td>
                   <td class="td_CTM23"></td>
                   <td class="td_CTM24"></td>
                   <td class="td_CTM25"></td>
                   <td class="td_CTM26"></td>
                   <td class="td_CTM27"></td>
                   <td class="td_CTM28"></td>
                   <td class="td_CTM29"></td>
                   <td class="td_CTM30"></td>
                   <td class="td_CTM31"></td>
                   <td class="td_CTM32"></td>
                   <td class="td_CTM33"></td>
                   <td class="td_CTM34"></td>
                   <td class="td_CTM35"></td>
                   <td class="td_CTM36"></td>
                   <td class="td_CTM37">' . $params['td_CTM37']['null'] . '</td>
                   <td class="td_CTM38"></td>
                   <td class="td_CTM39">' . $params['td_CTM39']['null'] . '</td>
                   <td class="td_CTM40">' . $params['td_CTM40']['null'] . '</td>
                   <td class="td_CTM41"></td>
                   <td class="td_CTM42"></td>
               </tr>';
    foreach ($params['numb'] as $item => $val) {

        $numb = $item + 1;
        $td_name = trim($params['td_name'][$item]);
        $td_CTM02 = trim($params['td_CTM02'][$item]);
        $td_CTM03 = trim($params['td_CTM03'][$item]);
        $td_CTM04 = trim($params['td_CTM04'][$item]);
        $td_CTM06 = trim($params['td_CTM06'][$item]);
        $td_CTM08 = trim($params['td_CTM08'][$item]);
        $td_CTM10 = trim($params['td_CTM10'][$item]);
        $td_CTM05 = trim($params['td_CTM05'][$item]);
        $td_CTM07 = trim($params['td_CTM07'][$item]);
        $td_CTM09 = trim($params['td_CTM09'][$item]);
        $td_CTM11 = trim($params['td_CTM11'][$item]);
        $td_CTM12 = trim($params['td_CTM12'][$item]);
        $td_CTM13 = trim($params['td_CTM13'][$item]);
        $td_CTM14 = trim($params['td_CTM14'][$item]);
        $td_CTM16 = trim($params['td_CTM16'][$item]);
        $td_CTM17 = trim($params['td_CTM17'][$item]);
        $td_CTM18 = trim($params['td_CTM18'][$item]);
        $td_CTM19 = trim($params['td_CTM19'][$item]);
        $td_CTM20 = trim($params['td_CTM20'][$item]);
        $td_CTM21 = trim($params['td_CTM21'][$item]);
        $td_CTM22 = trim($params['td_CTM22'][$item]);
        $td_CTM23 = trim($params['td_CTM23'][$item]);
        $td_CTM24 = trim($params['td_CTM24'][$item]);
        $td_CTM25 = trim($params['td_CTM25'][$item]);
        $td_CTM26 = trim($params['td_CTM26'][$item]);
        $td_CTM27 = trim($params['td_CTM27'][$item]);
        $td_CTM28 = trim($params['td_CTM28'][$item]);
        $td_CTM29 = trim($params['td_CTM29'][$item]);

        $td_CTM36 = trim($params['td_CTM36'][$item]);
        $td_CTM37 = trim($params['td_CTM37'][$item]);
        $td_CTM38 = trim($params['td_CTM38'][$item]);
        $td_CTM39 = trim($params['td_CTM39'][$item]);
        $td_CTM40 = trim($params['td_CTM40'][$item]);
        $td_CTM41 = trim($params['td_CTM41'][$item]);
        $td_CTM42 = trim($params['td_CTM42'][$item]);

        $html .= '
               <tr class="tr_' . $numb . '">
                   <td class="td_CTM00">' . $numb . '</td>
                   <td class="td_CTM01">' . $td_name . '</td>
                   <td class="td_CTM02">' . $td_CTM02 . '</td>
                   <td class="td_CTM03">' . $td_CTM03 . '</td>
                   <td class="td_CTM04">' . $td_CTM04 . '</td>
                   <td class="td_CTM05">' . $td_CTM05 . ' | ' . $td_CTM06 . '</td>
                   <td class="td_CTM07">' . $td_CTM07 . ' | ' . $td_CTM08 . '</td>
                   <td class="td_CTM09">' . $td_CTM09 . ' | ' . $td_CTM10 . '</td>
                   <td class="td_CTM11">' . $td_CTM11 . '</td>
                   <td class="td_CTM12">' . $td_CTM12 . '</td>
                   <td class="td_CTM13">' . $td_CTM13 . '</td>
                   <td class="td_CTM14">' . $td_CTM14 . '</td>
                   <td class="td_CTM15"></td>
                   <td class="td_CTM16">' . $td_CTM16 . '</td>
                   <td class="td_CTM17">' . $td_CTM17 . '</td>
                   <td class="td_CTM18">' . $td_CTM18 . '</td>
                   <td class="td_CTM19">' . $td_CTM19 . '</td>
                   <td class="td_CTM20">' . $td_CTM20 . '</td>
                   <td class="td_CTM21">' . $td_CTM21 . '</td>
                   <td class="td_CTM22">' . $td_CTM22 . '</td>
                   <td class="td_CTM23">' . $td_CTM23 . '</td>
                   <td class="td_CTM24">' . $td_CTM24 . '</td>
                   <td class="td_CTM25">' . $td_CTM25 . '</td>
                   <td class="td_CTM26">' . $td_CTM26 . '</td>
                   <td class="td_CTM27">' . $td_CTM27 . '</td>
                   <td class="td_CTM28">' . $td_CTM28 . '</td>
                   <td class="td_CTM29">' . $td_CTM29 . '</td>
                   <td class="td_CTM30"></td>
                   <td class="td_CTM31"></td>
                   <td class="td_CTM32"></td>
                   <td class="td_CTM33"></td>
                   <td class="td_CTM34"></td>
                   <td class="td_CTM35"></td>
                   <td class="td_CTM36">' . $td_CTM36 . '</td>
                   <td class="td_CTM37">' . $td_CTM37 . '</td>
                   <td class="td_CTM38">' . $td_CTM38 . '</td>
                   <td class="td_CTM39">' . $td_CTM39 . '</td>
                   <td class="td_CTM40">' . $td_CTM40 . '</td>
                   <td class="td_CTM41">' . $td_CTM41 . '</td>
                   <td class="td_CTM42">' . $td_CTM42 . '</td>
               </tr>';
    }

    $html .= '
               <tr class="tr_x">
                   <td class="td_CTM00">x</td>
                   <td class="td_CTM01"></td>
                   <td class="td_CTM02">0</td>
                   <td class="td_CTM03"></td>
                   <td class="td_CTM04"></td>
                   <td class="td_CTM05"></td>
                   <td class="td_CTM07"></td>
                   <td class="td_CTM09"></td>
                   <td class="td_CTM11"></td>
                   <td class="td_CTM12">0</td>
                   <td class="td_CTM13"></td>
                   <td class="td_CTM14"></td>
                   <td class="td_CTM15"></td>
                   <td class="td_CTM16"></td>
                   <td class="td_CTM17"></td>
                   <td class="td_CTM18"></td>
                   <td class="td_CTM19"></td>
                   <td class="td_CTM20">0</td>
                   <td class="td_CTM21"></td>
                   <td class="td_CTM22"></td>
                   <td class="td_CTM23"></td>
                   <td class="td_CTM24"></td>
                   <td class="td_CTM25"></td>
                   <td class="td_CTM26"></td>
                   <td class="td_CTM27"></td>
                   <td class="td_CTM28"></td>
                   <td class="td_CTM29"></td>
                   <td class="td_CTM30"></td>
                   <td class="td_CTM31"></td>
                   <td class="td_CTM32"></td>
                   <td class="td_CTM33"></td>
                   <td class="td_CTM34"></td>
                   <td class="td_CTM35"></td>
                   <td class="td_CTM36"></td>
                   <td class="td_CTM37">' . $params['td_CTM37']['x'] . '</td>
                   <td class="td_CTM38">' . $params['td_CTM38']['x'] . '</td>
                   <td class="td_CTM39">' . $params['td_CTM39']['x'] . '</td>
                   <td class="td_CTM40">' . $params['td_CTM40']['x'] . '</td>
                   <td class="td_CTM41"></td>
                   <td class="td_CTM42"></td>
               </tr>';
    $html .= '
               <tr class="tr_last">
                   <td class="td_CTM00"></td>
                   <td class="td_CTM01"></td>
                   <td class="td_CTM02"></td>
                   <td class="td_CTM03"></td>
                   <td class="td_CTM04"></td>
                   <td class="td_CTM05"></td>
                   <td class="td_CTM07"></td>
                   <td class="td_CTM09"></td>
                   <td class="td_CTM11"></td>
                   <td class="td_CTM12"></td>
                   <td class="td_CTM13"></td>
                   <td class="td_CTM14"></td>
                   <td class="td_CTM15"></td>
                   <td class="td_CTM16"></td>
                   <td class="td_CTM17"></td>
                   <td class="td_CTM18"></td>
                   <td class="td_CTM19"></td>
                   <td class="td_CTM20"></td>
                   <td class="td_CTM21"></td>
                   <td class="td_CTM22"></td>
                   <td class="td_CTM23"></td>
                   <td class="td_CTM24"></td>
                   <td class="td_CTM25"></td>
                   <td class="td_CTM26"></td>
                   <td class="td_CTM27"></td>
                   <td class="td_CTM28"></td>
                   <td class="td_CTM29"></td>
                   <td class="td_CTM30"></td>
                   <td class="td_CTM31"></td>
                   <td class="td_CTM32"></td>
                   <td class="td_CTM33"></td>
                   <td class="td_CTM34"></td>
                   <td class="td_CTM35"></td>
                   <td class="td_CTM36">' . $params['td_CTM36']['last'] . '</td>
                   <td class="td_CTM37">' . $params['td_CTM37']['last'] . '</td>
                   <td class="td_CTM38"></td>
                   <td class="td_CTM39">' . $params['td_CTM39']['last'] . '</td>
                   <td class="td_CTM40">' . $params['td_CTM40']['last'] . '</td>
                   <td class="td_CTM41"></td>
                   <td class="td_CTM42"></td>
               </tr>';

    $result[$room] = $html;
}



        return json_encode($result);

//        return $html;
    }

    public static function calcPMU($params)
    {
        $result = [];
        $rooms = [
            'living',
            'kitchen',
            'toilet',
            'bathroom',
            'rest_room',
            'hall',
            'vestibule',
            'pantry'
        ];

        $tab_cookie = User::getCookie('data1');
        $ab_json = User::getCookie('ab');

//        if ($tab_cookie != null) {
//            $ab = (json_decode($ab_json));
//            if($ab->$room == 'b') {
        foreach($rooms as $room) {


            if ($tab_cookie != null) {
                $data = (json_decode($tab_cookie));
                if($params['tab_num'] == 2) {
                    if (isset($data->living_room_w) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_w) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_w) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_w) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_w) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_w) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'pantry') {
                        continue;
                    }

                }elseif($params['tab_num'] == 3){
                    if (isset($data->living_room_r) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_r) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_r) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_r) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_r) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_r) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'pantry') {
                        continue;
                    }
                }elseif($params['tab_num'] == 4){
                    if (isset($data->living_room_b) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_b) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_b) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_b) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_b) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_b) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'pantry') {
                        continue;
                    }
                }


                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'rest_room'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'hall'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'vestibule'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'pantry'){
                    continue;
                }
            }


            $TD100 = $params['TD100'];
            /*if($room == 'living'){
                $TD100 = $params['TD100_1'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_1'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_1'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_1'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_1'];
            }*/
            if($room == 'kitchen'){
                $TD100 = $params['TD100_2'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_2'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_2'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_2'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_2'];
//                $if_x_lay = $params['val_td_CTM38_x_2'];
//                dd($params['val_td_CTM42']);
            }
            if($room == 'toilet'){
                $TD100 = $params['TD100_3'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_3'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_3'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_3'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_3'];
//                $if_x_lay = $params['val_td_CTM38_x_3'];
            }
            if($room == 'bathroom'){
                $TD100 = $params['TD100_4'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_4'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_4'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_4'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_4'];
//                $if_x_lay = $params['val_td_CTM38_x_4'];
            }
            if($room == 'rest_room'){
                $TD100 = $params['TD100_5'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_5'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_5'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_5'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_5'];
//                $if_x_lay = $params['val_td_CTM38_x_5'];
            }
            if($room == 'hall'){
                $TD100 = $params['TD100_6'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_6'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_6'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_6'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_6'];
//                $if_x_lay = $params['val_td_CTM38_x_6'];
            }
            if($room == 'vestibule'){
                $TD100 = $params['TD100_7'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_7'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_7'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_7'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_7'];
//                $if_x_lay = $params['val_td_CTM38_x_7'];
            }
            if($room == 'pantry'){
                $TD100 = $params['TD100_8'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_8'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_8'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_8'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_8'];
//                $if_x_lay = $params['val_td_CTM38_x_8'];
            }
            
            
            
            $content = "<br>Произведён поиск по условию наличия 'Ярковыраженного теплоизоляционного слоя' <br>";
            $td100_part = ($TD100 * 2 / 3);
            $sum_td_CTM36 = 0;
            $isset = 0;
            $expressed_layer = false;
            $calculation = false;
            $general_calculation = false;
            $arr_layout_val = false;

            $val_td_CTM37_arr = $params['val_td_CTM37'];
//            echo count($val_td_CTM37_arr);
//            echo "count\n";
            foreach ($val_td_CTM37_arr as $key => $value) {
                if ($isset == 1) {
                    $sum_td_CTM36 = $sum_td_CTM36 + $params['val_td_CTM36'][$key];
                } elseif ($td100_part <= $params['val_td_CTM37'][$key]) {
                    //            $(element_tr).css('background','#fcffc7');
                    $content .= "<br> Произведён анализ конструкции, выраженным слоем утеплителя назначен слой №$key  <br>";

                    $val_CTM36 = $params['val_td_CTM36'][$key];
                    $val_td_CTM07 = $params['val_td_CTM07'][$key];
                    $val_arr = explode('|', $val_td_CTM07);
//                    dd($val_td_CTM07);

                    $val_td_CTM07 = $val_arr[0];

                    if ($tab_cookie != null) {
                        $ab = (json_decode($ab_json));
                        if ($ab->$room == 'b') {
                            $val_td_CTM07 = $val_arr[1];
                        }
                    }
                    $val_td_CTM11 = $params['val_td_CTM11'][$key];
                    $layout = $key;
                    $isset = 1;
                    $expressed_layer = true;
                }
            }


            if ($expressed_layer) {
//            // 1. Вариант 2
                if ($val_CTM36 > $sum_td_CTM36) {
                    $content .= "<br> Произведена проверка по П.1 коэфф. паропрониц. - условие П.1 совпало, переход проверки условия П.2… <br>";
                    $div_val = $val_td_CTM11 / $val_td_CTM07;
                    if ($div_val > 2) {
                        $content .= " <br>Произведена проверка по П.2 отношения паропрониц. К тепловпров. - условие П.2 совпало.
                      ПМУ назначается между слоями № $layout и № " . ($layout + 1) . " <br>";
                        $general_calculation = false;
                    } else {
                        $content .= " <br> Произведена проверка по П.2 отношения паропрониц. К тепловпров. - условие П.2 не
                     совпало, переход к общему порядку поиска ПМУ…  <br>";
                        $general_calculation = true;
                    }
                } else {
                    $content .= " <br> Произведена проверка по П.1 коэфф. паропрониц. - условие П.1 не совпало, переход к общему порядку поиска ПМУ…  <br>";
                    $general_calculation = true;
                }


                if (!$general_calculation) {
                    if(count($val_td_CTM37_arr) < $layout + 1){
                        $content .= " <br> По дополнительным условиям проверки параметров \"Ярковыраженного теплоизоляционного
  слоя\" ПМУ находится на поверхности конструкции";
                    }else{
                        $content .= " <br> По дополнительным условиям проверки параметров \"Ярковыраженного теплоизоляционного слоя\" 
            ПМУ назначается между слоями № $layout и № " . ($layout + 1);
                    }

                    $arr_layout_val = $layout + 1;
                    $calculation = "calc_3_2";
                } else {
                    $content .= " <br>По дополнительным условиям проверки параметров \"Ярковыраженного теплоизоляционного слоя\"
                 нахождение ПМУ производится по общим правилам. <br>";
                }


            } else {
//            // 1. Вариант 1
//                if ($content == '') {
                    $content .= " <br> Произведён анализ конструкции, выраженного слоя утеплителя в конструкции не имеется. 
                Поиск ПМУ производится по общим правилам.  <br>";
//                }
                $general_calculation = true;
            }


            if ($general_calculation) {
                // переходит к полным расчётам в раздел 2.
                $val_td_CTM42_arr = $params['val_td_CTM42'];
                $val_td_CTM40_arr = $params['val_td_CTM40'];
                $PMU_in = false;
                $PMU_between = false;
                $arr_pmu_1 = array();
                $arr_pmu_2 = array();
                $point_2_1 = "";

                foreach ($val_td_CTM42_arr as $key => $value) {
                        $val_td_CTM42 = $value;
                        $val_td_CTM40 = $val_td_CTM40_arr[$key];
                        if (isset($val_td_CTM40_arr[$key + 1])) {
                            $val_td_CTM40_next = $val_td_CTM40_arr[$key + 1];
                        } else {
                            $val_td_CTM40_next = 1;
                        }
                        if ($val_td_CTM42 != null && $val_td_CTM42 != '' && $val_td_CTM40_next != '') {

                            // =================================================================================

                            if ($val_td_CTM40 > $val_td_CTM42 && $val_td_CTM42 > $val_td_CTM40_next) {
                                // пункт 2.1
                                $PMU_in = true;
                                $arr_pmu_1[] = $key;
                            }
                            if (!empty($arr_pmu_1)) {
                                $key_array = "";
                                foreach ($arr_pmu_1 as $item) {
                                    $key_array .= " № $item";
                                }
                                if (count($arr_pmu_1) > 1) {
                                    $point_2_1 = " <br>Плоскость ПМУ найдена в слоях $key_array  ";
                                } else {
                                    $point_2_1 = " <br>Плоскость ПМУ найдена в слое $key_array  ";
                                }

                            }
                            // =================================================================================

                        }
                }
                if (!empty($arr_pmu_1)) {
                    $content .= "$point_2_1";
                }
//dd($val_td_CTM42_arr);

                foreach ($val_td_CTM42_arr as $key => $value) {
                    // пункт 2.2

                        if ($key > 1) {
                            $layout = $key;

                            $val_td_CTM42 = $value;
                            $val_td_CTM42_prev = $val_td_CTM42_arr[$key - 1];
                            $val_td_CTM40 = $val_td_CTM40_arr[$key];
//echo "$key--".$val_td_CTM42."<br>";
//echo "$key--".$val_td_CTM42_prev."<br>";
                            if ($val_td_CTM42 != null && $val_td_CTM42 != '' && $val_td_CTM42_prev != '') {
                                // =================================================================================
                                if ($val_td_CTM42 > $val_td_CTM40 && $val_td_CTM42_prev < $val_td_CTM40) {
                                    // пункт 2.2
                                    $content .= " <br>ПМУ находится между слоями №$key и №" . ($key - 1) . "  ";
                                    $PMU_between = true;
                                    $arr_pmu_2[] = $key;

                                }
                                // =================================================================================
                            }
                        }
                }

                if ((!$PMU_in) && (!$PMU_between)) {
                    // пункт 2.3.4
                    $content .= " <br>ПМУ в данной конструкции не найдено, поэтому расчётным ПМУ условно назначается поверхность конструкции.  ";
//                $arr_layout_val = 'x';

                    // move the internal pointer to the end of the array
                    end($val_td_CTM37_arr);

                    $arr_layout_val = key($val_td_CTM37_arr);
                    $calculation = "calc_3_2";
                } else {
                    $important_value = '';
                    $arr_layout_val_1 = '';
                    $arr_layout_val_2 = '';
                    if (!empty($arr_pmu_1)) {
                        foreach ($arr_pmu_1 as $key => $value) {
                            $val_td_CTM14 = $params['val_td_CTM14'][$value];
                            if ($val_td_CTM14 == 'T') {
                                $content .= " <br> слой № $value является утеплителем, в толще которого найдено ПМУ, то этот слой назначается расчётным слоем, в котором найдено ПМУ. ";
                                $important_value = $value;
                            }
                            $arr_layout_val_1 = $value;

                        };
                    }
                    if (!empty($arr_pmu_2)) {
                        foreach ($arr_pmu_2 as $key => $value) {
                            $arr_layout_val_2 = $value;
                        }
                    }

                    if ($arr_layout_val_1 != '' && $arr_layout_val_2 != '') {
                        if ($arr_layout_val_1 < $arr_layout_val_2) {
                            $arr_layout_val = $arr_layout_val_2;
                            $calculation = "calc_3_2";
                        } else {
                            $arr_layout_val = $arr_layout_val_1;
                            $calculation = "calc_3_1";
                        }
                    } elseif ($arr_layout_val_1 == '') {
                        $arr_layout_val = $arr_layout_val_2;
                        $calculation = "calc_3_2";
                    } elseif ($arr_layout_val_2 == '') {
                        $arr_layout_val = $arr_layout_val_1;
                        $calculation = "calc_3_1";
                    }

                    if ($important_value != '') {
                        $arr_layout_val = $important_value;
                        $calculation = "calc_3_1";
                    }

                    if ((count($arr_pmu_1) > 0 && count($arr_pmu_2) > 0) || (count($arr_pmu_1) > 1 || count($arr_pmu_2) > 1)) {
                        $content .= " <br> Найдено несколько плоскостей ПМУ:";
                    }

                    if ($important_value != '') {
                        $content .= "<br>В слое № $arr_layout_val (данный слой является утеплителем) ";
                    } else {

                        if ((count($arr_pmu_1) > 1 && $important_value == '') || ($calculation == 'calc_3_1' && count($arr_pmu_1) > 0 && count($arr_pmu_2) > 0)) {
                            $content .= "<br>Расчётная плоскость ПМУ назначается в слое № $arr_layout_val .";
//                            $content .= "<br>Расчётная плоскость ПМУ назначается в слое № $arr_layout_val , так как ПМУ в слое утеплителя отсутствует и данный слой располагается ближе к улице.";
                        }
                        if ((count($arr_pmu_2) > 1 && $important_value == '') || ($calculation == 'calc_3_2' && count($arr_pmu_1) > 0 && count($arr_pmu_2) > 0)) {
                            $content .= "<br>Расчётная плоскость ПМУ назначается между слоями № $arr_layout_val И № " .
                                ($arr_layout_val - 1) . ", так как ПМУ в слое утеплителя отсутствует и данные слои располагаются ближе к улице.";
                        }
                        if (count($arr_pmu_1) == 1 && empty($arr_pmu_2)) {
                            $content .= " <br> Назначается расчётной единственная плоскость ПМУ, которая найдена в слое № $arr_layout_val";
                        }
                        if (count($arr_pmu_2) == 1 && empty($arr_pmu_1)) {
                            $content .= " <br> Назначается расчётной единственная плоскость ПМУ, которая найдена на границе слоёв № " . ($arr_pmu_2[0] - 1) . " И № " . $arr_pmu_2[0] . " ";
                        }
                    }
                    if ($important_value != '') {
                        $content .= "<br>Расчётная плоскость ПМУ назначается в слое № $arr_layout_val т.к. он является утеплителем.";
                    }

                }

            }
            if($params['tab_num'] == 4 && $params['selectCoefficient'] == 0.9) {
                $content = '';
            }

            $result["content"][$room] = $content;
            $result["calculation"][$room] = $calculation;
            $result["arr_layout_val"][$room] = $arr_layout_val;
        }
        return json_encode($result);
    }

    public static function calc_3_1($params)
    {

        $rooms = [
            'living',
            'kitchen',
            'toilet',
            'bathroom',
            'rest_room',
            'hall',
            'vestibule',
            'pantry'
        ];

        $tab_cookie = User::getCookie('data1');
        if ($tab_cookie != null) {
            $data = (json_decode($tab_cookie));

//            print_r($params['tab_num']);
        }
        foreach($rooms as $room) {
            if ($tab_cookie != null) {

                if($params['tab_num'] == 2) {
                    if (isset($data->living_room_w) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_w) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_w) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_w) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_w) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_w) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'pantry') {
                        continue;
                    }

                }elseif($params['tab_num'] == 3){
                    if (isset($data->living_room_r) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_r) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_r) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_r) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_r) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_r) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'pantry') {
                        continue;
                    }
                }elseif($params['tab_num'] == 4){
                    if (isset($data->living_room_b) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_b) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_b) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_b) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_b) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_b) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'pantry') {
                        continue;
                    }
                }
                
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'rest_room'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'hall'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'vestibule'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'pantry'){
                    continue;
                }
            }

            $content = "";
            $calculation = $params['calculation'];
            $arr_layout_val = $params['arr_layout_val'];
            $TD06 = $params['TD06'];
            
            $tab_num = $params['tab_num'];
            $selectCoefficient = $params['selectCoefficient'];



            $TD41 = $params['TD41'];
            $TD42 = $params['TD42'];
            $TD43 = $params['TD43'];
            $TD40 = $params['TD40'];
            $TD46 = $params['TD46'];
            $TD49 = $params['TD49'];
            $TD52 = $params['TD52'];

            $TD05 = $params['TD05'];
            $if_x_lay = $params['val_td_CTM38_x'];

            $TD100 = $params['TD100'];
            $TD105 = $params['TD105'];
//var_dump($room);
            if($room == 'kitchen'){
                $TD05 = $params['TD09'];
                $TD06 = $params['TD10'];
                $TD100 = $params['TD100_2'];
                $TD105 = $params['TD105_2'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_2'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_2'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_2'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_2'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_2'];
                $if_x_lay = $params['val_td_CTM38_x_2'];
            }
            if($room == 'toilet'){
                $TD05 = $params['TD13'];
                $TD06 = $params['TD14'];
                $TD100 = $params['TD100_3'];
                $TD105 = $params['TD105_3'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_3'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_3'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_3'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_3'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_3'];
                $if_x_lay = $params['val_td_CTM38_x_3'];
            }
            if($room == 'bathroom'){
                $TD05 = $params['TD17'];
                $TD06 = $params['TD18'];
                $TD100 = $params['TD100_4'];
                $TD105 = $params['TD105_4'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_4'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_4'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_4'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_4'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_4'];
                $if_x_lay = $params['val_td_CTM38_x_4'];
            }
            if($room == 'rest_room'){
                $TD05 = $params['TD21'];
                $TD06 = $params['TD22'];
                $TD100 = $params['TD100_5'];
                $TD105 = $params['TD105_5'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_5'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_5'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_5'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_5'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_5'];
                $if_x_lay = $params['val_td_CTM38_x_5'];
            }
            if($room == 'hall'){
                $TD05 = $params['TD25'];
                $TD06 = $params['TD26'];
                $TD100 = $params['TD100_6'];
                $TD105 = $params['TD105_6'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_6'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_6'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_6'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_6'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_6'];
                $if_x_lay = $params['val_td_CTM38_x_6'];
            }
            if($room == 'vestibule'){
                $TD05 = $params['TD29'];
                $TD06 = $params['TD30'];
                $TD100 = $params['TD100_7'];
                $TD105 = $params['TD105_7'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_7'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_7'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_7'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_7'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_7'];
                $if_x_lay = $params['val_td_CTM38_x_7'];
            }
            if($room == 'pantry'){
                $TD05 = $params['TD33'];
                $TD06 = $params['TD34'];
                $TD100 = $params['TD100_8'];
                $TD105 = $params['TD105_8'];
                $params['val_td_CTM36'] = $params['val_td_CTM36_8'];
                $params['val_td_CTM37'] = $params['val_td_CTM37_8'];
                $params['val_td_CTM38'] = $params['val_td_CTM38_8'];
                $params['val_td_CTM40'] = $params['val_td_CTM40_8'];
                $params['val_td_CTM42'] = $params['val_td_CTM42_8'];
                $if_x_lay = $params['val_td_CTM38_x_8'];
            }

            $TD45 = $params['TD45'];
            $TD48 = $params['TD48'];
            $TD51 = $params['TD51'];

//            var_dump($room);
//            var_dump($TD100);


            if($tab_num == 4) {
                if($selectCoefficient == 0.96) {
                    $TD40 = ($TD40 + $TD05)/5;
                    $TD43 = $TD42;
                    $TD45 = ($TD45 + $TD05)/3;
                    $TD48 = ($TD48 + $TD05)/5;

                }elseif($selectCoefficient == 0.97){
                    $TD40 = ($TD40 + $TD05)/7;
                    $TD43 = $TD42;
                    $TD45 = ($TD45 + $TD05)/4;
                    $TD48 = ($TD48 + $TD05)/7;
                }elseif($selectCoefficient == 0.9){
                    $TD40 = 4;
                    $TD45 = 4;
                    $TD48 = 4;
                    $TD51 = 4;
                }
            }


            $x_layer = false;



            if (count($params['val_td_CTM37']) == $arr_layout_val) $x_layer = true;
            if (count($params['val_td_CTM37']) < $arr_layout_val) {
                $x_layer = true;
                $arr_layout_val = $arr_layout_val-1;
            }
//            if (count($params['val_td_CTM37']) >= $arr_layout_val) $x_layer = true;
//            if ($calculation != 'calc_3_1') {
            // переходит к расчётам по получившемуся результату в раздел 3.1
            // раздел 3.1.1
            // Необходимо установить координату Хmy плоскости этого совпадения ПМУ.

            $val_td_CTM42 = $params['val_td_CTM42'][$arr_layout_val];
            $val_td_CTM38 = $params['val_td_CTM38'][$arr_layout_val];
            $val_td_CTM40 = $params['val_td_CTM40'][$arr_layout_val];
//            dd($params['val_td_CTM42']);
            if (!$x_layer) {

                $val_td_CTM38_next = $params['val_td_CTM38'][$arr_layout_val + 1];
                $val_td_CTM40_next = $params['val_td_CTM40'][$arr_layout_val + 1];
            }
            $cur_val_td_CTM37 = $params['val_td_CTM37'][$arr_layout_val];
            $sum_td_CTM37 = 0.1149425;

            if ($tab_num == 3) {
                $roof_type = $params['roof_type'];
                if ($roof_type != 1) {
                    $sum_td_CTM37 = 0.131578947;
                }
            }
            for ($i = 1; $i < $arr_layout_val; $i++) {
                $val_td_CTM37 = $params['val_td_CTM37'][$i];
                $sum_td_CTM37 += $val_td_CTM37;
            }

            $cur_val_td_CTM36 = $params['val_td_CTM36'][$arr_layout_val];
            $sum_td_CTM36 = 0;
            for ($i = 1; $i < $arr_layout_val; $i++) {
                $val_td_CTM36 = $params['val_td_CTM36'][$i];
                $sum_td_CTM36 += $val_td_CTM36;

            }

            if ($calculation == 'calc_3_2') {
                $TD120 = $params['val_td_CTM38'][$arr_layout_val];
                $TD121 = $sum_td_CTM37;
                $TD122 = $sum_td_CTM36;

                if(count($params['val_td_CTM37']) == 1){
                    $TD123_1 = $params['val_td_CTM38_x'];
                    $TD124_1 = $params['val_td_CTM38_x'];
                    $TD125_1 = $params['val_td_CTM38_x'];
                }else{
                    $TD123_1 = ($params['val_td_CTM02'][$arr_layout_val - 1]);
                    $TD124_1 = ($params['val_td_CTM20'][$arr_layout_val - 1]);
                    $TD125_1 = ($params['val_td_CTM12'][$arr_layout_val - 1]);
                }



                $TD123_2 = ($params['val_td_CTM02'][$arr_layout_val]);
                $TD124_2 = ($params['val_td_CTM20'][$arr_layout_val]);
                $TD125_2 = ($params['val_td_CTM12'][$arr_layout_val]);
                if ($x_layer) {
                    $TD120 = $if_x_lay;
                    $TD121 = $sum_td_CTM37 + $params['val_td_CTM37'][$arr_layout_val];
                    $TD122 = $sum_td_CTM36 + $params['val_td_CTM36'][$arr_layout_val];

                    $TD123_1 = ($params['val_td_CTM02'][$arr_layout_val]);
                    $TD124_1 = ($params['val_td_CTM20'][$arr_layout_val]);
                    $TD125_1 = ($params['val_td_CTM12'][$arr_layout_val]);


                    $TD123_2 = (0);
                    $TD124_2 = (0);
                    $TD125_2 = (0);
                }
                $TD123 = $TD123_1 . ' | ' . $TD123_2;
                $TD124 = $TD124_1 . ' | ' . $TD124_2;
                $TD125 = $TD125_1 . ' | ' . $TD125_2;


            } elseif ($calculation == 'calc_3_1') {
                if(count($params['val_td_CTM37']) == 1){
                    $TD120 = $params['val_td_CTM38_x'];
                    $TD121 = $params['val_td_CTM38_x'];
                    $TD122 = $params['val_td_CTM38_x'];
//                    dd($params['val_td_CTM38_x']);
                }else{
                    $TD120 = $val_td_CTM38 + ((($val_td_CTM40 - $val_td_CTM42) / ($val_td_CTM40 - $val_td_CTM40_next)) * ($val_td_CTM38_next - $val_td_CTM38));

                    // раздел 3.1.2
                    // Находим теплосопротивление от начала стены до найденной плоскости ПМУ
                    $TD121 = $sum_td_CTM37 + ($cur_val_td_CTM37 * (($val_td_CTM40 - $val_td_CTM42) / ($val_td_CTM40 - $val_td_CTM40_next)));

                    // раздел 3.1.3
                    // Также надо установить Rn - сопротивление паропроницанию от внутренней поверхности в конструкции  до плоскости ПМУ
                    $TD122 = $sum_td_CTM36 + ($cur_val_td_CTM36 * (($val_td_CTM40 - $val_td_CTM42) / ($val_td_CTM40 - $val_td_CTM40_next)));
                }

                // раздел 3.1.4
                // Для дальнейших расчётов нам надо получить данные по материалам слоя, в котором нашли ПМУ

                $TD123 = $params['val_td_CTM02'][$arr_layout_val];
                $TD124 = $params['val_td_CTM20'][$arr_layout_val];
                $TD125 = $params['val_td_CTM12'][$arr_layout_val];
            }

            // раздел 3.1.5
            // Находим температуру в плоскости ПМУ за три периода: зимний, весенне-осенний и летний
            $TD126 = $TD05 - ((($TD05 - $TD45) / $TD100) * $TD121);
            $TD127 = $TD05 - ((($TD05 - $TD48) / $TD100) * $TD121);
            $TD128 = $TD05 - ((($TD05 - $TD51) / $TD100) * $TD121);
            if ($TD128 < $TD51) {
                $TD128 = $TD51;
            }

            // раздел 3.1.6
            // Находим необходимые переменные для итоговых формул

            $TD129 = ($TD06 / 100) * 1.84 * 100000000000 * (exp(-5330 / (273 + $TD05)));
            if ($TD129 < $TD43) {
                $TD129 = $TD43;
            }
            $TD130 = 1.84 * 100000000000 * (exp(-5330 / (273 + $TD45)));
            $TD131 = 1.84 * 100000000000 * (exp(-5330 / (273 + $TD48)));
            $TD132 = 1.84 * 100000000000 * (exp(-5330 / (273 + $TD51)));
            $TD133 = (($TD130 * $TD46) + ($TD131 * $TD49) + ($TD132 * $TD52)) / 12;
            $TD134 = 1.84 * 100000000000 * (exp(-5330 / (273 + $TD40)));
            $TD135 = $TD105 - $TD122;
//            echo "$TD135 = $TD105 - $TD122"."\n";
//            echo "$TD135 = $TD105 - $TD122";
            $TD135 = round($TD135,9);
//            echo "TD122";
//            echo "$TD122";
//            echo "\n TD105";
//            echo "$TD105";
//            echo "\n $calculation";
            $TD139 = '';
            if($TD135 == 0){
                $TD136 = 0;
                $TD140 = 0;
                $TD141 = 0;
            }else{
                $TD136 = (0.0024 * ($TD134 - $TD42) * $TD41) / $TD135;

                // раздел 3.1.7
                // Получение окончательного результата вычисления по данному варианту ("при ПМУ в толще слоя")
                $TD140 = (($TD129 - $TD133) * $TD135) / ($TD133 - $TD43);

                if ($calculation == 'calc_3_2') {
                    $TD139 = ($TD123_1 * ($TD124_1 / 2) * $TD125_1) + ($TD123_2 * ($TD124_2 / 2) * $TD125_2);
                    $TD141 = (0.0024 * $TD41 * ($TD129 - $TD134)) / ($TD139 + $TD136);
                } elseif ($calculation == 'calc_3_1') {
                    $TD141 = (0.0024 * $TD41 * ($TD129 - $TD134)) / (($TD123 * $TD124 * $TD125) + $TD136);
                }

            }


            $TD148 = '';
            $TD149 = '';
            if($tab_num == 3){
                if($selectCoefficient == 0.96
                    || $selectCoefficient == 0.82
                    || $selectCoefficient == 0.9
                    || $selectCoefficient == 0.91){
                    $TD148 = 0.0012 * ($TD129 - $TD42);
                }

//                $TD148 = 0.0012 * ($TD129 - $TD42);

                $TD149 = $TD100;
            }

            $content .= "<tr style='display: none'><td> </td></tr>" .
                "    <tr><td>TD110</td><td></td><td class='TD110'>TD110</td></tr>" .
                "    <tr><td>TD111</td><td></td><td class='TD111'>TD111</td></tr>" .
                "    <tr><td>TD112</td><td></td><td class='TD112'>TD112</td></tr>" .
                "    <tr><td>TD113</td><td></td><td class='TD113'>TD113</td></tr>" .
                "    <tr><td>TD114</td><td></td><td class='TD114'>TD114</td></tr>" .
                "    <tr><td>TD115</td><td></td><td class='TD115'>TD115</td></tr>" .
                "    <tr><td>TD116</td><td></td><td class='TD116'>TD116</td></tr>" .
                "    <tr><td>TD117</td><td></td><td class='TD117'>TD117</td></tr>" .
                "    <tr><td>TD118</td><td></td><td class='TD118'>TD118</td></tr>" .
                "    <tr><td>TD119</td><td></td><td class='TD119'>TD119</td></tr>" .
                "" .
                "    <tr><td>TD120</td><td></td><td class='TD120'> $TD120 </td></tr>" .
                "    <tr><td>TD121</td><td>R <span class='small'>до пму</span></td><td class='TD121'> $TD121 </td></tr>" .
                "    <tr><td>TD122</td><td>R <span class='small'>n</span></td><td class='TD122'> $TD122 </td></tr>" .
                "    <tr><td>TD123</td><td>ρ <span class='small'>w</span></td><td class='TD123'> $TD123 </td></tr>" .
                "    <tr><td>TD124</td><td>δ <span class='small'>w</span></td><td class='TD124'> $TD124 </td></tr>" .
                "    <tr><td>TD125</td><td>∆ <span class='small'>w</span></td><td class='TD125'> $TD125 </td></tr>" .
                "    <tr><td>TD126</td><td>t <span class='small'>пму зима</span></td><td class='TD126'> $TD126 </td></tr>" .
                "    <tr><td>TD127</td><td>t <span class='small'>пму весна-осень</span></td><td class='TD127'> $TD127 </td></tr>" .
                "    <tr><td>TD128</td><td>t <span class='small'>пму лето</span></td><td class='TD128'> $TD128 </td></tr>" .
                "    <tr><td>TD129</td><td>е <span class='small'>в</span></td><td class='TD129'> $TD129 </td></tr>" .
                "    <tr><td>TD130</td><td>Е <span class='small'>зима</span></td><td class='TD130'> $TD130 </td></tr>" .
                "    <tr><td>TD131</td><td>Е <span class='small'>весна осень</span></td><td class='TD131'> $TD131 </td></tr>" .
                "    <tr><td>TD132</td><td>Е <span class='small'>лето</span></td><td class='TD132'> $TD132 </td></tr>" .
                "    <tr><td>TD133</td><td>E <span class='small'></span></td><td class='TD133'> $TD133 </td></tr>" .
                "    <tr><td>TD134</td><td>E <span class='small'>0</span></td><td class='TD134'> $TD134 </td></tr>" .
                "    <tr><td>TD135</td><td>R <span class='small'>п,н</span></td><td class='TD135'> $TD135 </td></tr>" .
                "    <tr><td>TD136</td><td>ɳ <span class='small'></span></td><td class='TD136'> $TD136 </td></tr>" .
                "    <tr><td>TD139</td><td> <span class='small'></span></td><td class='TD139'> $TD139 </td></tr>" .
                "    <tr><td>TD140</td><td>R <span class='small'>n1</span><span class='small' style='position: relative; top: -7px;'>тр</span></td><td class='TD140'> $TD140 </td></tr>" .
                "    <tr><td>TD141</td><td>R <span class='small'>n2</span><span class='small' style='position: relative; top: -7px;'>тр</span></td><td class='TD141'> $TD141 </td></tr>" .
                "" .
                "    <tr><td>TD142</td><td></td><td class='TD142'>TD142</td></tr>" .
                "    <tr><td>TD143</td><td></td><td class='TD143'>TD143</td></tr>" .
                "    <tr><td>TD144</td><td></td><td class='TD144'>TD144</td></tr>" .
                "    <tr><td>TD145</td><td></td><td class='TD145'>TD145</td></tr>" .
                "    <tr><td>TD146</td><td></td><td class='TD146'>TD146</td></tr>" .
                "    <tr><td>TD147</td><td></td><td class='TD147'>TD147</td></tr>" .
                "    <tr><td>TD148</td><td>Требуемое значение паропроницанию для КРОВЛИ</td><td class='TD148'>$TD148</td></tr>" .
                "    <tr><td>TD149</td><td></td><td class='TD149'>$TD149</td></tr>" .
                "    <tr><td>TD150</td><td></td><td class='TD150'>TD150</td></tr>" .
                "    <tr><td>TD151</td><td></td><td class='TD151'>TD151</td></tr>" .
                "    <tr><td>TD152</td><td></td><td class='TD152'>TD152</td></tr>" .
                "    <tr><td>TD153</td><td></td><td class='TD153'>TD153</td></tr>" .
                "    <tr><td>TD154</td><td></td><td class='TD154'>TD154</td></tr>" .
                "    <tr><td>TD155</td><td></td><td class='TD155'>TD155</td></tr>" .
                "    <tr><td>TD156</td><td></td><td class='TD156'>TD156</td></tr>" .
                "    <tr><td>TD159</td><td></td><td class='TD159'>TD159</td></tr>" .
                "" .
                "    <tr><td>TD170</td><td>Этажей в здании</td><td class='TD170'>TD170</td></tr>" .
                "    <tr><td>TD171</td><td>Малоэтажное индивидуальное здание</td><td class='TD171'>TD171</td></tr>" .
                "    <tr><td>TD172</td><td>Ожидаемое количество проживающих</td><td class='TD172'>TD172</td></tr>" .
                "    <tr><td>TD173</td><td>Тип энергии для отопления</td><td class='TD173'>TD173</td></tr>" .
                "    <tr><td>TD174</td><td>Тип оборудования нагрева (плита, водонагреватель и др</td><td class='TD174'>TD174</td></tr>" .
                "    <tr><td>TD175</td><td>Тип вентиляции (приток и удаление воздуха</td><td class='TD175'>TD175)</td></tr>" .
                "    <tr><td>TD176</td><td>Общая площадь стен наружного периметра</td><td class='TD176'>TD176</td></tr>" .
                "    <tr><td>TD178</td><td>Площадь кровли</td><td class='TD178'>TD178</td></tr>" .
                "    <tr><td>TD180</td><td>Площадь пола 1 этажа</td><td class='TD180'>TD180</td></tr>" .
                "    <tr><td>TD182</td><td>Площадь окон</td><td class='TD182'>TD182</td></tr>" .
                "    <tr><td>TD183</td><td>Теплосопротивление ( R ) оконной конструкции</td><td class='TD183'>TD183</td></tr>" .
                "    <tr><td>TD184</td><td>Тип оконного переплёта</td><td class='TD184'>TD184</td></tr>" .
                "    <tr><td>TD186</td><td>Площадь фонарей</td><td class='TD186'>TD186</td></tr>" .
                "    <tr><td>TD188</td><td>Теплосопротивление ( R ) фонаря</td><td class='TD188'>TD188</td></tr>" .
                "    <tr><td>TD200</td><td>Жилая комната - площадь по полу</td><td class='TD200'>TD200</td></tr>" .
                "    <tr><td>TD201</td><td>Жилая комната - площадь фасадной стены</td><td class='TD201'>TD201</td></tr>" .
                "    <tr><td>TD202</td><td>Кухня - площадь по полу</td><td class='TD202'>TD202</td></tr>" .
                "    <tr><td>TD203</td><td>Кухня - площадь фасадной стены</td><td class='TD203'>TD203</td></tr>" .
                "    <tr><td>TD204</td><td>Туалет - площадь по полу</td><td class='TD204'>TD204</td></tr>" .
                "    <tr><td>TD205</td><td>Туалет - площадь фасадной стены</td><td class='TD205'>TD205</td></tr>" .
                "    <tr><td>TD206</td><td>Ванная или Совмещённый санузел - площадь по полу</td><td class='TD206'>TD206</td></tr>" .
                "    <tr><td>TD207</td><td>Ванная или Совмещённый санузел -  площадь фасадной стены</td><td class='TD207'>TD207</td></tr>" .
                "    <tr><td>TD208</td><td>Помещения для отдыха и учебных занятий - площадь по полу</td><td class='TD208'>TD208</td></tr>" .
                "    <tr><td>TD209</td><td>Помещения для отдыха и учебных занятий - площадь  фасадной стены</td><td class='TD209'>TD209</td></tr>" .
                "    <tr><td>TD210</td><td>Межквартирный коридор - площадь по полу</td><td class='TD210'>TD210</td></tr>" .
                "    <tr><td>TD211</td><td>Межквартирный коридор - площадь фасадной стены</td><td class='TD211'>TD211</td></tr>" .
                "    <tr><td>TD212</td><td>Вестибюль, лестничная клетка - площадь по полу</td><td class='TD212'>TD212</td></tr>" .
                "    <tr><td>TD213</td><td>Вестибюль, лестничная клетка - площадь фасадной стены</td><td class='TD213'>TD213</td></tr>" .
                "    <tr><td>TD214</td><td>Кладовые - площадь по полу</td><td class='TD214'>TD214</td></tr>" .
                "    <tr><td>TD215</td><td>Кладовые - площадь фасадной стены</td><td class='TD215'>TD215</td></tr>";


        $result[$room] = $content;
    }

return json_encode($result);

   }

    public static function calc_4_tab($params)
    {

        $rooms = [
            'living',
            'kitchen',
            'toilet',
            'bathroom',
            'rest_room',
            'hall',
            'vestibule',
            'pantry'
        ];
        $tab_cookie = User::getCookie('data1');
        $tab_num = $params['tab_num'];
        if ($tab_cookie != null) {
            $data = (json_decode($tab_cookie));
        }
        foreach($rooms as $room) {
            if ($tab_cookie != null) {
                if($tab_num == 2) {
                    if (isset($data->living_room_w) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_w) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_w) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_w) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_w) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_w) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_w) && $room == 'pantry') {
                        continue;
                    }

                }elseif($tab_num == 3){
                    if (isset($data->living_room_r) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_r) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_r) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_r) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_r) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_r) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_r) && $room == 'pantry') {
                        continue;
                    }
                }elseif($tab_num == 4){
                    if (isset($data->living_room_b) && $room == 'living') {
                        continue;
                    }
                    if (isset($data->kitchen_b) && $room == 'kitchen') {
                        continue;
                    }
                    if (isset($data->toilet_b) && $room == 'toilet') {
                        continue;
                    }
                    if (isset($data->bathroom_b) && $room == 'bathroom') {
                        continue;
                    }
                    if (isset($data->rest_room_b) && $room == 'rest_room') {
                        continue;
                    }
                    if (isset($data->hall_b) && $room == 'hall') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'vestibule') {
                        continue;
                    }
                    if (isset($data->vestibule_b) && $room == 'pantry') {
                        continue;
                    }
                }


                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'rest_room'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'hall'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'vestibule'){
                    continue;
                }
                if(isset($data->TD171) && $data->TD171 == 'yes'  && $room == 'pantry'){
                    continue;
                }
            }


            $selectCoefficient = $params['selectCoefficient'];

            $TD101 = $params['TD101'];
            $TD108 = $params['TD108'];
            $TD109 = $params['TD109'];
            // 4-tab
            $content = "<b>1. Соответствие фактического теплосопротивления данной стеновой конструкции нормативным требованиям:</b>";
            $content .= "<br>Применён коэффициент теплотехнической однородности типа выбранной стеновой конструкции - ($TD101) в соответствии с ГОСТ Р 54851-2011</br>";
            $content .= "<br>Применён коэффициент теплотехнической однородности особенностей здания - ($TD108) в соответствии с ГОСТ Р 54851-2011 </br>";

//            $content_user_101 = $TD101;
//            $content_user_108 = $TD108;
//            $content_user_109 = $TD109;
            // 1.1 Находим температуру внутренней поверхности стены:


            $TD122 = $params['TD122'];
            $TD140 = $params['TD140'];
            $TD141 = $params['TD141'];

            $TD05 = $params['TD05'];
            $CTM39 = $params['CTM39_1'];
            $TD54 = $params['TD54'];

            $TD55 = $params['TD55'];
            $TD56 = $params['TD56'];
            $TD57 = $params['TD57'];
            $TD58 = $params['TD58'];
//dd($TD55);
            $TD100 = $params['TD100'];
            $TD148 = $params['TD148'];

   $text_td = $params['text_td07'];
            if($room == 'kitchen'){
                $TD54 = $params['TD82'];
                $TD55 = $params['TD82_1'];
                $TD56 = $params['TD82_2'];
                $TD05 = $params['TD09'];
                $CTM39 = $params['CTM39_2'];
                $TD122 = $params['TD122_2'];
                $TD140 = $params['TD140_2'];
                $TD141 = $params['TD141_2'];
                $TD100 = $params['TD100_2'];
                $text_td = $params['text_td11'];
                        $TD148 = $params['TD148_2'];
            }
            if($room == 'toilet'){
                $TD54 = $params['TD83'];
                $TD55 = $params['TD83_1'];
                $TD56 = $params['TD83_2'];
                $TD05 = $params['TD13'];
                $CTM39 = $params['CTM39_3'];
                $TD122 = $params['TD122_3'];
                $TD140 = $params['TD140_3'];
                $TD141 = $params['TD141_3'];
                $TD100 = $params['TD100_3'];
                $text_td = $params['text_td15'];
                        $TD148 = $params['TD148_3'];

            }
            if($room == 'bathroom'){
                $TD54 = $params['TD84'];
                $TD55 = $params['TD84_1'];
                $TD56 = $params['TD84_2'];
                $TD05 = $params['TD17'];
                $CTM39 = $params['CTM39_4'];
                $TD122 = $params['TD122_4'];
                $TD140 = $params['TD140_4'];
                $TD141 = $params['TD141_4'];
                $TD100 = $params['TD100_4'];
                $text_td = $params['text_td19'];
                        $TD148 = $params['TD148_4'];
            }
            if($room == 'rest_room'){
                $TD54 = $params['TD85'];
                $TD55 = $params['TD85_1'];
                $TD56 = $params['TD85_2'];
                $TD05 = $params['TD21'];
                $CTM39 = $params['CTM39_5'];
                $TD122 = $params['TD122_5'];
                $TD140 = $params['TD140_5'];
                $TD141 = $params['TD141_5'];
                $TD100 = $params['TD100_5'];
                $text_td = $params['text_td23'];
                        $TD148 = $params['TD148_5'];
            }
            if($room == 'hall'){
                $TD54 = $params['TD86'];
                $TD55 = $params['TD86_1'];
                $TD56 = $params['TD86_2'];
                $TD05 = $params['TD25'];
                $CTM39 = $params['CTM39_6'];
                $TD122 = $params['TD122_6'];
                $TD140 = $params['TD140_6'];
                $TD141 = $params['TD141_6'];
                $TD100 = $params['TD100_6'];
                $text_td = $params['text_td27'];
                        $TD148 = $params['TD148_6'];
            }
            if($room == 'vestibule'){
                $TD54 = $params['TD87'];
                $TD55 = $params['TD87_1'];
                $TD56 = $params['TD87_2'];
                $TD05 = $params['TD29'];
                $CTM39 = $params['CTM39_7'];
                $TD122 = $params['TD122_7'];
                $TD140 = $params['TD140_7'];
                $TD141 = $params['TD141_7'];
                $TD100 = $params['TD100_7'];
                $text_td = $params['text_td31'];
                        $TD148 = $params['TD148_7'];
            }
            if($room == 'pantry'){
                $TD54 = $params['TD88'];
                $TD55 = $params['TD88_1'];
                $TD56 = $params['TD88_2'];
                $TD05 = $params['TD33'];
                $CTM39 = $params['CTM39_8'];
                $TD122 = $params['TD122_8'];
                $TD140 = $params['TD140_8'];
                $TD141 = $params['TD141_8'];
                $TD100 = $params['TD100_8'];
                $text_td = $params['text_td35'];
                        $TD148 = $params['TD148_8'];
            }


            $TD115 = $CTM39;
            $TD116 = $TD05 - $TD115;


//dd($params);

            $TD59 = $params['TD59'];
            $TD61 = $params['TD61'];
            $TD03 = $params['TD03'];
            $term = $params['term'];
            $bearing = $params['bearing'];
            $CPO20 = 0.95;
            if ($term > $bearing) {
                $CPO20 = 0.9;
                $content .= "<br>Применён коэффициент 0,9";
            } else {
                $content .= "<br>Применён коэффициент 0,95";
            }
//
//            $TD100 = $params['TD100'];
//            $TD101 = $params['TD101'];
//            $TD108 = $params['TD108'];
//            $TD109 = $params['TD109'];

            $TD108 = $CPO20;
            $TD145 = $TD100 * $TD101 * $TD03 * $TD59 * $TD108 * $TD109;

            if($tab_num == 3) {
                $TD149 = $TD100;
                $TD145 = $TD100 * $TD101 * $TD03 * $TD61 * $TD109;

            }elseif($tab_num == 4){

                $TD149 = $TD100;
                $TD145 = $TD100 * $TD101 * $TD03 * $TD61 * $TD109;
            }

//        $TD145 = $TD100 * $TD101 * $TD03 * $TD59 * $CPO20 * $TD108 * $TD109;
//            $TD145_ = "$TD100 * $TD101 * $TD03 * $TD59 * $TD108 * $TD109";
//        3.9913157728147 * 0.87 * 1 * 1 * 0.95 * 0.98 * 1
            $TD117 = 0;
            // 1.2 Находим точку росы в помещении
            $params_117['val'] = $TD05;
            $result_117 = self::getDewData($params_117);
            if ($result_117 != null) {
                $TD117 = $result_117->per_55;
            }
//            $select_building = $params['selectBuilding'];
            // 1.3 Необходимо в выбор прописать "Нормируемый температурный перепад, °С,"
//                $TD145 = $TD100 * $TD101 * $TD03 * $TD59 * $CPO20 * $TD108 * $TD109;
//            if ($select_building == 4) {
//                $TD118 = $TD05 - $TD117;
//            } elseif ($select_building == 3) {
//                $TD118 = 4.5;
//            } else {
                $TD118 = 4;
//            }

            if($tab_num == 3) {
                $TD54 = $TD55;
            }elseif($tab_num == 4){
                $TD54 = $TD56;
            }
            $content .= "<table class='table'>" .
                "    <tr>" .
                "        <td>ТРЕБУЕМОЕ теплосопротивление конструкции:</td>" .
                "        <td>$TD54</td>" .
                "        <td>(м·°С)/Вт</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>При этом применён понижающий коэффициент:</td>" .
                "        <td>$TD59</td>" .
                "        <td></td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Приведённое теплосопротивление конструкции:</td>" .
                "        <td>$TD145</td>" .
                "        <td>(м·°С)/Вт</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Требуемая удельная теплозащитная характеристика здания:</td>" .
                "        <td>0.562</td>" .
                "        <td>Вт/(м·°С)</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Фактическая удельная теплозащитная характеристика здания:</td>" .
                "        <td>0.503</td>" .
                "        <td>Вт/(м·°С)</td>" .
                "    </tr>" .
                "</table>";

            $content_user_1 = '';
            $content_user_indicator_1 = '';
            $TD145 = round($TD145, 3);

if($tab_num == 3){
    $TD55 = round($TD55, 3);

    $is_accepted_3 = 'true';
    if ($TD145 < $TD55) {
        // 2.2.1
        $content .= "<br>Фактическое теплосопротивление  ($TD145) меньше нормируемого ($TD55).  Данная конструкция 
<span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content .= "<br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру, 
увеличить толщину слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при 
её наличии увеличить толщину";
        $is_accepted_3 = 'false';
        $content_user_indicator_1 = 'red';
        $content_user_1 = "<br>Приведённое теплосопротивление ($TD145) равно или больше нормируемого ($TD55). Данная
 конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.
 <br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру, увеличить толщину 
 слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при её наличии увеличить толщину";
    } else {
        // 2.2.2
        $content .= "<br>Фактическое теплосопротивление  ($TD145) равно или больше нормируемого ($TD55).  Данная
 конструкция <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_1 = "<br>Приведённое теплосопротивление  ($TD145) равно или больше нормируемого ($TD55).
  Данная конструкция <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_indicator_1 = 'green';
    }

}elseif($tab_num == 4){
    $is_accepted_4 = 'true';
    $TD54 = round($TD54, 3);
    $TD56 = round($TD56, 3);

    if ($TD145 < $TD56) {
        // 2.2.1
        $content .= "<br>Фактическое теплосопротивление  ($TD145) меньше нормируемого ($TD56).  Данная конструкция 
<span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content .= "<br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру,
 увеличить толщину слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при 
 её наличии увеличить толщину";
        $is_accepted_4 = 'false';
        $content_user_indicator_1 = 'red';
        $content_user_1 = "<br>Приведённое теплосопротивление ($TD145) меньше нормируемого ($TD56).  Данная конструкция 
<span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам. 
<br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру, увеличить толщину слоёв 
конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при её наличии увеличить толщину";
    } else {
        // 2.2.2
        $content .= "<br>Фактическое теплосопротивление  ($TD145) равно или больше нормируемого ($TD56).  Данная конструкция
 <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_1 = "<br>Приведённое теплосопротивление  ($TD145) равно или больше нормируемого ($TD56).  Данная
 конструкция <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_indicator_1 = 'green';

    }

}else{


    // 1.4 Сравниваем найденную точку росы в помещении с температурой внутренней поверхности стены и выдаём результат:
    $is_accepted_2 = 'true';

    if ($TD145 < $TD54) {
        // 2.2.1
        $content .= "<br>Фактическое теплосопротивление  ($TD145) меньше нормируемого ($TD54).  Данная стеновая конструкция 
<span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content .= "<br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру,
 увеличить толщину слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при её наличии увеличить толщину";
        $is_accepted_2 = 'false';
        $content_user_1 = "<br>Приведённое теплосопротивление  ($TD145) меньше нормируемого ($TD54).  Данная стеновая 
конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ</span> строительным нормативам.
        <br>Для соответствия строительным нормам следует подобрать другой состав конструкции, к примеру, увеличить
         толщину слоёв конструкции, применить материалы с меньшей теплопроводностью, добавить теплоизоляцию или при её 
         наличии увеличить толщину";
        $content_user_indicator_1 = 'red';
    } else {
        // 2.2.2
        $content .= "<br>Фактическое теплосопротивление  ($TD145) равно или больше нормируемого ($TD54).  Данная
 стеновая конструкция <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_1 .= "<br>Приведённое теплосопротивление  ($TD145) равно или больше нормируемого ($TD54). 
 Данная стеновая конструкция <span class='green'>СООТВЕТСТВУЕТ</span> строительным нормативам.";
        $content_user_indicator_1 = 'green';

    }

}

            // 2. Соответствие данной стеновой конструкции нормативу по санитарно-гигиеническому требованию:
            $content .= "<br><br><b>2. Соответствие данной стеновой конструкции нормативу по санитарно-гигиеническому
 требованию:</b>";
            // 2.1
            // var TD145 = TD100 * TD101 * TD03 * TD59;

            // 2.2


            $content .= "<table class='table'>" .
                "    <tr>" .
                "        <td>Температура помещения:</td>" .
                "        <td>$TD05</td>" .
                "        <td>°С</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Температура внутренней поверхности стены:</td>" .
                "        <td class='TD115'>$TD115</td>" .
                "        <td>°С</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Требуемая разница между температур. не более</td>" .
                "        <td class='TD118'>$TD118</td>" .
                "        <td>°С</td>" .
                "    </tr>" .
                "    <tr>" .
                "        <td>Температура росы (справочно):</td>" .
                "        <td class='TD117'>$TD117</td>" .
                "        <td>°С</td>" .
                "    </tr>" .
                "</table>";

            $content_user_2 = '';
            $content_user_indicator_2 = '';
            $TD115 = round($TD115, 3);
            $TD117 = round($TD117, 3);
            $TD118 = round($TD118, 3);

            if ($TD116 < $TD118) {
                // 1.4.2

                $content .= "<br>Разница температур помещения и внутренней поверхности стены не превышает установленное
 нормами значение.";
                $content .= "<br>По Санитарно-гигиеническому требованию данная конструкция <span class='green'>СООТВЕТСТВУЕТ
 ТРЕБОВАНИЯМ</span> СП 50.13330.2012.";
                $content_user_2 = "<br>Разница температур помещения и внутренней поверхности стены не превышает 
установленное нормами значение.
                <br>По Санитарно-гигиеническому требованию данная  конструкция <span class='green'>СООТВЕТСТВУЕТ ТРЕБОВАНИЯМ</span> СП 50.13330.2012.";
                $content_user_indicator_2 = 'green';
            } else {
                // 1.4.1
                $content .= "<br>В данной стеновой конструкции точка росы образуется при ($TD117), температура внутренней 
поверхности стены - ($TD115).";
                $content .= "<br>Допустимая разница температур между температурой внутренней поверхности стены - не более ($TD118).";
                $content .= "<br>Данная конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ ТРЕБОВАНИЯМ</span> СП 50.13330.2012.";
                $content_user_2 = "<br>В данной стеновой конструкции точка росы образуется при ($TD117), температура 
внутренней поверхности стены - ($TD115).
                <br>Допустимая разница температур между температурой внутренней поверхности стены - не более ($TD118).
                <br>Данная конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ ТРЕБОВАНИЯМ</span> СП 50.13330.2012.";
                $content_user_indicator_2 = 'red';
            }
//dd($selectCoefficient);


// ===================================================================
// ===================================================================
// ===================================================================
// ===================================================================
// ===================================================================

//            dd($params);
                $lay_count = $params['numb'];

            $content_user_3 = '';
            $content_user_indicator_3 = '';


            $construction = "стена";
            if($tab_num == 3)$construction = "кровля";
            if($tab_num == 4)$construction = "пол";


            if($lay_count == 1){

                $content .= "у вас 1 слой и с влагонакоплением в такой конструкции нет проблем";
                $content_user_3 .= "у вас 1 слой и с влагонакоплением в такой конструкции нет проблем";
                $content_user_indicator_3 = 'green';


            }else{
                if($tab_num == 4 && $selectCoefficient == 0.9) {
                    $content .= "<br><br><b>3. В данном случае обязательно выполняется надёжная и долговечная гидроизоляция
 полов по грунтам (фундаментной плиты), расчёт на переувлажнение не выполняется.</b>";
                }else{
                    // 3 Результат расчёта по возможному влагонакоплению в конструкции
                    $content .= "<br><br><b>3. Соответствие нормативу по защите данной стеновой конструкции от переувлажнения:</b>";



                    $content .= "<table class='table'>" .
                        "    <tr>" .
                        "        <td>Сопротивление паропроницанию от внутренней поверхности до плоскости максимального увлажнения:</td>" .
                        "        <td>$TD122</td>" .
                        "        <td>(м·ч·Па)/мг</td>" .
                        "    </tr>" .
                        "    <tr>" .
                        "        <td>Требуемое сопротивление паропроницанию (из условия недопустимости накопления влаги 
в ограждающей конструкции за годовой период)</td>\n" .
                        "        <td>$TD140</td>" .
                        "        <td>(м·ч·Па)/мг</td>" .
                        "    </tr>" .
                        "    <tr>" .
                        "        <td>Требуемого сопротивления паропроницанию, (из условия ограничения влаги в ограждающей 
конструкции за период с отрицательными средними месячными температурами наружного воздуха)</td>\n" .
                        "        <td>$TD141</td>" .
                        "        <td>(м·ч·Па)/мг</td>" .
                        "    </tr>" .
                        "</table>";
                }

                if ($TD140 > $TD141) {
                    $greatest_140_141 = $TD140;
                } else {
                    $greatest_140_141 = $TD141;
                }

                if ($TD122 > $greatest_140_141) {
                    // Условие1
                    $content .= "<br>Настоящий расчёт <span class='green'>СООТВЕТСТВУЕТ</span> требованию недопустимости 
влагонакопления в данной расчитываемой конструкции";
                    $content_user_3 = "<br>Настоящий расчёт <span class='green'>СООТВЕТСТВУЕТ</span> требованию 
недопустимости влагонакопления в данной расчитываемой конструкции";
                    $content_user_indicator_3 = 'green';

                    // Условие3
                    if (($TD122 - $greatest_140_141) <= ($TD122 / 10)) {
                        $content .= "<br>По анализу данных в вашем расчёте порог несоответствия нормативам очень близок. 
Во избежание возможных проблем рекомендуем немного улучшить конструкцию:";
                        $content .= "<br>например, на внутреннюю часть поставить материалы с более низкой паропроницаемостью, 
на наружную часть - с более высокой, при наличии слоя теплоизоляции - увеличить толщину слоя";
                        $content_user_3 .= "<br>По анализу данных в вашем расчёте порог несоответствия нормативам очень
 близок. Во избежание возможных проблем рекомендуем немного улучшить конструкцию:";
                        $content_user_3 .= "<br>например, на внутреннюю часть поставить материалы с более низкой 
паропроницаемостью, на наружную часть - с более высокой, при наличии слоя теплоизоляции - увеличить толщину слоя";

                    }

                } else {
                    // Условие2
                    $content .= "<br>Настоящий расчёт <span class='red'>НЕ СООТВЕТСТВУЕТ</span> требованию недопустимости влагонакопления в данной расчитываемой конструкции. Конструкцию необходимо изменить, т.к. $construction будет отсыревать.";
                    $content_user_3 = "<br>Настоящий расчёт <span class='red'>НЕ СООТВЕТСТВУЕТ</span> требованию недопустимости влагонакопления в данной расчитываемой конструкции. Конструкцию необходимо изменить, т.к. $construction будет отсыревать.";
                    $content_user_indicator_3 = 'red';
                    $is_accepted_2 = 'false';
                    $is_accepted_3 = 'false';
                    $is_accepted_4 = 'false';

                }

                $TD149 = $TD100;
                $TD148 = round($TD148, 3);
                $TD149 = round($TD149, 3);

                if($tab_num == 3){

                    if($TD148 != 0) {
                        if ($TD149 > $TD148) {
                            $content .= "<br>В соответствии с пунктом 8.2 СП 50.13330-2012 сопротивление паропроницанию
 кровельной конструкции ($TD149) выше требуемого сопротивления ";
                            $content .= "<br>паропроницанию  ($TD148), поэтому рассчитываемая кровельная конструкция
 <span class='green'>СООТВЕТСТВУЕТ</span> требованиям данного пункта. ";
                            $content_user_3 .= "<br>В соответствии с пунктом 8.2 СП 50.13330-2012 сопротивление
 паропроницанию кровельной конструкции ($TD149) выше требуемого сопротивления ";
                            $content_user_3 .= "<br>паропроницанию  ($TD148), поэтому рассчитываемая кровельная
 конструкция <span class='green'>СООТВЕТСТВУЕТ</span> требованиям данного пункта. ";

                        } else {
                            $content .= "<br>В соответствии с пунктом 8.2 СП 50.13330-2012 сопротивление паропроницанию 
кровельной конструкции ($TD149) ниже требуемого сопротивления ";
                            $content .= "<br>паропроницанию  ($TD148), поэтому рассчитываемая кровельная конструкция 
<span class='red'>НЕ СООТВЕТСТВУЕТ</span> требованиям данного пункта. ";
                            $content .= "<br>Рекомендации: необходимо улучшить данную кровельную конструкцию, например,
 добавить пароизоляционную плёнку и (или) применить материалы 
с низкой паропроницаемостью со стороны помещения.";
                            $content_user_3 .= "<br>В соответствии с пунктом 8.2 СП 50.13330-2012 сопротивление
 паропроницанию кровельной конструкции ($TD149) ниже требуемого сопротивления ";
                            $content_user_3 .= "<br>паропроницанию  ($TD148), поэтому рассчитываемая кровельная
 конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ</span> требованиям данного пункта. ";
                            $content_user_3 .= "<br>Рекомендации: необходимо улучшить данную кровельную конструкцию, 
например, добавить пароизоляционную плёнку и (или) применить материалы 
с низкой паропроницаемостью со стороны помещения.";

                            $is_accepted_3 = 'false';

                        }
                    }
                    $alert = 'dry';

                    if($text_td == 'нормальный' || $text_td == 'мокрый') {

                        $vapor = $params['vapor'];
                        $thermal_mat_first = $params['thermal_mat_first'];
                        $bearing = $params['bearing'];

                        if ($vapor != 'false') {
                            if ($thermal_mat_first != false) {
                                if ($vapor < $thermal_mat_first && $thermal_mat_first < $bearing) {
                                    $alert = 'ok';
                                } elseif ($thermal_mat_first < $vapor) {
                                    $alert = 'not_where_needed';
                                }
                            } elseif ($vapor > $bearing) {
                                $alert = 'not_where_needed';
                            } elseif ($vapor < $bearing) {
                                $alert = 'ok';
                            }
                        } else {
                            $alert = 'not_vapor';
                        }
                    }


                    $content .= "<br>В кровельной конструкции в соответствии с пунктом 8.3. СП 50.13330-2012 для защиты от увлажнения теплоизоляционного слоя (утеплителя) в 
покрытиях зданий с влажным или мокрым режимом следует предусматривать пароизоляцию ниже теплоизоляционного слоя, которую следует учитывать при определении сопротивления
 паропроницанию покрытия в соответствии с 8.7.";

                    $content_user_3 .= "<br><br>В кровельной конструкции в соответствии с пунктом 8.3. СП 50.13330-2012 для защиты от увлажнения теплоизоляционного слоя (утеплителя) в 
покрытиях зданий с влажным или мокрым режимом следует предусматривать пароизоляцию ниже теплоизоляционного слоя, которую следует учитывать при определении сопротивления
 паропроницанию покрытия в соответствии с 8.7.";
//                $alert = User::getCookie('alert');
//                    $data_vapor = (json_decode($alert));
                    if($alert == 'not_vapor') {
                        $content .= "<br> <span class='red'>В данной конструкции режим эксплуатации помещения - не сухой 
 и пароизоляционный слой отсутствует. Необходимо установить пароизоляцию в соответствии указанными требованиями. </span>";
                        $content_user_3 .= "<br><br> <div style='display: flex'>
<div class='indicator' style='background-color: red;width: 50px;'></div>
<span class='red'>В данной конструкции режим эксплуатации помещения - не сухой 
 и пароизоляционный слой отсутствует. Необходимо установить пароизоляцию в соответствии указанными требованиями. </span></div>";
                        $is_accepted_3 = 'false';
                    } elseif ($alert == 'not_where_needed') {
                        $content .= "<br> <span class='red'>В данной конструкции  режим эксплуатации помещения -
 не сухой  и пароизоляционный слой имеется, 
но он расположен не правильно. Следует установить его со стороны помещения до теплоизоляционного слоя. </span>";
                        $content_user_3 .= "<br> <br> <div style='display: flex'>
<div class='indicator' style='background-color: red;width: 50px;'></div>
<span class='red'>В данной конструкции  режим эксплуатации помещения - не сухой  и пароизоляционный слой имеется, 
но он расположен не правильно. Следует установить его со стороны помещения до теплоизоляционного слоя. </span></div>";
                        $is_accepted_3 = 'false';
                    } elseif ($alert == 'ok') {
                        $content .= "<br> <span class='green'>В данной конструкции пароизоляционный слой установлен в соответствии с нормативными требованиями п.8.3. </span>";
                        $content_user_3 .= "<br> <br> <div style='display: flex'>
 <div class='indicator' style='background-color: green;width: 50px;'></div>
 <span class='green'>В данной конструкции пароизоляционный слой установлен в соответствии с нормативными требованиями п.8.3. </span></div>";
                    } elseif ($alert == 'dry') {
                        $content .= "<br> <span class='green'>В данной конструкции  режим эксплуатации помещения - сухой, поэтому это условие не проверяется. </span>";
                        $content_user_3 .= "<br> <br> <div style='display: flex'>
<div class='indicator' style='background-color: green;width: 50px;'></div>
<span class='green'>В данной конструкции  режим эксплуатации помещения - сухой, поэтому это условие не проверяется. </span></div>";
                    }


                }
            }

//==============================================================
//==============================================================
//==============================================================
//==============================================================
//==============================================================

            if($tab_num == 2){
                Cookie::delete('is_accepted_2');
                Cookie::set('is_accepted_2', $is_accepted_2);
            }elseif($tab_num == 3){
                Cookie::delete('is_accepted_3');
                Cookie::set('is_accepted_3', $is_accepted_3);
            }elseif($tab_num == 4){
                Cookie::delete('is_accepted_4');
                Cookie::set('is_accepted_4', $is_accepted_4);
            }


//$content .="<div style='display: none'>$TD145_</div>";
//            $result = [
//                "content" => $content,
//                "TD115" => $TD115,
//                "TD116" => $TD116,
//                "TD117" => $TD117,
//                "TD118" => $TD118,
//                "TD145" => $TD145
//            ];

            $result["content"][$room] = $content;
            $result["TD115"][$room] = round($TD115, 3);
            $result["TD116"][$room] = round($TD116, 3);
            $result["TD117"][$room] = round($TD117, 3);
            $result["TD118"][$room] = round($TD118, 3);
            $result["TD145"][$room] = round($TD145, 3);

            $result["user_101"][$room] = round($TD101, 3);
            $result["user_108"][$room] = round($TD108, 3);
            $result["user_109"][$room] = round($TD109, 3);
            $result["user_03"][$room] = round($TD03, 3);
            $result["user_59"][$room] = round($TD59, 3);
            $result["user_54"][$room] = round($TD54, 3);
            $result["user_145"][$room] = round($TD145, 3);
            $result["user_05"][$room] = round($TD05, 3);
            $result["user_115"][$room] = round($TD115, 3);
            $result["user_118"][$room] = round($TD118, 3);
            $result["user_117"][$room] = round($TD117, 3);
            $result["user_122"][$room] = round($TD122, 3);
            $result["user_140"][$room] = round($TD140, 3);
            $result["user_141"][$room] = round($TD141, 3);
            $result["content_user_1"][$room] = $content_user_1;
            $result["content_user_2"][$room] = $content_user_2;
            $result["content_user_3"][$room] = $content_user_3;
            $result["content_user_indicator_1"][$room] = $content_user_indicator_1;
            $result["content_user_indicator_2"][$room] = $content_user_indicator_2;
            $result["content_user_indicator_3"][$room] = $content_user_indicator_3;





        }
        return json_encode($result);
    }

    public static function writeCookie($params)
    {

//        dd($params);
//        dd(self::$materialRepository);
        $json = json_encode($params);
        Cookie::delete($params['cookie_name']);
        Cookie::set($params['cookie_name'], $json);
//        if($params['cookie_name'] == 'data1'
////            || $params['cookie_name'] == 'data2'
////            || $params['cookie_name'] == 'data3'
////            || $params['cookie_name'] == 'data4'
//        ){
//            $con = substr($params['cookie_name'],-1);
//            Cookie::delete("tab_$con");
//            Cookie::set("tab_$con", $json);
//        }

        return $json;
    }
    public static function writeCookieSimple($params)
    {
        Cookie::delete($params['cookie_name']);
        Cookie::set($params['cookie_name'], $params['cookie_data']);
        return $params['cookie_data'];
    }

    public static function deleteCookie($params)
    {

        foreach ($params as $item){
            Cookie::delete($item);
        }
        return 'deleted';
    }


    public static function calcHeating($params)
    {
        $data1 = json_decode(Cookie::get('data1'));
        $data2 = json_decode(Cookie::get('data2'));
        $data3 = json_decode(Cookie::get('data3'));
        $data4 = json_decode(Cookie::get('data4'));
        $data5 = json_decode(Cookie::get('data5'));
        $data6 = json_decode(Cookie::get('data6'));
        $building = json_decode(Cookie::get('building'));
//        debug($data1);
//        debug($data2);
//        debug($data3);
//        debug($data5);
//        debug($data6);
//        debug_die($data4);


        $TD230 = 0;
        $TD231 = 0;
        $TD240 = 0;
        $TD241 = 0;
        $TD250 = 0;
        $TD251 = 0;
        $TD260 = 0;
        $TD261 = 0;
        $TD270 = 0;
        $TD271 = 0;

        $TD201 = 0;
        $TD203 = 0;
        $TD205 = 0;
        $TD207 = 0;
        $TD209 = 0;
        $TD211 = 0;
        $TD213 = 0;
        $TD215 = 0;

        $TD216 = 0;
        $TD217 = 0;
        $TD218 = 0;
        $TD219 = 0;
        $TD220 = 0;
        $TD221 = 0;
        $TD223 = 0;
        $TD225 = 0;

        $TD200 = 0;
        $TD202 = 0;
        $TD204 = 0;
        $TD206 = 0;
        $TD208 = 0;
        $TD210 = 0;
        $TD212 = 0;
        $TD214 = 0;

        $TD280 = 0;
        $TD281 = 0;
        $TD282 = 0;
        $TD283 = 0;
        $TD284 = 0;
        $TD285 = 0;
        $TD286 = 0;
        $TD287 = 0;

        foreach ($data1 as $key => $value) {
            $$key = $value;
        }
        foreach ($data2 as $key => $value) {
            $$key = $value;
        }
        foreach ($data5 as $key => $value) {
            $$key = $value;
        }
        foreach ($data6 as $key => $value) {
            $$key = $value;
        }

        if(empty($TD230)){
        $TD230 = 0;
        }
        if(empty($TD231)){
            $TD231 = 0;
        }
        if(empty($TD240)){
            $TD240 = 0;
        }
        if(empty($TD241)){
            $TD241 = 0;
        }
        if(empty($TD250)){
            $TD250 = 0;
        }
        if(empty($TD251)){
            $TD251 = 0;
        }
        if(empty($TD260)){
            $TD260 = 0;
        }
        if(empty($TD261)){
            $TD261 = 0;
        }
        if(empty($TD270)){
            $TD270 = 0;
        }
        if(empty($TD271)){
            $TD271 = 0;
        }

        if(empty($TD201)){
            $TD201 = 0;
        }
        if(empty($TD203)){
            $TD203 = 0;
        }
        if(empty($TD205)){
            $TD205 = 0;
        }
        if(empty($TD207)){
            $TD207 = 0;
        }
        if(empty($TD209)){
            $TD209 = 0;
        }
        if(empty($TD211)){
            $TD211 = 0;
        }
        if(empty($TD213)){
            $TD213 = 0;
        }
        if(empty($TD215)){
            $TD215 = 0;
        }

        if(empty($TD216)){
            $TD216 = 0;
        }
        if(empty($TD217)){
            $TD217 = 0;
        }
        if(empty($TD218)){
            $TD218 = 0;
        }
        if(empty($TD219)){
            $TD219 = 0;
        }
        if(empty($TD220)){
            $TD220 = 0;
        }
        if(empty($TD221)){
            $TD221 = 0;
        }
        if(empty($TD223)){
            $TD223 = 0;
        }
        if(empty($TD225)){
            $TD225 = 0;
        }

        if(empty($TD200)){
            $TD200 = 0;
        }
        if(empty($TD202)){
            $TD202 = 0;
        }
        if(empty($TD204)){
            $TD204 = 0;
        }
        if(empty($TD206)){
            $TD206 = 0;
        }
        if(empty($TD208)){
            $TD208 = 0;
        }
        if(empty($TD210)){
            $TD210 = 0;
        }
        if(empty($TD212)){
            $TD212 = 0;
        }
        if(empty($TD214)){
            $TD214 = 0;
        }

        if(empty($TD280)){
            $TD280 = 0;
        }
        if(empty($TD281)){
            $TD281 = 0;
        }
        if(empty($TD282)){
            $TD282 = 0;
        }
        if(empty($TD283)){
            $TD283 = 0;
        }
        if(empty($TD284)){
            $TD284 = 0;
        }
        if(empty($TD285)){
            $TD285 = 0;
        }
        if(empty($TD286)){
            $TD286 = 0;
        }
        if(empty($TD287)){
            $TD287 = 0;
        }

        $TD176 = $data2->TD176;
        $TD178 = $data3->TD178;
        $TD180 = $data4->TD180;
        $TD145W = $data2->td145;
        $TD145R = $data3->td145;
        $TD145F = $data4->td145;
        if($TD145W != '') {

            $TD230 =
                ((double)$TD201 * ($TD05 - $TD37) / $TD145W) +
                ((double)$TD203 * ($TD09 - $TD37) / $TD145W) +
                ((double)$TD205 * ($TD13 - $TD37) / $TD145W) +
                ((double)$TD207 * ($TD17 - $TD37) / $TD145W) +
                ((double)$TD209 * ($TD21 - $TD37) / $TD145W) +
                ((double)$TD211 * ($TD25 - $TD37) / $TD145W) +
                ((double)$TD213 * ($TD29 - $TD37) / $TD145W) +
                ((double)$TD215 * ($TD33 - $TD37) / $TD145W);

            $TD231 = 0.024 * (
                ((double)$TD201 * $TD44 / $TD145W) +
                ((double)$TD203 * $TD72  / $TD145W) +
                ((double)$TD205 * $TD73  / $TD145W) +
                ((double)$TD207 * $TD74  / $TD145W) +
                ((double)$TD209 * $TD75  / $TD145W) +
                ((double)$TD211 * $TD76  / $TD145W) +
                ((double)$TD213 * $TD76  / $TD145W) +
                ((double)$TD215 * $TD77  / $TD145W)
                );
        }
        if($TD145R != '') {
//            $TD240 = $TD178 * ($TD05 - $TD37) / $TD145R;
            $TD240 =
                ((double)$TD216 * ($TD05 - $TD37) / $TD145R) +
                ((double)$TD217 * ($TD09 - $TD37) / $TD145R) +
                ((double)$TD218 * ($TD13 - $TD37) / $TD145R) +
                ((double)$TD219 * ($TD17 - $TD37) / $TD145R) +
                ((double)$TD220 * ($TD21 - $TD37) / $TD145R) +
                ((double)$TD221 * ($TD25 - $TD37) / $TD145R) +
                ((double)$TD223 * ($TD29 - $TD37) / $TD145R) +
                ((double)$TD225 * ($TD33 - $TD37) / $TD145R);

//            $TD241 = 0.024 * $TD178 * $TD44 / $TD145R;
            $TD241 = 0.024 * (
                    ((double)$TD216 * $TD44 / $TD145R) +
                    ((double)$TD217 * $TD72  / $TD145R) +
                    ((double)$TD218 * $TD73  / $TD145R) +
                    ((double)$TD219 * $TD74  / $TD145R) +
                    ((double)$TD220 * $TD75  / $TD145R) +
                    ((double)$TD221 * $TD76  / $TD145R) +
                    ((double)$TD223 * $TD76  / $TD145R) +
                    ((double)$TD225 * $TD77  / $TD145R)
                );
        }
        if($TD145F != '') {
//            $TD250 = $TD180 * ($TD05 - $TD37) / $TD145F;
            $TD250 =
                ((double)$TD200 * ($TD05 - $TD37) / $TD145F) +
                ((double)$TD202 * ($TD09 - $TD37) / $TD145F) +
                ((double)$TD204 * ($TD13 - $TD37) / $TD145F) +
                ((double)$TD206 * ($TD17 - $TD37) / $TD145F) +
                ((double)$TD208 * ($TD21 - $TD37) / $TD145F) +
                ((double)$TD210 * ($TD25 - $TD37) / $TD145F) +
                ((double)$TD212 * ($TD29 - $TD37) / $TD145F) +
                ((double)$TD214 * ($TD33 - $TD37) / $TD145F);
            $TD251 = 0.024 * (
                    ((double)$TD200 * $TD44 / $TD145F) +
                    ((double)$TD202 * $TD72  / $TD145F) +
                    ((double)$TD204 * $TD73  / $TD145F) +
                    ((double)$TD206 * $TD74  / $TD145F) +
                    ((double)$TD208 * $TD75  / $TD145F) +
                    ((double)$TD210 * $TD76  / $TD145F) +
                    ((double)$TD212 * $TD76  / $TD145F) +
                    ((double)$TD214 * $TD77  / $TD145F)
                );
//            $TD251 = 0.024 * $TD180 * $TD44 / $TD145F;
        }
//        print_r("TD183 = $TD183");
        if($TD183 != '' || $TD183 != 0) {
            $TD260 = $TD182 * ($TD05 - $TD37) / $TD183;
            $TD261 = 0.024 * $TD182 * $TD44 / $TD183;
        }
//        print_r("TD188 = $TD188");
        if($TD188 != '' || $TD188 != 0){
            $TD270 = $TD186 * ($TD05 - $TD37) / $TD188;
            $TD271 = 0.024 * $TD186 * $TD44 / $TD188;
        }

//        $TD289 = 1;

        $TD300 = ((double)$TD288 * 60) + (((double)$TD289 + (double)$TD290) * 25);

    if ($TD280 >= $TD300){
        $TD301 = $TD280;
    } else {
        $TD301 = $TD300;
    }

    $TD302 = 0.28 * (double)$TD301 * 1.2 * ($TD05 - $TD37) * $TD184;
    $TD303 = 0.28 * (double)$TD301 * 1.2 * 0.024 * $TD44 * $TD184;


    $TD304 = $TD200 / $TD172;

   if ($TD304 <= 20) {
       $TD305 = 17;
   } elseif ($TD304 > 20 && $TD304 < 45) {
       if ($TD304 > 20 && $TD304 < 32.5) {
           $TD305 = (32.5 + 20) / 2;
       }
       if ($TD304 > 32.5 && $TD304 < 45) {
           $TD305 = (32.5 + 45) / 2;
       }
   } elseif ($TD304 >= 45) {
       $TD305 = 10;
   }

   $TD309 = $TD305 * $TD200;


    if (isset($TD208) && $building == 1) {


        $TD300 = ((double)$TD288 * (double)$TD174) + (((double)$TD289 + (double)$TD290) * 25);

        if(($TD200 + $TD208) / $TD172 < 20 ){
            $TD308 = 3 * ($TD200 + $TD208);
        }elseif(($TD200 + $TD208) / $TD172  >= 20){
            $TD308 = 30 * $TD172;
        }


        if ($TD308 >= $TD300){
            $TD301 = $TD308;
        } else {
            $TD301 = $TD300;
        }


    }

    if ($building == 2 || $building == 4) {

        $TD301 = 3 * ($TD280 + $TD281 + $TD282 + $TD283 + $TD284 + $TD285 + $TD286 + $TD287);
        $TD309 = 10 * ($TD200 + $TD202 + $TD204 + $TD206 + $TD208 + $TD210 + $TD212 + $TD214);


    }elseif ($building == 3){

        $TD301	 =	2 * ($TD280 + $TD284 + $TD285 + $TD286 + $TD287);
        $TD309	 =	40.47 * $TD172;

    }


$TD306	 =	($TD261 + $TD271)/200;
$TD307	 =	$TD231/300;
$TD310	 =	$TD302 * $TD190 * 0.01;
$TD311	 =	$TD310 - $TD302;
$TD315	 =	$TD303 * $TD190 * 0.01;
$TD316	 =	$TD315 - $TD303;

$TD317	 =	$TD309 * $TD39 * 0.024;
$TD318	 =	$TD306 * $TD39 * 0.024;
$TD319	 =	$TD307 * $TD39 * 0.024;


$TD312 =	$TD230 + $TD240 + $TD250 + $TD260 + $TD270 + $TD310;
//echo "$TD312 =	$TD230 + $TD240 + $TD250 + $TD260 + $TD270 + $TD310";
$TD313 =	$TD312 - $TD309 - $TD306 -$TD307;
$TD314 =	$TD231 + $TD241 + $TD251 + $TD261 + $TD271 + $TD315;
$TD320 =	$TD314 - $TD317 - $TD318 - $TD319;

$TD341 = $TD311 + $TD309 + $TD306 + $TD307;
        $TD321 = $TD230 / $TD312 *100;
        $TD322 = $TD240 / $TD312 *100;
        $TD323 = $TD250 / $TD312 *100;
        $TD324 = $TD260 / $TD312 *100;
        $TD325 = $TD270 / $TD312 *100;
//$TD326 = $TD312 - $TD230 - $TD240 - $TD250 - $TD260 - $TD270;
$TD326 = 100 - $TD321 - $TD322 - $TD323 - $TD324 - $TD325;

        $TD327 = 0;
        $TD328 = 0;
        $TD329 = 0;

if($TD341 != 0){
    $TD327 = $TD311 / $TD341 * 100;
    $TD328 = $TD309 / $TD341 * 100;
    $TD329 = $TD306 / $TD341 * 100;
}

//$TD340 = $TD307 / $TD341 * 100;
//$TD340 = $TD341 - $TD327 - $TD328 - $TD329;
$TD340 = 100 - $TD327 - $TD328 - $TD329;



$TD342 = $TD316 + $TD317 + $TD318 + $TD319;




$TD230 = round($TD230);
$TD231 = round($TD231);
$TD240 = round($TD240);
$TD241 = round($TD241);
$TD250 = round($TD250);
$TD251 = round($TD251);
$TD260 = round($TD260);
$TD261 = round($TD261);
$TD270 = round($TD270);
$TD271 = round($TD271);
$TD310 = round($TD310);
$TD315 = round($TD315);
$TD312 = round($TD312);
$TD314 = round($TD314);
$TD311 = round($TD311);
$TD316 = round($TD316);
$TD309 = round($TD309);
$TD317 = round($TD317);
$TD306 = round($TD306);
$TD318 = round($TD318);
$TD307 = round($TD307);
$TD319 = round($TD319);
$TD341 = round($TD341);
$TD342 = round($TD342);
$TD313 = round($TD313);
$TD320 = round($TD320);

$TD321 = round($TD321, 1);
$TD322 = round($TD322, 1);
$TD323 = round($TD323, 1);
$TD324 = round($TD324, 1);
$TD325 = round($TD325, 1);
$TD326 = round($TD326, 1);
$TD327 = round($TD327, 1);
$TD328 = round($TD328, 1);
$TD329 = round($TD329, 1);
$TD340 = round($TD340, 1);

$content ="<thead>
        <tr>
            <th></th>
            <th></th>
            <th>ТП1</th>
            <th>ТП2</th>
            <th>ТП3</th>
        </tr>
        </thead>
        <tbody>

        <tr>
            <td></td>
            <td></td>
            <td>теплопотери, Вт</td>
            <td>теплопотери за год кВт/ч</td>
            <td>в % к общим</td>
        </tr>
        <!-- ==============================================-->
        <tr>
            <td>ТПС-01</td>
            <td>Стена</td>
            <td class='TD230'>$TD230</td>
            <td class='TD231'>$TD231</td>
            <td class='TD321'>$TD321  %</td>
        </tr>
        <tr>
            <td>ТПС-02</td>
            <td>Кровля</td>
            <td class='TD240'>$TD240</td>
            <td class='TD241'>$TD241</td>
            <td class='TD322'>$TD322  %</td>
        </tr>
        <tr>
            <td>ТПС-03</td>
            <td>Полы</td>
            <td class='TD250'>$TD250</td>
            <td class='TD251'>$TD251</td>
            <td class='TD323'>$TD323  %</td>
        </tr>
        <tr>
            <td>ТПС-04</td>
            <td>Окна</td>
            <td class='TD260'>$TD260</td>
            <td class='TD261'>$TD261</td>
            <td class='TD324'>$TD324  %</td>
        </tr>
        <tr>
            <td>ТПС-05</td>
            <td>Фонари</td>
            <td class='TD270'>$TD270</td>
            <td class='TD271'>$TD271</td>
            <td class='TD325'>$TD325  %</td>
        </tr>
        <tr>
            <td>ТПС-06</td>
            <td>Вентиляция</td>
            <td class='TD310'>$TD310</td>
            <td class='TD315'>$TD315</td>
            <td class='TD326'>$TD326  %</td>
        </tr>
        <tr>
                <td>ТПС-07</td>
            <td>Всего теплопотерь:</td>
            <td class='TD312'>$TD312</td>
            <td class='TD314'>$TD314</td>
            <td></td>
        </tr>
        <tr>
            <td>ТПС-08</td>
            <td>Экономия тепла рекуператором</td>
            <td class='TD311'>$TD311</td>
            <td class='TD316'>$TD316</td>
            <td class='TD327'>$TD327  %</td>
        </tr>
        <tr>
            <td>ТПС-09</td>
            <td>Бытовые тепловыделения</td>
            <td class='TD309'>$TD309</td>
            <td class='TD317'>$TD317</td>
            <td class='TD328'>$TD328  %</td>
        </tr>
        <tr>
            <td>ТПС-10</td>
            <td>Поступление от солнечной радиации</td>
            <td class='TD306'>$TD306</td>
            <td class='TD318'>$TD318</td>
            <td class='TD329'>$TD329  %</td>
        </tr>
        <tr>
            <td>ТПС-11</td>
            <td>Поступление от сторон света</td>
            <td class='TD307'>$TD307</td>
            <td class='TD319'>$TD319</td>
            <td class='TD340'>$TD340  %</td>
        </tr>
        <tr>
            <td>ТПС-12</td>
            <td>Всего поступлений</td>
            <td class='TD341'>$TD341</td>
            <td class='TD342'>$TD342</td>
            <td></td>
        </tr>
        <tr>
            <td>ТПС-13</td>
            <td>Итого теплопотери составят:</td>
            <td class='TD313'>$TD313</td>
            <td class='TD320'>$TD320</td>
            <td></td>
        </tr>
        <!--==============================================-->
        </tbody>";

//    return $content;


        $content_out = "<tr>
                            <td>Конструкции стен</td>
                            <td class='TD230'>$TD230</td>
                            <td class='TD231'>$TD231</td>
                            <td class='TD321'>$TD321  %</td>
                        </tr>
                        <tr>
                            <td>Конструкции кровли</td>
                            <td class='TD240'>$TD240</td>
                            <td class='TD241'>$TD241</td>
                            <td class='TD322'>$TD322  %</td>
                        </tr>
                        <tr>
                            <td>Конструкции полов</td>
                            <td class='TD250'>$TD250</td>
                            <td class='TD251'>$TD251</td>
                            <td class='TD323'>$TD323  %</td>
                        </tr>
                        <tr>
                            <td>Окна</td>
                            <td class='TD260'>$TD260</td>
                            <td class='TD261'>$TD261</td>
                            <td class='TD324'>$TD324  %</td>
                        </tr>
                        <tr>
                            <td>Фонари (мансардные окна)</td>
                            <td class='TD270'>$TD270</td>
                            <td class='TD271'>$TD271</td>
                            <td class='TD325'>$TD325  %</td>
                        </tr>
                        <tr>
                            <td>Вентиляция здания</td>
                            <td class='TD310'>$TD310</td>
                            <td class='TD315'>$TD315</td>
                            <td class='TD326'>$TD326  %</td>
                        </tr>";

        $content_in = "<tr>
                            <td>Экономия тепловой энергии рекуператором</td>
                            <td class='TD311'>$TD311</td>
                            <td class='TD316'>$TD316</td>
                            <td class='TD327'>$TD327  %</td>
                        </tr>
                        <tr>
                                        <td>Бытовые тепловыделения</td>
                            <td class='TD309'>$TD309</td>
                            <td class='TD317'>$TD317</td>
                            <td class='TD328'>$TD328  %</td>
                        </tr>
                        <tr>
                            <td>Поступления от солнечного нагрева</td>
                            <td class='TD306'>$TD306</td>
                            <td class='TD318'>$TD318</td>
                            <td class='TD329'>$TD329  %</td>
                        </tr>
                        <tr>
                            <td>Поступления от ориентирования по сторонам света</td>
                            <td class='TD307'>$TD307</td>
                            <td class='TD319'>$TD319</td>
                            <td class='TD340'>$TD340  %</td>
                        </tr>";
        $result['in'] = $content_in;
        $result["out"] = $content_out;
        $result["TD312"] = $TD312;
        $result["TD314"] = $TD314;
        $result["TD341"] = $TD341;
        $result["TD342"] = $TD342;
        $result["TD313"] = $TD313;
        $result["TD320"] = $TD320;

        $result["TD321"] = $TD321;
        $result["TD322"] = $TD322;
        $result["TD323"] = $TD323;
        $result["TD324"] = $TD324;
        $result["TD325"] = $TD325;
        $result["TD326"] = $TD326;

//        dd(json_encode($result));
        return json_encode($result);

    }


    public static function calcWindow($params)
    {
$content = '';
        $data1 = json_decode(Cookie::get('data1'));
        $data2 = json_decode(Cookie::get('data2'));

        if($data1 == null || $data2 == null){
            die('error');
        }

        foreach ($data1 as $key => $value) {
            $$key = $value;
        }
        foreach ($data2 as $key => $value) {
            $$key = $value;
        }
        foreach ($params as $key => $value) {
            $$key = $value;
        }

//        debug($data1);
//        debug($data2);
//        debug($td117);
//        debug($td05);
//        $TD117 = $data2['TD117'];
        $params_117['val'] = $td05;
        $result_117 = self::getDewData($params_117);
        if($result_117 != null){
            $TD117 = $result_117->per_55;
        }
        $params_content['TD05'] = $TD05;
        $params_content['TD37'] = $TD37;


    if(isset($TD183)){
        $TD115WI =	$TD05 - ((($TD05 - $TD37) / (0.168478 + $TD183)) * 0.125);
        $TD145WI = $TD183 ? $TD183 : $TD183 + 0.168478;
//
//        if($TD145WI >= $TD57){
//            $content .= " <span class='content_3_2'>Фактическое теплосопротивление конструкции (ТД145-О) больше, чем нормируемое (ТД57)
//         (м2·°С)/Вт, значит конструкции окон (дверей) СООТВЕТСТВУЮТ нормативным требованиям. </span>";
//        }else{
//            $content .= "<span class='content_3_2'> Фактическое теплосопротивление конструкции (ТД145-О) меньше, чем нормируемое  (ТД57)
//         (м2·°С)/Вт, значит конструкции окон (дверей) НЕ СООТВЕТСТВУЮТ нормативным требованиям.
//         Совет: Необходимо подобрать оконные (дверные) конструкции с повышенными теплозащитными характеристиками. </span>";
//        }
//        if($TD115WI <= $TD117){
//            $content .= "<span class='content_3_3'> Температура внутренней поверхности окон (и дверей)  (ТД115-О) °С меньше температуры точки росы
//         в помещении (ТД117) °С, поэтому будет образовываться конденсат. Такая конструкция НЕ СООТВЕТСТВУЕТ
//          нормативным требованиям. Совет: необходимо применить окна (и двери) с повышенными теплозащитными характеристиками. </span>";
//        }else{
//            $content .= "<span class='content_3_3'> Температура внутренней поверхности окон (и дверей)  (ТД115-О)  °С больше температуры точки росы
//        в помещении (ТД117) °С, поэтому образования конденсата не будет. Такая конструкция СООТВЕТСТВУЕТ нормативным требованиям. </span>";
//        }

        $params_content['TD145WI'] = $TD145WI;
        $params_content['TD57'] = $TD57;
        $params_content['TD115WI'] = $TD115WI;
        $params_content['TD117'] = $TD117;
    }


    if(isset($TD188)){
        $TD115La =	$TD05 - ((($TD05 - $TD37) / (0.144488 + (double)$TD188)) * 0.10101);
        $TD145La	 = 	$TD188 ?? (double)$TD188 + 0.14488;
//
//        if($TD145La >= $TD58){
//            $content .= "<span class='content_3_5'> Фактическое теплосопротивление конструкции (ТД145-Ф) (м2·°С)/Вт больше, чем нормируемое
//            (ТД58) (м2·°С)/Вт, значит конструкции фонарей СООТВЕТСТВУЮТ нормативным требованиям. </span>";
//        }else{
//            $content .= "<span class='content_3_5'> Фактическое теплосопротивление конструкции (ТД145-Ф) (м2·°С)/Вт меньше, чем нормируемое (ТД58)
//             (м2·°С)/Вт, значит конструкции фонарей НЕ СООТВЕТСТВУЮТ нормативным требованиям. Совет: Необходимо подобрать
//             конструкции фонарей (мансардных окон) с повышенными теплозащитными характеристиками. </span>";
//        }
//
//        if($TD115La <= $TD117){
//            $content .= "<span class='content_3_6'> Температура внутренней поверхности фонарей (мансардных окон)  (ТД115-О) °С  меньше температуры
//             точки росы в помещении (ТД117) °С, поэтому
//             будет образовываться конденсат. Такая конструкция НЕ СООТВЕТСТВУЕТ нормативным требованиям. Совет: необходимо
//              применить фонари (мансардные окна) с повышенными теплозащитными характеристиками. </span>";
//        }else{
//            $content .= "<span class='content_3_6'> Температура внутренней поверхности фонарей (мансардных окон)  (ТД115-О) °С  больше температуры
//             точки росы в помещении (ТД117) °С, поэтому образования конденсата не будет. Такая конструкция СООТВЕТСТВУЕТ
//             нормативным требованиям. </span>";
//        }
        $params_content['TD58'] = $TD58;
        $params_content['TD145La'] = $TD145La;
        $params_content['TD115La'] = $TD115La;
        $params_content['TD117'] = $TD117;
    }

    $json = json_encode($params_content);
        $params['cookie_name'] = 'data5';
    $res = self::writeCookie($params);
        return $json;
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $params_get['col'] = 'layer_link';
        $params_get['val'] = $randomString;
        $params_get['operator'] = '=';
        $datalayer = self::getLayerDataByParams($params_get);
        if(!empty($datalayer)){
            $randomString = self::generateRandomString($length);
        }
        return $randomString;
    }

    public static function addLayerData($params)
    {
        $user_key = Cookie::get('auth-Key');
        if($user_key == ''){
            die;
        }
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = User::getUserBy($us_params);
        $user_id = $user[0]->id;

        $arr = explode('_', $params['cookie_name']);
        $construction = $arr[1];

        $params_get['col'] = 'user_id';
        $params_get['val'] = $user_id;
        $params_get['operator'] = '=';
        $datalayer = self::getLayerDataByParams($params_get);

        if(!isset($params['compare']))
            $params['compare']='-';

        $data_for_lay = [];
        if(isset($params['data'])){
            $data_for_lay = $params['data'];
            unset($params['data']);
        }

//        debug_die($data_for_lay);

        if(empty($datalayer)) {
            $params_new['user_id'] = $user_id;
            $params_new['name'] = 'Версия расчёта – Автосохранение';
            for($i = 1; $i<=6; $i++){
                $params_new["data_layer_$i"] = '-';
            }
            $data_layer = "data_layer_$construction";
            $params_new[$data_layer] = json_encode($params);
            $params_new['layer_link'] = self::generateRandomString(8);
//            Cookie::set("layer_link", $params_new['layer_link']);
            $params_compareData[$construction] = Cookie::get("data$construction");

            if ($construction == 1)
                $params_compareData['data']['index'] = $data_for_lay;

            if(!empty(Cookie::get("total_cost_$construction"))){
                $params_compareData["total_cost_$construction"] = Cookie::get("total_cost_$construction");
            }
            if(!empty(Cookie::get("total_cost_5_1"))){
                $params_compareData["total_cost_5_1"] = Cookie::get("total_cost_5_1");
            }
            if(!empty(Cookie::get("total_cost_5_2"))){
                $params_compareData["total_cost_5_2"] = Cookie::get("total_cost_5_2");
            }
            if(!empty(Cookie::get("total_cost_6_1"))){
                $params_compareData["total_cost_6_1"] = Cookie::get("total_cost_6_1");
            }
            if(!empty(Cookie::get("total_cost_6_2"))){
                $params_compareData["total_cost_6_2"] = Cookie::get("total_cost_6_2");
            }
            if(!empty(Cookie::get("total_cost_6_3"))){
                $params_compareData["total_cost_6_3"] = Cookie::get("total_cost_6_3");
            }

            $params_new['compare'] = json_encode($params_compareData);
            return self::$layerRepository->add($params_new);
        } else {
            $which_datalayer = $datalayer[0];
//            if(Cookie::get("layer_link")){
//                foreach ($datalayer as $item){
//                    if($item->layer_link == Cookie::get("layer_link")){
//                        $which_datalayer = $item;
//                    }
//                }
//            }

            $params_update['id']  = $which_datalayer->id;
            $data_layer = "layer_data_$construction";

            if($which_datalayer->compare_data == '-'){
                $params_compareData = array();
            }else{
                $params_compareData = (array)json_decode($which_datalayer->compare_data);
            }

            $params_compareData = (array)$params_compareData;

            $is_accepted_compare = 0;
            if(Cookie::get("is_accepted_2") == 'true' &&
                Cookie::get("is_accepted_3") == 'true' &&
                Cookie::get("is_accepted_4") == 'true' &&
                Cookie::get("is_accepted_5") == 'true' ){
                $is_accepted_compare = 1;
            }
            $params_compareData['is_accepted'] = $is_accepted_compare;

            if($construction == 6 && isset($params['td313']))
                $params_compareData['td313'] = $params['td313'];

            if(!is_null(Cookie::get("total_cost_$construction"))){
                $params_compareData["total_cost_$construction"] = Cookie::get("total_cost_$construction");
            }

            $params_compareData[$construction] = Cookie::get("data$construction");

            if(!empty($params_compareData->data))
                $params_compareData['data'] = (array)($params_compareData->data);


            switch ($construction) {
                case 1:
                    $params_compareData['data']['index'] = $data_for_lay;
                    break;
                case 2:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $data_for_lay['files'] = Cookie::get("files");
                    $params_compareData['data']['wall'] = serialize($data_for_lay);
                    break;
                case 3:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $data_for_lay['files'] = Cookie::get("files");
                    $params_compareData['data']['roof'] = serialize($data_for_lay);
                    break;
                case 4:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $data_for_lay['files'] = Cookie::get("files");
                    $params_compareData['data']['floor'] = serialize($data_for_lay);
                    break;
                case 5:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $params_compareData['data']['win'] = serialize($data_for_lay);
                    if(!is_null(Cookie::get("total_cost_5_1"))){
                        $params_compareData["total_cost_5_1"] = Cookie::get("total_cost_5_1");
                    }
                    if(!is_null(Cookie::get("total_cost_5_2"))){
                        $params_compareData["total_cost_5_2"] = Cookie::get("total_cost_5_2");
                    }
                    break;
                case 6:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $params_compareData['data']['heat'] = serialize($data_for_lay);
                    if(!is_null(Cookie::get("total_cost_6_1"))){
                        $params_compareData["total_cost_6_1"] = Cookie::get("total_cost_6_1");
                    }
                    if(!is_null(Cookie::get("total_cost_6_2"))){
                        $params_compareData["total_cost_6_2"] = Cookie::get("total_cost_6_2");
                    }
                    if(!is_null(Cookie::get("total_cost_6_3"))){
                        $params_compareData["total_cost_6_3"] = Cookie::get("total_cost_6_3");
                    }
                    break;
                case 7:
                    foreach($data_for_lay as $lay_k => $lay_v){
                        $data_for_lay[$lay_k] = htmlspecialchars($lay_v);
                    }
                    $params_compareData['data']['com'] = serialize($data_for_lay);;
                    break;
                default:
                    echo ('Unexpected value');
            }
            $params_update['col'] = "compare_data";
            $params_update['val'] = json_encode($params_compareData);

            $updateDataLayer1 = self::$layerRepository->updateLayerData($params_update);
            if($construction != 7){
                $params_update['col'] = $data_layer;
                $params_update['val'] = json_encode($params);
                $updateDataLayer = self::$layerRepository->updateLayerData($params_update);
            }

            return $updateDataLayer1;
        }
    }

    public static function addLayerDataName($params)
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = User::getUserBy($us_params);
        $user_id = $user[0]->id;

        $params_get['col'] = 'user_id';
        $params_get['val'] = $user_id;
        $params_get['operator'] = '=';
        $datalayer = self::getLayerDataByParams($params_get);
        $params_new = array();
        $params_new['name'] = $params['name'];
        $params_new['user_id'] = $user_id;
        if(count($datalayer)<=7) {
            for ($i = 1; $i <= 6; $i++) {
                $tab_cookie = Cookie::get("tab_$i");
                if($i == 1){
                    $tab_cookie = Cookie::get("data1");
                }
                if($tab_cookie == null)
                    $tab_cookie = '-';

                $params_new["data_layer_$i"] = $tab_cookie;
                $params_compare[$i] = Cookie::get("data$i");

                if(!is_null(Cookie::get("total_cost_$i"))){
                    $params_compare["total_cost_$i"] = Cookie::get("total_cost_$i");
                }
            }

            if(!is_null(Cookie::get("total_cost_5_1"))){
                $params_compare["total_cost_5_1"] = Cookie::get("total_cost_5_1");
            }
            if(!is_null(Cookie::get("total_cost_5_2"))){
                $params_compare["total_cost_5_2"] = Cookie::get("total_cost_5_2");
            }
            if(!is_null(Cookie::get("total_cost_6_1"))){
                $params_compare["total_cost_6_1"] = Cookie::get("total_cost_6_1");
            }
            if(!is_null(Cookie::get("total_cost_6_2"))){
                $params_compare["total_cost_6_2"] = Cookie::get("total_cost_6_2");
            }
            if(!is_null(Cookie::get("total_cost_6_3"))){
                $params_compare["total_cost_6_3"] = Cookie::get("total_cost_6_3");
            }

            $is_accepted_compare = 0;
            if(Cookie::get("is_accepted_2") == 'true' &&
                Cookie::get("is_accepted_3") == 'true' &&
                Cookie::get("is_accepted_4") == 'true' &&
                Cookie::get("is_accepted_5") == 'true' ){
                $is_accepted_compare = 1;
            }
            $params_compare['is_accepted'] = $is_accepted_compare;


            if(isset(json_decode(Cookie::get("tab_6"))->td313))
                $params_compare['td313'] = json_decode(Cookie::get("tab_6"))->td313;

            $params_new['compare'] = json_encode($params_compare);

//            $params_get['col'] = 'layer_link';
//            $params_get['val'] = Cookie::get('layer_link');
//            $params_get['operator'] = '=';
//            $datalayer = self::getLayerDataByParams($params_get);

            $params_new['compare'] = ($datalayer[0]->compare_data);

            $params_new['layer_link'] = self::generateRandomString(8);
            if($params['layer_id'] == '-'){
                $result = self::$layerRepository->add($params_new);
            } else {
                $params_update['id'] = $params['layer_id'];
//                if change link on update
                /*$params_update['col'] = "name";
                $params_update['val'] = $params_new['layer_link'];
                $result = self::$layerRepository->updateLayerData($params_update);*/
                $params_update['col'] = "compare_data";
                $params_update['val'] = $params_new['compare'];
                $result = self::$layerRepository->updateLayerData($params_update);
                if(!$result)
                    return "что-то пошло не так compare";

                $params_update['col'] = "name";
                $params_update['val'] = $params['name'];
                $result = self::$layerRepository->updateLayerData($params_update);
                if(!$result)
                    return "что-то пошло не так";

                for ($i = 1; $i <= 6; $i++) {
                    $tab_cookie = Cookie::get("tab_$i");
                    if($i == 1){
                        $tab_cookie = Cookie::get("data1");
                    }
                    if($tab_cookie == null)
                        $tab_cookie = '-';
                    $params_update['col'] = "layer_data_$i";
                    $params_update['val'] = $tab_cookie;

                    $result = self::$layerRepository->updateLayerData($params_update);
                    if(!$result)
                        return "что-то пошло не так";
                }
            }
            if($result)
                return 'Запись сохранена';
        } else{
            return 'У Вас уже 5 сохраненных записей';
        }
    }

    public static function editLayerDataName($params){

            $params_update['id'] =  $params['id'];
            $params_update['col'] = 'name';
            $params_update['val'] = $params['name'];
            $updatedatalayer = self::$layerRepository->updateLayerData($params_update);
            if($updatedatalayer != false){
                return 'Запись сохранена';
            }else{
                return 'что-то пошло не так';
            }

    }
    
    public static function removeLayerDataName($params){

            $layer = self::$layerRepository->getLayer($params['id']);
            if($layer->user_id != $params['user_id']){
                return 'error';
            }else{
                self::$layerRepository->remove($params['id']);
            }

    }

    public static function getLayerDataByParams($params){
        return self::$layerRepository->getLayerDataByParams($params);
    }

     public static function getLinks(){
         $user_login = Cookie::get('auth-Login');
         $us_params['col'] = 'email';
         $us_params['val'] = $user_login;
         $user_login = User::getUserBy($us_params);
         $user_id = $user_login[0]->id;
         $params_tab['val'] = $user_id;
         $params_tab['col'] = 'user_id';
         $params_tab['operator'] = '=';
         return self::$layerRepository->getLayerDataByParams($params_tab);

     }

//compare
    public static function sendToCompare($params){

        $params_update['col'] = 'compare';

        foreach($params['id'] as $k => $v){
            $params_update['id'] =  $k;
            $params_update['val'] =  $v;
            $updatedatalayer = self::$layerRepository->updateLayerData($params_update);
            if($updatedatalayer == false)
                return 'что-то пошло не так';
        }
            return 'Запись сохранена';
    }


}