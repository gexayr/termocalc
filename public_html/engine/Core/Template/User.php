<?php
/**
 * Created by PhpStorm.
 * User: gex
 * Date: 24.08.17
 * Time: 17:32
 */

namespace Engine\Core\Template;

use Admin\Model\User\UserRepository;
use Admin\Model\Post\PostRepository;
use Admin\Model\Help\HelpRepository;
use Admin\Model\Postcategory\PostcategoryRepository;
use Admin\Model\Helpcategory\HelpcategoryRepository;
use Engine\Helper\Cookie;
use Admin\Model\SelfCreate\SelfCreateRepository;
use Admin\Model\Estimate\EstimateRepository;



/**
 * Class User
 * @package Engine\Core\Template
 */
class User
{
    /**
     * @var
     */
    protected static $di;


    /**
     * @var UserRepository
     */
    protected static $userRepository;
    protected static $postRepository;
    protected static $helpRepository;
    protected static $postcategoryRepository;
    protected static $selfcreateRepository;
    protected static $estimateRepository;

    public function __construct($di)
    {

        self::$di = $di;
        self::$userRepository = new UserRepository(self::$di);
        self::$postRepository = new PostRepository(self::$di);
        self::$helpRepository = new HelpRepository(self::$di);
        self::$postcategoryRepository = new PostcategoryRepository(self::$di);
        self::$selfcreateRepository = new SelfCreateRepository(self::$di);
        self::$estimateRepository = new EstimateRepository(self::$di);

    }



//     estimate

    public static function getEstimateBy($params)
    {
        return self::$estimateRepository->getEstimatesByParams($params);
    }

//    end estimate



//     file

    public static function getFile($id)
    {
        return self::$estimateRepository->getUploadedFile($id);
    }
    public static function getFileBy($params)
    {
        return self::$estimateRepository->getFileBy($params);
    }

//    end file



    public static function getUserBy($params)
    {
        return  self::$userRepository->getUserBy($params);
    }

    public static function reg($params)
    {

        $params['secret'] = md5($params['email'].time());

            if( $params['status'] == 'other' && $params['status_other'] != ''){
                $params['status'] = $params['status_other'];
            }
            if($params['city']==''){
                $params['city'] = '(NULL)';
            }
            if($params['region']==''){
                $params['region'] = '(NULL)';
            }


        if($params['surname']==''){
            $params['surname'] = '(NULL)';
        }

        if($params['building']==''){
            $params['building'] = '(NULL)';
        }


        $userId = self::$userRepository->add($params);
            if($userId == 0){
                die('data error');
            }

            Cookie::set('auth-Login', $params['email'] );
            Cookie::set('auth-Key', $params['secret']);
        $name = '';
        if(isset($params['name']) && $params['name'] != ''){
            $name = $params['name'];
        }
            $base_url = 'http://' . $_SERVER['HTTP_HOST'];
            $email = $params['email'];
            $to=$email;
            $subject="Вы зарегистрировались на вебпортале «Термокальк» termocalc.ru";

            $secret = $params['secret'];
            $link = $base_url.'/activation/?code='.$secret;

            $body =  "<meta charset='utf-8'>Здравствуйте, $name <br>".
                "Наш портал ведётся профессионалами в строительстве и благодарим вас за доверие к нашей информации! 
                 Мы надеемся, что вы найдёте для себя необходимую Вам информацию.<br>
                 Проверить соответствие строительным нормативам Ваши варианты конструкций, а также прикинуть их стоимость 
                 и расходы на отопление можно в «Термокалькуляторе». В «Строительном справочнике» собираются знания по
                 всем основным строительным технологиям и взаимоотношениям.<br>
                 Если у Вас есть вопросы и предложения, а также какие-либо мнения, собственные взгляды и знания, то 
                 давайте общаться на нашем «Форуме».<br>
                 Если возникают трудности, то можно ознакомиться с решением проблем пользования порталом в разделе «Помощь»,
                 а также Вас будут сопровождать различные подсказки.<br>
                 Осталось немного, всего лишь подтвердить ваш емайл и телефон для пользования всеми сервисами портала.<br>
                 <a href=\"".$link."\" target=\"_blank\">".'Активируйте'."</a> вашу учётную запись.<br><br>
                 P.S. Если это не Вы регистрировались, просим извинить, вышла ошибка. Просто ничего не делайте и 
                 удалите это письмо. Спасибо!<br>";

            $headers = "From: noreply@termocalc.ru\n";
            $headers .= "Content-Type: text/html; charset=UTF-8";

            mail($to,$subject,$body,$headers);
            return header('Location: /reg-success');

    }

    public static function regFromForum($params)
    {

//        $params['secret'] = md5($params['email'].time());

            if( $params['status'] == 'other' && $params['status_other'] != ''){
                $params['status'] = $params['status_other'];
            }
            if($params['city']==''){
                $params['city'] = '(NULL)';
            }
            if($params['region']==''){
                $params['region'] = '(NULL)';
            }


        if($params['surname']==''){
            $params['surname'] = '(NULL)';
        }

        if($params['phone']==''){
            $params['phone'] = '(NULL)';
        }

        if($params['building']==''){
            $params['building'] = '(NULL)';
        }

        $userId = self::$userRepository->add($params);
            if($userId == 0){
                die('data error');
            }

//            Cookie::set('auth-Login', $params['email'] );
//            Cookie::set('auth-Key', $params['secret']);
//        $name = '';
//
//        if(isset($params['name']) && $params['name'] != ''){
//            $name = $params['name'];
//        }
//            $base_url = 'http://' . $_SERVER['HTTP_HOST'];
//            $email = $params['email'];
//            $to=$email;
//            $subject="Вы зарегистрировались на вебпортале «Термокальк» termocalc.ru";
//
//            $secret = $params['secret'];
//            $link = $base_url.'/activation/?code='.$secret;
//
//            $body =  "Вы зарегистрировались Здравствуйте, $name <br>".
//                "Наш портал ведётся профессионалами в строительстве и благодарим вас за доверие к нашей информации!
//                 Мы надеемся, что вы найдёте для себя необходимую Вам информацию.<br>
//                 Проверить соответствие строительным нормативам Ваши варианты конструкций, а также прикинуть их стоимость
//                 и расходы на отопление можно в «Термокалькуляторе». В «Строительном справочнике» собираются знания по
//                 всем основным строительным технологиям и взаимоотношениям.<br>
//                 Если у Вас есть вопросы и предложения, а также какие-либо мнения, собственные взгляды и знания, то
//                 давайте общаться на нашем «Форуме».<br>
//                 Если возникают трудности, то можно ознакомиться с решением проблем пользования порталом в разделе «Помощь»,
//                 а также Вас будут сопровождать различные подсказки.<br>
//                 Осталось немного, всего лишь подтвердить ваш емайл и телефон для пользования всеми сервисами портала.<br>
//                 <a href=\"".$link."\" target=\"_blank\">".'Активируйте'."</a> вашу учётную запись.<br><br>
//                 P.S. Если это не Вы регистрировались, просим извинить, вышла ошибка. Просто ничего не делайте и
//                 удалите это письмо. Спасибо!<br>";
//
//            $headers = "From: noreply@termocalc.ru\n";
//            $headers .= "Content-Type: text/html; charset=UTF-8";
//
//            mail($to,$subject,$body,$headers);
//            return header('Location: /reg-success');

    }



//    public static function getPage()
//    {
//        $users_count = self::$userRepository->getUsersCount();
//        $count = count($users_count);
//        $page = ceil($count/20);
//        return $page;
//    }
//
//    public static function getAllUs()
//    {
//        $users_count = self::$userRepository->getUsersCount();
//        $count = count($users_count);
//        return $count;
//    }


    public static function activity($id)
    {

//        $base_url = 'http://' . $_SERVER['HTTP_HOST'];
//        $email = $params['email'];
//        $to=$email;
//        $subject="Ваши данные";
//
//        $body =  "<meta charset='utf-8'>Поздравляем,\n<br>"."Вы активировали Ваш аккаунт.\n<br> и теперь можете пользоваться нашим сервисом";
//
//        $headers = "From: noreply@termocalc.ru\n";
//        $headers .= "Content-Type: text/html; charset=UTF-8\r\n";
//        mail($to,$subject,$body,$headers);
//        $id = $params['id'];
        return self::$userRepository->activity($id);
    }


    public static function changeEmail($params)
    {
        return self::$userRepository->changeEmail($params);
    }


    public static function auth($params)
    {
        return self::$userRepository->auth($params);
    }

    public static function getUser($id)
    {
        return self::$userRepository->getUser($id);
    }

//    COOKIES
    public static function getCookie($key)
    {
        return Cookie::get($key);
    }
    public static function setCookie($key,$value)
    {
        return Cookie::set($key,$value);
    }
    public static function deleteCookie($key)
    {
        return Cookie::delete($key);
    }
// END COOKIES

//
//
//    public static function getLoginData($email)
//    {
//        return self::$userRepository->getLoginData($email);
//    }
//
//

    public static function edit($params)
    {
        if( $params['status'] == 'other' && $params['status_other'] != ''){
            $params['status'] = $params['status_other'];
        }

        return self::$userRepository->edit($params);
    }

//    public static function editPass($params)
//    {
//
//        return self::$userRepository->editPass($params);
//
//    }



    public static function forgetPass($params)
    {

        $us_par['col'] = 'email';
        $us_par['val'] = $params['email'];
        $user_check_email = User::getUserBy($us_par);
            $user = $user_check_email[0];
            $secret = $user->secret;
            $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
            $email = $params['email'];
            $time = time();
            $secret = $time . '+' . $secret . '+' . $time;
            $secret = urlencode($secret);
            $to=$email;
            $link = $base_url.'/reset/?reset='.$secret;
            $subject="Сбросить пароль";

            $body =  "<meta charset='utf-8'>Привет,\n\n"."Мы получили запрос на сброс пароля вашего аккаунта на сайте ".$base_url.".\n\n<br><br><a href=\"".$link."\" target=\"_blank\" style=\"text-decoration: none; background-color: #008bee; color: #fdfbff; padding: 5px 10px 5px 10px; border-radius: 4px; margin: 5px 10px 5px 10px;\">".'Сбросить'."</a><br>\n\n";
            $body .= "<br>Если вы проигнорируете это сообщение, ваш пароль не будет изменен<br>\n\n";
            $body .= "(Этот запрос действителен в течение 30 минут)\n\n";
            $headers = "From: noreply@termocalc.ru\n";
            $headers .= "Content-Type: text/html; charset=UTF-8";

            mail($to,$subject,$body,$headers);

            header('Location: /forget-send-page/');


    }


    public static function updatePass($params)
    {
        return self::$userRepository->updatePass($params);
    }


    public static function feedback($params)
    {

//        if(isset($_POST['g-recaptcha-response'])){
//            $captcha=$_POST['g-recaptcha-response'];
//        }
//        if(!$captcha){
//            echo '<h2>Please check the the captcha form.</h2>';
//            exit;
//        }
//        $captcha=$_POST['g-recaptcha-response'];
//        $secretKey = "6LeqqqEUAAAAAAfjOVuN1W_Ymn_xcxexa3fAe40u";
//
//        $url = 'https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($secretKey) .  '&response=' . urlencode($captcha);
//        $response = file_get_contents($url);
//        $responseKeys = json_decode($response,true);

//        if ($responseKeys["success"]) {
            $base_url = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
            $email = $params['email'];
//            $to = "wdvs.ru@gmail.com";
        $to="gexo7777@gmail.com";
            $subject = "Сообщение от формы обратной связи сайта $base_url";
            $message = $params['message'];
            $name = $params['name'];
            $phone = $params['phone'];

            $body = "<meta charset='utf-8'>$message<br><br>";
            $body .= "<br> E-mail - $email <br>";
            $body .= "<br> Нмя - $name <br>";
            $body .= "<br> Телефон - $phone <br>";
            $headers = "From: noreply@termocalc.ru\n";
            $headers .= "Content-Type: text/html; charset=UTF-8";

            mail($to, $subject, $body, $headers);
            header('Location: /thank');

//        } else {
//            echo '<h2>You are spammer ! Get the @$%K out</h2>';
//        }
    }



//    POST SECTION
    public static function getPosts()
    {
        return  self::$postRepository->getPosts();
    }

    public static function getPostsByParams($id)
    {
        return  self::$postRepository->getChildPosts($id);
    }

    public static function getPostCategories()
    {
        return  self::$postcategoryRepository->getPostCategories();
    }

    public static function getCategoriesByName($lat_name)
    {
        return  self::$postcategoryRepository->getCategoriesByLatName($lat_name);
    }

    public static function getPostData($id)
    {
        return  self::$postRepository->getPostData($id);
    }

//    public static function getCategoriesByParams($params)
//    {
//        return  self::$postcategoryRepository->getCategoriesByParams($params);
//    }
//
    public static function getChildCategory($params)
    {
        return  self::$postcategoryRepository->getChild($params);
    }



//    HELP SECTION
    public static function getHelpPosts()
    {
        return  self::$helpRepository->getHelps();
    }

    public static function getHelpData($id)
    {
        return  self::$helpRepository->getHelpData($id);
    }



//    SELF CREATES SECTION
    public static function add($params)
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = User::getUserBy($us_params);
        $user_id = $user[0]->id;
        $params['user_id'] = $user_id;
        $params['category'] = $user_id;
        $params['category'] = 'Свои материалы';
        foreach($params as $key=>$value){
            if($value == ''){
                $params[$key] = null;
            }
        }
        for ($i=3;$i<=38;$i++){
            if($i == 5)continue;
            if(!isset($params["COL$i"]))$params["COL$i"]=null;
        }

        $result = self::$selfcreateRepository->createSelfCreate($params);
        if($result)
        return header('Location: /profile');

    }

    public static function getSelfCreates()
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = self::getUserBy($us_params);
        if(empty($user)){
            return;
        }
        $user_id = $user[0]->id;

        $self_params['col'] = 'user_id';
        $self_params['val'] = $user_id;
        return  self::$selfcreateRepository->getSelfCreateBy($self_params);
    }

    public static function getSelfCreateData($id)
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = self::getUserBy($us_params);
        $user_id = $user[0]->id;
        $result = self::$selfcreateRepository->getSelfCreateData($id);
        if($result != null){
            if($result->user_id != $user_id){
                return "error";
            }
        }
        return $result;

    }

    public static function updateSelf($params)
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = User::getUserBy($us_params);
        $user_id = $user[0]->id;
        $params['user_id'] = $user_id;
        $params['category'] = $user_id;
        $params['category'] = 'Свои материалы';

        for ($i=3;$i<=38;$i++){
            if($i == 5)continue;
            if(!isset($params["COL$i"]))$params["COL$i"]=null;
        }

        if (isset($params['name']) && $params['name'] != null) {
            $id = $params['material_id'];
            foreach($params as $key=>$value){
                if($value == ''){
                    $params[$key] = NULL;
                }
                if($key == 'material_id')
                    continue;
                $materialId = self::$selfcreateRepository->updateSelfCreateNew($key,$value,$id);
                if(!$materialId)
                    die('error' . $key);
            }
            header('Location: /profile');
        }
        return "empty name";
    }

    public static function removeSelf($params)
    {
        $user_key = Cookie::get('auth-Key');
        $us_params['col'] = 'secret';
        $us_params['val'] = $user_key;
        $user = User::getUserBy($us_params);
        $user_id = $user[0]->id;
        if($params['user_id'] != $user_id)
            return "error";
        $result = self::$selfcreateRepository->remove($params['id']);
        return '';
    }


}
