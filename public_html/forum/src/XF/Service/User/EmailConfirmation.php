<?php

namespace XF\Service\User;

class EmailConfirmation extends AbstractConfirmationService
{
	public function getType()
	{
		return 'email';
	}

	public function canTriggerConfirmation(&$error = null)
	{
		if (!$this->user->isAwaitingEmailConfirmation())
		{
			$error = \XF::phrase('your_account_does_not_require_confirmation');
			return false;
		}

		if (!$this->user->email)
		{
			$error = \XF::phrase('this_account_cannot_be_confirmed_without_email_address');
			return false;
		}

		return true;
	}

	public function emailConfirmed()
	{

        $user = $this->user;
		if (!$user->isAwaitingEmailConfirmation())
		{
			return false;
		}


        $originalUserState = $user->user_state;

		if ($user->user_state == 'email_confirm')
		{
			// don't log when changing from initial confirm state as it creates a lot of noise
			$user->getBehavior('XF:ChangeLoggable')->setOption('enabled', false);
		}



////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////               ///////////////////////////////////////////////
////////////////////////////////////////// MY ADDED PART ///////////////////////////////////////////////
//////////////////////////////////////////               ///////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

//        $_COOKIE["auth-Key"];

        $params['email_salt'] = $user->email;
//        $params['secret'] = $_COOKIE["auth-Key"];
        $postdata = http_build_query($params);
        $opts = array('http' =>
            array(
                'method'  => 'POST',
                'header'  => 'Content-type: application/x-www-form-urlencoded',
                'content' => $postdata
            )
        );
        $context  = stream_context_create($opts);

        $result = file_get_contents('http://dev-site.ml/email_confirm_forum', false, $context);
//
//        echo "<pre>";
//        print_r($result);
//        echo "</pre>";
//        return false;

////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////

        $this->advanceUserState();
		$user->save();

		if ($this->confirmation->exists())
		{
			$this->confirmation->delete();
		}


        if ($originalUserState == 'email_confirm' && $user->user_state == 'valid')
		{
			/** @var \XF\Service\User\Welcome $userWelcome */
			$userWelcome = $this->service('XF:User\Welcome', $user);
			$userWelcome->send();
		}

		return true;
	}

	protected function advanceUserState()
	{
		$user = $this->user;

		switch ($user->user_state)
		{
			case 'email_confirm':
				if ($this->app->options()->registrationSetup['moderation'])
				{
					$user->user_state = 'moderated';
					break;
				}
			// otherwise, fall through

			case 'email_confirm_edit': // this is a user editing email, never send back to moderation
			case 'moderated':
				$user->user_state = 'valid';
				break;
		}
	}
}