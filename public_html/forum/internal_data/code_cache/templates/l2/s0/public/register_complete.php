<?php
// FROM HASH: 3c0cc67a7719df37aefcec1ad5dc9596
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Регистрация');
	$__finalCompiled .= '

';
	$__templater->setPageParam('head.' . 'robots', $__templater->preEscaped('<meta name="robots" content="noindex" />'));
	$__finalCompiled .= '

<div class="blockMessage">
	';
	if ($__vars['xf']['visitor']['user_state'] == 'email_confirm') {
		$__finalCompiled .= '
		' . 'Спасибо за регистрацию. Для ее завершения, пожалуйста, перейдите по ссылке в письме, которое было только что отправлено Вам по электронной почте.' . '
	';
	} else if ($__vars['xf']['visitor']['user_state'] == 'moderated') {
		$__finalCompiled .= '
		' . 'Спасибо за регистрацию. Ваша учетная запись должна быть утверждена администратором. Когда это произойдет, Вы получите письмо по электронной почте.' . '
	';
	} else if ($__vars['facebook']) {
		$__finalCompiled .= '
		' . 'Спасибо за создание учетной записи с использованием Facebook. Теперь Ваш аккаунт полностью активен.' . '
	';
	} else {
		$__finalCompiled .= '
		' . 'Благодарим, что присоединились к нам. Ваша регистрация завершена.' . '
	';
	}
	$__finalCompiled .= '

	<ul>
		';
	if ($__vars['redirect']) {
		$__finalCompiled .= '<li><a href="' . $__templater->fn('link', array($__vars['redirect'], ), true) . '">' . 'Вернуться к просматриваемой странице' . '</a></li>';
	}
	$__finalCompiled .= '
		<li><a href="' . $__templater->fn('link', array('index', ), true) . '">' . 'Вернуться на главную страницу форума' . '</a></li>
		';
	if ($__templater->method($__vars['xf']['visitor'], 'canEditProfile', array())) {
		$__finalCompiled .= '
			<li><a href="' . $__templater->fn('link', array('account', ), true) . '">' . 'Редактировать информацию о Вашей учетной записи' . '</a></li>
		';
	}
	$__finalCompiled .= '
	</ul>
</div>';
	return $__finalCompiled;
});