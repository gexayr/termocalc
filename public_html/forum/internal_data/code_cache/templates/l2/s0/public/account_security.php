<?php
// FROM HASH: 90b88de3ce5bc1b2a56d68ad3e959910
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Пароль и безопасность');
	$__finalCompiled .= '

';
	$__templater->wrapTemplate('account_wrapper', $__vars);
	$__finalCompiled .= '

';
	$__compilerTemp1 = '';
	if ($__vars['xf']['visitor']['Option']['use_tfa']) {
		$__compilerTemp1 .= '
					' . 'Включено (' . $__templater->filter($__vars['enabledTfaProviders'], array(array('join', array(', ', )),), true) . ')' . '
				';
	} else {
		$__compilerTemp1 .= '
					' . 'Отключено' . '
				';
	}
	$__compilerTemp2 = '';
	if ($__vars['hasPassword']) {
		$__compilerTemp2 .= '
				' . $__templater->formPasswordBoxRow(array(
			'name' => 'old_password',
			'autofocus' => 'autofocus',
		), array(
			'label' => 'Ваш текущий пароль',
			'explain' => 'По соображениям безопасности, Вы должны указать текущий пароль перед его изменением.',
		)) . '

				' . $__templater->formPasswordBoxRow(array(
			'name' => 'password',
			'checkstrength' => 'true',
		), array(
			'label' => 'Новый пароль',
		)) . '

				' . $__templater->formPasswordBoxRow(array(
			'name' => 'password_confirm',
		), array(
			'label' => 'Подтвердить новый пароль',
		)) . '
			';
	} else {
		$__compilerTemp2 .= '
				' . $__templater->formRow('
					' . 'Сейчас Ваша учетная запись не имеет пароля.' . ' <a href="' . $__templater->fn('link', array('account/request-password', ), true) . '" data-xf-click="overlay">' . 'Запрос пароля будет отправлен Вам по электронной почте' . '</a>
				', array(
			'label' => 'Пароль',
		)) . '
			';
	}
	$__compilerTemp3 = '';
	if ($__vars['hasPassword']) {
		$__compilerTemp3 .= '
			' . $__templater->formSubmitRow(array(
			'icon' => 'save',
		), array(
		)) . '
		';
	}
	$__finalCompiled .= $__templater->form('
	<div class="block-container">
		<div class="block-body">
			' . $__templater->formRow('

				' . $__compilerTemp1 . '
				' . $__templater->button('Изменить', array(
		'href' => $__templater->fn('link', array('account/two-step', ), false),
		'class' => 'button--link',
	), '', array(
	)) . '
			', array(
		'rowtype' => 'button',
		'label' => 'Двухфакторная аутентификация',
	)) . '

			<hr class="formRowSep" />

			' . $__compilerTemp2 . '
		</div>
		' . $__compilerTemp3 . '
	</div>
', array(
		'action' => $__templater->fn('link', array('account/security', ), false),
		'ajax' => 'true',
		'class' => 'block',
		'data-force-flash-message' => 'true',
	));
	return $__finalCompiled;
});