<?php
// FROM HASH: 54fc6a4c292158d2190db7f29d65bf69
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= $__templater->formTextBoxRow(array(
		'name' => 'options[live_publishable_key]',
		'value' => $__vars['profile']['options']['live_publishable_key'],
	), array(
		'label' => 'Публичный ключ',
	)) . '

' . $__templater->formTextBoxRow(array(
		'name' => 'options[live_secret_key]',
		'value' => $__vars['profile']['options']['live_secret_key'],
	), array(
		'label' => 'Секретный ключ',
		'explain' => 'Укажите действующие secret и publishable ключи своего <a href="https://dashboard.stripe.com/account/apikeys" target="_blank">аккаунта Stripe</a>.<br />
<br />
Вам также необходимо настроить webhook endpoint в своем аккаунте Stripe со следующим URL-адресом: {boardUrl}/payment_callback.php?_xfProvider=stripe',
	)) . '

<hr class="formRowSep" />

' . $__templater->formTextBoxRow(array(
		'name' => 'options[test_publishable_key]',
		'value' => $__vars['profile']['options']['test_publishable_key'],
	), array(
		'label' => 'Публичный ключ для тестирования',
	)) . '

' . $__templater->formTextBoxRow(array(
		'name' => 'options[test_secret_key]',
		'value' => $__vars['profile']['options']['test_secret_key'],
	), array(
		'label' => 'Секретный ключ для тестирования',
		'explain' => 'Тестовые ключи используются только в том случае, если параметр <code>enableLivePayments</code> в файле <code>config.php</code> имеет значение false.<br />
<br /><b>Примечание:</b> перед тем, как начать принимать реальные платежи, Вам необходимо активировать свой аккаунт в панели управления Stripe.',
	)) . '

<hr class="formRowSep" />

' . $__templater->formRow('
	<div class="formRow-explain">
		' . '<strong>Note:</strong> You must set up a webhook endpoint so that Stripe can send messages in order to verify and process payments. You can do this on the <a href="https://dashboard.stripe.com/account/webhooks">Developers > Webhooks</a> page in your dashboard with the following URL:
		<pre><code>' . $__templater->escape($__vars['xf']['options']['boardUrl']) . '/payment_callback.php?_xfProvider=stripe</code></pre>
		For additional security, it is also recommended to input your "Signing secret" below.' . '
	</div>
', array(
		'label' => '',
	)) . '

<hr class="formRowSep" />

' . $__templater->formCheckBoxRow(array(
	), array(array(
		'label' => 'Проверять webhook вместе с секретом подписи' . $__vars['xf']['language']['label_separator'],
		'selected' => $__vars['profile']['options']['signing_secret'],
		'_dependent' => array($__templater->formTextBox(array(
		'name' => 'options[signing_secret]',
		'value' => $__vars['profile']['options']['signing_secret'],
	))),
		'_type' => 'option',
	)), array(
		'explain' => 'Для проверки входящих webhook-подписей и предотвращения атак необходимо указать &quot;Секретную подпись&quot; из панели управления Stripe (API > Webhooks section).',
	)) . '

<hr class="formRowSep" />

' . $__templater->formCheckBoxRow(array(
	), array(array(
		'name' => 'options[apple_pay_enable]',
		'selected' => $__vars['profile']['options']['apple_pay_enable'],
		'label' => '
		' . 'Включить поддержку Apple Pay' . '
	',
		'_type' => 'option',
	)), array(
		'explain' => 'Требуется аккаунт разработчика Apple и дополнительные настройки в личном кабинете Stripe.',
	));
	return $__finalCompiled;
});