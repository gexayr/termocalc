<?php
// FROM HASH: fa4218947090652ba2b20fb80c237dcc
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= $__templater->formCheckBoxRow(array(
	), array(array(
		'name' => $__vars['formBaseKey'] . '[register_date]',
		'selected' => $__vars['property']['property_value']['register_date'],
		'label' => '
		' . 'Дата регистрации' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[message_count]',
		'selected' => $__vars['property']['property_value']['message_count'],
		'label' => '
		' . 'Всего сообщений' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[reaction_score]',
		'selected' => $__vars['property']['property_value']['reaction_score'],
		'label' => '
		' . 'Reaction score' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[trophy_points]',
		'selected' => $__vars['property']['property_value']['trophy_points'],
		'label' => '
		' . 'Баллы' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[age]',
		'selected' => $__vars['property']['property_value']['age'],
		'label' => '
		' . 'Возраст' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[location]',
		'selected' => $__vars['property']['property_value']['location'],
		'label' => '
		' . 'Местоположение' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[website]',
		'selected' => $__vars['property']['property_value']['website'],
		'label' => '
		' . 'Сайт' . '
	',
		'_type' => 'option',
	),
	array(
		'name' => $__vars['formBaseKey'] . '[custom_fields]',
		'selected' => $__vars['property']['property_value']['custom_fields'],
		'label' => '
		' . 'Дополнительные поля' . '
	',
		'_type' => 'option',
	)), array(
		'rowclass' => $__vars['rowClass'],
		'label' => $__templater->escape($__vars['titleHtml']),
		'hint' => $__templater->escape($__vars['hintHtml']),
		'explain' => $__templater->escape($__vars['property']['description']),
	));
	return $__finalCompiled;
});