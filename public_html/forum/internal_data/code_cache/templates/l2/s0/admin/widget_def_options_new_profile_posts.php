<?php
// FROM HASH: ba9fea873f65044e91cc22a6a06668a2
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '<hr class="formRowSep" />

' . $__templater->formNumberBoxRow(array(
		'name' => 'options[limit]',
		'value' => $__vars['options']['limit'],
		'min' => '1',
	), array(
		'label' => 'Максимальное количество записей',
	)) . '

' . $__templater->formRadioRow(array(
		'name' => 'options[style]',
		'value' => ($__vars['options']['style'] ?: 'simple'),
	), array(array(
		'value' => 'simple',
		'label' => 'Простой',
		'hint' => 'Простой вид, предназначен для узких пространств, таких как боковые панели.',
		'_type' => 'option',
	),
	array(
		'value' => 'full',
		'label' => 'Полный',
		'hint' => 'Полноразмерный вид, отображаемый аналогично профилям.',
		'_type' => 'option',
	)), array(
		'label' => 'Стиль отображения',
	)) . '

' . $__templater->formRadioRow(array(
		'name' => 'options[filter]',
		'value' => ($__vars['options']['filter'] ?: 'latest'),
	), array(array(
		'value' => 'latest',
		'label' => 'Последнее',
		'hint' => 'Список всех последних сообщений профиля (по умолчанию для гостей).',
		'_type' => 'option',
	),
	array(
		'value' => 'followed',
		'label' => 'Подписчики',
		'hint' => 'Список сообщений профиля пользователей, на которых подписан автор или другие люди.',
		'_type' => 'option',
	)), array(
		'label' => 'Фильтровать',
	));
	return $__finalCompiled;
});