<?php
// FROM HASH: 9990829ec922485425685e54453df444
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	if ($__templater->method($__vars['forum'], 'isInsert', array())) {
		$__finalCompiled .= '
	';
		$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Создать раздел');
		$__finalCompiled .= '
';
	} else {
		$__finalCompiled .= '
	';
		$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Редактировать раздел' . $__vars['xf']['language']['label_separator'] . ' ' . $__templater->escape($__vars['node']['title']));
		$__finalCompiled .= '
';
	}
	$__finalCompiled .= '

';
	if ($__templater->method($__vars['forum'], 'isUpdate', array())) {
		$__templater->pageParams['pageAction'] = $__templater->preEscaped('
	' . $__templater->button('', array(
			'href' => $__templater->fn('link', array('forums/delete', $__vars['node'], ), false),
			'icon' => 'delete',
			'overlay' => 'true',
		), '', array(
		)) . '
');
	}
	$__finalCompiled .= '

';
	$__compilerTemp1 = '';
	if (!$__templater->test($__vars['availableFields'], 'empty', array())) {
		$__compilerTemp1 .= '
				<hr class="formRowSep" />

				';
		$__compilerTemp2 = array();
		if ($__templater->isTraversable($__vars['availableFields'])) {
			foreach ($__vars['availableFields'] AS $__vars['fieldId'] => $__vars['field']) {
				$__compilerTemp2[] = array(
					'value' => $__vars['fieldId'],
					'label' => $__templater->escape($__vars['field']['title']),
					'labelclass' => ($__vars['field']['required'] ? 'u-appendAsterisk' : ''),
					'_type' => 'option',
				);
			}
		}
		$__compilerTemp1 .= $__templater->formCheckBoxRow(array(
			'name' => 'available_fields',
			'value' => $__vars['forum']['field_cache'],
			'listclass' => 'field listColumns',
		), $__compilerTemp2, array(
			'label' => 'Доступные поля',
			'explain' => '* Поля, отмеченные звездочкой, являются обязательными для нового контента. Другие поля являются необязательными.',
			'hint' => '
						' . $__templater->formCheckBox(array(
			'standalone' => 'true',
		), array(array(
			'check-all' => '.field.listColumns',
			'label' => 'Выбрать все',
			'_type' => 'option',
		))) . '
					',
		)) . '
			';
	} else {
		$__compilerTemp1 .= '
				<hr class="formRowSep" />

				' . $__templater->formRow('
					' . $__templater->filter('Нет', array(array('parens', array()),), true) . ' <a href="' . $__templater->fn('link', array('custom-thread-fields/add', ), true) . '" target="_blank">' . 'Добавить поле' . '</a>
				', array(
			'label' => 'Доступные поля',
		)) . '
			';
	}
	$__compilerTemp3 = '';
	if (!$__templater->test($__vars['prefixesGrouped'], 'empty', array())) {
		$__compilerTemp3 .= '
				<hr class="formRowSep" />

				';
		$__compilerTemp4 = array();
		if ($__templater->isTraversable($__vars['prefixGroups'])) {
			foreach ($__vars['prefixGroups'] AS $__vars['prefixGroupId'] => $__vars['prefixGroup']) {
				if ($__vars['prefixesGrouped'][$__vars['prefixGroupId']]) {
					$__compilerTemp4[] = array(
						'check-all' => 'true',
						'listclass' => 'listColumns',
						'label' => ($__vars['prefixGroupId'] ? $__vars['prefixGroup']['title'] : 'Без группы'),
						'_type' => 'optgroup',
						'options' => array(),
					);
					end($__compilerTemp4); $__compilerTemp5 = key($__compilerTemp4);
					if ($__templater->isTraversable($__vars['prefixesGrouped'][$__vars['prefixGroupId']])) {
						foreach ($__vars['prefixesGrouped'][$__vars['prefixGroupId']] AS $__vars['prefixId'] => $__vars['prefix']) {
							$__compilerTemp4[$__compilerTemp5]['options'][] = array(
								'value' => $__vars['prefixId'],
								'selected' => $__vars['forum']['prefix_cache'][$__vars['prefixId']],
								'label' => '<span class="label ' . $__templater->escape($__vars['prefix']['css_class']) . '">' . $__templater->escape($__vars['prefix']['title']) . '</span>',
								'_type' => 'option',
							);
						}
					}
				}
			}
		}
		$__compilerTemp3 .= $__templater->formCheckBoxRow(array(
			'name' => 'available_prefixes',
			'listclass' => 'prefix',
			'data-xf-init' => 'checkbox-select-disabler',
			'data-select' => 'select[name=default_prefix_id]',
		), $__compilerTemp4, array(
			'rowtype' => 'explainOffset',
			'label' => 'Доступные префиксы',
			'explain' => 'Выберите префиксы, которые будут доступны для использования в этом разделе',
			'hint' => '
						' . $__templater->formCheckBox(array(
			'standalone' => 'true',
		), array(array(
			'check-all' => '.prefix',
			'label' => 'Выбрать все',
			'_type' => 'option',
		))) . '
					',
		)) . '

				';
		$__compilerTemp6 = array(array(
			'value' => '-1',
			'label' => 'Нет',
			'_type' => 'option',
		));
		if ($__templater->isTraversable($__vars['prefixGroups'])) {
			foreach ($__vars['prefixGroups'] AS $__vars['prefixGroupId'] => $__vars['prefixGroup']) {
				if (($__templater->fn('count', array($__vars['prefixesGrouped'][$__vars['prefixGroupId']], ), false) > 0)) {
					$__compilerTemp6[] = array(
						'label' => (($__vars['prefixGroupId'] > 0) ? $__vars['prefixGroup']['title'] : 'Без группы'),
						'_type' => 'optgroup',
						'options' => array(),
					);
					end($__compilerTemp6); $__compilerTemp7 = key($__compilerTemp6);
					if ($__templater->isTraversable($__vars['prefixesGrouped'][$__vars['prefixGroupId']])) {
						foreach ($__vars['prefixesGrouped'][$__vars['prefixGroupId']] AS $__vars['prefixId'] => $__vars['prefix']) {
							$__compilerTemp6[$__compilerTemp7]['options'][] = array(
								'value' => $__vars['prefixId'],
								'disabled' => (!$__templater->fn('in_array', array($__vars['prefixId'], $__vars['forum']['prefix_cache'], ), false)),
								'label' => $__templater->escape($__vars['prefix']['title']),
								'_type' => 'option',
							);
						}
					}
				}
			}
		}
		$__compilerTemp3 .= $__templater->formSelectRow(array(
			'name' => 'default_prefix_id',
			'value' => $__vars['forum']['default_prefix_id'],
		), $__compilerTemp6, array(
			'label' => 'Префикс тем по умолчанию',
			'explain' => 'Можно указать префикс тем, который будет выбираться автоматически при создании новых тем в этом разделе. Выбранный префикс также <b>должен</b> быть выбран в списке \'Доступных префиксов\' выше.',
		)) . '

				' . $__templater->formCheckBoxRow(array(
			'name' => 'require_prefix',
			'value' => $__vars['forum']['require_prefix'],
		), array(array(
			'value' => '1',
			'label' => 'Требовать от пользователей выбор префикса',
			'hint' => 'Если включено, то пользователи должны будут выбрать префикс при создании темы или редактировании ее названия. Это не будет иметь силы для модераторов или при перемещении темы.',
			'_type' => 'option',
		)), array(
		)) . '

			';
	} else {
		$__compilerTemp3 .= '

				<hr class="formRowSep" />

				' . $__templater->formRow('
					' . $__templater->filter('Нет', array(array('parens', array()),), true) . ' <a href="' . $__templater->fn('link', array('thread-prefixes/add', ), true) . '" target="_blank">' . 'Добавить префикс' . '</a>
				', array(
			'label' => 'Доступные префиксы',
		)) . '

				' . $__templater->formHiddenVal('default_thread_prefix', '0', array(
		)) . '
				' . $__templater->formHiddenVal('require_prefix', '0', array(
		)) . '

			';
	}
	$__compilerTemp8 = '';
	if (!$__templater->test($__vars['availablePrompts'], 'empty', array())) {
		$__compilerTemp8 .= '

				<hr class="formRowSep" />

				';
		$__compilerTemp9 = array();
		if ($__templater->isTraversable($__vars['promptGroups'])) {
			foreach ($__vars['promptGroups'] AS $__vars['promptGroupId'] => $__vars['promptGroup']) {
				if ($__vars['promptsGrouped'][$__vars['promptGroupId']]) {
					$__compilerTemp9[] = array(
						'check-all' => 'true',
						'listclass' => '_listColumns',
						'label' => ($__vars['promptGroupId'] ? $__vars['promptGroup']['title'] : 'Без группы'),
						'_type' => 'optgroup',
						'options' => array(),
					);
					end($__compilerTemp9); $__compilerTemp10 = key($__compilerTemp9);
					if ($__templater->isTraversable($__vars['promptsGrouped'][$__vars['promptGroupId']])) {
						foreach ($__vars['promptsGrouped'][$__vars['promptGroupId']] AS $__vars['promptId'] => $__vars['prompt']) {
							$__compilerTemp9[$__compilerTemp10]['options'][] = array(
								'value' => $__vars['promptId'],
								'selected' => $__vars['forum']['prompt_cache'][$__vars['promptId']],
								'label' => $__templater->escape($__vars['prompt']['title']),
								'_type' => 'option',
							);
						}
					}
				}
			}
		}
		$__compilerTemp8 .= $__templater->formCheckBoxRow(array(
			'name' => 'available_prompts',
			'listclass' => 'prompt',
		), $__compilerTemp9, array(
			'rowtype' => 'explainOffset',
			'label' => 'Доступные подсказки',
			'explain' => 'Пользователям будет предложено создание новой темы в этом разделе, используя одну из подсказок, выбранных здесь. Подсказка будет отображена в поле ввода заголовка темы, прежде чем будет указано ее название. Если никаких подсказок не выбрано, то будет использована фраза подсказки по умолчанию (<a href="' . $__templater->fn('link', array('phrases/edit-by-name', array(), array('title' => 'thread_prompt.default', ), ), true) . '"><code>thread_prompt.default</code></a>).',
			'hint' => '
						' . $__templater->formCheckBox(array(
			'standalone' => 'true',
		), array(array(
			'check-all' => '.prompt',
			'label' => 'Выбрать все',
			'_type' => 'option',
		))) . '
					',
		)) . '

			';
	}
	$__finalCompiled .= $__templater->form('
	<div class="block-container">
		<div class="block-body">
			' . $__templater->callMacro('node_edit_macros', 'title', array(
		'node' => $__vars['node'],
	), $__vars) . '
			' . $__templater->callMacro('node_edit_macros', 'description', array(
		'node' => $__vars['node'],
	), $__vars) . '

			<hr class="formRowSep" />
			' . $__templater->callMacro('node_edit_macros', 'node_name', array(
		'node' => $__vars['node'],
	), $__vars) . '
			' . $__templater->callMacro('node_edit_macros', 'position', array(
		'node' => $__vars['node'],
		'nodeTree' => $__vars['nodeTree'],
	), $__vars) . '
			' . $__templater->callMacro('node_edit_macros', 'navigation', array(
		'node' => $__vars['node'],
		'navChoices' => $__vars['navChoices'],
	), $__vars) . '
			<hr class="formRowSep" />

			' . $__templater->formCheckBoxRow(array(
	), array(array(
		'name' => 'allow_posting',
		'selected' => $__vars['forum']['allow_posting'],
		'label' => 'Разрешить размещение новых сообщений в этом разделе',
		'hint' => 'Если отключено, то пользователи не смогут добавлять новые, а также редактировать и удалять собственные сообщения. На модераторов форума данная настройка не распространяется.',
		'_type' => 'option',
	),
	array(
		'name' => 'allow_poll',
		'selected' => $__vars['forum']['allow_poll'],
		'label' => 'Разрешить создание опросов в этом разделе',
		'hint' => 'Если отключено, то у пользователя не будет возможности создания опроса при размещении новой темы или возможности добавления опроса в уже созданную тему. Если тема, содержащая опрос, будет перемещена в такой раздел, то опрос в ней останется без изменений.',
		'_type' => 'option',
	),
	array(
		'name' => 'moderate_threads',
		'selected' => $__vars['forum']['moderate_threads'],
		'label' => 'Проверять новые темы, размещаемые в этом разделе',
		'hint' => 'Если включено, то модератор должен будет вручную одобрять темы, размещаемые в этом разделе.',
		'_type' => 'option',
	),
	array(
		'name' => 'moderate_replies',
		'selected' => $__vars['forum']['moderate_replies'],
		'label' => 'Проверять ответы, размещаемые в этом разделе',
		'hint' => 'Если включено, модератор должен будет вручную одобрять сообщения, размещаемые в этом разделе.',
		'_type' => 'option',
	),
	array(
		'name' => 'count_messages',
		'selected' => $__vars['forum']['count_messages'],
		'label' => 'Считать сообщения пользователей в этом разделе',
		'hint' => 'Если отключено, то размещенные (напрямую) сообщения в этом разделе не будут учитываться в счетчике сообщений пользователя.',
		'_type' => 'option',
	),
	array(
		'name' => 'find_new',
		'selected' => $__vars['forum']['find_new'],
		'label' => 'Включать темы из этого раздела, когда пользователь нажимает \'Новые сообщения\'',
		'hint' => 'Если отключено, то темы из этого раздела не будут отображаться в списке новых/непрочитанных сообщений.',
		'_type' => 'option',
	)), array(
	)) . '

			' . $__templater->formNumberBoxRow(array(
		'name' => 'min_tags',
		'value' => $__vars['forum']['min_tags'],
		'min' => '0',
		'max' => '100',
	), array(
		'label' => 'Количество обязательных тегов',
		'explain' => 'Это потребует от пользователей указать, как минимум это количество тегов при создании темы.',
	)) . '

			' . $__templater->formRadioRow(array(
		'name' => 'allowed_watch_notifications',
		'value' => $__vars['forum']['allowed_watch_notifications'],
	), array(array(
		'value' => 'all',
		'label' => 'Новые сообщения',
		'_type' => 'option',
	),
	array(
		'value' => 'thread',
		'label' => 'Новые темы',
		'_type' => 'option',
	),
	array(
		'value' => 'none',
		'label' => 'Нет',
		'_type' => 'option',
	)), array(
		'label' => 'Ограничение на уведомления для отслеживаемых разделов',
		'explain' => 'Можно ограничить количество уведомлений, отправляемых пользователям отслеживаемых ими разделов. Например, если выбрано \'Новые темы\', то пользователи смогут выбирать только между опциями \'Не отправлять уведомления\' и \'Новые темы\'. Это может быть использовано для снижения нагрузки на сервер на посещаемых форумах.',
	)) . '

			' . $__templater->formRow('

				<div class="inputPair">
					' . $__templater->formSelect(array(
		'name' => 'default_sort_order',
		'value' => $__vars['forum']['default_sort_order'],
		'class' => 'input--inline',
	), array(array(
		'value' => 'last_post_date',
		'label' => 'Последнее сообщение',
		'_type' => 'option',
	),
	array(
		'value' => 'post_date',
		'label' => 'Дата начала',
		'_type' => 'option',
	),
	array(
		'value' => 'title',
		'label' => 'Заголовок',
		'_type' => 'option',
	),
	array(
		'value' => 'reply_count',
		'label' => 'Ответы',
		'_type' => 'option',
	),
	array(
		'value' => 'view_count',
		'label' => 'Просмотры',
		'_type' => 'option',
	))) . '
					' . $__templater->formSelect(array(
		'name' => 'default_sort_direction',
		'value' => $__vars['forum']['default_sort_direction'],
		'class' => 'input--inline',
	), array(array(
		'value' => 'desc',
		'label' => 'По убыванию',
		'_type' => 'option',
	),
	array(
		'value' => 'asc',
		'label' => 'По возрастанию',
		'_type' => 'option',
	))) . '
				</div>
			', array(
		'rowtype' => 'input',
		'label' => 'Порядок сортировки по умолчанию',
	)) . '

			' . $__templater->formSelectRow(array(
		'name' => 'list_date_limit_days',
		'value' => $__vars['forum']['list_date_limit_days'],
	), array(array(
		'value' => '0',
		'label' => 'Нет',
		'_type' => 'option',
	),
	array(
		'value' => '7',
		'label' => '' . '7' . ' дн.',
		'_type' => 'option',
	),
	array(
		'value' => '14',
		'label' => '' . '14' . ' дн.',
		'_type' => 'option',
	),
	array(
		'value' => '30',
		'label' => '' . '30' . ' дн.',
		'_type' => 'option',
	),
	array(
		'value' => '60',
		'label' => '' . '2' . ' мес.',
		'_type' => 'option',
	),
	array(
		'value' => '90',
		'label' => '' . '3' . ' мес.',
		'_type' => 'option',
	),
	array(
		'value' => '182',
		'label' => '' . '6' . ' мес.',
		'_type' => 'option',
	),
	array(
		'value' => '365',
		'label' => '1 год',
		'_type' => 'option',
	)), array(
		'label' => 'Ограничение списка тем по дате',
		'explain' => 'Может быть использовано на посещаемых форумах для повышения производительности, отображая список не всех последних и обновленных тем по умолчанию. Это применяется, только, если темы отсортированы по дате последнего сообщения. Также можно включить вручную отображение более старых тем.',
	)) . '

			' . $__templater->callMacro('node_edit_macros', 'style', array(
		'node' => $__vars['node'],
		'styleTree' => $__vars['styleTree'],
	), $__vars) . '

			' . $__compilerTemp1 . '

			' . $__compilerTemp3 . '

			' . $__compilerTemp8 . '

		</div>
		' . $__templater->formSubmitRow(array(
		'icon' => 'save',
		'sticky' => 'true',
	), array(
	)) . '
	</div>
', array(
		'action' => $__templater->fn('link', array('forums/save', $__vars['node'], ), false),
		'ajax' => 'true',
		'class' => 'block',
	));
	return $__finalCompiled;
});