<?php
// FROM HASH: d18f2262b8a173244232ed00935f698a
return array('macros' => array('step_users_config' => function($__templater, array $__arguments, array $__vars)
{
	$__vars = $__templater->setupBaseParamsForMacro($__vars, false);
	$__finalCompiled = '';
	$__vars = $__templater->mergeMacroArguments(array(
		'config' => '!',
	), $__arguments, $__vars);
	$__finalCompiled .= '
	' . $__templater->formCheckBoxRow(array(
	), array(array(
		'name' => 'step_config[users][merge_email]',
		'selected' => $__vars['config']['merge_email'],
		'label' => 'Автоматически объединять пользователей с одинаковыми адресами электронной почты',
		'_type' => 'option',
	),
	array(
		'name' => 'step_config[users][merge_name]',
		'selected' => $__vars['config']['merge_name'],
		'label' => 'Автоматически объединять пользователей с одинаковыми именами',
		'hint' => 'Обратите внимание, что имена, отличающиеся только акцентами, могут считаться одинаковыми.',
		'_type' => 'option',
	)), array(
		'label' => 'Пользователи',
	)) . '
';
	return $__finalCompiled;
},
'step_smilies_config' => function($__templater, array $__arguments, array $__vars)
{
	$__vars = $__templater->setupBaseParamsForMacro($__vars, false);
	$__finalCompiled = '';
	$__vars = $__templater->mergeMacroArguments(array(
		'config' => '',
	), $__arguments, $__vars);
	$__finalCompiled .= '
	' . $__templater->formTextBoxRow(array(
		'name' => 'step_config[smilies][filename]',
		'value' => $__vars['config']['filename'],
		'placeholder' => $__vars['config']['filename'],
	), array(
		'label' => 'Имя XML-файла',
		'explain' => 'Смайлы из исходной системы не импортируются напрямую XenForo, но вся информация о них обрабатывается и добавляется в XML-файл, который можно импортировать с помощью <a href="' . $__templater->fn('link', array('smilies/import', ), true) . '" target="_blank">Импорта смайлов</a>  позже.<br />
<br / >
XML-файл будет помещен в папку \'internal-data\' с указанным здесь именем.',
	)) . '
';
	return $__finalCompiled;
},), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '

';
	return $__finalCompiled;
});