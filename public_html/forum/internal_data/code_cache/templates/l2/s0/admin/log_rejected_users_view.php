<?php
// FROM HASH: 5fa1ba2977168fb644f00ecf64b90603
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Запись журнал отклоненных пользователей');
	$__finalCompiled .= '

<div class="block">
	<div class="block-container">
		<div class="block-body">
			' . $__templater->formRow('
				' . $__templater->fn('username_link', array($__vars['entry']['User'], false, array(
		'href' => $__templater->fn('link', array('users/edit', $__vars['entry']['User'], ), false),
	))) . '
			', array(
		'label' => 'Отклоненный пользователь',
	)) . '
			' . $__templater->formRow('
				' . $__templater->fn('date_dynamic', array($__vars['entry']['reject_date'], array(
	))) . '
			', array(
		'label' => 'Дата',
	)) . '
			';
	$__compilerTemp1 = '';
	if ($__vars['entry']['reject_user_id']) {
		$__compilerTemp1 .= '
					' . $__templater->fn('username_link', array($__vars['entry']['RejectUser'], false, array(
			'href' => $__templater->fn('link_type', array('users/edit', $__vars['entry']['RejectUser'], ), false),
		))) . '
				';
	} else {
		$__compilerTemp1 .= '
					' . 'Н/Д' . '
				';
	}
	$__finalCompiled .= $__templater->formRow('
				' . $__compilerTemp1 . '
			', array(
		'label' => 'Отклонил(а)',
	)) . '
			';
	$__compilerTemp2 = '';
	if ($__vars['entry']['reject_reason']) {
		$__compilerTemp2 .= '
					' . $__templater->escape($__vars['entry']['reject_reason']) . '
				';
	} else {
		$__compilerTemp2 .= '
					' . 'Н/Д' . '
				';
	}
	$__finalCompiled .= $__templater->formRow('
				' . $__compilerTemp2 . '
			', array(
		'label' => 'Причина',
	)) . '
		</div>
	</div>
</div>';
	return $__finalCompiled;
});