<?php
// FROM HASH: 9c5099accefc90d2d35d92680fbab73b
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__templater->pageParams['pageTitle'] = $__templater->preEscaped('Импорт пользователя');
	$__finalCompiled .= '

' . $__templater->form('
	<div class="block-container">
		<div class="block-body">
			' . $__templater->formUploadRow(array(
		'name' => 'upload',
		'accept' => '.xml',
	), array(
		'label' => 'Импорт из загруженного XML-файла',
		'explain' => 'Новому пользователю будет отправлен запрос на сброс пароля на его адрес электронной почты, прежде чем он сможет использовать свой аккаунт.',
	)) . '
		</div>
		' . $__templater->formSubmitRow(array(
		'icon' => 'import',
	), array(
	)) . '
	</div>
', array(
		'action' => $__templater->fn('link', array($__vars['urlPrefix'] . '/import', ), false),
		'upload' => 'true',
		'ajax' => 'true',
		'class' => 'block',
	));
	return $__finalCompiled;
});