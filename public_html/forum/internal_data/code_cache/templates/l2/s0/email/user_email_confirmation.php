<?php
// FROM HASH: faa943c4749072dae44bb3f53a4c2b4f
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '<mail:subject>
	' . '' . $__templater->escape($__vars['xf']['options']['boardTitle']) . ' - запрос на подтверждение учетной записи' . '
</mail:subject>

' . '<p>' . $__templater->escape($__vars['user']['username']) . ', для завершения регистрации на сайте ' . (((('<a href="' . $__templater->fn('link', array('canonical:index', ), true)) . '">') . $__templater->escape($__vars['xf']['options']['boardTitle'])) . '</a>') . ', Вам необходимо подтвердить свою учетную запись, перейдя по ссылке ниже.</p>' . '

<p><a href="' . $__templater->fn('link', array('canonical:account-confirmation/email', $__vars['user'], array('c' => $__vars['confirmation']['confirmation_key'], ), ), true) . '" class="button">' . 'Подтвердите Ваш адрес электронной почты' . '</a></p>';
	return $__finalCompiled;
});