<?php
// FROM HASH: fd73d72ed41d0e1fb7addbf2721170aa
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '<mail:subject>
	' . '' . $__templater->escape($__vars['xf']['options']['boardTitle']) . ' - адрес электронной почты изменен' . '
</mail:subject>

' . '<p>' . $__templater->escape($__vars['user']['username']) . ',</p>

<p>Ваш адрес электронной почты на сайте ' . (((('<a href="' . $__templater->fn('link', array('canonical:index', ), true)) . '">') . $__templater->escape($__vars['xf']['options']['boardTitle'])) . '</a>') . ' был недавно изменен на ' . $__templater->escape($__vars['newEmail']) . '. Если это изменение сделали Вы, то просто проигнорируйте данное сообщение.</p>

<p>Если Вы не запрашивали это изменение, пожалуйста, авторизуйтесь и измените свой пароль и адрес электронной почты. Если Вы не можете сделать это, свяжитесь с администрацией сайта.</p>

<p>Ваш адрес электронной почты был изменен с этого IP: ' . $__templater->escape($__vars['ip']) . '.</p>';
	return $__finalCompiled;
});