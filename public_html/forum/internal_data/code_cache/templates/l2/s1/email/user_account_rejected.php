<?php
// FROM HASH: 1d60ba3d84ffbd468e5a94ded45d07df
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '<mail:subject>
	' . 'Учетная запись на сайте ' . $__templater->escape($__vars['xf']['options']['boardTitle']) . ' отклонена' . '
</mail:subject>

' . '<p>' . $__templater->escape($__vars['user']['username']) . ', к сожалению, учетная запись на сайте ' . (((('<a href="' . $__templater->fn('link', array('canonical:index', ), true)) . '">') . $__templater->escape($__vars['xf']['options']['boardTitle'])) . '</a>') . ' не соответствует нашим правилам. Ваша учетная запись удалена и больше недоступна.</p>' . '

';
	if ($__vars['reason']) {
		$__finalCompiled .= '
	<p>' . 'Была указана следующая причина:' . ' ' . $__templater->escape($__vars['reason']) . '</p>
';
	}
	$__finalCompiled .= '
';
	return $__finalCompiled;
});