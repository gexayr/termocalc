<?php
// FROM HASH: 87a6f0c0240a3db368a5ab20e6291660
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= 'В данный момент Ваша учетная запись ожидает подтверждения. Письмо с подтверждением было отправлено на адрес: ' . $__templater->escape($__vars['xf']['visitor']['email']) . '.' . '<br />
<a href="' . $__templater->fn('link', array('account-confirmation/resend', ), true) . '" data-xf-click="overlay">' . 'Отправить письмо с подтверждением еще раз' . '</a>';
	return $__finalCompiled;
});