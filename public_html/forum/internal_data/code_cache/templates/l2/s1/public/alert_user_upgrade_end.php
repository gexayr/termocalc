<?php
// FROM HASH: a139e2fa0d7763df5d9be80c19143e4e
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= 'Одно из Ваших повышений прав истекло. <a href="' . $__templater->fn('link', array('account/upgrades', ), true) . '" data-tp-primary="on">Продлить сейчас!</a>';
	return $__finalCompiled;
});