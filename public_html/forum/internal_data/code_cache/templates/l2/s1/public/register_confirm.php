<?php
// FROM HASH: 759fbab5593c9c2d7136726103631da4
return array('macros' => array(), 'code' => function($__templater, array $__vars)
{
	$__finalCompiled = '';
	$__finalCompiled .= '<div class="blockMessage">
	';
	if ($__vars['xf']['visitor']['user_state'] == 'moderated') {
		$__finalCompiled .= '
		' . 'Вы подтвердили адрес электронной почты. Теперь Ваша регистрация должна быть одобрена администратором. Вы получите письмо, когда будет принято решение.' . '
	';
	} else if (($__templater->method($__vars['xf']['visitor'], 'getPreviousValue', array('user_state', )) == 'email_confirm_edit')) {
		$__finalCompiled .= '
		' . 'Вы подтвердили адрес электронной почты и теперь Ваша учетная запись снова активна' . '
	';
	} else {
		$__finalCompiled .= '
		' . 'Вы подтвердили адрес электронной почты и завершили регистрацию.' . '
	';
	}
	$__finalCompiled .= '

	<ul>
		';
	if ($__vars['redirect']) {
		$__finalCompiled .= '<li><a href="' . $__templater->escape($__vars['redirect']) . '">' . 'Вернуться к просматриваемой странице' . '</a></li>';
	}
	$__finalCompiled .= '
		<li><a href="' . $__templater->fn('link', array('index', ), true) . '">' . 'Вернуться на главную страницу форума' . '</a></li>
		';
	if ($__templater->method($__vars['xf']['visitor'], 'canEditProfile', array())) {
		$__finalCompiled .= '
			<li><a href="' . $__templater->fn('link', array('account', ), true) . '">' . 'Редактировать информацию о Вашей учетной записи' . '</a></li>
		';
	}
	$__finalCompiled .= '
	</ul>
</div>';
	return $__finalCompiled;
});