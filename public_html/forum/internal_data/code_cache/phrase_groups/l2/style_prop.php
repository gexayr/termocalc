<?php
return array (
  'style_prop.animationSpeed' => 'Скорость базовой анимации',
  'style_prop.avatarBg' => 'Цвет фона аватара',
  'style_prop.avatarBorderRadius' => 'Радиус рамки аватара',
  'style_prop.avatarDefaultImage' => 'URL изображения аватара по умолчанию',
  'style_prop.avatarDefaultTextContent' => 'Текст аватара по умолчанию',
  'style_prop.avatarDefaultType' => 'Тип аватара по умолчанию',
  'style_prop.avatarDynamicFont' => 'Шрифт динамических аватаров',
  'style_prop.avatarDynamicLineHeight' => 'Высота базовой линии динамического аватара',
  'style_prop.avatarDynamicTextPercent' => 'Размер текста динамических аватаров (в процентах)',
  'style_prop.badge' => 'Бейдж',
  'style_prop.badgeHighlighted' => 'Бейдж - подсвеченный',
  'style_prop.bbCodeBlock' => 'Блоки BB-кодов',
  'style_prop.bbCodeBlockExpandHeight' => 'Максимальная высота развернутого блока BB-кода',
  'style_prop.bbCodeBlockTitle' => 'Блок BB-кода - заголовок',
  'style_prop.bbCodeImgFloatMargin' => 'Floated image margin',
  'style_prop.bbCodeImgLarge' => 'BB Code Image - Large Max Width',
  'style_prop.bbCodeImgMedium' => 'BB Code Image - Medium Max Width',
  'style_prop.bbCodeImgSmall' => 'BB Code Image - Small Max Width',
  'style_prop.bbCodeInlineCode' => 'Блок BB-кода - однострочный код',
  'style_prop.blockBorder' => 'Радиус рамки структуры блока',
  'style_prop.blockBorderRadius' => 'Радиус рамки блока',
  'style_prop.blockFilterBar' => 'Панель фильтров в блоке',
  'style_prop.blockFooter' => 'Футер блока',
  'style_prop.blockFooterBg' => 'Block footer background color',
  'style_prop.blockFooterTextColor' => 'Block footer text color',
  'style_prop.blockHeader' => 'Заголовок блока',
  'style_prop.blockLink' => 'Ссылка в блоке',
  'style_prop.blockLinkSelected' => 'Ссылка в блоке - выбранная',
  'style_prop.blockMinorHeader' => 'Заголовок маленького блока',
  'style_prop.blockMinorTabHeader' => 'Заголовок маленькой вкладки блока',
  'style_prop.blockMinorTabHeaderSelected' => 'Заголовок маленькой вкладки блока - выбранная вкладка',
  'style_prop.blockPaddingH' => 'Горизонтальный отступ блока',
  'style_prop.blockPaddingV' => 'Вертикальный отступ блока',
  'style_prop.blockTabHeader' => 'Заголовок вкладки блока',
  'style_prop.blockTabHeaderBg' => 'Block tab header background color',
  'style_prop.blockTabHeaderSelected' => 'Заголовок вкладки блока - выбранная вкладка',
  'style_prop.blockTabHeaderTextColor' => 'Block tab header text color',
  'style_prop.blockTextHeader' => 'Заголовок блока текста',
  'style_prop.borderColor' => 'Цвет рамки',
  'style_prop.borderColorAccentContent' => 'Цвет рамки акцентированного контента',
  'style_prop.borderColorAttention' => 'Цвет рамки для привлечения внимания',
  'style_prop.borderColorFaint' => 'Тусклый цвет рамки',
  'style_prop.borderColorFeature' => 'Цвет функциональной рамки',
  'style_prop.borderColorHeavy' => 'Цвет толстой рамки',
  'style_prop.borderColorHighlight' => 'Цвет выделенной рамки',
  'style_prop.borderColorLight' => 'Светлый цвет рамки',
  'style_prop.borderRadiusLarge' => 'Большой радиус рамки',
  'style_prop.borderRadiusMedium' => 'Средний радиус рамки',
  'style_prop.borderRadiusSmall' => 'Малый радиус рамки',
  'style_prop.borderSize' => 'Размер рамки',
  'style_prop.borderSizeFeature' => 'Размер рамки элемента',
  'style_prop.borderSizeMinorFeature' => 'Размер рамки малого элемента',
  'style_prop.buttonBase' => 'Основная кнопка',
  'style_prop.buttonBg' => 'Default button background color',
  'style_prop.buttonCta' => 'Кнопка - призыв к действию',
  'style_prop.buttonCtaBg' => 'Call-to-action button background',
  'style_prop.buttonDefault' => 'Кнопка - по умолчанию',
  'style_prop.buttonDisabled' => 'Кнопка - отключенная',
  'style_prop.buttonPrimary' => 'Кнопка - основная',
  'style_prop.buttonPrimaryBg' => 'Primary button background color',
  'style_prop.buttonTextColor' => 'Default button text color',
  'style_prop.chip' => 'Марки',
  'style_prop.chipHover' => 'Марки - при наведении',
  'style_prop.chromeBg' => 'Page chrome background color',
  'style_prop.chromeTextColor' => 'Page chrome text color',
  'style_prop.contentAccentBase' => 'Область акцентированного контента',
  'style_prop.contentAccentBg' => 'Цвет фона акцентированного контента',
  'style_prop.contentAccentLink' => 'Область ссылок акцентированного контента',
  'style_prop.contentAltBase' => 'Базовая область альтернативного контента',
  'style_prop.contentAltBg' => 'Цвет фона альтернативного контента',
  'style_prop.contentBase' => 'Базовая область контента',
  'style_prop.contentBg' => 'Цвет фона контента',
  'style_prop.contentHighlightBase' => 'Область выделенного контента',
  'style_prop.contentHighlightBg' => 'Цвет фона выделенного контента',
  'style_prop.dataListFooter' => 'Футер списка данных',
  'style_prop.dataListHeader' => 'Заголовок списка данных',
  'style_prop.dataListPaddingH' => 'Горизонтальный отступ списка данных',
  'style_prop.dataListPaddingV' => 'Вертикальный отступ списка данных',
  'style_prop.dataListSection' => 'Секция заголовка списка данных',
  'style_prop.editorSelectedBg' => 'Цвет фона выделенного текста в редакторе',
  'style_prop.editorSelectedColor' => 'Цвет выделенного текста в редакторе',
  'style_prop.editorToolbarActiveColor' => 'Цвет активной панели инструментов редактора',
  'style_prop.editorToolbarBg' => 'Цвет фона панели инструментов редактора',
  'style_prop.editorToolbarBorderColor' => 'Цвет рамки панели инструментов редактора',
  'style_prop.editorToolbarColor' => 'Editor toolbar icon color',
  'style_prop.elementSpacer' => 'Интервалов между элементами',
  'style_prop.emailBg' => 'Цвет фона электронной почты',
  'style_prop.emailBorderColor' => 'Цвет границ электронного письма',
  'style_prop.emailContentAltBg' => 'Альтернативный цвет фона содержимого электронного письма',
  'style_prop.emailContentBg' => 'Цвет фона содержимого электронного письма',
  'style_prop.emailFont' => 'Шрифт электронной почты',
  'style_prop.emailHeaderColor' => 'Цвет текста заголовка электронной почты',
  'style_prop.emailLinkColor' => 'Цвет ссылок в электронном письме',
  'style_prop.emailTextColor' => 'Цвет текста электронного письма',
  'style_prop.emailTextColorMuted' => 'Цвет приглушенного текста в электронном письме',
  'style_prop.errorBg' => 'Error message background',
  'style_prop.errorColor' => 'Error message text color',
  'style_prop.errorFeatureColor' => 'Error message feature color',
  'style_prop.findThreadsNavStyle' => 'Стиль навигации страницы "Поиск тем"',
  'style_prop.fixedMessage' => 'Панель закрепленного сообщения',
  'style_prop.flashMessage' => 'Flash message bar',
  'style_prop.fontAwesomeWeight' => 'Font Awesome weight',
  'style_prop.fontFamilyBody' => 'Шрифт тела страницы',
  'style_prop.fontFamilyCode' => 'Шрифт кода',
  'style_prop.fontFamilyUi' => 'Шрифт пользовательского интерфейса',
  'style_prop.fontSizeLarge' => 'Большой размер шрифта',
  'style_prop.fontSizeLarger' => 'Больший размер шрифта',
  'style_prop.fontSizeLargest' => 'Самый большой размер шрифта',
  'style_prop.fontSizeNormal' => 'Нормальный размер шрифта',
  'style_prop.fontSizeSmall' => 'Малый размер шрифта',
  'style_prop.fontSizeSmaller' => 'Меньший размер шрифта',
  'style_prop.fontSizeSmallest' => 'Наименьший размер шрифта',
  'style_prop.fontWeightHeavy' => 'Толстое начертание шрифта',
  'style_prop.fontWeightLight' => 'Тонкое начертание шрифта',
  'style_prop.fontWeightNormal' => 'Нормальное начертание шрифта',
  'style_prop.formExplain' => 'Текст объяснения элемента формы',
  'style_prop.formHint' => 'Текст подсказки элемента формы',
  'style_prop.formLabel' => 'Столбец лейбла формы',
  'style_prop.formLabelWidth' => 'Ширина столбца лейбла формы',
  'style_prop.formResponsive' => 'Контрольная точка адаптивного дизайна для строки формы',
  'style_prop.formRowPaddingHInner' => 'Горизонтальный (внешний) отступ строки формы ',
  'style_prop.formRowPaddingHOuter' => 'Вертикальный (внешний) отступ строки формы ',
  'style_prop.formRowPaddingV' => 'Вертикальный отступ строки формы',
  'style_prop.formSectionHeader' => 'Заголовок секции формы',
  'style_prop.formSubmitRow' => 'Строка отправки формы',
  'style_prop.formSubmitSticky' => 'Включить плавающую строку отправки форм',
  'style_prop.globalActionColor' => 'Цвет индикатора глобальных действий',
  'style_prop.inlineModBar' => 'Панель встроенной модерации',
  'style_prop.inlineModHighlightColor' => 'Цвет подсветки встроенной модерации',
  'style_prop.input' => 'Форма ввода',
  'style_prop.inputBgColor' => 'Text input box background color',
  'style_prop.inputDisabled' => 'Форма ввода - отключено',
  'style_prop.inputFocus' => 'Форма ввода - сфокусировано',
  'style_prop.inputFocusBgColor' => 'Focused text box background color',
  'style_prop.inputTextColor' => 'Text input box text color',
  'style_prop.lineHeightDefault' => 'Межстрочный интервал',
  'style_prop.link' => 'Ссылки',
  'style_prop.linkColor' => 'Цвет ссылки',
  'style_prop.linkHover' => 'Ссылки - при наведении',
  'style_prop.linkHoverColor' => 'Цвет ссылки при наведении',
  'style_prop.majorHeadingBg' => 'Major heading background color',
  'style_prop.majorHeadingTextColor' => 'Major heading text color',
  'style_prop.memberHeader' => 'Заголовок профиля пользователя',
  'style_prop.memberHeaderName' => 'Заголовок профиля пользователя - имя',
  'style_prop.memberTooltipHeader' => 'Заголовок пользовательской подсказки',
  'style_prop.memberTooltipName' => 'Заголовок пользовательской подсказки - имя',
  'style_prop.menu' => 'Меню',
  'style_prop.menuBorderRadius' => 'Радиус рамки меню',
  'style_prop.menuFeatureBorderColor' => 'Цвет рамки функционального меню',
  'style_prop.menuFooter' => 'Футер меню',
  'style_prop.menuHeader' => 'Заголовок меню',
  'style_prop.menuLinkRow' => 'Строка ссылки меню',
  'style_prop.menuLinkRowSelected' => 'Строка ссылки меню - выбранная',
  'style_prop.menuTabHeader' => 'Заголовок вкладки меню',
  'style_prop.menuTabHeaderSelected' => 'Заголовок вкладки меню - выбранная вкладка',
  'style_prop.messageNewIndicator' => 'Индикатор нового сообщения',
  'style_prop.messagePadding' => 'Отступ сообщения',
  'style_prop.messagePaddingSmall' => 'Маленький отступ сообщения',
  'style_prop.messageSignature' => 'Подпись в сообщении',
  'style_prop.messageSingleColumnWidth' => 'Контрольная точка одного столбца сообщения',
  'style_prop.messageUserBlock' => 'Блок информации о пользователе',
  'style_prop.messageUserBlockWidth' => 'Ширина блока информации о пользователе',
  'style_prop.messageUserElements' => 'Элементы в блоке информации о пользователе',
  'style_prop.metaThemeColor' => 'Цвет метатемы Chrome',
  'style_prop.minorBlockContent' => 'Контент незначительного внутреннего блока',
  'style_prop.minorHeadingTextColor' => 'Minor heading text color',
  'style_prop.nodeIconReadColor' => 'Цвет иконки прочитанных узлов',
  'style_prop.nodeIconUnreadColor' => 'Цвет иконки непрочитанных узлов',
  'style_prop.nodeListDescriptionDisplay' => 'Стиль отображения описаний',
  'style_prop.nodeListSubDisplay' => 'Стиль отображения подкатегорий',
  'style_prop.noticeScrollInterval' => 'Интервал прокрутки объявлений (прокручивающегося типа)',
  'style_prop.overlayHeader' => 'Заголовок всплывающего окна',
  'style_prop.overlayMaskBlur' => 'Overlay mask blur amount',
  'style_prop.overlayMaskColor' => 'Цвет маски всплывающего окна',
  'style_prop.overlayTopMargin' => 'Отступ всплывающего окна',
  'style_prop.paddingLarge' => 'Большой отступ',
  'style_prop.paddingLargest' => 'Самый большой отступ',
  'style_prop.paddingMedium' => 'Средний отступ',
  'style_prop.paddingSmall' => 'Малый отступ',
  'style_prop.pageBackground' => 'Фон страницы',
  'style_prop.pageBg' => 'Цвет фона страницы',
  'style_prop.pageEdgeSpacer' => 'Отступ от края страницы',
  'style_prop.pageNavStyle' => 'Стиль постраничной навигации',
  'style_prop.pageWidthMax' => 'Максимальная ширина страницы',
  'style_prop.paletteAccent1' => 'Акцент 1',
  'style_prop.paletteAccent2' => 'Акцент 2',
  'style_prop.paletteAccent3' => 'Акцент 3',
  'style_prop.paletteColor1' => 'Цвет 1',
  'style_prop.paletteColor2' => 'Цвет 2',
  'style_prop.paletteColor3' => 'Цвет 3',
  'style_prop.paletteColor4' => 'Цвет 4',
  'style_prop.paletteColor5' => 'Цвет 5',
  'style_prop.paletteNeutral1' => 'Нейтральный 1',
  'style_prop.paletteNeutral2' => 'Нейтральный 2',
  'style_prop.paletteNeutral3' => 'Нейтральный 3',
  'style_prop.profilePostCommentToggle' => 'Enable profile post comment input toggle',
  'style_prop.progressBarColor' => 'Цвет индикатора выполнения',
  'style_prop.publicFaviconUrl' => 'Путь к Favicon (32x32)',
  'style_prop.publicFooter' => 'Футер',
  'style_prop.publicFooterLink' => 'Ссылки в футере',
  'style_prop.publicHeader' => 'Область шапки/логотипа',
  'style_prop.publicHeaderAdjustColor' => 'Регулировка цвета заголовка',
  'style_prop.publicLogoUrl' => 'Путь к логотипу',
  'style_prop.publicLogoUrl2x' => 'Путь к 2x логотипу',
  'style_prop.publicMetadataLogoUrl' => 'Путь к Metadata логотипу',
  'style_prop.publicNav' => 'Область навигации',
  'style_prop.publicNavCollapseWidth' => 'Ширина свернутой навигации',
  'style_prop.publicNavPaddingH' => 'Горизонтальный отступ навигации',
  'style_prop.publicNavPaddingV' => 'Вертикальный отступ навигации',
  'style_prop.publicNavSelected' => 'Область навигации - выбранная вкладка',
  'style_prop.publicNavSticky' => 'Тип плавающей навигации',
  'style_prop.publicNavTab' => 'Область навигации - вкладка',
  'style_prop.publicNavTabHover' => 'Область навигации - вкладка при наведении',
  'style_prop.publicNavTabMenuOpen' => 'Область навигации - открытое меню',
  'style_prop.publicPushBadgeUrl' => 'Push notification badge URL',
  'style_prop.publicStaffBar' => 'Панель инструментов для членов команды',
  'style_prop.publicSubNav' => 'Область субнавигации',
  'style_prop.publicSubNavElHover' => 'Область субнавигации - элемент при наведении',
  'style_prop.publicSubNavElMenuOpen' => 'Область субнавигации - элемент открытого меню',
  'style_prop.publicSubNavPaddingH' => 'Горизонтальный отступ субнавигации',
  'style_prop.publicSubNavPaddingV' => 'Вертикальный отступ субнавигации',
  'style_prop.reactionSummaryOnLists' => 'Show reaction summary on lists',
  'style_prop.responsiveEdgeSpacerRemoval' => 'Контрольная точка отмены применения отступа от края страницы',
  'style_prop.responsiveMedium' => 'Средняя контрольная точка адаптивного дизайна',
  'style_prop.responsiveNarrow' => 'Узкая контрольная точка адаптивного дизайна',
  'style_prop.responsiveWide' => 'Широкая контрольная точка адаптивного дизайна',
  'style_prop.scrollJumpButtons' => 'Отображение кнопок прокрутки страницы',
  'style_prop.sidebarSpacer' => 'Отступ боковой панели и боковой навигации',
  'style_prop.sidebarWidth' => 'Ширина боковой панели и боковой навигации',
  'style_prop.standaloneTab' => 'Отдельные вкладки',
  'style_prop.standaloneTabSelected' => 'Отдельные вкладки - выбранная вкладка',
  'style_prop.starEmptyColor' => 'Цвет пустой звезды рейтинга',
  'style_prop.starFullColor' => 'Цвет активной звезды рейтинга',
  'style_prop.styleType' => 'Тип стиля',
  'style_prop.subNavBg' => 'Page sub-navigation background',
  'style_prop.subNavTextColor' => 'Page sub-navigation text color',
  'style_prop.successBg' => 'Success message background',
  'style_prop.successColor' => 'Success message text color',
  'style_prop.successFeatureColor' => 'Success message feature color',
  'style_prop.textColor' => 'Цвет текста',
  'style_prop.textColorAccentContent' => 'Цвет акцентированного текста',
  'style_prop.textColorAttention' => 'Цвет текста для привлечения внимания',
  'style_prop.textColorDimmed' => 'Цвет тусклого текста',
  'style_prop.textColorEmphasized' => 'Цвет подчеркнутого текста',
  'style_prop.textColorFeature' => 'Цвет текста объекта',
  'style_prop.textColorMuted' => 'Цвет приглушенного текста',
  'style_prop.tooltip' => 'Всплывающая подсказка',
  'style_prop.warningBg' => 'Warning message background',
  'style_prop.warningColor' => 'Warning message text color',
  'style_prop.warningFeatureColor' => 'Warning message feature color',
  'style_prop.whatsNewNavStyle' => 'Стиль навигации страницы "Что нового"',
  'style_prop.zIndexMultiplier' => 'Множитель Z-индекса',
);