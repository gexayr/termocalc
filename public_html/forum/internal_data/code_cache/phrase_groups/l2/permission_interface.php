<?php
return array (
  'permission_interface.bookmarkPermissions' => 'Bookmark permissions',
  'permission_interface.conversationModeratorPermissions' => 'Права модератора в личных переписках',
  'permission_interface.conversationPermissions' => 'Права личных переписок',
  'permission_interface.forumModeratorPermissions' => 'Права модератора форума',
  'permission_interface.forumPermissions' => 'Права форума',
  'permission_interface.generalModeratorPermissions' => 'Основные права модератора',
  'permission_interface.generalPermissions' => 'Основные права',
  'permission_interface.postAttachmentPermissions' => 'Права вложений',
  'permission_interface.profilePostModeratorPermissions' => 'Права модератора в сообщениях профиля',
  'permission_interface.profilePostPermissions' => 'Права сообщений профиля',
  'permission_interface.signaturePermissions' => 'Права подписи',
);