<?php
return array (
  'tfa_desc.authy' => 'If you use the <a href="https://authy.com/download/" target="_blank">Authy app</a> on your device, you can utilise their OneTouch feature to authenticate requests with a simple tap/click.',
  'tfa_desc.backup' => 'Эти коды могут использоваться для входа в систему, если у вас нет доступа к другим методам проверки. Храните эти коды в надежном и безопасном месте.',
  'tfa_desc.email' => 'Коды подтверждения будут отправляться по электронной почте. По возможности, лучше выбирать другие способы двухфакторной аутентификации.',
  'tfa_desc.totp' => 'Генерация кодов подтверждения с помощью приложение на Вашем смартфоне.',
);