<?php
return array (
  'user_field_title.facebook' => 'Facebook',
  'user_field_title.phone_for_calc' => 'Телефон',
  'user_field_title.skype' => 'Skype',
  'user_field_title.status_for_calc' => 'Статус',
  'user_field_title.status_other_for_calc' => 'Другой',
  'user_field_title.surname_for_calc' => 'Фамилия',
  'user_field_title.twitter' => 'Twitter',
);