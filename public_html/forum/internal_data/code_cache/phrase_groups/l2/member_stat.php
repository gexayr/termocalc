<?php
return array (
  'member_stat.highest_reaction_score' => 'Highest reaction score',
  'member_stat.most_likes' => 'Больше всех симпатий',
  'member_stat.most_messages' => 'Больше всех сообщений',
  'member_stat.most_points' => 'Больше всех баллов',
  'member_stat.staff_members' => 'Члены команды',
  'member_stat.todays_birthdays' => 'День рождения сегодня',
);