<?php
return array (
  'widget.forum_overview_forum_statistics' => '',
  'widget.forum_overview_members_online' => '',
  'widget.forum_overview_new_posts' => '',
  'widget.forum_overview_new_profile_posts' => '',
  'widget.forum_overview_share_page' => '',
  'widget.latest_followed_profile_posts' => 'Последние сообщения профиля подписчиков',
  'widget.latest_posts' => 'Новые сообщения',
  'widget.latest_profile_posts' => 'Сообщения профиля',
  'widget.latest_watched' => 'Последнее отслеживаемое',
  'widget.member_wrapper_find_member' => '',
  'widget.member_wrapper_newest_members' => '',
  'widget.online_list_online_statistics' => '',
  'widget.unread_posts' => 'Непрочитанные сообщения',
  'widget.whats_new_new_posts' => '',
  'widget.whats_new_new_profile_posts' => '',
);