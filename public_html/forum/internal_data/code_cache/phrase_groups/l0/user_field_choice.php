<?php
return array (
  'user_field_choice.status_for_calc_architect' => 'Архитектор или инженер-конструктор',
  'user_field_choice.status_for_calc_construction' => 'Торговля стройтоварами и услугами',
  'user_field_choice.status_for_calc_contractor' => 'Исполнитель стройработ (частный подрядчик)',
  'user_field_choice.status_for_calc_customer' => 'Заказчик строительных услуг (бизнес-сектор)',
  'user_field_choice.status_for_calc_developer' => 'Самостоятельный застройщик для себя (коттеджа, квартиры и т.п.)',
  'user_field_choice.status_for_calc_organization' => 'Исполнитель работ (подрядная организация)',
  'user_field_choice.status_for_calc_other' => 'Другое (указать в строчке)',
  'user_field_choice.status_for_calc_private_customer' => 'Частный заказчик строительных услуг (найм подрядчиков для строительства)',
);