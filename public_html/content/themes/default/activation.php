<?php
$message = '';
if (!empty($_GET['code']) && isset($_GET['code'])) {
    $code = $_GET['code'];
    $params['col'] = 'secret';
    $params['val'] = $code;
    $active = User::getUserBy($params);
    $active = $active[0];

    if (!empty($active)) {
        $id = $active->id;
        $user = get_object_vars($active);
        if ($active->verified == 0) {
            User::activity($user);
            $class = 'success';
            $message = "Аккаунт активирован";
        } else {
            $class = 'info';
            $message = 'Ваша аккаунт уже активен, нет необходимости активировать его снова.';
        }
    } else {
        $class = 'danger';
        $message = 'Неверный код активации. :(';
    }
} else {
    $class = 'danger';
    $message = 'Это не код активации. :(';
}

?>
<?php $this->theme->header(); ?>
<div class="container activity-message">
    <div class="row">
        <div class="activity">
            <div class="text-center alert alert-<?=$class;?>">
                <?= $message ?>
            </div>
        </div>
    </div>
</div>
<style>
    footer{
        position: fixed;
        width: 100%;
        bottom: 0;
    }
</style>
<?php $this->theme->footer(); ?>
