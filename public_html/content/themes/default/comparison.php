<?php $this->theme->header(); ?>
    <div class="container post-block">
        <div class="row">
            <?php $this->theme->sidebar(); ?>
            <?php
            require  "part/login.php";
            require  "part/if_not_login.php";

            $links_data = Data::getLinks();
            //debug($links_data);
            if(empty($links_data)){
                echo "
        <div class='alert alert-danger alert-dismissible text-center'>
            У Вас пока нет сохраненных записей
        </div>
    </div>
</div>";
                $this->theme->footer();
                die;
            }

            $check_compare = false;
            foreach ($links_data as $item) {
                if($item->compare){
                    $check_compare = true;
                    break;
                }
            }

            if(empty($check_compare)){
                echo "
        <div class='alert alert-danger alert-dismissible text-center'>
            Вы пока не выбрали записи для сравнения, можно выбрать в личном кабинете
        </div>
    </div>
</div>";
                $this->theme->footer();
                die;
            }
            ?>
            <div class="compare_block" style="overflow: auto;">
                <form action="/pdf" method="post">
                    <table id="t01_">
                        <caption>Таблица сравнений результатов</caption>
                        <tbody id="t01_tbody">
                        <?php
                        $link = '';
                        if(!is_null(User::getCookie('layer_link'))){
                            $link = User::getCookie('layer_link');
                        }
                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>название расчёта</b>";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if($item->compare){
                                ${"result_all_$i"} = 0;
                                ${"result_25_all_$i"} = 0;
                                $itemName = $item->layer_link == $link
                                    ? "<a href='/?params=$item->layer_link'><b>$item->name</b></a>"
                                    : "<a href='/?params=$item->layer_link'>$item->name</a>";
                                echo "<td data-id='$item->id'>$itemName</td>";
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2'>";
                        echo "Здесь можете выбрать понравившийся вариант. После нажатия на кнопку,
                         появится ссылка для распечатки файла-отчёта в pdf, который вы можете
                         просматривать или распечатать на принтере.";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                echo "<td>
<input type='radio' class='radio-for-compare' name='item_radio' value='$item->id' data-id='$item->id'>
                                    </td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2'>";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                echo "<td><a href='#' data-toggle='modal' title='редактирование названия расчёта'
                                data-target='#myModalSave' onclick=\"editDataLayer($item->id, '$item->name')\">
                                <i class='fa fa-edit'></i></td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>индикатор наличия несоответствия</b>";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $color = '#efc02d';
                                $compareData = (json_decode($item->compare_data));
                                if($compareData->is_accepted)
                                    $color = '#65ae39';

                                echo "<td style='background-color: $color'></td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td rowspan='5' style='font-size: 10px;'>";
                        echo "<b>коэффициент теплосопротивления конструкции м2·°C/Вт</b>";
                        echo "</td>";
                        echo "<td style='font-size: 10px'>";
                        echo "стены";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $compareData_td145_2 = '';
                                foreach($compareData as $key=>$value){
                                    switch ($key) {
                                        case 2:
                                            $compareData_td145_2 = (json_decode($value))->td145;
                                            break;
                                    }
                                }
                                echo "<td>$compareData_td145_2</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "кровля";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $compareData_td145_3 = '';
                                foreach($compareData as $key=>$value){
                                    switch ($key) {
                                        case 3:
                                            $compareData_td145_3 = (json_decode($value))->td145;
                                            break;
                                    }
                                }
                                echo "<td>$compareData_td145_3</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "полы";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $compareData_td145_4 = '';
                                foreach($compareData as $key=>$value){
                                    switch ($key) {
                                        case 4:
                                            $compareData_td145_4 = (json_decode($value))->td145;
                                            break;
                                    }
                                }
                                echo "<td>$compareData_td145_4</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "окна";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $TD145WI = '';
                                $res = (json_decode($item->layer_data_5));
                                if(isset($res->TD145WI)){
                                    $TD145WI = $res->TD145WI;
                                }
                                echo "<td>$TD145WI</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "фонари";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $TD145La = '';
                                $res = (json_decode($item->layer_data_5));
                                if(isset($res->TD145La)){
                                    $TD145La = $res->TD145La;
                                }
                                echo "<td>$TD145La</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>мощность системы отопления, кВт</b>";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $compare_td313 = '';
                                if(isset($compareData->td313))
                                    $compare_td313 = $compareData->td313 * 0.001;

                                echo "<td>$compare_td313</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td rowspan='5' style='font-size: 10px'>";
                        echo "<b>Общая стоимость</b>";
                        echo "</td>";
                        echo "<td style='font-size: 10px'>";
                        echo "стены";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_2 = '';
                                if(isset($compareData->total_cost_2))
                                    $total_cost_2 = round($compareData->total_cost_2,2);

                                echo "<td>$total_cost_2</td>";
                                $total_cost_2_num = $total_cost_2 ? $total_cost_2 : 0;
                                ${"result_all_$i"} += $total_cost_2_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "кровля";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_3 = '';
                                if(isset($compareData->total_cost_3))
                                    $total_cost_3 = round($compareData->total_cost_3,2);

                                echo "<td>$total_cost_3</td>";
                                $total_cost_3_num = $total_cost_3 ? $total_cost_3 : 0;
                                ${"result_all_$i"} += $total_cost_3_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "полы";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_4 = '';
                                if(isset($compareData->total_cost_4))
                                    $total_cost_4 = round($compareData->total_cost_4,2);

                                echo "<td>$total_cost_4</td>";
                                $total_cost_4_num = $total_cost_4 ? $total_cost_4 : 0;
                                ${"result_all_$i"} += $total_cost_4_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "окна";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_5_1 = '';
                                if(isset($compareData->total_cost_5_1))
                                    $total_cost_5_1 = round($compareData->total_cost_5_1,2);

                                echo "<td>$total_cost_5_1</td>";
                                $total_cost_5_1_num = $total_cost_5_1 ? $total_cost_5_1 : 0;
                                ${"result_all_$i"} += $total_cost_5_1_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td style='font-size: 10px'>";
                        echo "фонари";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_5_2 = '';
                                if(isset($compareData->total_cost_5_2))
                                    $total_cost_5_2 = round($compareData->total_cost_5_2,2);

                                echo "<td>$total_cost_5_2</td>";
                                $total_cost_5_2_num = $total_cost_5_2 ? $total_cost_5_2 : 0;
                                ${"result_all_$i"} += $total_cost_5_2_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>стоимость системы отопления, рублей</b>";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_6_3 = '';
                                if(isset($compareData->total_cost_6_3))
                                    $total_cost_6_3 = round($compareData->total_cost_6_3,2);

                                echo "<td>$total_cost_6_3</td>";
                                $total_cost_6_3_num = $total_cost_6_3 ? $total_cost_6_3 : 0;
                                ${"result_all_$i"} += $total_cost_6_3_num;
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>стоимость отопления за 1 год, рублей</b>";
                        echo "</td>";
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_6_1 = '';
                                if(isset($compareData->total_cost_6_1))
                                    $total_cost_6_1 = round($compareData->total_cost_6_1,2);

                                echo "<td>$total_cost_6_1</td>";
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>стоимость отопления за 25 летний период, рублей</b>";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                $compareData = (json_decode($item->compare_data));
                                $total_cost_6_2 = '';
                                if(isset($compareData->total_cost_6_2))
                                    $total_cost_6_2 = round($compareData->total_cost_6_2,2);

                                echo "<td>$total_cost_6_2</td>";
                                $total_cost_6_2_num = $total_cost_6_2 ? $total_cost_6_2 : 0;
                                ${"result_25_all_$i"} = $total_cost_6_2_num + ${"result_all_$i"};
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>общая стоимость строительства, рублей</b>";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                echo "<td>${"result_all_$i"}</td>";
                                $i++;
                            }
                        }
                        echo "</tr>";

                        echo "<tr>";
                        echo "<td colspan='2' style='font-size: 10px'>";
                        echo "<b>стоимость с учётом инвестиционных и эксплуатационных расходов за период 25 лет, рублей</b>";
                        echo "</td>";
                        $i=0;
                        foreach ($links_data as $item) {
                            if ($item->compare) {
                                echo "<td>${"result_25_all_$i"}</td>";
                                $i++;
                            }
                        }
                        echo "</tr>";
                        ?>
                        </tbody>
                    </table>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade in" id="myModalSave" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="close_modal_saving_modal">×</button>
                    <h4 class="modal-title" style="text-align: center"></h4>
                </div>
                <div class="modal-body" id="modal-body">
                    <div align="center">
                        <input type="text" class="form-control width_90" id="edit_calc_name" placeholder="введите имя расчета ">
                        <input type="hidden" id="edit_calc_id">
                        <div class="red" style="display: none"></div>
                        <button class="btn" id="edit_calculation_button" style="margin-top: 10px">Сохранить</button>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $time = time(); ?>
    <script>
        $(document).ready(function() {
            var formData = new FormData();
            formData.append('cookie_name', "tab_7");
            formData.append('data[content_block]', $("#t01_tbody").html());
            $.ajax({
                url: '/writeLayer',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (result) {
                    console.log(result);
                }
            });
        });
</script>
    <script src="<?= Theme::getUrl()?>/js/profile.js?v=<?=$time?>" type="text/javascript"></script>

<?php $this->theme->footer(); ?>
