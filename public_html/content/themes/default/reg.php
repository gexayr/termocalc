<?php $this->theme->header(); ?>
<?php
$regions = Data::getRegions();
$buildings = Data::getBuildings();
?>
    <!-- Main Content -->
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">

                <?php if(isset($success)):?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Success!</strong>  <?=$success;?>
                    </div>
                <?php endif; ?>

                <?php if(isset($error)):?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Error!</strong> <?=$error;?>
                    </div>
                <?php endif; ?>


                <h1 style="text-align: center;">РЕГИСТРАЦИЯ</h1>
                <br>
                <h6>Данные, помеченные звёздочкой <span class="red">*</span> - обязательны к заполнению</h6>
                <br>
                <div class="well">
                    <form method="POST" id="reg_form" action="/reg/">
                        <div class="form-group">
                            <label class="control-label">Имя <span class="red">*</span></label>
                            <input type="text" class="form-control width_90" name="name" value="" required title="Please enter you name" placeholder="Введите ваше имя">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Фамилия</label>
                            <input type="text" class="form-control width_90" name="surname" value="" title="Please enter you surname" placeholder="Введите вашу фамилию">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Email <span class="red">*</span></label>
                            <input type="email" class="form-control width_90" name="email" value="" required title="Please enter you username" placeholder="example@gmail.com">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Логин <span class="red">*</span></label>
                            <h6 style="margin-bottom: 5px">Это ваш никнейм, который отображается на ФОРУМЕ</h6>
                            <input type="text" class="form-control width_90" name="login" value="" required title="Please enter you login" placeholder="Логин">
                            <span class="help-block"></span>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Пароль <span class="red">*</span></label>
                            <input type="password" class="form-control width_90" name="password" id="sPass" value="" required placeholder="Введите ваш пароль">
                        </div>
                        <div class="form-group">
                            <label class="control-label">Повторите пороль <span class="red">*</span></label>
                            <input type="password" class="form-control width_90" name="password" id="sRePass" value="" required placeholder="Подтвердите ваш пароль">
                        </div>

                        <div class="form-group">
                            <label class="control-label">Телефон <span class="red">*</span></label>
                            <input type="tel" class="form-control width_90" name="phone" value="" required title="Please enter you phone number" placeholder="Введите ваш номер телефона">
                            <span class="help-block"></span>
                        </div>


                        <div class="form-group">
                            <label class="control-label">Статус <span class="red">*</span></label>
                            <select  class="form-control width_90" name="status" onchange="set.ChangeRegStatus(this)">
                                <option>Архитектор или инженер-конструктор</option>
                                <option>Самостоятельный застройщик для себя (коттеджа, квартиры и т.п.)</option>
                                <option>Заказчик строительных услуг (бизнес-сектор)</option>
                                <option>Частный заказчик строительных услуг (найм подрядчиков для строительства)</option>
                                <option>Исполнитель стройработ (частный подрядчик)</option>
                                <option>Исполнитель работ (подрядная организация)</option>
                                <option>Торговля стройтоварами и услугами</option>
                                <option  value="other">Другое (указать в строчке)</option>
                            </select>
                        </div>
                        <div class="form-group" id="other-input">
                            <input type="text" class="form-control width_90" name="status_other" value="">
                        </div>


                        <div class="form-group">
                            <label class="control-label">Регион</label>
                            <select class="form-control width_90 form-control-lg" name="region" id="selectRegion" onchange="set.ChangeRegion()">
                                <option value="">Выберите регион </option>
                                <?php foreach($regions as $region){ ?>
                                    <option value="<?=$region->id?>"><?=$region->name?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Город</label>
                            <select class="form-control width_90" name="city" id="selectCity">
                                <option>Выберите город</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="control-label">Тип вашего здания</label>
                            <select class="form-control width_90" name="building">
                                <!--                            <option value="">Выберите тип здания </option>-->
                                <?php foreach($buildings as $building){ ?>
                                    <option value="<?=$building->id?>"><?=$building->name?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" id="exampleCheck1">
                            <label class="form-check-label" for="exampleCheck1"> Ознакомлен с <a href="/rules" target="_blank">правилами</a> пользования сайтом и согласен</label>
                        </div>
                        <div id="must_check" style="display: none;font-size: 14px"><span class="red">Вы не подтвердили что согласны с правилами пользования сайтом </span></div>
                        <br>
                        <div class="btn-input" style="margin: 0 auto; padding-bottom: 20px">
                            <input id="reg_user" type="submit" class="a_p log_a_p" value="Регистрация">
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
<?php $this->theme->footer(); ?>