<?php
ini_set('max_execution_time', 900);

require_once('TCPDF-master/ex/tcpdf_include.php');

$pdf = new TCPDF(/*PDF_PAGE_ORIENTATION*/"L", PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor(PDF_AUTHOR);
$pdf->SetTitle("Результаты расчёта");
$pdf->SetSubject(PDF_HEADER_STRING);

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
    require_once(dirname(__FILE__).'/lang/eng.php');
    $pdf->setLanguageArray($l);
}

// ---------------------------------------------------------
// ---------------------------------------------------------
$secret = User::getCookie('auth-Key');
$params['col'] = 'secret';
$params['val'] = $secret;
$user = User::getUserBy($params);
$user = $user[0];
$layer_params['val'] = $_POST['item_radio'];
$layer_params['col'] = 'id';
$layer_params['operator'] = '=';
$layerData = Data::getLayerDataByParams($layer_params);
$layer_data_1 = (json_decode($layerData[0]->layer_data_1));
$compare_data = (json_decode($layerData[0]->compare_data));
$region = Data::getRegion($layer_data_1->region);
$city = Data::getCity($layer_data_1->city);
$building = Data::getBuilding($layer_data_1->building);
$one_floor_building = ($layer_data_1->TD171 == 'yes')?"(малоэтажное индивидуальное)":"";

if(isset($compare_data->data)){
    $layer_data_index = ($compare_data->data->index);
    $layer_data_wall = unserialize($compare_data->data->wall);
    $layer_data_roof = unserialize($compare_data->data->roof);
    $layer_data_floor = unserialize($compare_data->data->floor);
    $layer_data_win = unserialize($compare_data->data->win);
    $layer_data_heat = unserialize($compare_data->data->heat);
    $layer_data_com = isset($compare_data->data->com) ? unserialize($compare_data->data->com) : '';
}


$params_get['col'] = 'user_id';
$params_get['val'] = $user->id;
$params_get['operator'] = '=';
$datalayer = Data::getLayerDataByParams($params_get);
$tmpDataLayer = $datalayer[0];
$tmp_compare_data = (json_decode($tmpDataLayer->compare_data));
if(isset($tmp_compare_data->data)) {
    $layer_data_com = isset($tmp_compare_data->data->com) ? unserialize($tmp_compare_data->data->com) : '';
}

$pdf->SetFont('dejavusans', '', 10);

$layer_data_wall_files = !empty($layer_data_wall['files']) ? json_decode($layer_data_wall['files']) : [];
$layer_data_roof_files = !empty($layer_data_roof['files']) ? json_decode($layer_data_roof['files']) : [];
$layer_data_floor_files = !empty($layer_data_floor['files']) ? json_decode($layer_data_floor['files']) : [];



$skip = [];
foreach ($layer_data_1 as $k=>$v){
    if($v == 'skip')
        $skip[] = $k;
}

// set font
$pdf->SetFont('dejavusans', '', 9);


$pdf->AddPage();

$data = date("H:i:s d-m-Y");
$html = '';
$html .= "<h1>Данный отчёт по результатам расчёта конструкций здания был сделан $data.</h1>";
$html .= "<span>Имя: $user->name </span><br>";
$html .= "<span>Фамилия: $user->surname </span><br>";
$html .= "<span>Телефон: $user->phone </span><br>";
$html .= "<span>емайл: $user->email </span><br>";
$html .= "<h3>Данные по местонахождению и типу рассчитываемого здания:</h3>";
$html .= "<span>Регион: $region->name</span><br>";
$html .= "<span>Город: $city->name</span><br>";
$html .= "<span>Тип здания: $building->name $one_floor_building</span><br>";
$html .= "<span>Сколько этажей: $layer_data_1->TD170</span><br>";
$html .= "<span>Предполагаемое количество проживающих: $layer_data_1->TD172</span><br>";

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';
$html .= '<div></div>';
$html .= '<div></div>';
$html .= '<h3>Полученные данные для теплотехнического расчёта на основании географического положения объекта:</h3>
<table border="1" cellpadding="4">';
$html .= "<tr>
		<td style=\"width:400px\">Город</td>
		<td style=\"width:120px\">$city->name</td>
		<td style=\"width:118px\"> </td>
	</tr>
	<tr>
		<td>Зона влажности в которой находится населённый пункт</td>
		<td>$layer_data_index->TD04</td>
		<td> </td>
	</tr>
	<tr>
		<td>Назначаемый региональный коэффициент m р</td>
		<td>$layer_data_index->TD03</td>
		<td> </td>
	</tr>
	<tr>
		<td>Наружная температура (наиболее холодной пятидневки обеспеченностью 0,92)</td>
		<td>$layer_data_index->TD37</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Средняя температура наружного воздуха, °С, отопительного периода</td>
		<td>$layer_data_index->TD38</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Количество суток отопительного периода</td>
		<td>$layer_data_index->TD39</td>
		<td>суток</td>
	</tr>
	<tr>
		<td>Средняя температура периода с среднемесячными отрицательными температурами</td>
		<td>$layer_data_index->TD40</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Количество суток периода со среднемесячными отрицательными температурами </td>
		<td>$layer_data_index->TD41</td>
		<td>суток</td>
	</tr>
	<tr>
		<td>Среднее парциальное давление наружного воздуха за период отрицательных температур</td>
		<td>$layer_data_index->TD42</td>
		<td>Па</td>
	</tr>
	<tr>
		<td>Среднее парциальное давления наружного воздуха за годовой период</td>
		<td>$layer_data_index->TD43</td>
		<td>Па</td>
	</tr>
	<tr>
		<td>ГСОП (градусо-сутки отопительного периода)  'Жилая комната'</td>
		<td>$layer_data_index->TD44</td>
		<td>°С · сутки</td>
	</tr>
	<tr>
		<td>Средняя наружная температура зимнего периода </td>
		<td>$layer_data_index->TD45</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Количество месяцев зимнего периода</td>
		<td>$layer_data_index->TD46</td>
		<td>месяцев</td>
	</tr>
	<tr>
		<td>Среднее парциальное давление зимнего периода</td>
		<td>$layer_data_index->TD47</td>
		<td>Па</td>
	</tr>
	<tr>
		<td>Средняя наружная температура весенне-осеннего периода</td>
		<td>$layer_data_index->TD48</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Количество месяцев весенне-осеннего периода</td>
		<td>$layer_data_index->TD49</td>
		<td>месяцев</td>
	</tr>
	<tr>
		<td>Среднее парциальное давление весенне-осеннего периода</td>
		<td>$layer_data_index->TD50</td>
		<td>Па</td>
	</tr>
	<tr>
		<td>Средняя наружная температура летнего периода</td>
		<td>$layer_data_index->TD51</td>
		<td>°С</td>
	</tr>
	<tr>
		<td>Количество месяцев летнего периода</td>
		<td>$layer_data_index->TD52</td>
		<td>месяцев</td>
	</tr>
	<tr>
		<td>Среднее парциальное давление летнего периода </td>
		<td>$layer_data_index->TD53</td>
		<td>Па</td>
	</tr>
	<tr>
		<td>Понижающий коэффициент при превышении удельного расхода отопления стен</td>
		<td>$layer_data_index->TD59</td>
		<td></td>
	</tr>
	<tr>
		<td>Понижающий коэффициент при превышении удельного расхода отопления окон </td>
		<td>$layer_data_index->TD60</td>
		<td></td>
	</tr>
	<tr>
		<td>Понижающий коэффициент при превышении удельного расхода отопления прочее</td>
		<td>$layer_data_index->TD61</td>
		<td></td>
	</tr>
</table>
";

/*
$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();


$pdf->AddPage();
$html = '';*/

if(in_array('living_room_w', $skip)
    && in_array("living_room_r", $skip)
    && in_array("living_room_b", $skip)){
} else {
    $html .= '<div></div>';
    $html .= '<div></div>';
    $html .= '<h2>Полученные данные по помещениям:</h2>';
    $html .= '<h3>Жилая комната</h3>
        <table border="1" cellpadding="4">';
   $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD05</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD06</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD07</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD08</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD44</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD54</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD55</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD56</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD200</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD201</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD216</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD280</td>
                <td>куб.м. </td>
            </tr>
            <tr>
                <td>Количество помещений</td>
                <td></td>
                <td>шт. </td>
            </tr>
        </table>";
}


if(in_array('kitchen_w', $skip)
    && in_array("kitchen_r", $skip)
    && in_array("kitchen_b", $skip)){
} else {
    $html .= '<div></div>';
    $html .= '<div></div>';
    $html .= '<h2>Полученные данные по помещениям:</h2>';
    $html .= '<h3>Кухня</h3>
        <table border="1" cellpadding="4">';
   $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD09</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD10</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD11</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD12</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD72</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD82</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD82_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD82_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD202</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD203</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD217</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD281</td>
                <td>куб.м. </td>
            </tr>
            <tr>
                <td>Количество помещений</td>
                <td>$layer_data_1->TD288</td>
                <td>шт. </td>
            </tr>
        </table>";
}

if(in_array('toilet_w', $skip)
    && in_array("toilet_r", $skip)
    && in_array("toilet_b", $skip)){
} else {
    $html .= '<div></div>';
    $html .= '<div></div>';
    $html .= '<h2>Полученные данные по помещениям:</h2>';
    $html .= '<h3>Туалет</h3>
        <table border="1" cellpadding="4">';
   $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD13</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD14</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD15</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD16</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD73</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD83</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD83_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD83_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD204</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD205</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD218</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD282</td>
                <td>куб.м. </td>
            </tr>
            <tr>
                <td>Количество помещений</td>
                <td>$layer_data_1->TD289</td>
                <td>шт. </td>
            </tr>
        </table>";
}


if(in_array('bathroom_w', $skip)
    && in_array("bathroom_r", $skip)
    && in_array("bathroom_b", $skip)){
} else {
    $html .= '<div></div>';
    $html .= '<div></div>';
    $html .= '<h2>Полученные данные по помещениям:</h2>';
    $html .= '<h3>Ванная или Совмещённый санузел</h3>
        <table border="1" cellpadding="4">';
   $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD17</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD18</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD19</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD20</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD74</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD84</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD84_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD84_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD206</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD207</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD219</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD283</td>
                <td>куб.м. </td>
            </tr>
            <tr>
                <td>Количество помещений</td>
                <td>$layer_data_1->TD290</td>
                <td>шт. </td>
            </tr>
        </table>";
}

if($layer_data_1->TD171 != "yes") {


    if (in_array('rest_room_w', $skip)
        && in_array("rest_room_r", $skip)
        && in_array("rest_room_b", $skip)) {
    } else {
        $html .= '<div></div>';
        $html .= '<div></div>';
        $html .= '<h2>Полученные данные по помещениям:</h2>';
        $html .= '<h3>Помещения для отдыха</h3>
        <table border="1" cellpadding="4">';
        $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD21</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD22</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD23</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD24</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD75</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD85</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD85_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD85_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD208</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD209</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD220</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD284</td>
                <td>куб.м. </td>
            </tr>
            <tr>
            <td>Количество помещений</td>
                 <td></td>
                <td>шт. </td>
            </tr>
        </table>";
    }

    if (in_array('hall_w', $skip)
        && in_array("hall_r", $skip)
        && in_array("hall_b", $skip)) {
    } else {
        $html .= '<div></div>';
        $html .= '<div></div>';
        $html .= '<h2>Полученные данные по помещениям:</h2>';
        $html .= '<h3>Межквартирный коридор</h3>
        <table border="1" cellpadding="4">';
        $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD25</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD26</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD27</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD28</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD76</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD86</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD86_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD86_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD210</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD211</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD221</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD285</td>
                <td>куб.м. </td>
            </tr>
            <tr>
            <td>Количество помещений</td>
                 <td></td>
                <td>шт. </td>
            </tr>
        </table>";
    }

    if (in_array('vestibule_w', $skip)
        && in_array("vestibule_r", $skip)
        && in_array("vestibule_b", $skip)) {
    } else {
        $html .= '<div></div>';
        $html .= '<div></div>';
        $html .= '<h2>Полученные данные по помещениям:</h2>';
        $html .= '<h3>Вестибюль, лестничная клетка</h3>
        <table border="1" cellpadding="4">';
        $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD29</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD30</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD31</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD32</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD77</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD87</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD87_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD87_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD212</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD213</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD223</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD286</td>
                <td>куб.м. </td>
            </tr>
            <tr>
            <td>Количество помещений</td>
                 <td></td>
                <td>шт. </td>
            </tr>
        </table>";
    }

    if (in_array('pantry_w', $skip)
        && in_array("pantry_r", $skip)
        && in_array("pantry_b", $skip)) {
    } else {
        $html .= '<div></div>';
        $html .= '<div></div>';
        $html .= '<h2>Полученные данные по помещениям:</h2>';
        $html .= '<h3>Кладовые</h3>
        <table border="1" cellpadding="4">';
        $html .= "<tr>
                <td style=\"width:400px\">Внутренняя температура помещения</td>
                <td style=\"width:120px\">$layer_data_index->TD33</td>
                <td style=\"width:118px\">°С </td>
            </tr>
            <tr>
                <td>Максимальная относительная влажность помещения</td>
                <td>$layer_data_index->TD34</td>
                <td>% </td>
            </tr>
            <tr>
                <td>Влажностный режим помещения</td>
                <td>$layer_data_index->TD35</td>
                <td> </td>
            </tr>
            <tr>
                <td>Условия эксплуатации конструкций</td>
                <td>$layer_data_index->TD36</td>
                <td> </td>
            </tr>
            <tr>
                <td>ГСОП (градусо-сутки отопительного периода) </td>
                <td>$layer_data_index->TD78</td>
                <td>°С · сутки </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче стеновых конструкций</td>
                <td>$layer_data_index->TD88</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций кровли (перекрытий)</td>
                <td>$layer_data_index->TD88_1</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче конструкций  полов </td>
                <td>$layer_data_index->TD88_2</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче окон</td>
                <td>$layer_data_index->TD57</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Нормируемое значение приведенного сопротивления теплопередаче фонарей</td>
                <td>$layer_data_index->TD58</td>
                <td>(м2·°С)/Вт </td>
            </tr>
            <tr>
                <td>Площадь помещений по полу</td>
                <td>$layer_data_1->TD214</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь стен</td>
                <td>$layer_data_1->TD215</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Площадь кровли</td>
                <td>$layer_data_1->TD225</td>
                <td>кв.м. </td>
            </tr>
            <tr>
                <td>Объём помещений</td>
                <td>$layer_data_1->TD287</td>
                <td>куб.м. </td>
            </tr>
            <tr>
            <td>Количество помещений</td>
                 <td></td>
                <td>шт. </td>
            </tr>
        </table>";
    }

}

$html .= '<div></div>';
$html .= '<h2>Конструкция стен </h2>';

if(array_key_exists("living", $layer_data_wall)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["living"])
        .'
</table>
';

}

if(array_key_exists("kitchen", $layer_data_wall)){
    $html .= '<div></div>';

    $html .= '<h3>Помещение: Кухня</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
            <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["kitchen"])
        .'
</table>
';
}

if(array_key_exists("toilet", $layer_data_wall)){
    $html .= '<div></div>';

    $html .= '<h3>Помещение: Туалет</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["toilet"])
        .'
</table>
';

}

if(array_key_exists("bathroom", $layer_data_wall)){
    $html .= '<div></div>';
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["bathroom"])
        .'
</table>
';

}

if(array_key_exists("rest_room", $layer_data_wall)){
    $html .= '<div></div>';
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["rest_room"])
        .'
</table>
';
}

if(array_key_exists("hall", $layer_data_wall)){
    $html .= '<div></div>';
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["hall"])
        .'
</table>
';

}

if(array_key_exists("vestibule", $layer_data_wall)){
    $html .= '<div></div>';
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["vestibule"])
        .'
</table>
';

}

if(array_key_exists("pantry", $layer_data_wall)){
    $html .= '<div></div>';
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_wall["pantry"])
        .'
</table>
';

}
$html .= '<div></div>';
$html .= '<div></div>';
$html .= '<h3>Данные по граничным условиям:</h3>
<table border="1" cellpadding="4">
    <tr>
        <td style="width:400px">Термическое сопротивление на внутренней поверхности конструкции:</td>
        <td style="width:120px">'
    .$layer_data_wall["tr_0-td_CTM37"]
    .'</td>
        <td style="width:118px">(м2·°С)/Вт</td>
    </tr>
    <tr>
        <td>Термическое сопротивление на внешней поверхности конструкции:</td>
        <td>'
    .$layer_data_wall["tr_x-td_CTM37"]
    .'</td>
        <td>(м2·°С)/Вт</td>
    </tr>
    </table>';
$html .= '<div></div>';
$html .= '<div></div>';
$html .= '<div></div>';
//$pdf->writeHTML($html, true, 0, true, true);
//$pdf->lastPage();
$html .= '<img src="'.$layer_data_wall["image"].'">';

//$pdf->SetXY(10, 10);
//$pdf->Image($layer_data_wall["image"]);

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();*/

/*$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<br><br>
<table border="1" cellpadding="4">
    <tr>
        <th  style="width:50px"><b>№ слоя</b></th>
        <th style="width:400px"><b>Рассчитанные слои материалов ("пирог конструкции")</b></th>
        <th  style="width:190px"><b>весённая клиентом толщина слоя в мм</b></th>
    </tr>' .htmlspecialchars_decode($layer_data_wall["construct_send"]) . '
</table>';
$html .= '
<table  border="1" cellpadding="4">
<tr>
    <td><b>Общая площадь стен (фасада) здания:</b></td>
    <td>'
    .$layer_data_wall["all_wall_volume"]
    .' кв.м.</td>
    </tr>
</table>';

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
if(array_key_exists("living_data", $layer_data_wall)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["living_data"]);
   /* $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();
    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("kitchen_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Кухня</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["kitchen_data"]);
}

if(array_key_exists("toilet_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Туалет</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["toilet_data"]);
}

if(array_key_exists("bathroom_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .=  htmlspecialchars_decode($layer_data_wall["bathroom_data"]);
}

if(array_key_exists("rest_room_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["rest_room_data"]);
}

if(array_key_exists("hall_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["hall_data"]);
}

if(array_key_exists("vestibule_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["vestibule_data"]);
}

if(array_key_exists("pantry_data", $layer_data_wall)){
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .= htmlspecialchars_decode($layer_data_wall["pantry_data"]);
}

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();


//Roof

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<h2>Конструкция кровли </h2>';

if(array_key_exists("living", $layer_data_roof)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["living"])
        .'
</table>
';


    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("kitchen", $layer_data_roof)){
    $html .= '<h3>Помещение: Кухня</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
            <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["kitchen"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("toilet", $layer_data_roof)){
    $html .= '<h3>Помещение: Туалет</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["toilet"])
        .'
</table>
';

   /* $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("bathroom", $layer_data_roof)){
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["bathroom"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("rest_room", $layer_data_roof)){
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["rest_room"])
        .'
</table>
';

   /* $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("hall", $layer_data_roof)){
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["hall"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("vestibule", $layer_data_roof)){
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["vestibule"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("pantry", $layer_data_roof)){
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_roof["pantry"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

$html .= '<h3>Данные по граничным условиям:</h3>
<table border="1" cellpadding="4">
    <tr>
        <td style="width:400px">Термическое сопротивление на внутренней поверхности конструкции:</td>
        <td style="width:120px">'
    .$layer_data_roof["tr_0-td_CTM37"]
    .'</td>
        <td style="width:118px">(м2·°С)/Вт</td>
    </tr>
    <tr>
        <td>Термическое сопротивление на внешней поверхности конструкции:</td>
        <td>'
    .$layer_data_roof["tr_x-td_CTM37"]
    .'</td>
        <td>(м2·°С)/Вт</td>
    </tr>
    </table>';

/*$pdf->writeHTML($html, true, 0, true, true);
$html = "\n";*/

$html .= '<img src="'.$layer_data_roof["image"].'">';

/*$pdf->SetXY(50, 60);
$pdf->Image($layer_data_roof["image"]);*/

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<br><br>
<table border="1" cellpadding="4">
    <tr>
        <th  style="width:50px"><b>№ слоя</b></th>
        <th style="width:400px"><b>Рассчитанные слои материалов ("пирог конструкции")</b></th>
        <th  style="width:190px"><b>весённая клиентом толщина слоя в мм</b></th>
    </tr>
   '
    .htmlspecialchars_decode($layer_data_roof["construct_send"])
.'
       </table>';
$html .= '<br><br>
<table  border="1" cellpadding="4">
<tr>
    <td><b>Общая площадь всей кровли:</b></td>
    <td>'
    .$layer_data_roof["roof_square"]
    .' кв.м.</td>
    </tr>
</table>';

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
if(array_key_exists("living_data", $layer_data_roof)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["living_data"])
        .'
</div>
';
    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();
    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("kitchen_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Кухня</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["kitchen_data"])
        .'
</div>
';
}

if(array_key_exists("toilet_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Туалет</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["toilet_data"])
        .'
</div>
';
}

if(array_key_exists("bathroom_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["bathroom_data"])
        .'
</div>
';
}

if(array_key_exists("rest_room_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["rest_room_data"])
        .'
</div>
';
}

if(array_key_exists("hall_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["hall_data"])
        .'
</div>
';
}

if(array_key_exists("vestibule_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["vestibule_data"])
        .'
</div>
';
}

if(array_key_exists("pantry_data", $layer_data_roof)){
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_roof["pantry_data"])
        .'
</div>
';
}

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

//Floor

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<h2>Конструкции пола </h2>';

if(array_key_exists("living", $layer_data_floor)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["living"])
        .'
</table>
';


    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("kitchen", $layer_data_floor)){
    $html .= '<h3>Помещение: Кухня</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
            <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["kitchen"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("toilet", $layer_data_floor)){
    $html .= '<h3>Помещение: Туалет</h3>';
    $html .= '<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["toilet"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("bathroom", $layer_data_floor)){
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["bathroom"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("rest_room", $layer_data_floor)){
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["rest_room"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("hall", $layer_data_floor)){
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["hall"])
        .'
</table>
';

   /* $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("vestibule", $layer_data_floor)){
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["vestibule"])
        .'
</table>
';

    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("pantry", $layer_data_floor)){
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .='
<table border="1" cellpadding="3">
        <tr>
        <th style="width:20px"></th>
            <th style="width:85px"></th>
            <th><b>плотность кг/куб.м.</b></th>
            <th><b>теплопроводность Вт/(м·°С) (A)</b>|<b>теплопроводность Вт/(м·°С) (B)</b></th>
            <th><b>паропроницаемость мг/(м·ч·Па)</b></th>
            <th><b>R сопротивление паропроницанию</b></th>
            <th><b>R теплосопр.слоя</b></th>
            <th><b>координата х график толщины (м)</b></th>
            <th><b>t слоёв при пятидневке</b></th>
            <th><b>температура слоя при средней темп. Периода отрицательных температур</b></th>
            <th><b>вычисление комплекса tmy</b></th>
            <th><b>температура в слое</b></th>
        </tr>'
        . htmlspecialchars_decode($layer_data_floor["pantry"])
        .'
</table>
';

/*    $pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();


    $pdf->AddPage();
    $html = '';*/

    $html .= "<div></div>";
    $html .= "<div></div>";
}

$html .= '<h3>Данные по граничным условиям:</h3>
<table border="1" cellpadding="4">
    <tr>
        <td style="width:400px">Термическое сопротивление на внутренней поверхности конструкции:</td>
        <td style="width:120px">'
    .$layer_data_floor["tr_0-td_CTM37"]
    .'</td>
        <td style="width:118px">(м2·°С)/Вт</td>
    </tr>
    <tr>
        <td>Термическое сопротивление на внешней поверхности конструкции:</td>
        <td>'
    .$layer_data_floor["tr_x-td_CTM37"]
    .'</td>
        <td>(м2·°С)/Вт</td>
    </tr>
    </table>';

/*$pdf->writeHTML($html, true, 0, true, true);
$html = "\n";
$pdf->SetXY(50, 60);
$pdf->Image($layer_data_floor["image"]);*/
$html .= '<img src="'.$layer_data_floor["image"].'">';

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<br><br>
<table border="1" cellpadding="4">
    <tr>
        <th  style="width:50px"><b>№ слоя</b></th>
        <th style="width:400px"><b>Рассчитанные слои материалов ("пирог конструкции")</b></th>
        <th  style="width:190px"><b>весённая клиентом толщина слоя в мм</b></th>
    </tr>
   '
    .htmlspecialchars_decode($layer_data_floor["construct_send"])
.'
       </table>';
$html .= '<br><br>
<table  border="1" cellpadding="4">
<tr>
    <td><b>Общая площадь теплового контура всего пола:</b></td>
    <td>'
    .$layer_data_floor["floor_square"]
    .' кв.м.</td>
    </tr>
</table>';

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
if(array_key_exists("living_data", $layer_data_floor)){

    $html .= '<h3>Помещение: Жилая комната</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["living_data"])
        .'
</div>
';
    /*$pdf->writeHTML($html, true, false, true, false, '');
    $pdf->lastPage();
    $pdf->AddPage();
    $html = '';*/
    $html .= "<div></div>";
    $html .= "<div></div>";
}

if(array_key_exists("kitchen_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Кухня</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["kitchen_data"])
        .'
</div>
';
}

if(array_key_exists("toilet_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Туалет</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["toilet_data"])
        .'
</div>
';
}

if(array_key_exists("bathroom_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Ванная или Совмещённый санузел</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["bathroom_data"])
        .'
</div>
';
}

if(array_key_exists("rest_room_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Помещения для отдыха и учебных занятий</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["rest_room_data"])
        .'
</div>
';
}

if(array_key_exists("hall_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Межквартирный коридор</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["hall_data"])
        .'
</div>
';
}

if(array_key_exists("vestibule_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Вестибюль, лестничная клетка</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["vestibule_data"])
        .'
</div>
';
}

if(array_key_exists("pantry_data", $layer_data_floor)){
    $html .= '<h3>Помещение: Кладовые</h3>';
    $html .='
<div>'
        . htmlspecialchars_decode($layer_data_floor["pantry_data"])
        .'
</div>
';
}

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<h2>Окна - Фонари</h2>';
$win_type = '';
switch ($layer_data_win["TD184"]) {
    case 0.8:
        $win_type = "двойной";
        break;
    case 0.7:
        $win_type = "тройной";
        break;
    default:
        $win_type = "одинарный, спаренный или открытый";
}

if($layer_data_win["TD182"] != ""){
    $html .= '<h3>Окна</h3>';

    $html .='
<table border="1" cellpadding="3">
        <tr>
        <td style="width:380px">Площадь окон (+ входных и балконных дверей)</td>
            <td style="width:255px">'
        .$layer_data_win["TD182"]
        .' кв.м</td>
        </tr>
        <tr>
        <td>Теплосопротивление ( R ) оконной конструкции</td>
            <td>'
        .$layer_data_win["TD183"]
        .' м²·С°/Вт</td>
        </tr>
        <tr>
        <td>Тип оконного переплёт</td>
            <td>'
        .$win_type
        .'</td>
        </tr>
</table>
';
}

if($layer_data_win["TD186"] != "") {

    $html .= '<h3>Фонари (мансардные окна)</h3>';

    $html .= '
<table border="1" cellpadding="3">
        <tr>
        <td style="width:380px">Площадь фонарей</td>
            <td style="width:255px">'
        . $layer_data_win["TD186"]
        . ' кв.м</td>
        </tr>
        <tr>
        <td>Теплосопротивление ( R ) фонаря</td>
            <td>'
        . $layer_data_win["TD183"]
        . ' м²·С°/Вт</td>
        </tr>
</table>
';
}
$html .= '<div>';
$html .= htmlspecialchars_decode($layer_data_win["content_block"]);
$html .= '</div>';

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = '';*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= '<h2>Отопление</h2>';

$heat_type = " - на газе";
if($layer_data_heat["TD174"] == 60)
    $heat_type = " - на электричестве";
$html .='
<table border="1" cellpadding="3">
        <tr>
        <td style="width:380px">тип энергии для отопления</td>
            <td style="width:255px">'
    .$layer_data_heat["TD173"]
    .'</td>
        </tr>
        <tr>
        <td>Тип оборудования нагрева (плита, водонагреватель…)</td>
            <td>'
    .$heat_type
    .'</td>
        </tr>
        <tr>
        <td>Тип жентиляции</td>
            <td>'
    .$layer_data_heat["TD175"]
    .'</td>
        </tr>
</table>
';

$html .= "<h3>Тепловые потери здания</h3>";
$html .= '<table border="1" cellpadding="3">
<tr>
    <th><b>Зоны тепловых потерь здания:</b></th>
    <th><b>Одномоментные теплопотери, Вт:</b></th>
    <th><b>Теплопотери за год, кВт/час</b></th>
    <th><b>Структура тепловых поступлений, в % к общим</b></th>
</tr>';
$html .= isset($layer_data_heat["content_block_term_out"])
    ? htmlspecialchars_decode($layer_data_heat["content_block_term_out"]) : '';
$html .= "</table>";
$html .= '<table border="1" cellpadding="3">';
$html .= isset($layer_data_heat["content_block_term_out_result"])
    ? htmlspecialchars_decode($layer_data_heat["content_block_term_out_result"]) : '';
$html .= "</table>";

$html .= "<h3>Тепловые поступления</h3>";
$html .= '<table border="1" cellpadding="3">
<tr>
    <th><b>Тепловые поступления в здание:</b></th>
    <th><b>Одномоментные поступления, Вт:</b></th>
    <th><b>Поступления за год, кВт/час</b></th>
    <th><b>Структура тепловых поступлений, в % к общим</b></th>
</tr>';
$html .= isset($layer_data_heat["content_block_term_in"])
    ? htmlspecialchars_decode($layer_data_heat["content_block_term_in"]) : '';
$html .= "</table>";
$html .= '<table border="1" cellpadding="3">';
$html .= isset($layer_data_heat["content_block_term_in_result"])
    ? htmlspecialchars_decode($layer_data_heat["content_block_term_in_result"]) : '';
$html .= "</table>";

$html .= "<div>";
$html .= isset($layer_data_heat["content_block_all_res_com"])
    ? htmlspecialchars_decode($layer_data_heat["content_block_all_res_com"]) : '';
$html .= "</div>";

//$pdf->writeHTML($html, true, 0, true, true);
//$html = "\n";
/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = "";*/
$html .= "<div></div>";
$html .= "<div></div>";
$html .= "<h3>Структура теплопотерь здания</h3>";
/*$pdf->SetXY(50, 60);
$pdf->Image($layer_data_heat["image"]);*/
$html .= '<img src="'.$layer_data_heat["image"].'">';


/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = "";*/

if(isset($layer_data_wall['estimate'])) {
    $html .= "<div></div>";
    $html .= "<h2>Смета по конструкции стены</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_wall['estimate']);
    $html .= "</table>";
}
if(isset($layer_data_roof['estimate'])) {
    $html .= "<div></div>";
    $html .= "<h2>Смета по кровельной конструкции</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_roof['estimate']);
    $html .= "</table>";
}
if(isset($layer_data_floor['estimate'])) {
    $html .= "<div></div>";
    $html .= "<h2>Смета по полам</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_floor['estimate']);
    $html .= "</table>";
}
if(isset($layer_data_win['estimate'])) {
    $html .= "<div></div>";
    $html .= "<h2>Смета по окнам</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_win['estimate']);
    $html .= "</table>";
}
if(isset($layer_data_heat['estimate'])) {
    $html .= "<div></div>";
    $html .= "<h2>Данные по отоплениию и сметные стоимости</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_heat['estimate']);
    $html .= "</table>";
}

/*$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

$pdf->AddPage();
$html = "";*/
if ($layer_data_com != '') {
    $html .= "<div></div>";
    $html .= "<h2>Таблица сравнений результатов</h2>";
    $html .= "<h2>Для сравнения были выбраны следующие варианты конструкций со следующими данными:</h2>";
    $html .= '<table border="1" cellpadding="3">';
    $html .= htmlspecialchars_decode($layer_data_com["content_block"]);
    $html .= "</table>";
}

$pdf->writeHTML($html, true, false, true, false, '');
$pdf->lastPage();

//$pdf->Output('example_006.pdf', 'I');
//$filename = Data:: generateRandomString($length = 5);
$filename = "termocalc_report_".date("d_m_Y_H_i_s");

$path = ROOT_DIR . "/content/themes/default/";
if(is_dir($path . 'pdf-files/'.$user->id.'/') === false) {
    mkdir($path . 'pdf-files/'.$user->id, 0777, true);
}
$dir = $path . 'pdf-files/'.$user->id.'/';
$pdf->Output($dir . $filename.'.pdf', 'F');

$filesArr = array_unique(array_merge($layer_data_wall_files, $layer_data_roof_files, $layer_data_floor_files));
$materialFiles = [];
$fileParams['col'] = 'id';
foreach ($filesArr as $item) {
    $fileParams['val'] = $item;
    $matFile = User::getFileBy($fileParams);
    $materialFiles[] = !empty($matFile) ?  $path. "files/" . $matFile[0]->name . ".pdf" : '';
}
$materialFiles = array_filter($materialFiles);
//==================

if (isset($layer_data_heat) && isset($layer_data_heat['heatingFile'])) {
    $zip_name = $filename.".zip";

    $files =[
        $path. "files/" . $layer_data_heat['heatingFile'] . ".pdf",
        $dir . $filename.'.pdf'
    ];
$files = array_merge($files, $materialFiles);
    $zip = new ZipArchive;

    $file_name = basename($zip_name);

    if($zip->open($zip_name, ZipArchive::CREATE) !== TRUE) {
        echo("Zip failed");
    } else {
        foreach ($files as $file) {
            $new_filename = substr($file, strrpos($file,'/') + 1);
            $zip->addFile($file, $new_filename);
        }

        $zip->close();

        header('Content-Type: application/zip');
        header('Content-disposition: attachment; filename=' . $zip_name);
        header('Content-Length: ' . filesize($zip_name));
        readfile($zip_name);

        if(file_exists(ROOT_DIR . "/" . $zip_name)){
            unlink($zip_name);
        }
//        if(file_exists($dir . $filename.'.pdf')){
//            unlink($dir . $filename.'.pdf');
//        }

    }
} else {
    $pdf->Output(time().'.pdf', 'I');
}
