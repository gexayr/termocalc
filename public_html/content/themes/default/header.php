<?php
$the_cookie = User::getCookie('auth-Login');
$secret = User::getCookie('auth-Key');
$params['col'] = 'secret';
$params['val'] = $secret;
$user = User::getUserBy($params);
if($user != null){
    $user = $user[0];
    if($user->email == $the_cookie){
        $login = $user->email;
    }
}else{
    $login = null;
}
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php Theme::title() ?></title>
    <?php Asset::render('css'); ?>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,600&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
    <script src="<?= Theme::getUrl() ?>/vendor/jquery/jquery.min.js" type="text/javascript"></script>

</head>
<body>
<header style="padding-bottom: 10px">
    <nav>
        <div class="container">
            <!-- Меню -->
            <div class="adaptive-menu">
                <div class="menu-toggle">
                    <ul>
                        <li <?php if($_SERVER['REQUEST_URI'] == '/')echo "class='active-menu'";?>><a class="header-menu" href="/">
                                <img src="<?= Theme::getUrl() ?>/img/home.png" alt="Главная">
                                Главная</a></li>
                        <li <?php if($_SERVER['REQUEST_URI'] == '/help')echo "class='active-menu'";?>><a class="header-menu" href="/help">
                                <img src="<?= Theme::getUrl() ?>/img/tool.png" alt="Помощь">
                                Помощь</a></li>
                        <li <?php if($_SERVER['REQUEST_URI'] == '/catalog')echo "class='active-menu'";?>><a class="header-menu" href="/catalog">
                                <img src="<?= Theme::getUrl() ?>/img/info.png" alt="Строительный справочник">
                                Строительный справочник</a></li>
                        <li <?php if($_SERVER['REQUEST_URI'] == '/forum')echo "class='active-menu'";?>><a class="header-menu" href="/forum">
                                <img src="<?= Theme::getUrl() ?>/img/forum.png" alt="Форум">
                                Форум</a></li>
                        <li <?php if($_SERVER['REQUEST_URI'] == '/examples')echo "class='active-menu'";?>><a class="header-menu" href="/examples">
                                <img src="<?= Theme::getUrl() ?>/img/data.png" alt="Примеры">
                                Примеры</a></li>
                    </ul>
                </div>
            </div>
            <!-- Изображение пользователя, имя пользователя, вход в кабинет, выход -->
            <div class="user">
                <?php if ($login == null) { ?>
                    <div class="user-cabinet-log-out">
                        <a href="/sign-up">Зарегистрироваться</a>
                        <div class="point"></div>
                        <a href="/login">Войти</a>
                    </div>
                <?php } else { ?>
<!--                <div class="user-img">-->
<!--                    <img src="--><?//= Theme::getUrl() ?><!--/img/avatar.jpg" alt="Картинка пользователя">-->
<!--                </div>-->
                <div class="user-name">
                    <p><?=$login?></p>
                    <div class="user-cabinet-log-out">
                        <a class="header-menu" href="/profile">Личный кабинет</a>
                        <div class="point"></div>
                        <a class="header-menu" href="/logout">Выход</a>
                    </div>
                </div>
                <?php } ?>
                <button type="button" class="" data-toggle="modal" data-target="#previewModal" id="preview_modal_button"
                    <?php if (!empty($login)) { ?>
                        style="top: 14px"
                <?php } ?> >Показать</button>
            </div>

        </div>
    </nav>
</header>