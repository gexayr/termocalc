<?php $this->theme->header(); ?>
<?php
$postcategories = User::getPostCategories();
if($category == null)die("<h2 align='center'>неверный адрес, нет категории</h2>");
$posts = User::getPostsByParams($category->id);
?>
    <div class="container">
        <div class="row">
            <div class="passing-test-content">
                <div class="post-block">
                    <?php foreach ($postcategories as $item): ?>
                        <?php
                        if ($item->id != $category->id) { ?>
                            <div class="blank-help">
                                <a class="catalog-menu-item"
                                   href="/catalog/<?= $item->lat_name ?>"><span><?= $item->name ?></span>
                                </a>
                            </div>
                            <?php
                                $item_posts = User::getPostsByParams($item->id);
                                if ($item_posts != NULL) {
                                    foreach ($item_posts as $post) { ?>
                                        <div class="blank-sub">
                                            <a class="catalog-menu-item" href="<?= $item->lat_name ?>/<?= $post->id ?>">
                                                - <span><?= $post->title ?></span></a>
                                        </div>
                                    <?php }
                                }
                            ?>
                        <?php } else { ?>
                            <div class="blank-help">
                                <span style="font-size: 15px"><?= $category->name ?></span>
                            </div>
                            <?php if ($posts != NULL) {
                                foreach ($posts as $post) { ?>
                                    <div class="blank-sub">
                                        <a class="catalog-menu-item" href="<?= $category->lat_name ?>/<?= $post->id ?>"> - <span><?= $post->title ?></span></a>
                                    </div>
                                <?php }
                            } ?>
                        <?php } ?>
                    <?php endforeach; ?>
                    <div class="activity post-block">
                        <?php if ($posts != NULL) {
                            foreach ($posts as $post) { ?>
                                <div class="activity post-block">
                                    <h2 align="center"><?= $post->title ?></h2>
                                    <div class="post_content">
                                        <?= $post->content ?>
                                    </div>
                                    <div class="post_date" align="right">
                                        <?= $post->date ?>
                                    </div>
                                </div>
                        <?php }
                        } else { ?>
                            <div class="post_content" align="center">
                                <span>нет записей в этой категории</span>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>

        </div>
    </div>
<?php $this->theme->footer(); ?>