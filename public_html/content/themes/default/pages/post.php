<?php $this->theme->header(); ?>
<?php $postcategories = User::getPostCategories();
$posts = User::getPostsByParams($category->id);
if($post == null)die('Ошибка');
?>
    <div class="container">
        <div class="row">
            <div class="passing-test-content">
                <div class="post-block">
                    <?php foreach ($postcategories as $item): ?>
                        <?php
                        if ($item->id == $category->id) { ?>
                            <div class="blank-help">
                                <span style="font-size: 15px"><?= $item->name ?></span>
                            </div>
                            <?php if ($posts != NULL) {
                                foreach ($posts as $item_post) {

                                    if ($item_post->id == $post->id) { ?>
                                        <div class="blank-sub">
                                            - <span><?= $item_post->title ?></span>
                                        </div>
                                        <?php } else { ?>
                                            <div class="blank-sub">
                                                <a class="catalog-menu-item" href="/catalog/<?= $category->lat_name ?>/<?= $item_post->id ?>">
                                                    - <span><?= $item_post->title ?></span>
                                                </a>
                                            </div>
                                        <?php } ?>
                                <?php }
                            } ?>
                        <?php } else { ?>
                            <div class="blank-help">
                                <a class="catalog-menu-item"
                                   href="/catalog/<?= $item->lat_name ?>"><span><?= $item->name ?></span></a>
                            </div>
                            <?php
                            $item_posts = User::getPostsByParams($item->id);
                            if ($item_posts != NULL) {
                                foreach ($item_posts as $item_post) { ?>
                                    <div class="blank-sub">
                                        <a class="catalog-menu-item" href="/catalog/<?= $item->lat_name ?>/<?= $item_post->id ?>">
                                            - <span><?= $item_post->title ?></span></a>
                                    </div>
                                <?php }
                            }
                            ?>
                        <?php } ?>
                    <?php endforeach; ?>
                    <div class="activity post-block">
                        <h2 align="center"><?= $post->title ?></h2>
                        <div class="post_content">
                            <?= $post->content ?>
                        </div>
                        <div class="post_date" align="right">
                            <?= $post->date ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
<?php $this->theme->footer(); ?>