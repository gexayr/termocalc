<?php $this->theme->header(); ?>
<?php $posts = User::getHelpPosts(); ?>
<?php

?>
    <div class="container">
        <div class="row">
            <div class="passing-test-content">
                <div class="post-block">

                    <?php foreach ($posts as $item): ?>
                        <?php
                        if ($item->id == 8) {
                            ?>
                            <div class="blank-help">
                                <?php if ($post->id == $item->id) { ?>
                                    <span style="font-size: 15px"><?= $item->title ?></span>
                                <?php } else { ?>
                                    <a class="catalog-menu-item"
                                       href="/help/<?= $item->id ?>">
                                        <span><?= $item->title ?></span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php endforeach; ?>
                    <?php foreach ($posts as $item): ?>
                        <?php
                        if ($item->id != 8) {
                            ?>
                            <div class="blank-help">
                                <?php if ($post->id == $item->id) { ?>
                                    <span style="font-size: 15px"><?= $item->title ?></span>
                                <?php } else { ?>
                                    <a class="catalog-menu-item"
                                       href="/help/<?= $item->id ?>">
                                        <span><?= $item->title ?></span>
                                    </a>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    <?php endforeach; ?>
                    <div class="activity post-block">
                        <?php
                        ?>
                        <h2 align="center"><?= $post->title ?></h2>
                        <div class="post_content">
                            <?= $post->content ?>
                        </div>
                        <div class="post_date" align="right">
                            <?= $post->date ?>
                        </div>
                        <?php
                        ?>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php $this->theme->footer(); ?>