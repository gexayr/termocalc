<!-- Footer -->
<footer>
    <div class="container">
        <div class="project-img">
            <img src="<?= Theme::getUrl()?>/img/project.png" alt="Проектирование">
        </div>
        <nav>
            <ul class="footer_menu">
                <li><a href="/">Главная</a></li>
                <li><a href="/help">Помощь</a></li>
                <li><a href="/catalog">Строительный справочник</a></li>
                <li><a href="/forum">Форум</a></li>
                <li><a href="/rules">Правила пользования ресурсом</a></li>
                <button type="button" class="" data-toggle="modal" data-target="#myModal" id="modal_button">Обратная связь</button>
            </ul>

        </nav>
<!--        <div class="contact-copyright">-->
<!--            <div class="contact">-->
<!--                <p>Т: + 7 (495) 543-15-25</p>-->
<!--                <div class="point"></div>-->
<!--                <p>E: kalc@info.ru</p>-->
<!--                <div class="point"></div>-->
<!--                <p>А: г. Москва, ул. Арбата, дом. 35</p>-->
<!--            </div>-->
<!--            <div class="copyright">-->
<!--                <p>&copy;  Все права защищены. ООО “СтройФирм” 2017-2018</p>-->
<!--            </div>-->
<!--        </div>-->
    </div>
    <div id="waiting"></div>
    <div id="upbutton"></div>


    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Форма обратной связи</h4>
                </div>
                <form action="/feedback" method="post" accept-charset="utf-8">
                    <div class="modal-body">
                            <div class="modal-body" style="padding: 5px;">
                                <script src="https://www.google.com/recaptcha/api.js" async defer></script>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control width_90" name="name" placeholder="Укажите Ваше имя" type="text" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control width_90" name="email" placeholder="Укажите Ваш E-mail" type="email" required />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="padding-bottom: 10px;">
                                        <input class="form-control width_90" name="phone" placeholder="Укажите Ваш телефон (при желании)" type="tel" />
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                        <textarea style="resize:vertical;" class="form-control width_90" placeholder="Введите сообщение..." rows="6" name="message" required></textarea>
                                    </div>
                                </div>
                                <br>
<!--                                <div class="g-recaptcha" data-sitekey="6LeqqqEUAAAAALd2NIdabmIYTF3VnToTsRKvcUGY" data-callback="enableBtn"></div>-->
                            </div>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-default" id="button1" value="Отправить"/>
                        <input type="reset" class="btn btn-default" value="Очистить" />
                        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Закрыть</button>
                    </div>
                </form>
                <script>
                    document.getElementById("button1").disabled = true;
                    function enableBtn(){
                        document.getElementById("button1").disabled = false;
                    }
                </script>
            </div>

        </div>
    </div>
    <!--End Modal-->

<?php
$videoLink = Setting::get('preview_video_link');
?>
    <!-- Modal -->
    <div class="modal fade" id="previewModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Ознакомительное Видео</h4>
                </div>
                <div class="modal-body" id="previewVideo">
                    <?=$videoLink?>
                </div>
                    <div class="modal-footer">
                        <input type="checkbox" <?php if (!empty(User::getCookie('dont_show_preview'))){ ?> checked <?php } ?>
                        id="dont-show-checkbox"><label for="dont-show-checkbox"> &nbsp; Не показать больше</label>
                        <button type="button" class="btn btn-default btn-close" data-dismiss="modal">Закрыть</button>
                    </div>
            </div>

        </div>
    </div>
    <!--End Modal-->


    <!-- Modal -->
    <div class="modal fade" id="myModalHelp" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" onclick="clearContent()">&times;</button>
                    <h4 class="modal-title" style="text-align: center">Подсказки</h4>
                </div>
                <div class="modal-body" id="modal-body">
                </div>
            </div>
        </div>
    </div>

    <button id="modal-material-button" data-toggle="modal" data-target="#myModalMaterial" style="display: none"></button>

    <!-- Modal -->
    <div class="modal fade" id="myModalMaterial" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="myModalMaterialClose">&times;</button>
                    <h4 class="modal-title" id="material-modal-title" style="text-align: center">материалы</h4>
                </div>
                <div class="modal-body" id="modal-material-body">
                </div>
            </div>
        </div>
    </div>

    <p style="text-align: center; color: #fff; font-size: 13px; padding: 10px 0;">Обратиться к администрации ресурса можно через <strong data-toggle="modal" data-target="#myModal" style="cursor: pointer">обратную связь</strong>.</p>

<!--    <p style="text-align: center; color: #fff; font-size: 13px; padding: 10px 0;">По вопросам сотрудничества обратитесь через форму <strong data-toggle="modal" data-target="#myModal" style="cursor: pointer">обратной связи</strong> к администрации ресурса.</p>-->
</footer>
<?php Asset::render('js'); ?>




<?php $time = time(); ?>

<!--<script src="--><?//= Theme::getUrl()?><!--/js/comboTreePlugin.js?v=--><?//=$time?><!--" type="text/javascript"></script>-->
<script src="<?= Theme::getUrl()?>/js/settings.js?v=<?=$time?>" type="text/javascript"></script>
<script src="<?= Theme::getUrl()?>/js/script.js?v=<?=$time?>" type="text/javascript"></script>
<script src="<?= Theme::getUrl()?>/js/calc.js?v=<?=$time?>" type="text/javascript"></script>
<script src="<?= Theme::getUrl()?>/js/termo.js?v=<?=$time?>" type="text/javascript"></script>

</body>
</html>