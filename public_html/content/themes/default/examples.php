<?php $this->theme->header();
$generalData = Setting::get('general_data');
$generalDataNames = Setting::getName('general_data');
?>
    <div class="container" style="height: 80vh;">
        <div class="row">

<?php //$this->theme->sidebar(); ?>

            <div class="general-data-table">
                <p class="note_profile red">*Внимание! Есть особенность возврата запомненного расчёта обратно в калькулятор.
                    После того, как вы кликнете на одну из ссылок запомненных расчётов, для того, чтобы данные полностью
                    загрузились обратно в калькулятор надо просто пройти снова заново по вкладкам. Нужно лишь кликнуть
                    слева в меню на все вкладки начиная с верхней. Как пройдёте по всем вкладкам, все данные корректно
                    загрузятся.</p>
                <table class="table">
                <?php
                if (!empty($generalData)) {
                    $arrData = json_decode($generalData);
                    $arrDataLink = $arrData->link;
                    $arrDatashow = (array) $arrData->show;
                    foreach ($arrDataLink as $key=>$value) {
                ?>
                    <tr>
                        <td class="<?=(!empty($arrDatashow[$key])) ? 'inactive-data' : ''?>"><a href="<?=(!empty($arrDatashow[$key])) ? '#' : $value?>"><?=$arrData->name[$key]?></a></td>
                    </tr>
                <?php
                    }
                }
                ?>
                </table>
            </div>
        </div>
    </div>


<?php $this->theme->footer(); ?>