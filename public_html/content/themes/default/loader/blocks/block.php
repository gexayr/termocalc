<?php

$par_seam['col'] = 'COL34';
$par_seam['val'] = 'Б';
$par_seam['operator'] = '=';
$material_seam = Data::getMaterialsByParams($par_seam);
$par_block['col'] = 'COL35';
$par_block['val'] = 'Ш';
$par_block['operator'] = '=';
$material_block = Data::getMaterialsByParams($par_block);

$selfCreates=[];
if(User::getCookie('auth-Key')!= null){
    $selfCreates = User::getSelfCreates();
    if(!$selfCreates)$selfCreates=[];
};

$material_block = array_merge($material_block, $selfCreates);
$material_seam = array_merge($material_seam, $selfCreates);
?>
<li class="col-md-12 margin-li li_<?=$_GET['count'];?>" data-id="<?=$_GET['count'];?>" style="display: block">

    <h4>тип слоя: Кладка из блоков</h4>
    <div class="first_select" style="display: inline-flex">
        <input name="mat_cat" onclick="set.clickSelect(this, '<?=$_GET['type'];?>')"
               value="Известняк, плотность 2000 кг/куб.м.">

    </div>
        <input type="text" class="thickness" value="" placeholder="мм" oninput="this.value=proverka(this.value)">
    <?php
    if($_GET['isset'] != 'html') {
        ?>
        <a class="up btn btn-primary" onclick="upField(this)" style="display: inline-flex; margin-left: 10px"><i class="glyphicon glyphicon-arrow-up"></i></a>
        <a class="down btn btn-primary" onclick="downField(this)" style="display: inline-flex; margin-left: 10px"><i class="glyphicon glyphicon-arrow-down"></i>
        </a>
        <a class="remove btn btn-primary" onclick="removeField(this)" style="display: inline-flex; margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>

        <?php
    }
    ?>

    <div class="second_select" style="margin-top: 10px;">

        <input name="mat_cat" onclick="set.clickSelect(this, '<?=$_GET['type'];?>')"
               value="Раствор цементно-песчаный">


    </div>

    <div class="data-input-block block-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
        <div class="col-sm-4">
            <div class="">
                <label for="">Размер блока</label>
            </div>
            <div class="col-sm-4">
                <label style="margin-left: 25px">ширина</label>
                <input type="text" class="block-w" value="" placeholder="мм" oninput="this.value=proverka(this.value)">
            </div>
            <div class="col-sm-4">
                <label style="margin-left: 25px">высота</label>
                <input type="text" class="block-h" value="" placeholder="мм" oninput="this.value=proverka(this.value)">
            </div>
        </div>
        <div class="col-sm-3">
            <div class="">
                <label for="">Толщина шва </label>
            </div>
            <div class="">
                <label style="opacity: 0; display: block">Толщина шва </label>
                <input type="text" class="block-seam" value="" placeholder="мм" oninput="this.value=proverka(this.value)">
            </div>
        </div>
    </div>
</li>