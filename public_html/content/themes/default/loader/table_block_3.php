<?php
$params['material_id'] = 28;
$materials = '';
$result = Data::getMaterial($params['material_id']);
if(User::getCookie('auth-Key')!= null){
    if($params['material_id'] > 10000)$result = User::getSelfCreateData($params['material_id']);
};
if(!isset($result)){
    die();
}
$materials.= '<div class="table_3" data-tab="3"><li><table class="table"><tbody class="tab-homogeneous"><tr>';
foreach ($result as $key=>$value){
    $style = '';
    if($key == 'id'){ $style = "style='width: 60px;'";}
    $materials.='
                            <th scope="row" '.$style.'>
                            '.$key.'
                        </th>';
}
$materials.= '</tr><tr>';

foreach ($result as $key=>$value){
    $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
}
$materials.= '</tr></tbody></table></li></div>';

print_r($materials);