<?php

$par_thermal['col'] = 'COL18';
$par_thermal['val'] = 'Т';
$par_thermal['operator'] = '=';
$material_thermal = Data::getMaterialsByParams($par_thermal);

$selfCreates=[];
if(User::getCookie('auth-Key')!= null){
    $selfCreates = User::getSelfCreates();
    if(!$selfCreates)$selfCreates=[];
};
$material_thermal = array_merge($material_thermal, $selfCreates);

?>
<li class="col-md-12 margin-li li_<?=$_GET['count'];?>" data-id="<?=$_GET['count'];?>">
    <div>
        <input name="mat_cat" onclick="set.clickSelect(this, '<?=$_GET['type'];?>')"
               value="Плиты минераловатные из каменного волокна, плотность 180 кг/куб.м.">

    </div>
    <input type="number" class="thickness" value="" placeholder="мм" oninput="this.value=proverka(this.value)">

    <a class="up btn btn-primary"  onclick="upField(this)" style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-up"></i></a>
    <a class="down btn btn-primary"  onclick="downField(this)" style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-down"></i></a>
    <a class="remove btn btn-primary" onclick="removeField(this)" style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>


</li>

