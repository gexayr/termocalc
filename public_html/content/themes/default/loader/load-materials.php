<?php

$material_category = Data::getMaterialCategory();
$selfCreates=[];
if(User::getCookie('auth-Key')!= null){
    $selfCreates = User::getSelfCreates();
    if(!$selfCreates)$selfCreates=[];
};

$par_homogeneous['col'] = 'COL18';
$par_homogeneous['val'] = 'Т';
$par_homogeneous['operator'] = '!=';
$material_homogeneous = Data::getMaterialsByParamsNullHomo($par_homogeneous);
//
//$material_homogeneous = array_merge($material_homogeneous, $selfCreates);


$par_thermal['col'] = 'COL18';
$par_thermal['val'] = 'Т';
$par_thermal['operator'] = '=';
$material_thermal = Data::getMaterialsByParams($par_thermal);


$par_seam['col'] = 'COL34';
$par_seam['val'] = 'Б';
$par_seam['operator'] = '=';
$material_seam = Data::getMaterialsByParams($par_seam);


$par_block['col'] = 'COL35';
$par_block['val'] = 'Ш';
$par_block['operator'] = '=';
$material_block = Data::getMaterialsByParams($par_block);


$par_holder['col'] = 'COL17';
$par_holder['val'] = 'H';
$par_holder['operator'] = '=';
$material_holder = Data::getMaterialsByParams($par_holder);



$par_holder['col'] = 'id';
$par_holder['val'] = '0';
$par_holder['operator'] = '>';
$material_all = Data::getMaterialsByParams($par_holder);

$material_filling = $material_holder;
$material_frame = $material_all;

$type = $_POST['type'];

switch ($type) {
    case 'homogeneous':
        $material = $material_homogeneous;
        break;
    case 'thermal':
        $material = $material_thermal;
        break;
    case 'holder':
        $material = $material_holder;
        break;
    case 'frame':
        $material = $material_frame;
        break;
    case 'filling':
        $material = $material_filling;
        break;
    case 'blocks':
        $material = $material_block;
        break;
    case 'seam':
        $material = $material_seam;
        break;
    default:
        echo ('Unexpected value');
        die();
}
$material = array_merge($material, $selfCreates);

    $material_category = array();
    foreach ($material as $item) {
        $material_category[] = $item->category;
        $material_subcategory[$item->category][] = $item->COL3;
    }
    $material_category = array_unique($material_category);
foreach ($material_category as $item){
    $material_subcategory[$item] = array_unique($material_subcategory[$item]);
    foreach($material_subcategory[$item] as $key => $link){
        if(empty($link)){
            unset($material_subcategory[$item][$key]);
        }
    }
}
//debug_die($material)
?>
<div class="panel-group" id="accordion">

<?php
$iter = 0;
    foreach ($material_category as $key=>$item_category) {
        ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapse_<?=$key?>">
                        <?=$item_category?>
                    </a>
                </h4>
            </div>
            <div id="collapse_<?=$key?>" class="panel-collapse collapse">
                <div class="panel-body">

                    <?php
                        $subcategories1 = $material_subcategory[$item_category];
                        foreach ($subcategories1 as $k => $subcategory1) {
                    ?>

                    <div class="panel panel-default" id="accordion_<?=$k?>">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a data-toggle="collapse" data-parent="#accordion_<?=$k?>" href="#collapse_sub_<?=$iter?>_<?=$k?>">
                                    <?=$subcategory1?>
                                </a>
                            </h4>
                        </div>
                        <div id="collapse_sub_<?=$iter?>_<?=$k?>" class="panel-collapse collapse">
                            <div class="panel-body">

                                <?php
                                $par_materials1['val'] = $subcategory1;
                                $materials1_data = Data::getMaterialsByParams1($par_materials1);
                                ?>
                                <?php foreach($materials1_data as $materials1_datum){
                                    if(in_array($materials1_datum, array($material)[0])) {

                                        ?>
                                        <br>
                                        <h5><a class="material-item"
                                               onclick="set.clickMaterial(<?= $materials1_datum->id ?>)"><?= $materials1_datum->name ?></a>
                                        </h5>
                                        <?php
                                    }
                                } ?>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                    <?php
                        if($item_category == "Свои материалы"){
                            $materials2_data = $selfCreates;
                        }else{
                            $par_materials2['val'] = $item_category;
                            $materials2_data = Data::getMaterialsByParams2($par_materials2);
                        }
                    ?>
                    <?php foreach($materials2_data as $materials2_datum){
                        if(in_array($materials2_datum, array($material)[0])) {
                            ?>
                            <br>
                            <h5><a class="material-item"
                                   onclick="set.clickMaterial(<?= $materials2_datum->id ?>)"><?= $materials2_datum->name ?></a>
                            </h5>
                            <?php
                        }
                    } ?>
                </div>
            </div>
        </div>
    <?php
    $iter++;
    } ?>

</div>
