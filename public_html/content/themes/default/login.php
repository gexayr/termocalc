<?php $this->theme->header(); ?>
<!--    <link rel="stylesheet" href="--><?//= Theme::getUrl() ?><!--/css/profile.css">-->

    <!-- Main Content -->
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                <?php if(isset($success)):?>
                    <div class="alert alert-success alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Успешно !</strong>  <?=$success;?>
                    </div>
                <?php endif; ?>

                <?php if(isset($error)):?>
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Ошибка!</strong> <?=$error;?>
                    </div>
                <?php endif; ?>
                <h1 style="text-align: center; margin-bottom: 20px;">Логин </h1>
                <div class="well">
                    <form id="loginForm" method="POST" action="/auth/" novalidate="novalidate" style="padding-bottom: 15px">
                        <div class="form-group width_90">
                            <label for="email" class="control-label">Email</label>
                            <input type="email" class="form-control" name="email" value="" required="" title="Please enter you username" placeholder="example@gmail.com">
                        </div>
                        <div class="form-group width_90">
                            <label for="password" class="control-label">Пароль</label>
                            <input type="password" class="form-control" id="password" name="password" value="" required="" placeholder="Пожалуйста, введите пароль">
                        </div>
                        <div class="btn-input" style="margin: 0 auto; padding-bottom: 20px">
                            <input type="submit" value="Войти" class="a_p log_a_p" >
                        </div>
                        <a href="/forgot/" class="pull-right forgot">Забыли пароль</a>

                    </form>
                </div>
            </div>

        </div>
    </div>

    <style>
        footer{
            position: absolute;
            bottom: 0px;
            width: 100%;
        }
    </style>
<?php $this->theme->footer(); ?>