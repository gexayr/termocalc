<?php $this->theme->header(); ?>
    <!-- Main Content -->
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-md-offset-3">
                <h1>Восстановление пароля</h1>
                <div class="well">
                    <div class="alert alert-success text-center">
                        Мы отправили на ваш емайл ссылку для сброса пароля,
                        которая действительна в течение 30 минут.
                    </div>
                </div>
            </div>

        </div>
    </div>
    <style>
        footer{
            position: absolute;
            bottom: 0px;
            width: 100%;
        }
    </style>
<?php $this->theme->footer(); ?>