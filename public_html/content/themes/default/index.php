<?php $this->theme->header(); ?>
<!-- Main Content -->
<?php
$regions = Data::getRegions();
$cities = array();
$buildings = Data::getBuildings();
$rooms = Data::getRooms();

if (isset($_GET['params'])) {
    if(strlen($_GET['params']) == 8) {
        $params_tab['val'] = $_GET['params'];
        $params_tab['col'] = 'layer_link';
        $params_tab['operator'] = '=';

        $tab_params = Data::getLayerDataByParams($params_tab);
        if (!empty($tab_params)) {
            $tab_cookie = $tab_params[0]->layer_data_1;

            User::deleteCookie('layer_link');
            User::setCookie('layer_link', $_GET['params']);

            if($tab_params[0]->layer_data_1 != '-'){
                User::deleteCookie('data1');
                User::setCookie('data1', $tab_params[0]->layer_data_1);
            }
            if($tab_params[0]->layer_data_2 != '-'){
                User::deleteCookie('tab_2');
                User::setCookie('tab_2', $tab_params[0]->layer_data_2);
            }
            if($tab_params[0]->layer_data_3 != '-'){
                User::deleteCookie('tab_3');
                User::setCookie('tab_3', $tab_params[0]->layer_data_3);
            }
            if($tab_params[0]->layer_data_4 != '-'){
                User::deleteCookie('tab_4');
                User::setCookie('tab_4', $tab_params[0]->layer_data_4);
            }
            if($tab_params[0]->layer_data_5 != '-'){
                User::deleteCookie('tab_5');
                User::setCookie('tab_5', $tab_params[0]->layer_data_5);
            }
            if($tab_params[0]->layer_data_6 != '-'){
                User::deleteCookie('tab_6');
                User::setCookie('tab_6', $tab_params[0]->layer_data_6);
            }

        } else {
            $error_params = "неправильные параметры!";
            echo "  <div class='alert alert-danger alert-dismissible'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                <strong>Ошибка!</strong> $error_params
                            </div>";
            $tab_cookie = null;
        }
    }else{
        $error_params = "неправильные параметры!";
        echo "  <div class='alert alert-danger alert-dismissible'>
                                <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
                                <strong>Ошибка!</strong> $error_params
                            </div>";
        $tab_cookie = null;
    }
    echo "<script> let runAutoLoadData = 1 </script>";
} else {
    $tab_cookie = User::getCookie('data1');
}

require  "part/login.php";
require "part/calc_title.php";
?>
<section>
    <div class="container">
        <div class="calc-img-desc">
            <table>
                <tr>
                    <td>
                        <div class="calc-img">
                            <img src="<?= Theme::getUrl() ?>/img/calc-img.png" alt="Проектировка">
                        </div>
                    </td>
                    <td>
                        <div class="calc-desc">
                            <p>С помощью данного теплотехнического онлайн калькулятора вы можете выполнить все теплотехнические
                                расчёты с учётом  географического местоположения вашего здания по действующему СП 50.13330 «Тепловая защита зданий».<br>
                                Результаты расчёта «Термокальк» выдаёт комплексно сразу для всех конструкций здания
                                (стены, кровля, полы, окна) и для всех предусмотренных нормативами помещений (жилые комнаты, кухни, ванные и т.д.).</p>
                            <p>После ввода данных по объекту (объёмов, площадей, «пирога» каждой конструкции) вы получаете расчёт:
                                <br>
                                - соответствия коэффициентов теплосопротивления всех ограждающих конструкций здания установленным строительными нормам<br>
                                -  будет ли влагонакопление в конструкциях<br>
                                - соответствия конструкций здания установленным санитарно-гигиеническим нормативам<br>
                                - теплопотерь как отдельных элементов, так и в целом здания, на основе чего формируется график
                                «структуры теплопотерь» и устанавливается необходимая мощность котла и системы отопления здания<br>
                                - стоимости системы отопления в зависимости от полученных теплопотерь здания<br>
                                - стоимости строительных материалов и работы по строительству рассчитываемого здания (смета)<br>
                                - стоимости затрат на отопление за годовой период и за период «условного минимального гарантированного
                                срока эксплуатации здания» - 25 лет с учётом прогноза ежегодного удорожания энергоносителей</p>
                            <p>На основе полученных данных вы можете подбором толщин, систем, размеров, материалов сконструировать
                                для себя наиболее оптимальный набор характеристик. К примеру, самый недорогой вариант,
                                самый энергосберегающий и затем с минимальными затратами на отопление или определить по стоимостям,
                                когда какое-либо решение окупается ли за счёт стоимости последующего отопления или нет.
                                <br>
                                В таблице основных полученных характеристик можно сравнить до 5 вариантов изменений, выбрать
                                который наиболее подходит и получить на ваш компьютер файл-отчёт в формате pdf с подробными данными
                                результатов выполненных расчётов с рекомендациями по применению материалов и технологий.</p>
                            <p>Подробнее описание возможностей онлайн-калькулятора и рекомендации по работе вы можете получить в разделе
                                <a href="/help">ПОМОЩЬ.</a>
                                В случае, если у вас есть вопросы, пожелания, предложения или вы хотите что-то обсудить,
                                то это можно сделать на <a href="/forum">ФОРУМЕ.</a> В разделе <a href="/catalog">СТРОИТЕЛЬНЫЙ СПРАВОЧНИК</a> постепенно собираются проверенные
                                рекомендации и описание строительных материалов и технологий.<br>
                                Обратиться к Администрации сайта вы можете в <a data-toggle="modal" data-target="#myModal" href="javascript:void(0)">ФОРМЕ ОБРАТНОЙ СВЯЗИ</a> (работает без регистрации).  </p>
                            <p class="about-browser"><br>*** Программа оптимизирована под браузеры Chrome, Firefox, Opera, Yandex.
                                Не работает в Internet Explorer и Microsoft Edge</p>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</section>
<?php
if($login == null) {
    ?>
    <div class="container">
        <div class="row">

            <div class="admin">
                <div class="admin-title">
                    <div class="admin-img">
                        <img src="<?= Theme::getUrl() ?>/img/info1.png" alt="Info">
                    </div>
                    <h3>Сейчас вы НЕ АВТОРИЗОВАНЫ в системе. <a href="/login">ВОЙДИТЕ</a> или <a href="/sign-up">ЗАРЕГИСТРИРУЙТЕСЬ.</a></h3>
                </div>
                <div class="info-content for-guest">
                    В «неавторизованном» режиме вам доступно получение климатических данных для дальнейшего расчёта в
                    зависимости от географического местоположения объекта.
                    <br>
                    Теплотехнический расчёт доступен только для стеновой конструкции и только для «жилой комнаты».
                    В результате вы получите все требуемые СП 50.13330 расчёты:
                    <br>
                    - соответствие вашей стеновой конструкции установленному нормативами теплосопротивлению <br>
                    - наличия или отсутствия влагонакопления в стеновой конструкции <br>
                    - санитарно-гигиенических требований в стеновой конструкции <br>
                    Данное ограничение по расчётам продиктовано особенностями работы программы,
                    так как для корректной работы всего расчёта необходимо запоминание всех введённых
                    данных с которыми производится огромное количество различных расчётных операций,
                    а также сохраняются результаты произведённых 5 вариантов расчётов, необходимых для анализа и сравнения,
                    чтобы выбрать наиболее для вас подходящий. Зарегистрируйтесь и вам будет доступен полный
                    функционал онлайн-калькулятора.
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
<section class="pt-l-ad">
    <div class="container">
        <?php $this->theme->sidebar(); ?>

        <!-- Местоположение объекта -->
        <div class="location col-md-3">
            <div class="location-title">
                <div class="passing-test-img">
                    <img src="<?= Theme::getUrl() ?>/img/world.png" alt="Прохождение">
                </div>
                <h3>Местоположение объекта:</h3>
            </div>
            <?php if ($tab_cookie == null) { ?>
                <form action="#" class="location-form">
                    <div class="area">
                        <p>Область</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="region" id="selectRegion"
                                    onchange="set.ChangeRegion()" required>
                                <option value="">Выберите регион</option>
                                <?php foreach ($regions as $region) { ?>
                                    <option value="<?= $region->id ?>"><?= $region->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <div class="help-info" data-id="1" data-toggle="modal" data-target="#myModalHelp">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="city">
                        <p>Город</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="city" id="selectCity"
                                    onchange="set.ChangeCity()">
                                <option value="">Выберите город</option>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city->id ?>"><?= $city->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <div class="help-info" data-id="2" data-toggle="modal" data-target="#myModalHelp">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="region-number">
                        <p>Тип здания</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="building" id="selectBuilding"
                                    onchange="set.ChangeBuilding()">
                                <option value="">Выберите тип здания</option>
                                <?php foreach ($buildings as $building) { ?>
                                    <option value="<?= $building->id ?>"><?= $building->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <!--                                <div class="form-label-verified">-->
                                <!--                                    <img src="--><?//= Theme::getUrl() ?><!--/img/verified.png" alt="Подтверждено">-->
                                <!--                                </div>-->
                                <div class="help-info" data-id="3" data-toggle="modal" data-target="#myModalHelp">

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            <?php } else {
                $data = (json_decode($tab_cookie));
                if (isset($data->city)) { echo "<script>
                                $( document ).ready(function() {
                                    set.ChangeCity()
                                });</script>";
                }
                if (isset($data->building)) {
                    echo "<script>
                    $( document ).ready(function() {
                        set.ChangeBuilding()
                    });</script>";
                }
                $cities = Data::getCitiesByReg($data->region);
                ?>
                <!------------------------>
                <form action="#" class="location-form">
                    <div class="area">
                        <p>Область</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="region" id="selectRegion"
                                    onchange="set.ChangeRegion()" required>
                                <option value="">Выберите регион</option>
                                <?php foreach ($regions as $region) { ?>
                                    <option value="<?= $region->id ?>" <?php if ($region->id == $data->region) echo "selected"; ?>><?= $region->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <div class="help-info" data-id="1" data-toggle="modal" data-target="#myModalHelp">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="city">
                        <p>Город</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="city" id="selectCity"
                                    onchange="set.ChangeCity()">
                                <option value="">Выберите город</option>
                                <?php foreach ($cities as $city) { ?>
                                    <option value="<?= $city->id ?>" <?php if ($city->id == $data->city) echo "selected"; ?>><?= $city->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <div class="help-info" data-id="2" data-toggle="modal" data-target="#myModalHelp">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="region-number">
                        <p>Тип здания</p>
                        <div class="input-label-info">
                            <select class="form-control form-control-lg" name="building" id="selectBuilding"
                                    onchange="set.ChangeBuilding()">
                                <option value="">Выберите тип здания</option>
                                <?php foreach ($buildings as $building) { ?>
                                    <option value="<?= $building->id ?>" <?php if ($building->id == $data->building) echo "selected"; ?>><?= $building->name ?></option>
                                <?php } ?>
                            </select>
                            <div class="label-info_block">
                                <div class="help-info" data-id="3" data-toggle="modal" data-target="#myModalHelp">

                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!------------------------>
                <?php if (isset($data->city)){
                    $city = Data::getCity($data->city);
                }
            }
            ?>

        </div>
        <!-- Дополнительные данные -->
        <div class="additional-data">
            <div class="additional-data-title">
                <div class="passing-test-img">
                    <img src="<?= Theme::getUrl() ?>/img/edit.png" alt="Прохождение">
                </div>
                <h3>Введите дополнительные данные:</h3>
            </div>
            <form action="#" class="additional-data-form">
                <div class="individual">
                    <p>Это малоэтажное индивидуальное здание (коттедж) ?</p>
                    <div class="input-label-info">
                        <select name="" id="one_floor_building" class="form-control"
                                onchange="set.AnswerFloorBuilding()">
                            <option value=""></option>
                            <option value="yes" <?php if ($tab_cookie != null && $data->TD171 == 'yes') echo "selected"; ?>>
                                Да
                            </option>
                            <option value="no" <?php if ($tab_cookie != null && $data->TD171 == 'no') echo "selected"; ?>>
                                Нет
                            </option>
                        </select>
                        <div class="help-info" data-id="4" data-toggle="modal" data-target="#myModalHelp">

                        </div>
                    </div>
                </div>
                <div class="floors">
                    <p>Сколько этажей в здании? (включая отапливаемый подвал)</p>
                    <div class="input-label-info">
                        <input type="text" class="form-control" id="living_floors" placeholder="Введите свой ответ"
                               value='<?php if ($tab_cookie != null) echo $data->TD170 ?>'>
                        <div class="help-info" data-id="5" data-toggle="modal" data-target="#myModalHelp">

                        </div>
                    </div>
                </div>
                <div class="resident">
                    <p>предполагаемое количество проживающих</p>
                    <div class="input-label-info">
                        <input type="text" class="form-control" id="people_count" placeholder="Введите свой ответ"
                               value='<?php if ($tab_cookie != null) echo $data->TD172 ?>'>
                        <div class="help-info" data-id="6" data-toggle="modal" data-target="#myModalHelp">

                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>

<section class="room-size-structure">
    <div class="container">
        <!-- Размеры помещений -->
        <div class="row room-size col-sm-12">
            <div class="room-size-title">
                <div class="room-size-img" align="center">
                    <img src="<?= Theme::getUrl() ?>/img/expand.png" alt="Прохождение">
                    <h3>Введите <b>внутренние</b> размеры помещений:
                    </h3>
                </div>
                <div class="help-info" data-id="13" data-toggle="modal" data-target="#myModalHelp"></div>

            </div>

            <div class="name-room-form">
                <?php if($login == null)echo "<span style='position: absolute; font-size: 12px; margin-top: 25px'>
                * Таблица заполняется для расчётов в режиме зарегистрированного пользователя</span>";?>
                <div class="name-room">
                    <p class="first-child">Жилая комната</p>
                    <p>Кухня</p>
                    <p>Туалет</p>
                    <p>Ванная</p>
                    <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                        echo 'display: none;';
                    } ?>">
                        <p>Помещения для отдыха</p>
                        <p>Межквартирный коридор</p>
                        <p><span class="hide_p">Вестибюль, </span>лестничная клетка</p>
                        <p>Кладовые</p>
                    </div>
                </div>
                <form action="#" class="room-size-form">
                    <div class="inner-room-size-form floor-space">
                        <div class="label-info_block">
                            <p>Площадь по полу (кв.м)</p>
                            <div class="help-info internal_info" data-id="8" data-toggle="modal" data-target="#myModalHelp">
                            </div>
                        </div>

                        <input type="text" placeholder="кв.м." id="living_room_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD200)) echo $data->TD200 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="kitchen_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD202)) echo $data->TD202 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="toilet_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD204)) echo $data->TD204 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="bathroom_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD206)) echo $data->TD206 ?>' oninput="this.value=proverka(this.value)">
                        <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                            echo 'display: none;';
                        } ?>">
                            <input type="text" placeholder="кв.м." id="rest_room_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD208)) echo $data->TD208 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="hall_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD210)) echo $data->TD210 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="vestibule_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD212)) echo $data->TD212 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="pantry_floor" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD214)) echo $data->TD214 ?>' oninput="this.value=proverka(this.value)">
                        </div>
                    </div>
                    <div class="inner-room-size-form area-outer-wall">
                        <div class="label-info_block">
                            <p>Площадь стен (кв.м)</p>

                            <div class="help-info internal_info" data-id="9" data-toggle="modal" data-target="#myModalHelp">
                            </div>
                        </div>
                        <input type="text" placeholder="кв.м." id="living_room_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD201)) echo $data->TD201; if($login == null && !isset($data->TD201))echo '1'; ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="kitchen_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD203)) echo $data->TD203 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="toilet_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD205)) echo $data->TD205 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="bathroom_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD207)) echo $data->TD207 ?>' oninput="this.value=proverka(this.value)">
                        <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                            echo 'display: none;';
                        } ?>">
                            <input type="text" placeholder="кв.м." id="rest_room_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD209)) echo $data->TD209 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="hall_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD211)) echo $data->TD211 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="vestibule_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD213)) echo $data->TD213 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="pantry_wall" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD215)) echo $data->TD215 ?>' oninput="this.value=proverka(this.value)">
                        </div>
                    </div>
                    <div class="inner-room-size-form area-outer-roof">
                        <div class="label-info_block">
                            <p>Площадь кровли (кв.м)</p>
                            <div class="help-info internal_info" data-id="10" data-toggle="modal" data-target="#myModalHelp">
                            </div>
                        </div>
                        <input type="text" placeholder="кв.м." id="living_room_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD216)) echo $data->TD216 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="kitchen_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD217)) echo $data->TD217 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="toilet_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD218)) echo $data->TD218 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="кв.м." id="bathroom_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD219)) echo $data->TD219 ?>' oninput="this.value=proverka(this.value)">
                        <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                            echo 'display: none;';
                        } ?>">
                            <input type="text" placeholder="кв.м." id="rest_room_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD220)) echo $data->TD220 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="hall_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD221)) echo $data->TD221 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="vestibule_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD223)) echo $data->TD223 ?>' oninput="this.value=proverka(this.value)">
                            <input type="text" placeholder="кв.м." id="pantry_roof" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD225)) echo $data->TD225 ?>' oninput="this.value=proverka(this.value)">
                        </div>
                    </div>
                    <div class="inner-room-size-form room-volume">
                        <div class="label-info_block">
                            <p>Объём помеще<span>-</span><br>ния (куб.м)</p>
                            <div class="help-info internal_info" data-id="11" data-toggle="modal" data-target="#myModalHelp">
                            </div>
                        </div>
                        <input type="text" placeholder="куб.м." id="living_room_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD280)) echo $data->TD280 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="куб.м." id="kitchen_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD281)) echo $data->TD281 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="куб.м." id="toilet_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD282)) echo $data->TD282 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="куб.м." id="bathroom_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD283)) echo $data->TD283 ?>' oninput="this.value=proverka(this.value)">
                        <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                            echo 'display: none;';
                        } ?>">
                            <div class="form-group form-group-sm">
                                <input type="text" placeholder="куб.м." id="rest_room_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD284)) echo $data->TD284 ?>' oninput="this.value=proverka(this.value)">
                                <input type="text" placeholder="куб.м." id="hall_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD285)) echo $data->TD285 ?>' oninput="this.value=proverka(this.value)">
                                <input type="text" placeholder="куб.м." id="vestibule_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD286)) echo $data->TD286 ?>' oninput="this.value=proverka(this.value)">
                                <input type="text" placeholder="куб.м." id="pantry_volume_air" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD287)) echo $data->TD287 ?>' oninput="this.value=proverka(this.value)">
                            </div>
                        </div>
                    </div>
                    <div class="inner-room-size-form number-rooms">
                        <div class="label-info_block">
                            <p>Количество помещений (шт)</p>
                            <div class="help-info internal_info" data-id="12" data-toggle="modal" data-target="#myModalHelp">
                            </div>
                        </div>
                        <input type="number" name="nr-living-room" disabled="disabled" style="opacity: 0">
                        <input type="text" placeholder="шт." id="kitchen_count" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD288)) echo $data->TD288 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="шт." id="toilet_count" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD289)) echo $data->TD289 ?>' oninput="this.value=proverka(this.value)">
                        <input type="text" placeholder="шт." id="bathroom_count" <?php if($login == null)echo 'disabled';?> value='<?php if (isset($data->TD290)) echo $data->TD290 ?>' oninput="this.value=proverka(this.value)">
                        <div class="for_multistory" style="<?php if ($tab_cookie != null && $data->TD171 == 'yes') {
                            echo 'display: none;';
                        } ?>">
                            <div class="form-group form-group-sm">

                            </div>
                        </div>
                    </div>

                </form>
            </div>
        </div>
        <!-- Конструкция помещений -->
    </div>
</section>
<section class="completeness-test-standards-core-data-conclusion-admin">
    <div class="container">
        <div class="row">
            <div class="col-md-2">
                <div class="standards">
                    <div class="standards-title">
                        <div class="standards-img">
                            <img src="<?= Theme::getUrl() ?>/img/group.png" alt="Нормативы">
                        </div>
                        <h3>Термокалькулятор производит
                            расчёты в соответствии с нормативами:</h3>
                    </div>
                    <div class="standards-content">
                        <p>- СП 50.13330.2012 Тепловая защита зданий</p>
                        <p>- ГОСТ 30494-2011 Здания жилые и общественные. Параметры микроклимата в помещениях</p>
                        <p>- СП 60.13330.2012 Отопление, вентиляция и кондиционирование воздуха</p>
                        <p>- СанПиН 2.1.2.2645-10 Санитарно-эпидемиологические требования к условиям проживания в жилых зданиях и помещениях</p>
                        <p>- СП 131.13330.2012 Строительная климатология</p>
                        <p>- ГОСТ Р 54851-2011 Конструкции строительные ограждающие неоднородные. Расчет приведенного сопротивления теплопередаче</p>
                        <p>- ГОСТ 26253-84 Здания и сооружения. Метод определения теплоустойчивости ограждающих конструкций</p>
                        <p>- СанПиН 2.1.2.2645-10 Санитарно-эпидемиологические требования к условиям проживания в жилых зданиях и помещениях</p>
                        <p>- СП 118.13330.2012 Общественные здания и сооружения</p>
                        <p>- СП 44.13330.2011 Административные и бытовые здания</p>
                        <p>- СП 54.13330.2011 Здания жилые многоквартирные</p>
                        <p>- СП 55.13330.2011 Дома жилые одноквартирные</p>
                    </div>
                </div>
            </div>
            <div class="col-md-9">
                <div class="btn_block">
                    <div class="btn-input">
                        <a href="javascript: void(0)" id="send"><p>Ввод</p></a>
                        <p>Если ввод данных завершён, нажмите кнопку ВВОД</p>
                    </div>
                    <div class="btn-clear">
                        <a id="clear" href="/reset_cookie?tab=1" onclick="localStorage.clear()"><p>ОЧИСТИТЬ ВКЛАДКУ</p></a>
                        <p>Очистка результатов в данной вкладке</p>
                    </div>
                    <div class="btn-clear-all">
                        <a  href="/reset_cookie?tab=all" onclick="localStorage.clear()"><p>ОЧИСТИТЬ ВЕСЬ РАСЧЁТ</p></a>
                        <p>Очистка всего расчёта</p>
                    </div>
                </div>
                <!-- Основные данные -->
                <div class="core-data_" id="core-data-conclusion-admin" style="display: none">
                    <div class="core-data-title">
                        <div class="core-data-img">
                            <img src="<?= Theme::getUrl() ?>/img/data.png" alt="Нормативы">
                        </div>
                        <h3>Основные данные географического местоположения объекта:</h3>
                    </div>
                    <div class="core-data-content">
                        <div class="data">
                            <p class="data-name">Город :</p>
                            <p class="data-value">
                                <input type="text" id="TD01_info" disabled="disabled"
                                       value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Выбранный тип здания :</p>
                            <p class="data-value">
                                <input type="text" id="TD02_info" disabled="disabled"
                                       value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Зона влажности в которой находится населённый пункт :</p>
                            <p class="data-value">
                                <input type="text" id="TD04_info" disabled="disabled"
                                       value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Наружная температура наиболее холодной пятидневки (обеспеченностью 0,92) :</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD37_info" value=''>
                            </p>
                        </div><div class="data">
                            <p class="data-name">Средняя температура наружного воздуха отопительного периода:</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD38_info" value=''>
                            </p>
                        </div>

                        <div class="data">
                            <p class="data-name">Количество суток отопительного периода :</p>
                            <!--                            <p class="data-name">Средняя температура наружного воздуха, °С, отопительного периода, °С :</p>-->

                            <p class="data-value">
                                <span> суток</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD39_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Среднее парциальное давления наружного воздуха за годовой период :</p>
                            <p class="data-value">
                                <span> гПа</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD43_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Средняя температура периода с среднемесячными отрицательными температурами :</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD40_info" value=''>
                            </p>
                        </div>

                        <div class="data">
                            <p class="data-name">Количество суток периода со среднемесячными отрицательными температурами :</p>
                            <p class="data-value">
                                <span> суток</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD41_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Среднее парциальное давление наружного воздуха за период отрицательных температур :</p>
                            <p class="data-value">
                                <span> гПа</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD42_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Средняя наружная температура зимнего периода :</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD45_info" value=''>
                            </p>
                        </div>

                        <div class="data">
                            <p class="data-name">Количество месяцев зимнего периода :</p>
                            <p class="data-value">
                                <span> месяцев</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD46_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Среднее парциальное давление зимнего периода :</p>
                            <p class="data-value">
                                <span> гПа</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD47_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Средняя наружная температура весенне-осеннего периода :</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD48_info" value=''>
                            </p>
                        </div>

                        <div class="data">
                            <p class="data-name">Количество месяцев весенне-осеннего периода :</p>
                            <p class="data-value">
                                <span> месяцев</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD49_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Среднее парциальное давление весенне-осеннего периода :</p>
                            <p class="data-value">
                                <span> гПа</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD50_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Средняя наружная температура летнего периода :</p>
                            <p class="data-value">
                                <span> °С</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD51_info" value=''>
                            </p>
                        </div>

                        <div class="data">
                            <p class="data-name">Количество месяцев летнего периода :</p>
                            <p class="data-value">
                                <span> месяцев</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD52_info" value=''>
                            </p>
                        </div>
                        <div class="data">
                            <p class="data-name">Среднее парциальное давление летнего периода :</p>
                            <p class="data-value">
                                <span> гПа</span>
                            </p>
                            <p class="data-value">
                                <input type="text" disabled="disabled" id="TD53_info" value=''>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="admin">
                    <div class="admin-title">
                        <div class="admin-img">
                            <img src="<?= Theme::getUrl() ?>/img/info1.png" alt="Info">
                        </div>
                        <h3>Информация</h3>
                    </div>
                    <div class="info-content">
                        Наш проект недавно открылся. Возможны небольшие недоработки. Если вы заметите ошибки или у вас
                        будут пожелания улучшить сервис, то напишите об этом в <a href="/forum">ФОРУМ</a> в раздел Технической поддержки
                        работы проекта или в <a data-toggle="modal" data-target="#myModal" href="javascript:void(0)">ФОРМУ ОБРАТНОЙ СВЯЗИ</a>
                    </div>
                </div>
                <div class="conclusion"  style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                    echo 'block';
                }else{
                    echo 'none';
                }
                ?>">
                    <div class="conclusion-title">

                        <div class="admin-img">
                            <img src="<?= Theme::getUrl() ?>/img/group.png" alt="Администратор">
                        </div>
                        <h3>Администраторская область</h3>
                    </div>
                    <div class="conclusion-content" id="conclusion_content">
                        <div class="type-result">
                            <div class="panel-heading">
                                <ul class="nav nav-tabs card-header-tabs">
                                    <li class="nav-item active" id="panel_tab_result">
                                        <a class="nav-link" href="#Result" data-toggle="tab">Результат</a>
                                    </li>
                                    <li class="nav-item" id="panel_tab_city">
                                        <a class="nav-link" href="#City" data-toggle="tab">Город</a>
                                    </li>
                                    <li class="nav-item" id="panel_tab_building">
                                        <a class="nav-link" href="#Building" data-toggle="tab">Тип здания</a>
                                    </li>
                                </ul>
                            </div>

                            <div class="panel-body">
                                <div class="tab-content">

                                    <div class="tab-pane fade in active" id="Result">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th style="text-align: center">Value</th>
                                            </tr>
                                            </thead>
                                            <tbody id="AllTbody">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="City">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Field</th>
                                                <th>Value</th>
                                            </tr>
                                            </thead>
                                            <tbody id="cityTbody">
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="tab-pane fade" id="Building">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Field</th>
                                                <th>Value1</th>
                                                <th>Value2</th>
                                            </tr>
                                            </thead>
                                            <tbody id="buildingTbody">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php
if ($tab_cookie != null) {
    echo "<script>
            $(document).ready(function() {
                send_city('auto');
            });
        </script>";
}

    if (empty(User::getCookie('dont_show_preview'))){
?>
    <script>
        $( document ).ready(function() {
            $('#preview_modal_button').click()
        });
    </script>
<?php
    }

?>
<?php $this->theme->footer(); ?>
