<?php $this->theme->header(); ?>
<?php //$posts = User::getHelpPostsByParams('-'); ?>
<?php $posts = User::getHelpPosts(); ?>

<div class="container">
    <div class="row">
        <div class="passing-test-content">
            <div class="post-block">

                <?php foreach ($posts as $item): ?>
                    <?php
                    if($item->id == 8) {
                        ?>
                        <div class="blank-help">
                            <a class="catalog-menu-item" href="/help/<?= $item->id ?>">
                                <span><?= $item->title ?></span>
                            </a>
                        </div>
                    <?php } else {
                        $post = $item;
                    }?>
                <?php endforeach; ?>
                <?php foreach ($posts as $item): ?>
                    <?php
                        if($item->id != 8) {
                            ?>
                            <div class="blank-help">
                                <a class="catalog-menu-item" href="/help/<?= $item->id ?>">
                                    <span><?= $item->title ?></span>
                                </a>
                            </div>
                        <?php } else {
                            $post = $item;
                        }?>
                <?php endforeach; ?>
                <div class="activity post-block">
                    <div class="posts_content">
                        <?php
                        if(isset($post)) {
                            ?>
                            <h2 align="center"><?= $post->title ?></h2>
                            <div class="post_content">
                                <?= $post->content ?>
                            </div>
                            <div class="post_date" align="right">
                                <?= $post->date ?>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->theme->footer(); ?>
