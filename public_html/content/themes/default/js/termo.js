
$('#recuperator').change(function() {
    if($('#recuperator').val() == 100){
        $('#recuperator_isset').fadeOut("slow");
    }else{
        $('#recuperator_isset').fadeIn("slow");
    }
});


// EDIT CALCULATION SECTION
$('#edit_calculation_button').click(function() {
    var formData = new FormData();
    var id = $('#edit_calc_id').val();
    var name = $('#edit_calc_name').val();
    formData.append('name', name);
    formData.append('id', id);

    $.ajax({
        url: '/editLayerName',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            // console.log(result)
            $('#myModalSave').find('.red').text(result);
            $('#myModalSave').find('.red').fadeIn("slow");
            setTimeout(function () {
                $('#myModalSave').find('.red').fadeOut("slow");
            }, 2000);
            if(result == 'Запись сохранена'){
                setTimeout(function () {
                    $('#close_modal_saving_modal').click();
                }, 2800);
                var element = ($("#menu1").find(`[data-id='${id}']`));
                $(element).text(name)
                var element = ($("#t01_").find(`[data-id='${id}']`));
                $(element).text(name)
            }
        }
    });
});
// END EDIT CALCULATION SECTION


$('#edit_calc_name,#save_calc_name').on('keydown', function(e) {
    if(this.value.length > 100) {
        $('#myModalSave').find('.red').text("максимальное количество символов 100");
        $('#myModalSave').find('.red').fadeIn("slow");
        setTimeout(function () {
            $('#myModalSave').find('.red').fadeOut("slow");
        }, 2000);
        if(e.keyCode != 8)
            return false;
    }
    // console.log(100-this.value.length);
});

$('#changeLinkName').on('change', function() {
    var layerId = $('#changeLinkName').val();
    var layerName = $("#changeLinkName option:selected").text().trim();
    $('#layerDataId').val(layerId);
    $('#save_calc_name').val(layerName);
    console.log(layerName);
});


// SAVE CALCULATION SECTION

$('#save_calculation_button').click(function() {
    var formData = new FormData();
    formData.append('name', $('#save_calc_name').val());
    formData.append('layer_id', $('#layerDataId').val());

    $.ajax({
        url: '/writeLayerName',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            console.log(result);
            $('#myModalSave').find('.red').text(result);
            $('#myModalSave').find('.red').fadeIn("slow");
            setTimeout(function () {
                $('#myModalSave').find('.red').fadeOut("slow");
            }, 2000);
            if(result === 'Запись сохранена') {
                setTimeout(function () {
                    $('#close_modal_saving_modal').click();
                }, 2800);
            }

        }
    });
});
// END SAVE CALCULATION SECTION


$('#calculate_heating').click(function() {
    // console.log('calculate heating');
    var formData = new FormData();

    formData.append('TD173', $('#energy_type').val());
    formData.append('TD174', $('#heater_type').val());
    formData.append('TD175', $('#vent_type').val());
    if($('#recuperator').val() == 100){
        formData.append('TD190', $('#recuperator').val());
    }else{
        formData.append('TD190', $('#recuperator_value').val());
    }
    formData.append('cookie_name', 'data6');
    $.ajax({
        url: '/writeCookie',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
        },
        success: function (result) {


            $.ajax({
                url: '/calc_heating',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
//                     console.log(result);
                    // $('#table_heating').html(result);

                    var res = JSON.parse(result)
                    $('#term_in').html(res.in);
                    $('#term_out').html(res.out);
                    $('#table_heating .TD312').html(res.TD312);
                    $('#table_heating .TD314').html(res.TD314);
                    $('#table_heating .TD341').html(res.TD341);
                    $('#table_heating .TD342').html(res.TD342);
                    $('#table_heating .TD313').html(res.TD313);
                    $('#table_heating .TD320').html(res.TD320);

                    var percent_walls = res.TD321;
                    var percent_roof = res.TD322;
                    var percent_floor = res.TD323;
                    var percent_windows = res.TD324;
                    var percent_lantern = res.TD325;
                    var percent_vent = res.TD326;

                    var percent_walls_arrow = percent_walls;
                    var percent_roof_arrow = percent_roof;
                    var percent_floor_arrow = percent_floor;
                    var percent_windows_arrow = percent_windows;
                    var percent_lantern_arrow = percent_lantern;
                    var percent_vent_arrow = percent_vent;

                    if(percent_walls_arrow < 10) percent_walls_arrow = 8;
                    if(percent_roof_arrow < 10) percent_roof_arrow = 8;
                    if(percent_floor_arrow < 10) percent_floor_arrow = 8;
                    if(percent_windows_arrow < 10) percent_windows_arrow = 8;
                    if(percent_lantern_arrow < 10) percent_lantern_arrow = 8;
                    if(percent_vent_arrow < 10) percent_vent_arrow = 8;
// ######################## for convas ############################


                    var width_arrow_walls = 2*res.TD321;
                    var width_arrow_roof = 2*res.TD322;
                    var width_arrow_floor = 2*res.TD323;
                    var width_arrow_vent = 2*res.TD326;
                    var width_arrow_windows = 2*res.TD324;
                    var width_arrow_lantern = 2*res.TD325;


                    if(width_arrow_walls  > 50) width_arrow_walls = 50;
                    if(width_arrow_roof  > 50) width_arrow_roof = 50;
                    if(width_arrow_floor  > 50) width_arrow_floor = 50;
                    if(width_arrow_vent  > 50) width_arrow_vent = 50;
                    if(width_arrow_windows  > 50) width_arrow_windows = 50;
                    if(width_arrow_lantern  > 50) width_arrow_lantern = 50;

                    if(width_arrow_walls < 10) width_arrow_walls = 30;
                    if(width_arrow_roof < 10) width_arrow_roof = 30;
                    if(width_arrow_floor < 10) width_arrow_floor = 30;
                    if(width_arrow_vent < 10) width_arrow_vent = 30;
                    if(width_arrow_windows  < 10) width_arrow_windows = 30;
                    if(width_arrow_lantern  < 10) width_arrow_lantern = 30;

                    var canvas = document.getElementById('convas_heating');
                    var ctx = canvas.getContext('2d');
                    ctx.clearRect(0, 0, 500, 350);
                    var img = document.getElementById("texture_heating_");
                    ctx.drawImage(img, 10, 100, 400, 300);
                    var pi = Math.PI;

                    var img_arrow = document.getElementById("img_arrow");

                    // vent
                    if(percent_vent != 0){
                        ctx.drawImage(img_arrow, 130, 120 - 2*percent_vent, width_arrow_vent, 2*percent_vent_arrow);
                        ctx.font = "18px Arial";
                        ctx.fillText("Вентиляция - "+ percent_vent + '%', 0, 105);
                    }

                    // roof
                    if(percent_roof != 0) {
                        ctx.drawImage(img_arrow, 190, 170 - 2 * percent_roof, width_arrow_roof, 2 * percent_roof_arrow);
                        ctx.fillText("Кровля - " + percent_roof + '%', 225, 132);
                    }

                    // lantern
                    if(percent_lantern != 0) {
                        // var img_arrow_l = document.getElementById("img_arrow_l");
                        ctx.drawImage(img_arrow_w, 340, 190, 2 * percent_lantern_arrow, 20 + width_arrow_lantern);
                        ctx.fillText("Фонари - " + percent_lantern + '%', 380, 190);
                    }

                    // walls
                    if(percent_walls != 0) {
                        // var img_arrow_w = document.getElementById("img_arrow_w");
                        ctx.drawImage(img_arrow_w, 360, 300, 2 * percent_walls_arrow, width_arrow_walls + 20);
                        ctx.fillText("Стены - " + percent_walls + '%', 350, 300);
                    }

                    // windows
                    if(percent_windows != 0) {
                        // var img_arrow_wi = document.getElementById("img_arrow_w");
                        ctx.drawImage(img_arrow_f, 250, 340, 20 + width_arrow_windows, 2 * percent_windows_arrow);
                        ctx.fillText("Окна - " + percent_windows + '%', 215, 285);
                    }

                    // floor
                    if(percent_floor != 0) {
                        // var img_arrow_f = document.getElementById("img_arrow_f");
                        ctx.drawImage(img_arrow_f, 150, 380, width_arrow_floor + 20, 2 * percent_floor_arrow);
                        ctx.fillText("Полы - " + percent_floor + '%', 65, 430);
                    }
// #########################################################
    function calculateEstimate() {
        var formData_estimate = new FormData();
        formData_estimate.append('energy_type', $('#energy_type').val());
        formData_estimate.append('TD320', res.TD320);
        formData_estimate.append('TD313', res.TD313);
        $.ajax({
            url: '/heating_estimate',
            type: 'POST',
            data: formData_estimate,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
            },
            success: function (result) {
//                console.log(result);
                $('#for-estimate').html(result);
                $('#for-estimate').show();
                writeLayerTermo();
            }
        });
    }
// =================================================
// =================================================
                    formData.append('cookie_name', "tab_6");
                    formData.append('td313', res.TD313);

                    $.ajax({
                        url: '/writeCookie',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                        },
                        success: function (result) {
                            calculateEstimate()
                        }
                    });


                function writeLayerTermo(){

                    var canvas_base = document.querySelector('#convas_heating');
                    var dataURL = canvas_base.toDataURL();
                    formData.append('data[TD173]', $('#energy_type').val());
                    formData.append('data[TD174]', $('#heater_type').val());
                    formData.append('data[TD175]', $('#vent_type').val());
                    formData.append('data[image]',  dataURL);
                    formData.append('data[content_block_term_out]', $('#term_out').html());
                    formData.append('data[content_block_term_out_result]', $('#term_out_result').html());
                    formData.append('data[content_block_term_in]', $('#term_in').html());
                    formData.append('data[content_block_term_in_result]', $('#term_in_result').html());
                    formData.append('data[content_block_all_res_com]', $('#all_res_com').html());
                    formData.append('data[estimate]', $("#for-estimate").html());
                    formData.append('data[heatingFile]', $("#heating-file").val());

                    $.ajax({
                        url: '/writeLayer',
                        type: 'POST',
                        data: formData,
                        cache: false,
                        processData: false,
                        contentType: false,
                        beforeSend: function () {
                        },
                        success: function (result) {
                            console.log(result);
                            if(
                                typeof runAutoLoadData != 'undefined'
                                && typeof logged != 'undefined'
                            ) {
                                console.log('cookie');
                                localStorage.setItem("activate_buttons", 'activate');
                            }
                        }
                    });
                }
// =================================================
// =================================================

                }
            });

        }
    });
    $('.schedule_block').fadeIn();
    $('.heating_basic_data').fadeIn();

    $('#table_heating').fadeIn();
    $('#save_calculation').fadeIn();
    $('#save_calculation .btn-clear').css('margin', '0 auto');
});
