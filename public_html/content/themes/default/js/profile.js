function editDataLayer(id,name) {
    $('#edit_calc_name').val(name);
    $('#edit_calc_id').val(id);
}
function copyLink (element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).find('span').text().trim()).select();
    document.execCommand("copy");
    $temp.remove();

    $(element).parent().find('.info-text').fadeIn()
    setTimeout(function(){
        $(element).find('.info-text').fadeOut()
    }, 2000);
}
function removeDataLayer(id,userId,element) {
    if (!confirm("are you sure?")) {
        return false;
    }

    var formData = new FormData();
    formData.append('id', id);
    formData.append('user_id', userId);

    $.ajax({
        url: '/remove_data_layer',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            console.log(result);
            if(result==''){
                $(element).parent().parent().remove();
            }
        }
    });
}

function removeSelfCreated(id,userId,element) {
    console.log(id);
    console.log(userId);
    console.log(element);
    if (!confirm("are you sure?")) {
        return false;
    }

    var formData = new FormData();
    formData.append('id', id);
    formData.append('user_id', userId);

    $.ajax({
        url: '/self_material_remove',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            console.log(result);
            if(result==''){
                $(element).parent().parent().remove();
            }
        }
    });
}

$( document ).ready(function() {
    checkCompare();
});

$('.checkbox-for-compare').click(function () {
    // console.log($(this).data("id"));
    checkCompare();
});
function checkCompare() {
    if(($("#t01 .checkbox-for-compare:checked").length)>0){
        $('#send-to-compare').fadeIn();
    }else{
        $('#send-to-compare').fadeOut();
    }
}

$('#send-to-compare').click(function () {
    var arrChecked = $("#t01 .checkbox-for-compare");
    if($("#t01 .checkbox-for-compare:checked").length == 0) {
        alert("нет выбранных записей ");
        return false;
    }

    var formData = new FormData();
    for(let i = 0; i < arrChecked.length; i++){
        var stat = 0;
        if($(arrChecked[i]).prop('checked'))
        stat = 1;
        formData.append('id[' + $(arrChecked[i]).data("id") + ']', stat);
    }

    $.ajax({
        url: '/send_to_compare',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            console.log(result);
            if(result.trim() == 'Запись сохранена'){
                window.location.replace('/comparison');
            }
        }
    });
});

$('.radio-for-compare').click(function () {
    $('#t01_ tr .button_td').remove();
    $(this).parent().parent().append( "<td class='button_td'><button type='submit' class='log_a_p com_button'>сохранить и распечатать файл-отчёт</button></td>" );
});

$('.com_button').click(function () {
    // var id = $(this).parent().parent().find('.radio-for-compare').data("id");
    // $('#id_compare').val(id);
    // var formData = new FormData;
    // formData.append('id', id);
    // $.ajax({
    //     url: '/downloadPdfFile',
    //     type: 'POST',
    //     data: formData,
    //     cache: false,
    //     processData: false,
    //     contentType: false,
    //     beforeSend: function () {
    //     },
    //     success: function (result) {
    //         $('#compare_form input[type=submit]').click();
    //         console.log(result);
    //
    //     }
    // });

});
