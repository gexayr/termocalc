$("#calculate").click(function () {

    if ($('.alert-danger-city').length != 0) {
        alert('Ошибка! Вы не выбрали город')
        return false
    }

sizes = $('#ol').find('input[type=text]')

for(var i=0;i<sizes.length;i++){
    if($(sizes[i]).val() == '' || $(sizes[i]).val() == 0){
        alert('Размеры или площадь отсутствуют')
        return false;
    }
}

    var tab_num = $('#tab_num').val();
    // SAVING DATA
    var formData_data = new FormData();
    $('#for-estimate').show();

    // END SAVING DATA
    $('#for_graph_section').html('');

// =================
    var bearing = +$('.table_block_2').children("div").attr("data-tab");
    // alert (bearing)

    $('#calk-tbody_1').html("");
    $('#calk-tbody_1').show();

    $('#calk-tbody_2').html("");
    $('#calk-tbody_2').show();

    $('#calk-tbody_3').html("");
    $('#calk-tbody_3').show();

    $('#calk-tbody_4').html("");
    $('#calk-tbody_4').show();

    $('#calk-tbody_5').html("");
    $('#calk-tbody_5').show();

    $('#calk-tbody_6').html("");
    $('#calk-tbody_6').show();

    $('#calk-tbody_7').html("");
    $('#calk-tbody_7').show();

    $('#calk-tbody_8').html("");
    $('#calk-tbody_8').show();

    var tables = $('#dataTbody');
    var table = tables.find('.table');
    var l = table.length; // total items

    var td_CTM38 = 0;
    var sum_td_CTM38 = 0;
    var sum_td_CTM36 = 0;
    var sum_td_CTM37 = 0;
// =========================
    var bearing_elements = 'one';
    var termal_mat = false;
    var mineral = false;
    var mat_category = false;
    var col19 = false;
    var membrana = false;
    var panel = false;
    var before_bearing = false;
    var after_bearing = false;
    var cond_aerial = false;
    var cond_foil = false;
    var cond_spec = false;
    var cond_Ventilated = false;
    var iterator = true;

    if(tab_num == 3) {

        var vapor = false;
        var roof_cond = false;

        var termal_mat_first = false;
        var termal_mat_iterator = false;

    }

    if ($('.table_block_2').find("table").hasClass('table_first')) {
        var bearing_elements = 'two';
    }
    for (var i = 0; i < l; i++) {
        var element = table[i];
        var cond_term = $(element).find('.COL18').text();
        var cond_mat = $(element).find('.COL4').text();
        var cond_mineral = $(element).find('.COL3').text();
        var cond_membrana = $(element).find('.COL4').text();
        var cond_panel = $(element).find('.COL4').text();
        var material = $(element).find('.name').text();
        var mat_category = $(element).find('.category').text();
        var col19 = $(element).find('.COL19').text();

        if (cond_mat.trim() === 'Замкнутая воздушная прослойка') {
            cond_aerial = i;
        }
        if (cond_mineral.trim() === 'Минеральные') {
            mineral = i;
        }
        if (cond_membrana.trim() === 'Ветрозащитная мембрана') {
            membrana = i;
        }
        if (cond_panel.trim() === 'Ветрозащитная плита') {
            panel = i;
        }

        if (material.trim() === 'Алюминиевая фольга') {
            cond_foil = i;
        }
        // if (cond_term.trim() == 'T') {
        if (cond_term.trim() == 'Т') {
            termal_mat = i;

            if(tab_num == 3) {

                if (termal_mat_iterator == false) {
                    termal_mat_first = i;
                    termal_mat_iterator = true;
                }
            }
        }
        if (cond_aerial != false && cond_foil != false) {
            var cond_difference = ((cond_aerial) * 1) - ((cond_foil) * 1);
            if (cond_difference == 1 || cond_difference == -1) {
                cond_spec = i;
            }
        }

        if (material.trim() === 'Вентилируемая воздушная прослойка') {
            cond_Ventilated = i;
            if (i + 1 < bearing) {
                before_bearing = i;
            } else if (iterator == true) {
                after_bearing = i;
                if (bearing_elements == 'two') {
                    after_bearing = i - 1;
                }
                iterator = false;
            }
        }


        // if there are blocks or filling
        var bearing_array = new Array('two');




        // for roof

        if(tab_num == 3){
            // if (material.trim() === 'Пароизоляционная плёнка') {
            //     vapor = i;
            // }

            if(vapor == false){
                // if (mat_category.trim() === 'Пароизолирующие и пароограничивающие материалы, мембраны, окраски'
                //     || material.trim() == 'Алюминиевая фольга'
                //     || material.trim() == 'Алюминий'
                //     || material.trim() == 'Пенополиэтилен, плотность 30 кг/куб.м.'
                //     || material.trim() == 'Пенополиэтилен, плотность 26 кг/куб.м.'
                // ) {
                //     vapor = i;
                //     // alert(i);
                // }
                if (col19.trim() === 'П') {
                    vapor = i;
                }
            }

        }

        // end for roof



        var thickness = $('#ol').find('.li_' + i).find('.thickness').val();
        if (thickness == '') {
            alert('Размеры отсутствуют ')
            return false
        }
        $('#waiting').css('display', 'block');


        // SAVING DATA

        var thickness = $('#ol').find('.li_' + i).find('.thickness').val();

        if ($(element).hasClass('table_first')) {
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][type]', ($(element).children().attr('class')));
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][val][]', ($(element).find('.id').text()).trim());
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][thickness]', $('#ol').find('.li_' + $(element).parent().parent().attr("data-tab")).find('.thickness').val());
        }else if($(element).hasClass('table_second')){
            var data_in = $('#ol').find('.li_' + $(element).parent().parent().attr("data-tab")).find('.data-input-block');
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+($(element).parent().parent().attr("data-tab"))+'][val][]', ($(element).find('.id').text()).trim());
            // formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']'+$(element).parent().parent().attr("data-tab")+'][params]', "");

            if ($(data_in).hasClass('frame-input')) {
                formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][params][]', +(data_in.find('.frame-between').val()));
                formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][params][]', +(data_in.find('.frame-width').val()));
            }else if ($(data_in).hasClass('block-input')) {
                formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][params][]', +(data_in.find('.block-w').val()));
                formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][params][]', +(data_in.find('.block-h').val()));
                formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][params][]', +(data_in.find('.block-seam').val()));
            }

        }else{
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][type]', ($(element).children().attr('class')));
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][val]', ($(element).find('.id').text()).trim());
            formData_data.append('layer['+($(element).parent().parent().parent().attr("class"))+']['+$(element).parent().parent().attr("data-tab")+'][thickness]', $('#ol').find('.li_' + $(element).parent().parent().attr("data-tab")).find('.thickness').val());
        }

        // END SAVING DATA



    }

    // ================================================================
    // =========================== FOR GRAPH ==========================
    // ================================================================


    var formData_for_graph = new FormData();

    var tables = $('#dataTbody');
    var table = tables.find('.table');
    var l_ = table.length; // total items


    var var_cont = false;
    var image_condition = false;
    var arr_block = $('#dataTbody').find('.table_first');
    if(arr_block.length != 0) {
        for (var j_ = 0; j_ < arr_block.length; j_++) {

            var hide_element = $(arr_block[j_]).parent().parent().attr("data-tab");
            formData_for_graph.append('hide_element[]', hide_element);

        }
        var_cont = true;
    }
    // ------------------------------convas----------------------------------------
    for (var i = 0; i < l_; i++) {
        var image = $.trim($(table[i]).find('.COL39').text());
        if(image == ''){
            image = '042.jpg';
        }
        if(i == hide_element){
            image_condition = true;
        }
        formData_for_graph.append('image['+i+']', image);

        // ======================
        // ======================

        if (arr_block.length != 0) {
            if (arr_block.length == 1 && i + 1 == l_) {
                var_cont = true;
            }
            if (arr_block.length == 2 && i + 2 == l_) {
                var_cont = true;
            }
            if (arr_block.length == 3 && i + 3 == l_) {
                var_cont = true;
            }
        }
        // if (var_cont == true) continue;

        // ======================
        // ======================
        var i_ = i;

        var img = $('<img>');

    }

    $.ajax({
        url: '/for_graph',
        type: 'POST',
        data: formData_for_graph,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
            var res = JSON.parse(result)
            for(var i=0;i<res.length;i++){
                var img = $('<img>');
                img.attr('src', res[i]);
                img.attr('id', 'texture_' + i);
                $('#for_graph_section').append(img);
            }
        }
    });

// ================================================================
// =========================== END GRAPH ==========================
// ================================================================


    $('.core-data').css("display", "block");


    function forVapor(option){

        var formData_data_cookie = new FormData();
        formData_data_cookie.append('cookie_name', 'alert');
        formData_data_cookie.append('option', option);
        $.ajax({
            url: '/writeCookie',
            type: 'POST',
            data: formData_data_cookie,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
            }
        });

    }

    // end for roof

    // ========================================
    // SAVING DATA
    $("#change_coefficient").val($("#selectCoefficient").val());

    formData_data.append('selectCoefficient', $("#selectCoefficient option:selected" ).val());
    formData_data.append('volume', $("#all_wall_volume" ).val());
    if(tab_num == 3) {
        formData_data.append('roof_type', $("#roof_type option:selected" ).val());
        formData_data.append('volume', $("#roof_square" ).val());

        if (mineral != false) {
            if (($("#selectCoefficient option:selected").text()).trim() == '1. Скатная кровля') {
                $('#change_coefficient').val(0.9);
            }
        }
    }else if(tab_num == 4){
        if($("#selectCoefficient").val() == 0.9){
            $('.floor_pmu').fadeOut();
        }else{
            $('.floor_pmu').fadeIn();
        }
        formData_data.append('volume', $("#floor_square" ).val());
        if (mineral != false) {
            if ($("#selectCoefficient").val() == 0.98) {
                $('#change_coefficient').val(0.92);
                // $("#selectCoefficient").val(0.92)
            }
        }
    }


    // END SAVING DATA

// ========================================


// =========================
    $('.schedule_block').fadeIn();
    // var tab_num = $('#tab_num').val();
    var TD108 = 1;
    var TD109 = 1;


    if (tab_num == 2) {

        if (termal_mat != false && termal_mat > bearing) {
            TD108 = 0.95;
        } else {
            TD108 = 0.98;
        }
        if (mineral != false) {
            var thick = +($('#ol').find('.li_' + mineral).find('.thickness').val());
            if (($("#selectCoefficient option:selected").text()).trim() == '3. Стена с навесным фасадом') {
                $('#change_coefficient').val(0.65);
            } else if (($('#selectCoefficient option:selected').text()).trim() == '5. Стена с "мокрым фасадом" (СФТК)') {
                $('#change_coefficient').val(0.85);
            }

            if (mineral >= bearing) {
                if ((mineral + 1 == l) || mineral + 1 == after_bearing) {
                    if (thick <= 50) {
                        TD109 = 0.7;
                    } else if (51 <= thick <= 100) {
                        TD109 = 0.75;
                    } else if (101 <= thick <= 150) {
                        TD109 = 0.78;
                    } else if (150 < thick) {
                        TD109 = 0.8;
                    }
                } else if (mineral + 1 == membrana) {
                    if (thick <= 50) {
                        TD109 = 0.8;
                    } else if (51 <= thick <= 100) {
                        TD109 = 0.82;
                    } else if (101 <= thick) {
                        TD109 = 0.85;
                    }
                } else if (mineral + 1 == panel) {
                    TD109 = 0.98;
                }
            }
        }

    }else if(tab_num == 3){
        if (mineral != false && after_bearing != false) {
            if (mineral >= bearing) {
                if (bearing_elements == 'two') {
                    if(mineral == after_bearing){
                        TD109 = 0.9;
                    }
                }else{
                    if((mineral + 1) == after_bearing){
                        TD109 = 0.9;
                    }
                }
                if(mineral + 1 == membrana) {
                    TD109 = 0.95;
                }
            }else{
                if (bearing_elements == 'two') {
                    if((bearing - mineral) == 1){
                        if ((mineral + 1) == after_bearing) {
                            TD109 = 0.9;
                        }
                        if(mineral + 2 == membrana) {
                            TD109 = 0.95;
                        }
                    }
                }
            }
        }
    }else if(tab_num == 4){
        if ($("#selectCoefficient").val() == 0.98 || $("#selectCoefficient").val() == 0.92 || $('#change_coefficient').val(0.92)) {
            if (mineral != false && after_bearing != false) {
                if (mineral >= bearing) {
                    if (bearing_elements == 'two') {
                        if (mineral == after_bearing) {
                            // alert('d');
                            TD109 = 0.9;
                        }
                    } else {
                        if ((mineral + 1) == after_bearing) {
                            TD109 = 0.9;
                        }
                    }
                }
            }

            if (mineral != false && after_bearing != false) {
                if (mineral >= bearing) {
                    if (bearing_elements == 'two') {
                        if(mineral == after_bearing){
                            TD109 = 0.9;
                        }
                    }else{
                        if((mineral + 1) == after_bearing){
                            TD109 = 0.9;
                        }
                    }
                    if(mineral + 1 == membrana) {
                        TD109 = 0.95;
                    }
                }else{
                    if (bearing_elements == 'two') {
                        if((bearing - mineral) == 1){
                            if ((mineral + 1) == after_bearing) {
                                TD109 = 0.9;
                            }
                            if(mineral + 2 == membrana) {
                                TD109 = 0.95;
                            }
                        }
                    }
                }
            }


        }

    }
    // -------------------------------------------------------
    var formData = new FormData();
    // -------------------------------------------------------
    var formData_estimate = new FormData();

    var term = 0;
    for (var i = 0; i < l; i++) {
        var j = (i + 1);
        var element = table[i];
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];
        formData.append('numb[]', numb);

        var thickness = $('#ol').find('.li_' + numb).find('.thickness').val();
        formData_estimate.append('estimate[size][' + numb + ']', (thickness * 0.001));

        var COL4 = $(table[i]).find('.COL4').text();
        var termal = $(element).find('.COL18').text();
        var material = $(element).find('.name').text();
        if (termal.trim() == 'T') {
            term = i + 1;
        }

        var td_name = $(element).find('.name').text();
        var td_CTM02 = $(element).find('.COL6').text();
        var td_CTM03 = $(element).find('.COL7').text();
        var td_CTM04 = $(element).find('.COL8').text();
        var td_CTM05 = $(element).find('.COL9').text();
        var td_CTM06 = $(element).find('.COL10').text();
        var td_CTM07 = $(element).find('.COL11').text();
        var td_CTM08 = $(element).find('.COL12').text();
        var td_CTM09 = $(element).find('.COL13').text();
        var td_CTM10 = $(element).find('.COL14').text();
        var td_CTM11 = $(element).find('.COL15').text();
        var td_CTM12 = $(element).find('.COL16').text();
        var td_CTM13 = $(element).find('.COL17').text();
        var td_CTM14 = $(element).find('.COL18').text();
        var td_CTM16 = $(element).find('.COL20').text();
        var td_CTM17 = $(element).find('.COL21').text();
        var td_CTM18 = $(element).find('.COL22').text();
        var td_CTM19 = thickness;
        var td_CTM20 = thickness * 0.001;
        var td_CTM21 = $(element).find('.COL25').text();
        var td_CTM22 = $(element).find('.COL26').text();
        var td_CTM23 = $(element).find('.COL27').text();
        var td_CTM24 = $(element).find('.COL28').text();
        var td_CTM25 = '';
        var td_CTM26 = '';
        var td_CTM27 = '';
        var td_CTM28 = '';
        var td_CTM29 = '';


        if ($(element).hasClass('table_first')) {

            formData.append('table_first[]', i);

            i++;
            td_name = td_name + ' + ' + $(table[i]).find('.name').text();

            var data_input = $('#ol').find('.li_' + numb).find('.data-input-block');


            if ($(data_input).hasClass('block-input')) {
                formData.append('block_input[]', i);

                td_CTM27 = +(data_input.find('.block-w').val());
                td_CTM28 = +(data_input.find('.block-h').val());
                td_CTM29 = +(data_input.find('.block-seam').val());

// ================================

                var next_td_CTM02 = $(table[i]).find('.COL6').text();
                var next_td_CTM03 = $(table[i]).find('.COL7').text();
                var next_td_CTM04 = $(table[i]).find('.COL8').text();
                var next_td_CTM12 = $(table[i]).find('.COL16').text();
                var next_td_CTM05 = +$(table[i]).find('.COL9').text();
                var next_td_CTM06 = +$(table[i]).find('.COL10').text();
                var next_td_CTM07 = +$(table[i]).find('.COL11').text();
                var next_td_CTM08 = +$(table[i]).find('.COL12').text();
                var next_td_CTM09 = +$(table[i]).find('.COL13').text();
                var next_td_CTM10 = +$(table[i]).find('.COL14').text();
                var next_td_CTM11 = +$(table[i]).find('.COL15').text();
                var next_td_CTM12 = +$(table[i]).find('.COL16').text();

                var new_td_CTM02_1 = (td_CTM27 * td_CTM28);
                var new_td_CTM02_2 = ((td_CTM27 + td_CTM28) * td_CTM29);
                var new_td_CTM02_3 = (new_td_CTM02_1 / td_CTM02);
                var new_td_CTM02_4 = (new_td_CTM02_2 / next_td_CTM02);
                var new_td_CTM02 = (new_td_CTM02_1 + new_td_CTM02_2) / (new_td_CTM02_3 + new_td_CTM02_4);

                var new_td_CTM03 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM03) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM03);
                var new_td_CTM04 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM04) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM04);
                var new_td_CTM05 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM05) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM05);
                var new_td_CTM06 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM06) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM06);
                var new_td_CTM07 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM07) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM07);
                var new_td_CTM08 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM08) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM08);
                var new_td_CTM09 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM09) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM09);
                var new_td_CTM10 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM10) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM10);
                var new_td_CTM11 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM11) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM11);
                var new_td_CTM12 = ((td_CTM27 * td_CTM28) + ((td_CTM27 + td_CTM28) * td_CTM29)) / (((td_CTM27 * td_CTM28) / td_CTM12) + ((td_CTM27 + td_CTM28) * td_CTM29) / next_td_CTM12);


                td_CTM02 = new_td_CTM02;
                td_CTM03 = new_td_CTM03;
                td_CTM04 = new_td_CTM04;
                td_CTM05 = new_td_CTM05;
                td_CTM06 = new_td_CTM06;
                td_CTM07 = new_td_CTM07;
                td_CTM08 = new_td_CTM08;
                td_CTM09 = new_td_CTM09;
                td_CTM10 = new_td_CTM10;
                td_CTM11 = new_td_CTM11;
                td_CTM12 = new_td_CTM12;
// ================================

            }
            if ($(data_input).hasClass('frame-input')) {
                formData.append('frame_input[]', i);

                td_CTM25 = +(data_input.find('.frame-between').val());
                td_CTM26 = +(data_input.find('.frame-width').val());

                formData_estimate.append('estimate[frame-size][frame-between][' + numb + ']', td_CTM25);
                formData_estimate.append('estimate[frame-size][frame-width][' + numb + ']', td_CTM26);
                var next_td_CTM02 = $(table[i]).find('.COL6').text();
                var next_td_CTM03 = $(table[i]).find('.COL7').text();
                var next_td_CTM04 = $(table[i]).find('.COL8').text();

                var next_td_CTM05 = +($(table[i]).find('.COL9').text());
                var next_td_CTM06 = +($(table[i]).find('.COL10').text());
                var next_td_CTM07 = +($(table[i]).find('.COL11').text());
                var next_td_CTM08 = +($(table[i]).find('.COL12').text());
                var next_td_CTM09 = +($(table[i]).find('.COL13').text());
                var next_td_CTM10 = +($(table[i]).find('.COL14').text());
                var next_td_CTM11 = +($(table[i]).find('.COL15').text());
                var next_td_CTM12 = +($(table[i]).find('.COL16').text());
                var next_td_CTM13 = +($(table[i]).find('.COL17').text());

                var new_td_CTM02 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM02) + (td_CTM26 / td_CTM02));
                var new_td_CTM03 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM03) + (td_CTM26 / td_CTM03));
                var new_td_CTM04 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM04) + (td_CTM26 / td_CTM04));
                var new_td_CTM05 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM05) + (td_CTM26 / td_CTM05));
                var new_td_CTM06 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM06) + (td_CTM26 / td_CTM06));
                var new_td_CTM07 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM07) + (td_CTM26 / td_CTM07));
                var new_td_CTM08 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM08) + (td_CTM26 / td_CTM08));
                var new_td_CTM09 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM09) + (td_CTM26 / td_CTM09));
                var new_td_CTM10 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM10) + (td_CTM26 / td_CTM10));
                var new_td_CTM11 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM11) + (td_CTM26 / td_CTM11));
                var new_td_CTM12 = (td_CTM25 + td_CTM26) / ((td_CTM25 / next_td_CTM12) + (td_CTM26 / td_CTM12));

                td_CTM02 = new_td_CTM02;
                td_CTM03 = new_td_CTM03;
                td_CTM04 = new_td_CTM04;
                td_CTM05 = new_td_CTM05;
                td_CTM06 = new_td_CTM06;
                td_CTM07 = new_td_CTM07;
                td_CTM08 = new_td_CTM08;
                td_CTM09 = new_td_CTM09;
                td_CTM10 = new_td_CTM10;
                td_CTM11 = new_td_CTM11;
                td_CTM12 = new_td_CTM12;
                td_CTM13 = next_td_CTM13;

            }


        }

// _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-
        formData.append('td_name[]', td_name);
        formData.append('td_CTM02[]', td_CTM02);
        formData.append('td_CTM03[]', td_CTM03);
        formData.append('td_CTM04[]', td_CTM04);
        formData.append('td_CTM05[]', td_CTM05);
        formData.append('td_CTM06[]', td_CTM06);
        formData.append('td_CTM07[]', td_CTM07);
        formData.append('td_CTM08[]', td_CTM08);
        formData.append('td_CTM09[]', td_CTM09);
        formData.append('td_CTM10[]', td_CTM10);
        formData.append('td_CTM11[]', td_CTM11);
        formData.append('td_CTM12[]', td_CTM12);
        formData.append('td_CTM13[]', td_CTM13);
        formData.append('td_CTM14[]', td_CTM14);
        formData.append('td_CTM16[]', td_CTM16);
        formData.append('td_CTM17[]', td_CTM17);
        formData.append('td_CTM18[]', td_CTM18);
        formData.append('td_CTM19[]', td_CTM19);
        formData.append('td_CTM20[]', td_CTM20);
        formData.append('td_CTM21[]', td_CTM21);
        formData.append('td_CTM22[]', td_CTM22);
        formData.append('td_CTM23[]', td_CTM23);
        formData.append('td_CTM24[]', td_CTM24);
        formData.append('td_CTM25[]', td_CTM25);
        formData.append('td_CTM26[]', td_CTM26);
        formData.append('td_CTM27[]', td_CTM27);
        formData.append('td_CTM28[]', td_CTM28);
        formData.append('td_CTM29[]', td_CTM29);
// _-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-


    }


    var AR49_1 = +($("#AllTbody").find(".td05").text());
    var AR49_2 = +($("#AllTbody").find(".td09").text());
    var AR49_3 = +($("#AllTbody").find(".td13").text());
    var AR49_4 = +($("#AllTbody").find(".td17").text());
    var AR49_5 = +($("#AllTbody").find(".td21").text());
    var AR49_6 = +($("#AllTbody").find(".td25").text());
    var AR49_7 = +($("#AllTbody").find(".td29").text());
    var AR49_8 = +($("#AllTbody").find(".td33").text());


    var AP40 = +($("#AllTbody").find('.td37').text());
    var AR58 = +($("#AllTbody").find('.td40').text());
    var AP33 = +($("#AllTbody").find(".td42").text());


    // $('#schedule_TD37_1').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_2').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_3').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_4').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_5').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_6').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_7').text(AR58 + "  температура на улице");
    // $('#schedule_TD37_8').text(AR58 + "  температура на улице");
    //
    // $('#schedule_TD05_1').text("температура помещения \n" + AR49_1 + "");
    // $('#schedule_TD05_2').text("температура помещения \n" + AR49_2 + "");
    // $('#schedule_TD05_3').text("температура помещения \n" + AR49_3 + "");
    // $('#schedule_TD05_4').text("температура помещения \n" + AR49_4 + "");
    // $('#schedule_TD05_5').text("температура помещения \n" + AR49_5 + "");
    // $('#schedule_TD05_6').text("температура помещения \n" + AR49_6 + "");
    // $('#schedule_TD05_7').text("температура помещения \n" + AR49_7 + "");
    // $('#schedule_TD05_8').text("температура помещения \n" + AR49_8 + "");

    $('#TD05_calc_1').val($("#AllTbody").find(".td05").text());
    $('#TD05_calc_2').val($("#AllTbody").find(".td09").text());
    $('#TD05_calc_3').val($("#AllTbody").find(".td13").text());
    $('#TD05_calc_4').val($("#AllTbody").find(".td17").text());
    $('#TD05_calc_5').val($("#AllTbody").find(".td21").text());
    $('#TD05_calc_6').val($("#AllTbody").find(".td25").text());
    $('#TD05_calc_7').val($("#AllTbody").find(".td29").text());
    $('#TD05_calc_8').val($("#AllTbody").find(".td33").text());

    $('#TD06_calc_1').val($("#AllTbody").find(".td06").text());
    $('#TD06_calc_2').val($("#AllTbody").find(".td10").text());
    $('#TD06_calc_3').val($("#AllTbody").find(".td14").text());
    $('#TD06_calc_4').val($("#AllTbody").find(".td18").text());
    $('#TD06_calc_5').val($("#AllTbody").find(".td22").text());
    $('#TD06_calc_6').val($("#AllTbody").find(".td26").text());
    $('#TD06_calc_7').val($("#AllTbody").find(".td30").text());
    $('#TD06_calc_8').val($("#AllTbody").find(".td34").text());

    $('#TD07_calc_1').val($("#AllTbody").find(".td07").text());
    $('#TD07_calc_2').val($("#AllTbody").find(".td11").text());
    $('#TD07_calc_3').val($("#AllTbody").find(".td15").text());
    $('#TD07_calc_4').val($("#AllTbody").find(".td19").text());
    $('#TD07_calc_5').val($("#AllTbody").find(".td23").text());
    $('#TD07_calc_6').val($("#AllTbody").find(".td27").text());
    $('#TD07_calc_7').val($("#AllTbody").find(".td31").text());
    $('#TD07_calc_8').val($("#AllTbody").find(".td35").text());

    $('#TD08_calc_1').val($("#AllTbody").find(".td08").text());
    $('#TD08_calc_2').val($("#AllTbody").find(".td12").text());
    $('#TD08_calc_3').val($("#AllTbody").find(".td16").text());
    $('#TD08_calc_4').val($("#AllTbody").find(".td20").text());
    $('#TD08_calc_5').val($("#AllTbody").find(".td24").text());
    $('#TD08_calc_6').val($("#AllTbody").find(".td28").text());
    $('#TD08_calc_7').val($("#AllTbody").find(".td32").text());
    $('#TD08_calc_8').val($("#AllTbody").find(".td36").text());

    $('#TD44_calc_1').val($("#AllTbody").find(".td44").text());
    $('#TD44_calc_2').val($("#AllTbody").find(".td72").text());
    $('#TD44_calc_3').val($("#AllTbody").find(".td73").text());
    $('#TD44_calc_4').val($("#AllTbody").find(".td74").text());
    $('#TD44_calc_5').val($("#AllTbody").find(".td75").text());
    $('#TD44_calc_6').val($("#AllTbody").find(".td76").text());
    $('#TD44_calc_7').val($("#AllTbody").find(".td77").text());
    $('#TD44_calc_8').val($("#AllTbody").find(".td78").text());


    if(tab_num == 4) {
        if($("#selectCoefficient").val() == 0.96) {
            var AR58_1 = (+($("#AllTbody").find('.td40').text()) + AR49_1)/5;
            var AR58_2 = (+($("#AllTbody").find('.td40').text()) + AR49_2)/5;
            var AR58_3 = (+($("#AllTbody").find('.td40').text()) + AR49_3)/5;
            var AR58_4 = (+($("#AllTbody").find('.td40').text()) + AR49_4)/5;
            var AR58_5 = (+($("#AllTbody").find('.td40').text()) + AR49_5)/5;
            var AR58_6 = (+($("#AllTbody").find('.td40').text()) + AR49_6)/5;
            var AR58_7 = (+($("#AllTbody").find('.td40').text()) + AR49_7)/5;
            var AR58_8 = (+($("#AllTbody").find('.td40').text()) + AR49_8)/5;

            $('#schedule_TD37_1').text(AR58_1 + "  температура на улице");
            $('#schedule_TD37_2').text(AR58_2 + "  температура на улице");
            $('#schedule_TD37_3').text(AR58_3 + "  температура на улице");
            $('#schedule_TD37_4').text(AR58_4 + "  температура на улице");
            $('#schedule_TD37_5').text(AR58_5 + "  температура на улице");
            $('#schedule_TD37_6').text(AR58_6 + "  температура на улице");
            $('#schedule_TD37_7').text(AR58_7 + "  температура на улице");
            $('#schedule_TD37_8').text(AR58_8 + "  температура на улице");

        }else if($("#selectCoefficient").val() == 0.97){
            var AR58_1 = (+($("#AllTbody").find('.td40').text()) + AR49_1)/7;
            var AR58_2 = (+($("#AllTbody").find('.td40').text()) + AR49_2)/7;
            var AR58_3 = (+($("#AllTbody").find('.td40').text()) + AR49_3)/7;
            var AR58_4 = (+($("#AllTbody").find('.td40').text()) + AR49_4)/7;
            var AR58_5 = (+($("#AllTbody").find('.td40').text()) + AR49_5)/7;
            var AR58_6 = (+($("#AllTbody").find('.td40').text()) + AR49_6)/7;
            var AR58_7 = (+($("#AllTbody").find('.td40').text()) + AR49_7)/7;
            var AR58_8 = (+($("#AllTbody").find('.td40').text()) + AR49_8)/7;
            $('#schedule_TD37_1').text(AR58_1 + "  температура на улице");
            $('#schedule_TD37_2').text(AR58_2 + "  температура на улице");
            $('#schedule_TD37_3').text(AR58_3 + "  температура на улице");
            $('#schedule_TD37_4').text(AR58_4 + "  температура на улице");
            $('#schedule_TD37_5').text(AR58_5 + "  температура на улице");
            $('#schedule_TD37_6').text(AR58_6 + "  температура на улице");
            $('#schedule_TD37_7').text(AR58_7 + "  температура на улице");
            $('#schedule_TD37_8').text(AR58_8 + "  температура на улице");
        }else if($("#selectCoefficient").val() == 0.9){
            var AR58_1 = 4;
            var AR58_2 = 4;
            var AR58_3 = 4;
            var AR58_4 = 4;
            var AR58_5 = 4;
            var AR58_6 = 4;
            var AR58_7 = 4;
            var AR58_8 = 4;
            $('#schedule_TD37_1').text(AR58_1 + "  температура на улице");
            $('#schedule_TD37_2').text(AR58_2 + "  температура на улице");
            $('#schedule_TD37_3').text(AR58_3 + "  температура на улице");
            $('#schedule_TD37_4').text(AR58_4 + "  температура на улице");
            $('#schedule_TD37_5').text(AR58_5 + "  температура на улице");
            $('#schedule_TD37_6').text(AR58_6 + "  температура на улице");
            $('#schedule_TD37_7').text(AR58_7 + "  температура на улице");
            $('#schedule_TD37_8').text(AR58_8 + "  температура на улице");
        }
    }







    formData.append('AP40', AP40);
    formData.append('AR58', AR58);
    formData.append('AP33', AP33);
    formData.append('condFoil', cond_foil);

    formData.append('AR49', AR49_1);
    formData.append('AR49_2', AR49_2);
    formData.append('AR49_3', AR49_3);
    formData.append('AR49_4', AR49_4);
    formData.append('AR49_5', AR49_5);
    formData.append('AR49_6', AR49_6);
    formData.append('AR49_7', AR49_7);
    formData.append('AR49_8', AR49_8);

    // add data in '#AllTD'
    var TD101 = (+$("#change_coefficient").val());

    var room_section_1 = $("#AllTbody .roomType_1");
    var room_section_2 = $("#AllTbody .roomType_2");
    var room_section_3 = $("#AllTbody .roomType_3");
    var room_section_4 = $("#AllTbody .roomType_4");
    var room_section_5 = $("#AllTbody .roomType_5");
    var room_section_6 = $("#AllTbody .roomType_6");
    var room_section_7 = $("#AllTbody .roomType_7");
    var room_section_8 = $("#AllTbody .roomType_8");



    var G216_1 = +((room_section_1).parent().next('tr').find('td:nth-child(2)').text());
    var G216_2 = +((room_section_2).parent().next('tr').find('td:nth-child(2)').text());
    var G216_3 = +((room_section_3).parent().next('tr').find('td:nth-child(2)').text());
    var G216_4 = +((room_section_4).parent().next('tr').find('td:nth-child(2)').text());
    var G216_5 = +((room_section_5).parent().next('tr').find('td:nth-child(2)').text());
    var G216_6 = +((room_section_6).parent().next('tr').find('td:nth-child(2)').text());
    var G216_7 = +((room_section_7).parent().next('tr').find('td:nth-child(2)').text());
    var G216_8 = +((room_section_8).parent().next('tr').find('td:nth-child(2)').text());

    var TD102 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_1))));
    var TD102_2 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_2))));
    var TD102_3 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_3))));
    var TD102_4 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_4))));
    var TD102_5 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_5))));
    var TD102_6 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_6))));
    var TD102_7 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_7))));
    var TD102_8 = (1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + G216_8))));

    // var TD103 = 1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + AP40)));
    var TD103 = 1.84 * (Math.pow(10, 11)) * (Math.exp(-5330 / (273 + AR58)));
    var G228_1 = +((room_section_1).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_2 = +((room_section_2).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_3 = +((room_section_3).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_4 = +((room_section_4).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_5 = +((room_section_5).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_6 = +((room_section_6).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_7 = +((room_section_7).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var G228_8 = +((room_section_8).parent().next('tr').next('tr').find('td:nth-child(2)').text());
    var TD104 = (G228_1 / 100) * TD102;
    var TD104_2 = (G228_2 / 100) * TD102_2;
    var TD104_3 = (G228_3 / 100) * TD102_3;
    var TD104_4 = (G228_4 / 100) * TD102_4;
    var TD104_5 = (G228_5 / 100) * TD102_5;
    var TD104_6 = (G228_6 / 100) * TD102_6;
    var TD104_7 = (G228_7 / 100) * TD102_7;
    var TD104_8 = (G228_8 / 100) * TD102_8;


    formData.append('F230', TD104);
    formData.append('F230_2', TD104_2);
    formData.append('F230_3', TD104_3);
    formData.append('F230_4', TD104_4);
    formData.append('F230_5', TD104_5);
    formData.append('F230_6', TD104_6);
    formData.append('F230_7', TD104_7);
    formData.append('F230_8', TD104_8);
    formData.append('cond_Ventilated', cond_Ventilated);
    formData.append('before_bearing', before_bearing);
    formData.append('after_bearing', after_bearing);
    formData.append('bearing', bearing);
    formData.append('cond_aerial', cond_aerial);

    formData.append('tab_num', tab_num);
    // formData.append('selectCoefficient', +$("#change_coefficient").val());
    formData.append('selectCoefficient', $("#selectCoefficient").val());

if(tab_num == 3){
    formData.append('roof_type', +$('#roof_type').val());
}
// +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
// +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_

        $.ajax({
            url: '/calculate',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {
//                    $('#waiting').css('display', 'block');
            },
            success: function (result) {

                if(result == '""'){
                    $('#waiting').css('display', 'none');
                    alert('во вкладе начальных данных не заполнили поля')
                    return false;
                }
                var res = JSON.parse(result);

                $('#calk-tbody_1').append(res.living);
                $('#calk-tbody_2').append(res.kitchen);
                $('#calk-tbody_3').append(res.toilet);
                $('#calk-tbody_4').append(res.bathroom);
                $('#calk-tbody_5').append(res.rest_room);
                $('#calk-tbody_6').append(res.hall);
                $('#calk-tbody_7').append(res.vestibule);
                $('#calk-tbody_8').append(res.pantry);

                var TD100 = +($("#calk-tbody_1").find(".tr_last .td_CTM37").text());
                var TD105 = +($("#calk-tbody_1").find(".tr_last .td_CTM36").text());

                var TD100_2 = +($("#calk-tbody_2").find(".tr_last .td_CTM37").text());
                var TD105_2 = +($("#calk-tbody_2").find(".tr_last .td_CTM36").text());
                var TD100_3 = +($("#calk-tbody_3").find(".tr_last .td_CTM37").text());
                var TD105_3 = +($("#calk-tbody_3").find(".tr_last .td_CTM36").text());
                var TD100_4 = +($("#calk-tbody_4").find(".tr_last .td_CTM37").text());
                var TD105_4 = +($("#calk-tbody_4").find(".tr_last .td_CTM36").text());
                var TD100_5 = +($("#calk-tbody_5").find(".tr_last .td_CTM37").text());
                var TD105_5 = +($("#calk-tbody_5").find(".tr_last .td_CTM36").text());
                var TD100_6 = +($("#calk-tbody_6").find(".tr_last .td_CTM37").text());
                var TD105_6 = +($("#calk-tbody_6").find(".tr_last .td_CTM36").text());
                var TD100_7 = +($("#calk-tbody_7").find(".tr_last .td_CTM37").text());
                var TD105_7 = +($("#calk-tbody_7").find(".tr_last .td_CTM36").text());
                var TD100_8 = +($("#calk-tbody_8").find(".tr_last .td_CTM37").text());
                var TD105_8 = +($("#calk-tbody_8").find(".tr_last .td_CTM36").text());

                $('#AllTD_1').html('');
                $('#AllTD_2').html('');
                $('#AllTD_3').html('');
                $('#AllTD_4').html('');
                $('#AllTD_5').html('');
                $('#AllTD_6').html('');
                $('#AllTD_7').html('');
                $('#AllTD_8').html('');

                var td_html = '' +
                    '<tr><td>TD100</td><td>R<span class="small">o</span><span class="small" style="position: relative; top: -7px;">усл</span> - условное теплоспротивл. Констр (общее)</td><td class="TD100">' + TD100 + '</td></tr>\n' +
                    '    <tr><td>TD101</td><td>Выбранный коэффициент конструкции</td><td class="TD101">' + TD101 + '</td></tr>\n' +
                    '    <tr><td>TD102</td><td>Е<span class="small">в</span> - парциальное давление (внутри)</td><td class="TD102">' + TD102 + '</td></tr>\n' +
                    '    <tr><td>TD103</td><td>Е<span class="small">н отр</span> - парц. давл. наруж. при отриц. периоде</td><td class="TD103">' + TD103 + '</td></tr>\n' +
                    '    <tr><td>TD104</td><td>e<span class="small">в</span> - значение парциального давления е внутри</td><td class="TD104">' + TD104 + '</td></tr>\n' +
                    '    <tr><td>TD105</td><td>R <span class="small">оп</span> - общее паросопротивление конструкции</td><td class="TD105">' + TD105 + '</td></tr>\n' +
                    '    <tr><td>TD108</td><td>Обязательно устанавливаемый коэффициент:</td><td class="TD108">' + TD108 + '</td></tr>\n' +
                    '    <tr><td>TD109</td><td>"Продуваемая минвата" коэффициент:</td><td class="TD109">' + TD109 + '</td></tr>\n' +
                    '';

                $('#AllTD_1').html(td_html);
                $('#AllTD_2').html(td_html);
                $('#AllTD_3').html(td_html);
                $('#AllTD_4').html(td_html);
                $('#AllTD_5').html(td_html);
                $('#AllTD_6').html(td_html);
                $('#AllTD_7').html(td_html);
                $('#AllTD_8').html(td_html);

                $('#AllTD_2 .TD100').text(TD100_2);
                $('#AllTD_3 .TD100').text(TD100_3);
                $('#AllTD_4 .TD100').text(TD100_4);
                $('#AllTD_5 .TD100').text(TD100_5);
                $('#AllTD_6 .TD100').text(TD100_6);
                $('#AllTD_7 .TD100').text(TD100_7);
                $('#AllTD_8 .TD100').text(TD100_8);

                $('#AllTD_2 .TD102').text(TD102_2);
                $('#AllTD_3 .TD102').text(TD102_3);
                $('#AllTD_4 .TD102').text(TD102_4);
                $('#AllTD_5 .TD102').text(TD102_5);
                $('#AllTD_6 .TD102').text(TD102_6);
                $('#AllTD_7 .TD102').text(TD102_7);
                $('#AllTD_8 .TD102').text(TD102_8);

                $('#AllTD_2 .TD104').text(TD104_2);
                $('#AllTD_3 .TD104').text(TD104_3);
                $('#AllTD_4 .TD104').text(TD104_4);
                $('#AllTD_5 .TD104').text(TD104_5);
                $('#AllTD_6 .TD104').text(TD104_6);
                $('#AllTD_7 .TD104').text(TD104_7);
                $('#AllTD_8 .TD104').text(TD104_8);

                $('#AllTD_2 .TD105').text(TD105_2);
                $('#AllTD_3 .TD105').text(TD105_3);
                $('#AllTD_4 .TD105').text(TD105_4);
                $('#AllTD_5 .TD105').text(TD105_5);
                $('#AllTD_6 .TD105').text(TD105_6);
                $('#AllTD_7 .TD105').text(TD105_7);
                $('#AllTD_8 .TD105').text(TD105_8);

                call_another(TD108, TD109);
            }
        });
// +_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_+_
    function call_another(TD108, TD109) {

            $('#notice_block_1').text('');
            $('#notice_block_2').text('');
            $('#notice_block_3').text('');
            $('#notice_block_4').text('');
            $('#notice_block_5').text('');
            $('#notice_block_6').text('');
            $('#notice_block_7').text('');
            $('#notice_block_8').text('');

        // 3-tab


        // var TD102 = E220;
        // var TD103 = E221;
        // var TD104 = F230;


        var sum_td_CTM36 = 0;
        var isset = 0;
        var expressed_layer = false;
        var general_calculation = false;

        // ==========================================
        var formDataPMU = new FormData();
        // formDataPMU.append('td100', TD100);
        for (var i = 1; i <= numb; i++) {

            var element_tr_1 = $('#calk-tbody_1 .tr_' + i);
            var element_tr_2 = $('#calk-tbody_2 .tr_' + i);
            var element_tr_3 = $('#calk-tbody_3 .tr_' + i);
            var element_tr_4 = $('#calk-tbody_4 .tr_' + i);
            var element_tr_5 = $('#calk-tbody_5 .tr_' + i);
            var element_tr_6 = $('#calk-tbody_6 .tr_' + i);
            var element_tr_7 = $('#calk-tbody_7 .tr_' + i);
            var element_tr_8 = $('#calk-tbody_8 .tr_' + i);

            // var val_td_CTM07 = $(element_tr_1).find('.td_CTM07').text();
            // var val_td_CTM11 = +($(element_tr_1).find('.td_CTM11').text());
            // var val_td_CTM14 = ($(element_tr_1).find('.td_CTM14').text());

            if(element_tr_1.length != 0){
                var val_td_CTM07 = $(element_tr_1).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_1).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_1).find('.td_CTM14').text());
            }else if(element_tr_2.length != 0){
                var val_td_CTM07 = $(element_tr_2).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_2).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_2).find('.td_CTM14').text());
            }else if(element_tr_3.length != 0){
                var val_td_CTM07 = $(element_tr_3).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_3).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_3).find('.td_CTM14').text());
            }else if(element_tr_4.length != 0){
                var val_td_CTM07 = $(element_tr_4).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_4).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_4).find('.td_CTM14').text());
            }else if(element_tr_5.length != 0){
                var val_td_CTM07 = $(element_tr_5).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_5).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_5).find('.td_CTM14').text());
            }else if(element_tr_6.length != 0){
                var val_td_CTM07 = $(element_tr_6).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_6).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_6).find('.td_CTM14').text());
            }else if(element_tr_7.length != 0){
                var val_td_CTM07 = $(element_tr_7).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_7).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_7).find('.td_CTM14').text());
            }else if(element_tr_8.length != 0){
                var val_td_CTM07 = $(element_tr_8).find('.td_CTM07').text();
                var val_td_CTM11 = +($(element_tr_8).find('.td_CTM11').text());
                var val_td_CTM14 = ($(element_tr_8).find('.td_CTM14').text());
            }

            var val_td_CTM36_1 = +($(element_tr_1).find('.td_CTM36').text());
            var val_td_CTM37_1 = +($(element_tr_1).find('.td_CTM37').text());
            var val_td_CTM40_1 = +($(element_tr_1).find('.td_CTM40').text());
            var val_td_CTM42_1 = +($(element_tr_1).find('.td_CTM42').text());

            var val_td_CTM36_2 = +($(element_tr_2).find('.td_CTM36').text());
            var val_td_CTM37_2 = +($(element_tr_2).find('.td_CTM37').text());
            var val_td_CTM40_2 = +($(element_tr_2).find('.td_CTM40').text());
            var val_td_CTM42_2 = +($(element_tr_2).find('.td_CTM42').text());

            var val_td_CTM36_3 = +($(element_tr_3).find('.td_CTM36').text());
            var val_td_CTM37_3 = +($(element_tr_3).find('.td_CTM37').text());
            var val_td_CTM40_3 = +($(element_tr_3).find('.td_CTM40').text());
            var val_td_CTM42_3 = +($(element_tr_3).find('.td_CTM42').text());

            var val_td_CTM36_4 = +($(element_tr_4).find('.td_CTM36').text());
            var val_td_CTM37_4 = +($(element_tr_4).find('.td_CTM37').text());
            var val_td_CTM40_4 = +($(element_tr_4).find('.td_CTM40').text());
            var val_td_CTM42_4 = +($(element_tr_4).find('.td_CTM42').text());

            var val_td_CTM36_5 = +($(element_tr_5).find('.td_CTM36').text());
            var val_td_CTM37_5 = +($(element_tr_5).find('.td_CTM37').text());
            var val_td_CTM40_5 = +($(element_tr_5).find('.td_CTM40').text());
            var val_td_CTM42_5 = +($(element_tr_5).find('.td_CTM42').text());

            var val_td_CTM36_6 = +($(element_tr_6).find('.td_CTM36').text());
            var val_td_CTM37_6 = +($(element_tr_6).find('.td_CTM37').text());
            var val_td_CTM40_6 = +($(element_tr_6).find('.td_CTM40').text());
            var val_td_CTM42_6 = +($(element_tr_6).find('.td_CTM42').text());

            var val_td_CTM36_7 = +($(element_tr_7).find('.td_CTM36').text());
            var val_td_CTM37_7 = +($(element_tr_7).find('.td_CTM37').text());
            var val_td_CTM40_7 = +($(element_tr_7).find('.td_CTM40').text());
            var val_td_CTM42_7 = +($(element_tr_7).find('.td_CTM42').text());

            var val_td_CTM36_8 = +($(element_tr_8).find('.td_CTM36').text());
            var val_td_CTM37_8 = +($(element_tr_8).find('.td_CTM37').text());
            var val_td_CTM40_8 = +($(element_tr_8).find('.td_CTM40').text());
            var val_td_CTM42_8 = +($(element_tr_8).find('.td_CTM42').text());



            if($(element_tr_1).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_1 = ($(element_tr_1).find('.td_CTM42').text());
            }
            if($(element_tr_2).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_2 = ($(element_tr_2).find('.td_CTM42').text());
            }
            if($(element_tr_3).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_3 = ($(element_tr_3).find('.td_CTM42').text());
            }
            if($(element_tr_4).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_4 = ($(element_tr_4).find('.td_CTM42').text());
            }
            if($(element_tr_5).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_5 = ($(element_tr_5).find('.td_CTM42').text());
            }
            if($(element_tr_6).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_6 = ($(element_tr_6).find('.td_CTM42').text());
            }
            if($(element_tr_7).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_7 = ($(element_tr_7).find('.td_CTM42').text());
            }
            if($(element_tr_8).find('.td_CTM42').text().length <= 0) {
                val_td_CTM42_8 = ($(element_tr_8).find('.td_CTM42').text());
            }


            formDataPMU.append('val_td_CTM07['+i+']', val_td_CTM07);
            formDataPMU.append('val_td_CTM11['+i+']', val_td_CTM11);
            formDataPMU.append('val_td_CTM14['+i+']', val_td_CTM14);


            formDataPMU.append('val_td_CTM36[' + i + ']', val_td_CTM36_1);
            formDataPMU.append('val_td_CTM37[' + i + ']', val_td_CTM37_1);
            formDataPMU.append('val_td_CTM40[' + i + ']', val_td_CTM40_1);
            formDataPMU.append('val_td_CTM42[' + i + ']', val_td_CTM42_1);

            formDataPMU.append('val_td_CTM36_2[' + i + ']', val_td_CTM36_2);
            formDataPMU.append('val_td_CTM37_2[' + i + ']', val_td_CTM37_2);
            formDataPMU.append('val_td_CTM40_2[' + i + ']', val_td_CTM40_2);
            formDataPMU.append('val_td_CTM42_2[' + i + ']', val_td_CTM42_2);

            formDataPMU.append('val_td_CTM36_3[' + i + ']', val_td_CTM36_3);
            formDataPMU.append('val_td_CTM37_3[' + i + ']', val_td_CTM37_3);
            formDataPMU.append('val_td_CTM40_3[' + i + ']', val_td_CTM40_3);
            formDataPMU.append('val_td_CTM42_3[' + i + ']', val_td_CTM42_3);

            formDataPMU.append('val_td_CTM36_4[' + i + ']', val_td_CTM36_4);
            formDataPMU.append('val_td_CTM37_4[' + i + ']', val_td_CTM37_4);
            formDataPMU.append('val_td_CTM40_4[' + i + ']', val_td_CTM40_4);
            formDataPMU.append('val_td_CTM42_4[' + i + ']', val_td_CTM42_4);

            formDataPMU.append('val_td_CTM36_5[' + i + ']', val_td_CTM36_5);
            formDataPMU.append('val_td_CTM37_5[' + i + ']', val_td_CTM37_5);
            formDataPMU.append('val_td_CTM40_5[' + i + ']', val_td_CTM40_5);
            formDataPMU.append('val_td_CTM42_5[' + i + ']', val_td_CTM42_5);

            formDataPMU.append('val_td_CTM36_6[' + i + ']', val_td_CTM36_6);
            formDataPMU.append('val_td_CTM37_6[' + i + ']', val_td_CTM37_6);
            formDataPMU.append('val_td_CTM40_6[' + i + ']', val_td_CTM40_6);
            formDataPMU.append('val_td_CTM42_6[' + i + ']', val_td_CTM42_6);

            formDataPMU.append('val_td_CTM36_7[' + i + ']', val_td_CTM36_7);
            formDataPMU.append('val_td_CTM37_7[' + i + ']', val_td_CTM37_7);
            formDataPMU.append('val_td_CTM40_7[' + i + ']', val_td_CTM40_7);
            formDataPMU.append('val_td_CTM42_7[' + i + ']', val_td_CTM42_7);

            formDataPMU.append('val_td_CTM36_8[' + i + ']', val_td_CTM36_8);
            formDataPMU.append('val_td_CTM37_8[' + i + ']', val_td_CTM37_8);
            formDataPMU.append('val_td_CTM40_8[' + i + ']', val_td_CTM40_8);
            formDataPMU.append('val_td_CTM42_8[' + i + ']', val_td_CTM42_8);
        }

        formDataPMU.append('TD100', +($("#calk-tbody_1").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_2', +($("#calk-tbody_2").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_3', +($("#calk-tbody_3").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_4', +($("#calk-tbody_4").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_5', +($("#calk-tbody_5").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_6', +($("#calk-tbody_6").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_7', +($("#calk-tbody_7").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD100_8', +($("#calk-tbody_8").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('tab_num', tab_num);
        formDataPMU.append('selectCoefficient', $("#selectCoefficient").val());

        $.ajax({
                url: '/calcPMU',
                type: 'POST',
                data: formDataPMU,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    var res = JSON.parse(result)

                    if(res.content.living != undefined){
                        var living_user_not = res.content.living.split("<br>").pop();
                        $('#span_pmu_1').html(living_user_not);
                    }
                    if(res.content.kitchen != undefined){
                        var kitchen_user_not = res.content.kitchen.split("<br>").pop();
                        $('#span_pmu_2').html(kitchen_user_not);
                    }
                    if(res.content.toilet != undefined){
                        var toilet_user_not = res.content.toilet.split("<br>").pop();
                        $('#span_pmu_3').html(toilet_user_not);
                    }
                    if(res.content.bathroom != undefined){
                        var bathroom_user_not = res.content.bathroom.split("<br>").pop();
                        $('#span_pmu_4').html(bathroom_user_not);
                    }
                    if(res.content.rest_room != undefined){
                        var rest_room_user_not = res.content.rest_room.split("<br>").pop();
                        $('#span_pmu_5').html(rest_room_user_not);
                    }
                    if(res.content.hall != undefined){
                        var hall_user_not = res.content.hall.split("<br>").pop();
                        $('#span_pmu_6').html(hall_user_not);
                    }
                    if(res.content.vestibule != undefined){
                        var vestibule_user_not = res.content.vestibule.split("<br>").pop();
                        $('#span_pmu_7').html(vestibule_user_not);
                    }
                    if(res.content.pantry != undefined){
                        var pantry_user_not = res.content.pantry.split("<br>").pop();
                        $('#span_pmu_8').html(pantry_user_not);
                    }

                    $('#span_pmu_1').html(living_user_not);
                    $('#span_pmu_2').html(kitchen_user_not);
                    $('#span_pmu_3').html(toilet_user_not);
                    $('#span_pmu_4').html(bathroom_user_not);
                    $('#span_pmu_5').html(rest_room_user_not);
                    $('#span_pmu_6').html(hall_user_not);
                    $('#span_pmu_7').html(vestibule_user_not);
                    $('#span_pmu_8').html(pantry_user_not);

                    $('#notice_block_1').html(res.content.living);
                    $('#notice_block_2').html(res.content.kitchen);
                    $('#notice_block_3').html(res.content.toilet);
                    $('#notice_block_4').html(res.content.bathroom);
                    $('#notice_block_5').html(res.content.rest_room);
                    $('#notice_block_6').html(res.content.hall);
                    $('#notice_block_7').html(res.content.vestibule);
                    $('#notice_block_8').html(res.content.pantry);
if(res.calculation.living){
    res.calculation.item = res.calculation.living;
    res.arr_layout_val.item = res.arr_layout_val.living;
}else if(res.calculation.kitchen){
    res.calculation.item = res.calculation.kitchen;
    res.arr_layout_val.item = res.arr_layout_val.kitchen;
}else if(res.calculation.toilet){
    res.calculation.item = res.calculation.toilet;
    res.arr_layout_val.item = res.arr_layout_val.toilet;
}else if(res.calculation.bathroom){
    res.calculation.item = res.calculation.bathroom;
    res.arr_layout_val.item = res.arr_layout_val.bathroom;
}else if(res.calculation.rest_room){
    res.calculation.item = res.calculation.rest_room;
    res.arr_layout_val.item = res.arr_layout_val.rest_room;
}else if(res.calculation.hall){
    res.calculation.item = res.calculation.hall;
    res.arr_layout_val.item = res.arr_layout_val.hall;
}else if(res.calculation.vestibule){
    res.calculation.item = res.calculation.vestibule;
    res.arr_layout_val.item = res.arr_layout_val.vestibule;
}
                    calc_3_1(res.calculation.item, res.arr_layout_val.item, TD101, TD108, TD109)
                }
            });
        // ==========================================
    }
    function calc_3_1(calculation,arr_layout_val,TD101,TD108,TD109) {
        // ==========================================

        var formDataPMU = new FormData();
        formDataPMU.append('calculation', calculation);
        formDataPMU.append('arr_layout_val', arr_layout_val);

        for (var i = 1; i <= numb; i++) {
            // var element_tr_1 = $('.tr_' + i)[0];
            // var element_tr_2 = $('.tr_' + i)[1];
            // var element_tr_3 = $('.tr_' + i)[2];
            // var element_tr_4 = $('.tr_' + i)[3];
            // var element_tr_5 = $('.tr_' + i)[4];
            // var element_tr_6 = $('.tr_' + i)[5];
            // var element_tr_7 = $('.tr_' + i)[6];
            // var element_tr_8 = $('.tr_' + i)[7];

            var element_tr_1 = $('#calk-tbody_1 .tr_' + i);
            var element_tr_2 = $('#calk-tbody_2 .tr_' + i);
            var element_tr_3 = $('#calk-tbody_3 .tr_' + i);
            var element_tr_4 = $('#calk-tbody_4 .tr_' + i);
            var element_tr_5 = $('#calk-tbody_5 .tr_' + i);
            var element_tr_6 = $('#calk-tbody_6 .tr_' + i);
            var element_tr_7 = $('#calk-tbody_7 .tr_' + i);
            var element_tr_8 = $('#calk-tbody_8 .tr_' + i);

            if(element_tr_1.length != 0){
                var val_td_CTM02 = +($(element_tr_1).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_1).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_1).find('.td_CTM20').text());
            }else if(element_tr_2.length != 0){
                var val_td_CTM02 = +($(element_tr_2).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_2).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_2).find('.td_CTM20').text());
            }else if(element_tr_3.length != 0){
                var val_td_CTM02 = +($(element_tr_3).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_3).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_3).find('.td_CTM20').text());
            }else if(element_tr_4.length != 0){
                var val_td_CTM02 = +($(element_tr_4).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_4).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_4).find('.td_CTM20').text());
            }else if(element_tr_5.length != 0){
                var val_td_CTM02 = +($(element_tr_5).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_5).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_5).find('.td_CTM20').text());
            }else if(element_tr_6.length != 0){
                var val_td_CTM02 = +($(element_tr_6).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_6).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_6).find('.td_CTM20').text());
            }else if(element_tr_7.length != 0){
                var val_td_CTM02 = +($(element_tr_7).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_7).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_7).find('.td_CTM20').text());
            }else if(element_tr_8.length != 0){
                var val_td_CTM02 = +($(element_tr_8).find('.td_CTM02').text());
                var val_td_CTM12 = +($(element_tr_8).find('.td_CTM12').text());
                var val_td_CTM20 = +($(element_tr_8).find('.td_CTM20').text());
            }

            var val_td_CTM36_1 = +($(element_tr_1).find('.td_CTM36').text());
            var val_td_CTM37_1 = +($(element_tr_1).find('.td_CTM37').text());
            var val_td_CTM38_1 = +($(element_tr_1).find('.td_CTM38').text());
            var val_td_CTM40_1 = +($(element_tr_1).find('.td_CTM40').text());
            var val_td_CTM42_1 = +($(element_tr_1).find('.td_CTM42').text());

            var val_td_CTM36_2 = +($(element_tr_2).find('.td_CTM36').text());
            var val_td_CTM37_2 = +($(element_tr_2).find('.td_CTM37').text());
            var val_td_CTM38_2 = +($(element_tr_2).find('.td_CTM38').text());
            var val_td_CTM40_2 = +($(element_tr_2).find('.td_CTM40').text());
            var val_td_CTM42_2 = +($(element_tr_2).find('.td_CTM42').text());

            var val_td_CTM36_3 = +($(element_tr_3).find('.td_CTM36').text());
            var val_td_CTM37_3 = +($(element_tr_3).find('.td_CTM37').text());
            var val_td_CTM38_3 = +($(element_tr_3).find('.td_CTM38').text());
            var val_td_CTM40_3 = +($(element_tr_3).find('.td_CTM40').text());
            var val_td_CTM42_3 = +($(element_tr_3).find('.td_CTM42').text());

            var val_td_CTM36_4 = +($(element_tr_4).find('.td_CTM36').text());
            var val_td_CTM37_4 = +($(element_tr_4).find('.td_CTM37').text());
            var val_td_CTM38_4 = +($(element_tr_4).find('.td_CTM38').text());
            var val_td_CTM40_4 = +($(element_tr_4).find('.td_CTM40').text());
            var val_td_CTM42_4 = +($(element_tr_4).find('.td_CTM42').text());

            var val_td_CTM36_5 = +($(element_tr_5).find('.td_CTM36').text());
            var val_td_CTM37_5 = +($(element_tr_5).find('.td_CTM37').text());
            var val_td_CTM38_5 = +($(element_tr_5).find('.td_CTM38').text());
            var val_td_CTM40_5 = +($(element_tr_5).find('.td_CTM40').text());
            var val_td_CTM42_5 = +($(element_tr_5).find('.td_CTM42').text());

            var val_td_CTM36_6 = +($(element_tr_6).find('.td_CTM36').text());
            var val_td_CTM37_6 = +($(element_tr_6).find('.td_CTM37').text());
            var val_td_CTM38_6 = +($(element_tr_6).find('.td_CTM38').text());
            var val_td_CTM40_6 = +($(element_tr_6).find('.td_CTM40').text());
            var val_td_CTM42_6 = +($(element_tr_6).find('.td_CTM42').text());

            var val_td_CTM36_7 = +($(element_tr_7).find('.td_CTM36').text());
            var val_td_CTM37_7 = +($(element_tr_7).find('.td_CTM37').text());
            var val_td_CTM38_7 = +($(element_tr_7).find('.td_CTM38').text());
            var val_td_CTM40_7 = +($(element_tr_7).find('.td_CTM40').text());
            var val_td_CTM42_7 = +($(element_tr_7).find('.td_CTM42').text());

            var val_td_CTM36_8 = +($(element_tr_8).find('.td_CTM36').text());
            var val_td_CTM37_8 = +($(element_tr_8).find('.td_CTM37').text());
            var val_td_CTM38_8 = +($(element_tr_8).find('.td_CTM38').text());
            var val_td_CTM40_8 = +($(element_tr_8).find('.td_CTM40').text());
            var val_td_CTM42_8 = +($(element_tr_8).find('.td_CTM42').text());
            // var val_td_CTM14 = ($(element_tr).find('.td_CTM14').text());

            formDataPMU.append('val_td_CTM02[' + i + ']', val_td_CTM02);
            formDataPMU.append('val_td_CTM12[' + i + ']', val_td_CTM12);
            formDataPMU.append('val_td_CTM20[' + i + ']', val_td_CTM20);

            formDataPMU.append('val_td_CTM36[' + i + ']', val_td_CTM36_1);
            formDataPMU.append('val_td_CTM37[' + i + ']', val_td_CTM37_1);
            formDataPMU.append('val_td_CTM38[' + i + ']', val_td_CTM38_1);
            formDataPMU.append('val_td_CTM40[' + i + ']', val_td_CTM40_1);
            formDataPMU.append('val_td_CTM42[' + i + ']', val_td_CTM42_1);

            formDataPMU.append('val_td_CTM36_2[' + i + ']', val_td_CTM36_2);
            formDataPMU.append('val_td_CTM37_2[' + i + ']', val_td_CTM37_2);
            formDataPMU.append('val_td_CTM38_2[' + i + ']', val_td_CTM38_2);
            formDataPMU.append('val_td_CTM40_2[' + i + ']', val_td_CTM40_2);
            formDataPMU.append('val_td_CTM42_2[' + i + ']', val_td_CTM42_2);

            formDataPMU.append('val_td_CTM36_3[' + i + ']', val_td_CTM36_3);
            formDataPMU.append('val_td_CTM37_3[' + i + ']', val_td_CTM37_3);
            formDataPMU.append('val_td_CTM38_3[' + i + ']', val_td_CTM38_3);
            formDataPMU.append('val_td_CTM40_3[' + i + ']', val_td_CTM40_3);
            formDataPMU.append('val_td_CTM42_3[' + i + ']', val_td_CTM42_3);

            formDataPMU.append('val_td_CTM36_4[' + i + ']', val_td_CTM36_4);
            formDataPMU.append('val_td_CTM37_4[' + i + ']', val_td_CTM37_4);
            formDataPMU.append('val_td_CTM38_4[' + i + ']', val_td_CTM38_4);
            formDataPMU.append('val_td_CTM40_4[' + i + ']', val_td_CTM40_4);
            formDataPMU.append('val_td_CTM42_4[' + i + ']', val_td_CTM42_4);

            formDataPMU.append('val_td_CTM36_5[' + i + ']', val_td_CTM36_5);
            formDataPMU.append('val_td_CTM37_5[' + i + ']', val_td_CTM37_5);
            formDataPMU.append('val_td_CTM38_5[' + i + ']', val_td_CTM38_5);
            formDataPMU.append('val_td_CTM40_5[' + i + ']', val_td_CTM40_5);
            formDataPMU.append('val_td_CTM42_5[' + i + ']', val_td_CTM42_5);

            formDataPMU.append('val_td_CTM36_6[' + i + ']', val_td_CTM36_6);
            formDataPMU.append('val_td_CTM37_6[' + i + ']', val_td_CTM37_6);
            formDataPMU.append('val_td_CTM38_6[' + i + ']', val_td_CTM38_6);
            formDataPMU.append('val_td_CTM40_6[' + i + ']', val_td_CTM40_6);
            formDataPMU.append('val_td_CTM42_6[' + i + ']', val_td_CTM42_6);

            formDataPMU.append('val_td_CTM36_7[' + i + ']', val_td_CTM36_7);
            formDataPMU.append('val_td_CTM37_7[' + i + ']', val_td_CTM37_7);
            formDataPMU.append('val_td_CTM38_7[' + i + ']', val_td_CTM38_7);
            formDataPMU.append('val_td_CTM40_7[' + i + ']', val_td_CTM40_7);
            formDataPMU.append('val_td_CTM42_7[' + i + ']', val_td_CTM42_7);

            formDataPMU.append('val_td_CTM36_8[' + i + ']', val_td_CTM36_8);
            formDataPMU.append('val_td_CTM37_8[' + i + ']', val_td_CTM37_8);
            formDataPMU.append('val_td_CTM38_8[' + i + ']', val_td_CTM38_8);
            formDataPMU.append('val_td_CTM40_8[' + i + ']', val_td_CTM40_8);
            formDataPMU.append('val_td_CTM42_8[' + i + ']', val_td_CTM42_8);

        }

        formDataPMU.append('TD05', +($('#AllTbody').find('.td05').text()));
        formDataPMU.append('TD09', +($('#AllTbody').find('.td09').text()));
        formDataPMU.append('TD13', +($('#AllTbody').find('.td13').text()));
        formDataPMU.append('TD17', +($('#AllTbody').find('.td17').text()));
        formDataPMU.append('TD21', +($('#AllTbody').find('.td21').text()));
        formDataPMU.append('TD25', +($('#AllTbody').find('.td25').text()));
        formDataPMU.append('TD29', +($('#AllTbody').find('.td29').text()));
        formDataPMU.append('TD33', +($('#AllTbody').find('.td33').text()));

        formDataPMU.append('TD45', +($('#AllTbody').find('.td45').text()));
        formDataPMU.append('TD48', +($('#AllTbody').find('.td48').text()));
        formDataPMU.append('TD51', +($('#AllTbody').find('.td51').text()));

        // formDataPMU.append('TD100', TD100);
        // formDataPMU.append('TD105', TD105);
        formDataPMU.append('TD100', +($("#calk-tbody_1").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105', +($("#calk-tbody_1").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_2', +($("#calk-tbody_2").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_2', +($("#calk-tbody_2").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_3', +($("#calk-tbody_3").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_3', +($("#calk-tbody_3").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_4', +($("#calk-tbody_4").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_4', +($("#calk-tbody_4").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_5', +($("#calk-tbody_5").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_5', +($("#calk-tbody_5").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_6', +($("#calk-tbody_6").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_6', +($("#calk-tbody_6").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_7', +($("#calk-tbody_7").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_7', +($("#calk-tbody_7").find(".tr_last .td_CTM36").text()));
        formDataPMU.append('TD100_8', +($("#calk-tbody_8").find(".tr_last .td_CTM37").text()));
        formDataPMU.append('TD105_8', +($("#calk-tbody_8").find(".tr_last .td_CTM36").text()));


        formDataPMU.append('TD06', +($('#AllTbody').find('.td06').text()));
        formDataPMU.append('TD10', +($('#AllTbody').find('.td10').text()));
        formDataPMU.append('TD14', +($('#AllTbody').find('.td14').text()));
        formDataPMU.append('TD18', +($('#AllTbody').find('.td18').text()));
        formDataPMU.append('TD22', +($('#AllTbody').find('.td22').text()));
        formDataPMU.append('TD26', +($('#AllTbody').find('.td26').text()));
        formDataPMU.append('TD30', +($('#AllTbody').find('.td30').text()));
        formDataPMU.append('TD34', +($('#AllTbody').find('.td34').text()));

        formDataPMU.append('TD41', +($('#AllTbody').find('.td41').text()));
        formDataPMU.append('TD42', +($('#AllTbody').find('.td42').text()));
        formDataPMU.append('TD43', +($('#AllTbody').find('.td43').text()));
        formDataPMU.append('TD40', +($('#AllTbody').find('.td40').text()));
        formDataPMU.append('TD46', +($('#AllTbody').find('.td46').text()));
        formDataPMU.append('TD49', +($('#AllTbody').find('.td49').text()));
        formDataPMU.append('TD52', +($('#AllTbody').find('.td52').text()));

        // var TD120 = +($('#calk-tbody_1 .tr_x').find('.td_CTM38').text());
        // var TD121 = +($('#calk-tbody_1 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_1 .tr_x').find('.td_CTM37').text()));
        var TD122 = +($('#calk-tbody_1 .tr_last').find('.td_CTM36').text());

        // var TD120_2 = +($('#calk-tbody_2 .tr_x').find('.td_CTM38').text());
        // var TD121_2 = +($('#calk-tbody_2 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_2 .tr_x').find('.td_CTM37').text()));
        var TD122_2 = +($('#calk-tbody_2 .tr_last').find('.td_CTM36').text());

        // var TD120_3 = +($('#calk-tbody_3 .tr_x').find('.td_CTM38').text());
        // var TD121_3 = +($('#calk-tbody_3 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_3 .tr_x').find('.td_CTM37').text()));
        var TD122_3 = +($('#calk-tbody_3 .tr_last').find('.td_CTM36').text());

        // var TD120_4 = +($('#calk-tbody_4 .tr_x').find('.td_CTM38').text());
        // var TD121_4 = +($('#calk-tbody_4 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_4 .tr_x').find('.td_CTM37').text()));
        var TD122_4 = +($('#calk-tbody_4 .tr_last').find('.td_CTM36').text());

        // var TD120_5 = +($('#calk-tbody_5 .tr_x').find('.td_CTM38').text());
        // var TD121_5 = +($('#calk-tbody_5 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_5 .tr_x').find('.td_CTM37').text()));
        var TD122_5 = +($('#calk-tbody_5 .tr_last').find('.td_CTM36').text());

        // var TD120_6 = +($('#calk-tbody_6 .tr_x').find('.td_CTM38').text());
        // var TD121_6 = +($('#calk-tbody_6 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_6 .tr_x').find('.td_CTM37').text()));
        var TD122_6 = +($('#calk-tbody_6 .tr_last').find('.td_CTM36').text());

        // var TD120_7 = +($('#calk-tbody_7 .tr_x').find('.td_CTM38').text());
        // var TD121_7 = +($('#calk-tbody_7 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_7 .tr_x').find('.td_CTM37').text()));
        var TD122_7 = +($('#calk-tbody_7 .tr_last').find('.td_CTM36').text());

        // var TD120_8 = +($('#calk-tbody_8 .tr_x').find('.td_CTM38').text());
        // var TD121_8 = +($('#calk-tbody_8 .tr_last').find('.td_CTM37').text()) - (+($('#calk-tbody_8 .tr_x').find('.td_CTM37').text()));
        var TD122_8 = +($('#calk-tbody_8 .tr_last').find('.td_CTM36').text());

        // formDataPMU.append('TD120', TD120);
        // formDataPMU.append('TD121', TD121);
        formDataPMU.append('TD122', TD122);

        // formDataPMU.append('TD120_2', TD120_2);
        // formDataPMU.append('TD121_2', TD121_2);
        formDataPMU.append('TD122_2', TD122_2);

        // formDataPMU.append('TD120_3', TD120_3);
        // formDataPMU.append('TD121_3', TD121_3);
        formDataPMU.append('TD122_3', TD122_3);

        // formDataPMU.append('TD120_4', TD120_4);
        // formDataPMU.append('TD121_4', TD121_4);
        formDataPMU.append('TD122_4', TD122_4);

        // formDataPMU.append('TD120_5', TD120_5);
        // formDataPMU.append('TD121_5', TD121_5);
        formDataPMU.append('TD122_5', TD122_5);

        // formDataPMU.append('TD120_6', TD120_6);
        // formDataPMU.append('TD121_6', TD121_6);
        formDataPMU.append('TD122_6', TD122_6);

        // formDataPMU.append('TD120_7', TD120_7);
        // formDataPMU.append('TD121_7', TD121_7);
        formDataPMU.append('TD122_7', TD122_7);

        // formDataPMU.append('TD120_8', TD120_8);
        // formDataPMU.append('TD121_8', TD121_8);
        formDataPMU.append('TD122_8', TD122_8);


        // formDataPMU.append('val_td_CTM02_x', +($('calk-tbody_1 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x', +($('calk-tbody_1 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x', +($('calk-tbody_1 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_2', +($('calk-tbody_2 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_2', +($('calk-tbody_2 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_2', +($('calk-tbody_2 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_3', +($('calk-tbody_3 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_3', +($('calk-tbody_3 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_3', +($('calk-tbody_3 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_4', +($('calk-tbody_4 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_4', +($('calk-tbody_4 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_4', +($('calk-tbody_4 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_5', +($('calk-tbody_5 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_5', +($('calk-tbody_5 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_5', +($('calk-tbody_5 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_6', +($('calk-tbody_6 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_6', +($('calk-tbody_6 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_6', +($('calk-tbody_6 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_7', +($('calk-tbody_7 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_7', +($('calk-tbody_7 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_7', +($('calk-tbody_7 .tr_x').find('.td_CTM12').text()));
        //
        // formDataPMU.append('val_td_CTM02_x_8', +($('calk-tbody_8 .tr_x').find('.td_CTM02').text()));
        // formDataPMU.append('val_td_CTM20_x_8', +($('calk-tbody_8 .tr_x').find('.td_CTM20').text()));
        // formDataPMU.append('val_td_CTM12_x_8', +($('calk-tbody_8 .tr_x').find('.td_CTM12').text()));


        formDataPMU.append('val_td_CTM37_x', +($('#calk-tbody_1 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x', +($('#calk-tbody_1 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_2', +($('#calk-tbody_2 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_2', +($('#calk-tbody_2 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_3', +($('#calk-tbody_3 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_3', +($('#calk-tbody_3 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_4', +($('#calk-tbody_4 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_4', +($('#calk-tbody_4 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_5', +($('#calk-tbody_5 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_5', +($('#calk-tbody_5 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_6', +($('#calk-tbody_6 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_6', +($('#calk-tbody_6 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_7', +($('#calk-tbody_7 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_7', +($('#calk-tbody_7 .tr_x').find('.td_CTM38').text()));

        formDataPMU.append('val_td_CTM37_x_8', +($('#calk-tbody_8 .tr_x').find('.td_CTM37').text()));
        formDataPMU.append('val_td_CTM38_x_8', +($('#calk-tbody_8 .tr_x').find('.td_CTM38').text()));


        formDataPMU.append('tab_num', tab_num);
        formDataPMU.append('selectCoefficient', +$('#selectCoefficient').val());
        if(tab_num == 3){
            formDataPMU.append('roof_type', +$('#roof_type').val());
        }

            $.ajax({
                url: '/calc_3_1',
                type: 'POST',
                data: formDataPMU,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    var res = JSON.parse(result)
                    $('#AllTD_1').append(res.living);
                    $('#AllTD_2').append(res.kitchen);
                    $('#AllTD_3').append(res.toilet);
                    $('#AllTD_4').append(res.bathroom);
                    $('#AllTD_5').append(res.rest_room);
                    $('#AllTD_6').append(res.hall);
                    $('#AllTD_7').append(res.vestibule);
                    $('#AllTD_8').append(res.pantry);
                    // $('#AllTD').html(result);
                    tab_4(TD101, TD108, TD109);
                }
            });

        // ==========================================
    }

    function tab_4(TD101,TD108,TD109)
    {

        // ######################## for convas ############################

        // for (var i_ = 1; i_ <= numb; i_++) {
        //     var element_tr_1 = $('#calk-tbody_1 .tr_' + i_);
        //     var element_tr_2 = $('#calk-tbody_2 .tr_' + i_);
        //     var element_tr_3 = $('#calk-tbody_3 .tr_' + i_);
        //     var element_tr_4 = $('#calk-tbody_4 .tr_' + i_);
        //     var element_tr_5 = $('#calk-tbody_5 .tr_' + i_);
        //     var element_tr_6 = $('#calk-tbody_6 .tr_' + i_);
        //     var element_tr_7 = $('#calk-tbody_7 .tr_' + i_);
        //     var element_tr_8 = $('#calk-tbody_8 .tr_' + i_);
        //     if (element_tr_1.length != 0) {
        //         var num_isset = 1;
        //         break;
        //     } else if (element_tr_2.length != 0) {
        //         var num_isset = 2;
        //         break;
        //     } else if (element_tr_3.length != 0) {
        //         var num_isset = 3;
        //         break;
        //     } else if (element_tr_4.length != 0) {
        //         var num_isset = 4;
        //         break;
        //     } else if (element_tr_5.length != 0) {
        //         var num_isset = 5;
        //         break;
        //     } else if (element_tr_6.length != 0) {
        //         var num_isset = 6;
        //         break;
        //     } else if (element_tr_7.length != 0) {
        //         var num_isset = 7;
        //         break;
        //     } else if (element_tr_8.length != 0) {
        //         var num_isset = 8;
        //         break;
        //     }
        // }
        // #########################################################
for (var i_ = 1; i_ <= 8; i_++) {
    var num_isset = i_;
    if ($('#calk-tbody_' + num_isset).length != 0) {
        // --------------------------------------------convas------------------------------------------
        var coord = 20;
        var canvas = document.getElementById('c' + num_isset);
        var ctx = canvas.getContext('2d');
        var pi = Math.PI;
        ctx.clearRect(0, 0, 820, 500);
        ctx.font = "10px Arial";
        ctx.moveTo(20, 10);
        ctx.lineTo(20, 30);
        ctx.strokeStyle = "#808080";
        // -------------------------------------------end-convas------------------------------------------
        var var_cont = false;
        // ------------------------------convas----------------------------------------
        for (var i = 0; i < l; i++) {

            var arr_block = $('#tab-ol').find('.table_first');
            if (arr_block.length != 0) {
                if (arr_block.length == 1 && i + 1 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 2 && i + 2 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 3 && i + 3 == l) {
                    var_cont = true;
                }
            }
            if (var_cont == true) continue;

            var con_td_CTM19 = +($('#calk-tbody_' + num_isset + ' .tr_' + (i + 1) + ' .td_CTM19').text())
            ctx.beginPath();
            //data over graph
            if (con_td_CTM19 < 15) {
                ctx.arc(coord + (con_td_CTM19 / 2), 123, 7, 90, pi / 2, true);
                ctx.font = "8px Arial";
                ctx.fillStyle = "black";
                ctx.fillText(con_td_CTM19, coord + 1, 115);
                ctx.font = "11px Arial";
                ctx.strokeText(i + 1, coord + (con_td_CTM19 / 3), 127);
            } else if (con_td_CTM19 > 30) {
                ctx.arc(coord + (con_td_CTM19 / 2 - 6), 140, 7, 90, pi / 2, true);
                ctx.font = "8px Arial";
                ctx.fillStyle = "black";
                ctx.fillText(con_td_CTM19, coord + (con_td_CTM19 / 2 - 10), 130);
                ctx.font = "11px Arial";
                ctx.strokeText(i + 1, coord + (con_td_CTM19 / 2 - 9), 144);
            } else {
                ctx.arc(coord + 11, 140, 7, 90, pi / 2, true);
                ctx.font = "8px Arial";
                ctx.fillStyle = "black";
                ctx.fillText(con_td_CTM19, coord + 7, 130);
                ctx.font = "11px Arial";
                ctx.strokeText(i + 1, coord + 7, 144);
            }
            // line for data over graph
            ctx.moveTo(coord, 130);
            ctx.lineTo(coord, 370);
            ctx.stroke();
            // get texture
            if (tab_num == 2 || tab_num == 3 || tab_num == 4) {
                var img = document.getElementById("texture_" + i);

                // if ($('#calk-tbody_1 .tr_' + (i + 1) + ' .td_CTM14').text() == 'Т') {
                //     img = document.getElementById("texture_t");
                // }
                ctx.drawImage(img, coord, 150, con_td_CTM19, 220);
            }
            coord += con_td_CTM19;
        }
        // img pmu
        var con_td_120 = +($('#AllTD_' + num_isset + ' .TD120').text()) * 1000;
        var imgPmu = document.getElementById("texture_pmu");
        ctx.drawImage(imgPmu, con_td_120 + 5, 150, 30, 220);

        // last line for data over graph
        ctx.moveTo(coord, 130);
        ctx.lineTo(coord, 370);
        ctx.stroke();
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        ctx.beginPath();
        ctx.strokeStyle = "black";
        ctx.lineWidth = "2";
        var coord = 20;
        ctx.moveTo(15, 180);
        var room_temp = +($('#calk-tbody_' + num_isset + ' .tr_0 .td_CTM39').text());

        var var_cont = false;
        for (var i = 0; i < l; i++) {
            var arr_block = $('#tab-ol').find('.table_first');
            if (arr_block.length != 0) {
                if (arr_block.length == 1 && i + 1 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 2 && i + 2 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 3 && i + 3 == l) {
                    var_cont = true;
                }
            }
            if (var_cont == true) continue;
            var convas_CTM19 = +($('#calk-tbody_' + num_isset + ' .tr_' + (i + 1) + ' .td_CTM19').text())
            var convas_CTM39 = +($('#calk-tbody_' + num_isset + ' .tr_' + (i + 1) + ' .td_CTM39').text())
            ctx.lineTo(coord, 3 * (60 + (room_temp - convas_CTM39)));
            coord = coord + (+convas_CTM19);
        }
        var convas_CTM39_x = +($('#calk-tbody_' + num_isset + ' .tr_x .td_CTM39').text())
        ctx.lineTo(coord, 3 * (60 + (room_temp - convas_CTM39_x)));
        var convas_CTM39_last = +($('#calk-tbody_' + num_isset + ' .tr_last .td_CTM39').text())
        ctx.lineTo(coord + 20, 3 * (60 + (room_temp - convas_CTM39_last)));
        ctx.stroke();
        var coord = 20;
        ctx.beginPath();
        ctx.strokeStyle = "blue";
        ctx.moveTo(15, 180);

        var var_cont = false;
        for (var i = 0; i < l; i++) {
            var arr_block = $('#tab-ol').find('.table_first');
            if (arr_block.length != 0) {
                if (arr_block.length == 1 && i + 1 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 2 && i + 2 == l) {
                    var_cont = true;
                }
                if (arr_block.length == 3 && i + 3 == l) {
                    var_cont = true;
                }
            }
            if (var_cont == true) continue;
            var convas_CTM19 = +($('#calk-tbody_' + num_isset + ' .tr_' + (i + 1) + ' .td_CTM19').text())
            var convas_CTM40 = +($('#calk-tbody_' + num_isset + ' .tr_' + (i + 1) + ' .td_CTM40').text())
            ctx.lineTo(coord, 3 * (60 + (room_temp - convas_CTM40)));
            coord = coord + (+convas_CTM19);
        }
        var convas_CTM40_x = +($('#calk-tbody_' + num_isset + ' .tr_x .td_CTM40').text())
        ctx.lineTo(coord, 3 * (60 + (room_temp - convas_CTM40_x)));

        var convas_CTM40_last = +($('#calk-tbody_' + num_isset + ' .tr_last .td_CTM40').text())
        ctx.lineTo(coord + 20, 3 * (60 + (room_temp - convas_CTM40_last)));
        ctx.stroke();

        ctx.beginPath();
        ctx.fillStyle = "blue";
        ctx.font = "11px Arial";
        ctx.fillText(coord - 20, coord - 10, 390);

        // description
        //lines under the description
        ctx.beginPath();
        ctx.strokeStyle = "#d8d8d8";
        ctx.lineWidth = "1";
        ctx.moveTo(coord + 10, 3 * (60 + (room_temp - convas_CTM40_last)));
        ctx.lineTo(coord + 30, 3 * (33 + (room_temp - convas_CTM40_last)));
        ctx.lineTo(coord + 120, 3 * (33 + (room_temp - convas_CTM40_last)));
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(coord + 10, 3 * (60 + (room_temp - convas_CTM39_last)));
        ctx.lineTo(coord + 30, 3 * (35 + (room_temp - convas_CTM39_last)));
        ctx.lineTo(coord + 150, 3 * (35 + (room_temp - convas_CTM39_last)));
        ctx.stroke();

        //lines under the text PMU
        ctx.beginPath();
        ctx.moveTo(con_td_120 + 120, 100);
        ctx.lineTo(con_td_120 + 50, 100);
        ctx.lineTo(con_td_120 + 20, 160);
        ctx.stroke();

        // text PMU
        ctx.beginPath();
        ctx.fillStyle = "#989696";
        ctx.font = "9px Arial";
        ctx.fillText("Плоскость максимального увлажнения", con_td_120 + 20, 90);

  // description text1
        ctx.beginPath();
        ctx.fillStyle = "#989696";
        ctx.font = "9px Arial";
        ctx.fillText("График температуры", coord + 30, 3 * (27 + (room_temp - convas_CTM40_last)));
        ctx.fillText("отопительного периода", coord + 30, 3 * (30 + (room_temp - convas_CTM40_last)));

        // description text2
        ctx.beginPath();
        ctx.fillStyle = "#989696";
        ctx.font = "9px Arial";
        ctx.fillText("График температуры при", coord + 30, 3 * (29 + (room_temp - convas_CTM39_last)));
        ctx.fillText("наиболее холодной пятидневке ", coord + 30, 3 * (32 + (room_temp - convas_CTM39_last)));

        // description values
        ctx.beginPath();
        ctx.fillStyle = "red";
        ctx.font = "10px Arial";
        ctx.fillText(convas_CTM39_last, coord + 30, 3 * (60 + (room_temp - convas_CTM39_last)));
        ctx.fillText(convas_CTM40_last, coord + 30, 3 * (60 + (room_temp - convas_CTM40_last)));

        // 0 temp. line
        ctx.beginPath();
        ctx.lineWidth = "1";
        ctx.strokeStyle = "#2b2b2b";
        ctx.moveTo(10, 240);
        ctx.lineTo(coord + 60, 240);
        ctx.stroke();

        // 0 temp. (0)
        ctx.beginPath();
        ctx.fillStyle = "#808080";
        ctx.font = "9px Arial";
        ctx.fillText(0, 0, 122 * 2);


        // vertical temperature scale
        ctx.beginPath();
        // ctx.strokeStyle = "#d8d8d8";
        ctx.strokeStyle = "#808080";
        ctx.lineWidth = "1";
        ctx.moveTo(15, 120);
        ctx.lineTo(15, 387);
        ctx.stroke();

        // marks in vertical temperature scale

        ctx.beginPath();
        ctx.moveTo(10, 150);
        ctx.lineTo(15, 150);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 180);
        ctx.lineTo(15, 180);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 210);
        ctx.lineTo(15, 210);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 270);
        ctx.lineTo(15, 270);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 300);
        ctx.lineTo(15, 300);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 330);
        ctx.lineTo(15, 330);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(10, 360);
        ctx.lineTo(15, 360);
        ctx.stroke();

        // numbers in vertical temperature scale
        ctx.beginPath();
        ctx.fillStyle = "blue";
        ctx.font = "11px Arial";
        ctx.fillText(room_temp, 0, 178);
        ctx.fillStyle = "#808080";
        ctx.font = "9px Arial";
        ctx.fillText(30, 0, 148);
        ctx.fillText(10, 0, 208);
        ctx.fillText(-10, 0, 268);
        ctx.fillText(-20, 0, 298);
        ctx.fillText(-30, 0, 328);
        ctx.fillText(-40, 0, 358);

        // horizontal temperature scale
        ctx.beginPath();
        ctx.moveTo(0, 370);
        ctx.lineTo(400, 370);
        ctx.stroke();

        // marks in horizontal temperature scale
        ctx.beginPath();
        ctx.moveTo(20, 370);
        ctx.lineTo(20, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(70, 370);
        ctx.lineTo(70, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(120, 370);
        ctx.lineTo(120, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(170, 370);
        ctx.lineTo(170, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(220, 370);
        ctx.lineTo(220, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(270, 370);
        ctx.lineTo(270, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(320, 370);
        ctx.lineTo(320, 375);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(370, 370);
        ctx.lineTo(370, 375);
        ctx.stroke();
        if (coord - 20 > 350) {

            ctx.beginPath();
            ctx.moveTo(420, 370);
            ctx.lineTo(420, 375);
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(470, 370);
            ctx.lineTo(470, 375);
            ctx.stroke();

            ctx.beginPath();
            ctx.moveTo(0, 370);
            ctx.lineTo(500, 370);
            ctx.stroke();
        }
        // numbers in horizontal temperature scale
        ctx.fillText(0, 22, 380);
        ctx.fillText(50, 72, 380);
        ctx.fillText(100, 122, 380);
        ctx.fillText(150, 172, 380);
        ctx.fillText(200, 222, 380);
        ctx.fillText(250, 272, 380);
        ctx.fillText(300, 322, 380);
        ctx.fillText(350, 372, 380);
        if (coord - 20 > 350) {
            ctx.fillText(400, 422, 380);
            ctx.fillText(450, 472, 380);

            ctx.beginPath();
            ctx.fillStyle = "blue";
            ctx.font = "11px Arial";
            ctx.fillText("мм", 492, 382);
        } else {
            ctx.beginPath();
            ctx.fillStyle = "blue";
            ctx.font = "11px Arial";
            ctx.fillText("мм", 392, 382);
        }

        ctx.beginPath();
        ctx.fillStyle = "blue";
        ctx.font = "15px Arial";
        ctx.fillText("t", 4, 130);

        ctx.beginPath();
        ctx.fillStyle = "blue";
        ctx.font = "7px Arial";
        ctx.fillText("o", 8, 122);
// ----------------------end-convas----------------------------------
    }
}
        // ==========================================
        var formDataTAB = new FormData();

        formDataTAB.append('numb', numb);

        formDataTAB.append('TD05', +($('#AllTbody').find('.td05').text()));
        formDataTAB.append('TD09', +($('#AllTbody').find('.td09').text()));
        formDataTAB.append('TD13', +($('#AllTbody').find('.td13').text()));
        formDataTAB.append('TD17', +($('#AllTbody').find('.td17').text()));
        formDataTAB.append('TD21', +($('#AllTbody').find('.td21').text()));
        formDataTAB.append('TD25', +($('#AllTbody').find('.td25').text()));
        formDataTAB.append('TD29', +($('#AllTbody').find('.td29').text()));
        formDataTAB.append('TD33', +($('#AllTbody').find('.td33').text()));


        formDataTAB.append('CTM39_1', +($('#calk-tbody_1 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_2', +($('#calk-tbody_2 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_3', +($('#calk-tbody_3 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_4', +($('#calk-tbody_4 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_5', +($('#calk-tbody_5 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_6', +($('#calk-tbody_6 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_7', +($('#calk-tbody_7 .tr_1 .td_CTM39').text()));
        formDataTAB.append('CTM39_8', +($('#calk-tbody_8 .tr_1 .td_CTM39').text()));

        formDataTAB.append('TD54', +($('#AllTbody').find('.td54').text()));

        formDataTAB.append('TD55', +($('#AllTbody').find('.td55').text()));
        formDataTAB.append('TD56', +($('#AllTbody').find('.td56').text()));
        formDataTAB.append('TD57', +($('#AllTbody').find('.td57').text()));
        formDataTAB.append('TD58', +($('#AllTbody').find('.td58').text()));


        formDataTAB.append('TD59', +($('#AllTbody').find('.td59').text()));
        formDataTAB.append('TD60', +($('#AllTbody').find('.td60').text()));
        formDataTAB.append('TD61', +($('#AllTbody').find('.td61').text()));

        formDataTAB.append('TD03', +($('#AllTbody').find('.td03').text()));
        formDataTAB.append('term', term);
        formDataTAB.append('bearing', bearing);
        // formDataTAB.append('selectBuilding', $('#selectBuilding').val());

        formDataTAB.append('TD122',  +($('#AllTD_1').find('.TD122').text()));
        formDataTAB.append('TD140',  +($('#AllTD_1').find('.TD140').text()));
            formDataTAB.append('TD141',  +($('#AllTD_1').find('.TD141').text()));
            formDataTAB.append('TD148',  +($('#AllTD_1').find('.TD148').text()));

        formDataTAB.append('TD122_2',  +($('#AllTD_2').find('.TD122').text()));
        formDataTAB.append('TD140_2',  +($('#AllTD_2').find('.TD140').text()));
            formDataTAB.append('TD141_2',  +($('#AllTD_2').find('.TD141').text()));
            formDataTAB.append('TD148_2',  +($('#AllTD_2').find('.TD148').text()));

        formDataTAB.append('TD122_3',  +($('#AllTD_3').find('.TD122').text()));
        formDataTAB.append('TD140_3',  +($('#AllTD_3').find('.TD140').text()));
            formDataTAB.append('TD141_3',  +($('#AllTD_3').find('.TD141').text()));
            formDataTAB.append('TD148_3',  +($('#AllTD_3').find('.TD148').text()));

        formDataTAB.append('TD122_4',  +($('#AllTD_4').find('.TD122').text()));
        formDataTAB.append('TD140_4',  +($('#AllTD_4').find('.TD140').text()));
            formDataTAB.append('TD141_4',  +($('#AllTD_4').find('.TD141').text()));
            formDataTAB.append('TD148_4',  +($('#AllTD_4').find('.TD148').text()));

        formDataTAB.append('TD122_5',  +($('#AllTD_5').find('.TD122').text()));
        formDataTAB.append('TD140_5',  +($('#AllTD_5').find('.TD140').text()));
            formDataTAB.append('TD141_5',  +($('#AllTD_5').find('.TD141').text()));
            formDataTAB.append('TD148_5',  +($('#AllTD_5').find('.TD148').text()));

        formDataTAB.append('TD122_6',  +($('#AllTD_6').find('.TD122').text()));
        formDataTAB.append('TD140_6',  +($('#AllTD_6').find('.TD140').text()));
            formDataTAB.append('TD141_6',  +($('#AllTD_6').find('.TD141').text()));
            formDataTAB.append('TD148_6',  +($('#AllTD_6').find('.TD148').text()));

        formDataTAB.append('TD122_7',  +($('#AllTD_7').find('.TD122').text()));
        formDataTAB.append('TD140_7',  +($('#AllTD_7').find('.TD140').text()));
            formDataTAB.append('TD141_7',  +($('#AllTD_7').find('.TD141').text()));
            formDataTAB.append('TD148_7',  +($('#AllTD_7').find('.TD148').text()));

        formDataTAB.append('TD122_8',  +($('#AllTD_8').find('.TD122').text()));
        formDataTAB.append('TD140_8',  +($('#AllTD_8').find('.TD140').text()));
            formDataTAB.append('TD141_8',  +($('#AllTD_8').find('.TD141').text()));
            formDataTAB.append('TD148_8',  +($('#AllTD_8').find('.TD148').text()));


        formDataTAB.append('TD100', +($("#calk-tbody_1").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_2', +($("#calk-tbody_2").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_3', +($("#calk-tbody_3").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_4', +($("#calk-tbody_4").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_5', +($("#calk-tbody_5").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_6', +($("#calk-tbody_6").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_7', +($("#calk-tbody_7").find(".tr_last .td_CTM37").text()));
        formDataTAB.append('TD100_8', +($("#calk-tbody_8").find(".tr_last .td_CTM37").text()));

        formDataTAB.append('TD101',  TD101);
        formDataTAB.append('TD108',  TD108);
        formDataTAB.append('TD109',  TD109);

        formDataTAB.append('TD82', +($('#AllTbody').find('.td82').text()));
        formDataTAB.append('TD83', +($('#AllTbody').find('.td83').text()));
        formDataTAB.append('TD84', +($('#AllTbody').find('.td84').text()));
        formDataTAB.append('TD85', +($('#AllTbody').find('.td85').text()));
        formDataTAB.append('TD86', +($('#AllTbody').find('.td86').text()));
        formDataTAB.append('TD87', +($('#AllTbody').find('.td87').text()));
        formDataTAB.append('TD88', +($('#AllTbody').find('.td88').text()));

        formDataTAB.append('tab_num', tab_num);
        formDataTAB.append('selectCoefficient', +$('#selectCoefficient').val());

        formDataTAB.append('TD82_1', +($('#AllTbody').find('.td82_1').text()));
        formDataTAB.append('TD83_1', +($('#AllTbody').find('.td83_1').text()));
        formDataTAB.append('TD84_1', +($('#AllTbody').find('.td84_1').text()));
        formDataTAB.append('TD85_1', +($('#AllTbody').find('.td85_1').text()));
        formDataTAB.append('TD86_1', +($('#AllTbody').find('.td86_1').text()));
        formDataTAB.append('TD87_1', +($('#AllTbody').find('.td87_1').text()));
        formDataTAB.append('TD88_1', +($('#AllTbody').find('.td88_1').text()));

        formDataTAB.append('TD82_2', +($('#AllTbody').find('.td82_2').text()));
        formDataTAB.append('TD83_2', +($('#AllTbody').find('.td83_2').text()));
        formDataTAB.append('TD84_2', +($('#AllTbody').find('.td84_2').text()));
        formDataTAB.append('TD85_2', +($('#AllTbody').find('.td85_2').text()));
        formDataTAB.append('TD86_2', +($('#AllTbody').find('.td86_2').text()));
        formDataTAB.append('TD87_2', +($('#AllTbody').find('.td87_2').text()));
        formDataTAB.append('TD88_2', +($('#AllTbody').find('.td88_2').text()));

        formDataTAB.append('text_td07', $("#AllTbody").find(".td07").text().trim());
        formDataTAB.append('text_td11', $("#AllTbody").find(".td11").text().trim());
        formDataTAB.append('text_td15', $("#AllTbody").find(".td15").text().trim());
        formDataTAB.append('text_td19', $("#AllTbody").find(".td19").text().trim());
        formDataTAB.append('text_td23', $("#AllTbody").find(".td23").text().trim());
        formDataTAB.append('text_td27', $("#AllTbody").find(".td27").text().trim());
        formDataTAB.append('text_td31', $("#AllTbody").find(".td31").text().trim());
        formDataTAB.append('text_td35', $("#AllTbody").find(".td35").text().trim());

if(tab_num == 3){
    formDataTAB.append('roof_type', +$('#roof_type').val());
    formDataTAB.append('vapor', vapor);
    formDataTAB.append('thermal_mat_first', termal_mat_first);
    formDataTAB.append('bearing', bearing);

}
        // }

        $.ajax({
                url: '/calc_4_tab',
                type: 'POST',
                data: formDataTAB,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    var res = JSON.parse(result)

                    $('#result_block_1').html(res.content.living);
                    $('#result_block_2').html(res.content.kitchen);
                    $('#result_block_3').html(res.content.toilet);
                    $('#result_block_4').html(res.content.bathroom);
                    $('#result_block_5').html(res.content.rest_room);
                    $('#result_block_6').html(res.content.hall);
                    $('#result_block_7').html(res.content.vestibule);
                    $('#result_block_8').html(res.content.pantry);

                    $('#AllTD_1 .TD115').text(res.TD115.living);
                    $('#AllTD_1 .TD116').text(res.TD116.living);
                    $('#AllTD_1 .TD117').text(res.TD117.living);
                    $('#AllTD_1 .TD118').text(res.TD118.living);
                    $('#AllTD_1 .TD145').text(res.TD145.living);

                    $('#AllTD_2 .TD115').text(res.TD115.kitchen);
                    $('#AllTD_2 .TD116').text(res.TD116.kitchen);
                    $('#AllTD_2 .TD117').text(res.TD117.kitchen);
                    $('#AllTD_2 .TD118').text(res.TD118.kitchen);
                    $('#AllTD_2 .TD145').text(res.TD145.kitchen);

                    $('#AllTD_3 .TD115').text(res.TD115.toilet);
                    $('#AllTD_3 .TD116').text(res.TD116.toilet);
                    $('#AllTD_3 .TD117').text(res.TD117.toilet);
                    $('#AllTD_3 .TD118').text(res.TD118.toilet);
                    $('#AllTD_3 .TD145').text(res.TD145.toilet);

                    $('#AllTD_4 .TD115').text(res.TD115.bathroom);
                    $('#AllTD_4 .TD116').text(res.TD116.bathroom);
                    $('#AllTD_4 .TD117').text(res.TD117.bathroom);
                    $('#AllTD_4 .TD118').text(res.TD118.bathroom);
                    $('#AllTD_4 .TD145').text(res.TD145.bathroom);

                    $('#AllTD_5 .TD115').text(res.TD115.rest_room);
                    $('#AllTD_5 .TD116').text(res.TD116.rest_room);
                    $('#AllTD_5 .TD117').text(res.TD117.rest_room);
                    $('#AllTD_5 .TD118').text(res.TD118.rest_room);
                    $('#AllTD_5 .TD145').text(res.TD145.rest_room);

                    $('#AllTD_6 .TD115').text(res.TD115.hall);
                    $('#AllTD_6 .TD116').text(res.TD116.hall);
                    $('#AllTD_6 .TD117').text(res.TD117.hall);
                    $('#AllTD_6 .TD118').text(res.TD118.hall);
                    $('#AllTD_6 .TD145').text(res.TD145.hall);

                    $('#AllTD_7 .TD115').text(res.TD115.vestibule);
                    $('#AllTD_7 .TD116').text(res.TD116.vestibule);
                    $('#AllTD_7 .TD117').text(res.TD117.vestibule);
                    $('#AllTD_7 .TD118').text(res.TD118.vestibule);
                    $('#AllTD_7 .TD145').text(res.TD145.vestibule);

                    $('#AllTD_8 .TD115').text(res.TD115.pantry);
                    $('#AllTD_8 .TD116').text(res.TD116.pantry);
                    $('#AllTD_8 .TD117').text(res.TD117.pantry);
                    $('#AllTD_8 .TD118').text(res.TD118.pantry);
                    $('#AllTD_8 .TD145').text(res.TD145.pantry);

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    $('#TD101_result_1').html(res.user_101.living);
                    $('#TD108_result_1').html(res.user_108.living);
                    $('#TD109_result_1').html(res.user_109.living);
                    $('#TD03_result_1').html(res.user_03.living);
                    $('#TD59_result_1').html(res.user_59.living);
                    $('#TD54_result_1').html(res.user_54.living);
                    $('#TD145_result_1').html(res.user_145.living);
                    $('#content_user_1_result_1').html(res.content_user_1.living);
                    $('#TD05_result_1').html(res.user_05.living);
                    $('#TD115_result_1').html(res.user_115.living);
                    $('#TD118_result_1').html(res.user_118.living);
                    $('#TD117_result_1').html(res.user_117.living);
                    $('#content_user_2_result_1').html(res.content_user_2.living);
                    $('#TD122_result_1').html(res.user_122.living);
                    $('#TD140_result_1').html(res.user_140.living);
                    $('#TD141_result_1').html(res.user_141.living);
                    $('#content_user_3_result_1').html(res.content_user_3.living);
                    $('#ind_1_room_1').css("backgroundColor", res.content_user_indicator_1.living);
                    $('#ind_2_room_1').css("backgroundColor", res.content_user_indicator_2.living);
                    $('#ind_3_room_1').css("backgroundColor", res.content_user_indicator_3.living);


                    $('#TD101_result_2').text(res.user_101.kitchen);
                    $('#TD108_result_2').text(res.user_108.kitchen);
                    $('#TD109_result_2').text(res.user_109.kitchen);
                    $('#TD03_result_2').text(res.user_03.kitchen);
                    $('#TD59_result_2').text(res.user_59.kitchen);
                    $('#TD54_result_2').text(res.user_54.kitchen);
                    $('#TD145_result_2').text(res.user_145.kitchen);
                    $('#content_user_1_result_2').html(res.content_user_1.kitchen);
                    $('#TD05_result_2').html(res.user_05.kitchen);
                    $('#TD115_result_2').html(res.user_115.kitchen);
                    $('#TD118_result_2').html(res.user_118.kitchen);
                    $('#TD117_result_2').html(res.user_117.kitchen);
                    $('#content_user_2_result_2').html(res.content_user_2.kitchen);
                    $('#TD122_result_2').html(res.user_122.kitchen);
                    $('#TD140_result_2').html(res.user_140.kitchen);
                    $('#TD141_result_2').html(res.user_141.kitchen);
                    $('#content_user_3_result_2').html(res.content_user_3.kitchen);
                    $('#ind_1_room_2').css("backgroundColor", res.content_user_indicator_1.kitchen);
                    $('#ind_2_room_2').css("backgroundColor", res.content_user_indicator_2.kitchen);
                    $('#ind_3_room_2').css("backgroundColor", res.content_user_indicator_3.kitchen)

                    $('#TD101_result_3').text(res.user_101.toilet);
                    $('#TD108_result_3').text(res.user_108.toilet);
                    $('#TD109_result_3').text(res.user_109.toilet);
                    $('#TD03_result_3').text(res.user_03.toilet);
                    $('#TD59_result_3').text(res.user_59.toilet);
                    $('#TD54_result_3').text(res.user_54.toilet);
                    $('#TD145_result_3').text(res.user_145.toilet);
                    $('#content_user_1_result_3').html(res.content_user_1.toilet);
                    $('#TD05_result_3').html(res.user_05.toilet);
                    $('#TD115_result_3').html(res.user_115.toilet);
                    $('#TD118_result_3').html(res.user_118.toilet);
                    $('#TD117_result_3').html(res.user_117.toilet);
                    $('#content_user_2_result_3').html(res.content_user_2.toilet);
                    $('#TD122_result_3').html(res.user_122.toilet);
                    $('#TD140_result_3').html(res.user_140.toilet);
                    $('#TD141_result_3').html(res.user_141.toilet);
                    $('#content_user_3_result_3').html(res.content_user_3.toilet);
                    $('#ind_1_room_3').css("backgroundColor", res.content_user_indicator_1.toilet);
                    $('#ind_2_room_3').css("backgroundColor", res.content_user_indicator_2.toilet);
                    $('#ind_3_room_3').css("backgroundColor", res.content_user_indicator_3.toilet);

                    $('#TD101_result_4').text(res.user_101.bathroom);
                    $('#TD108_result_4').text(res.user_108.bathroom);
                    $('#TD109_result_4').text(res.user_109.bathroom);
                    $('#TD03_result_4').text(res.user_03.bathroom);
                    $('#TD59_result_4').text(res.user_59.bathroom);
                    $('#TD54_result_4').text(res.user_54.bathroom);
                    $('#TD145_result_4').text(res.user_145.bathroom);
                    $('#content_user_1_result_4').html(res.content_user_1.bathroom);
                    $('#TD05_result_4').html(res.user_05.bathroom);
                    $('#TD115_result_4').html(res.user_115.bathroom);
                    $('#TD118_result_4').html(res.user_118.bathroom);
                    $('#TD117_result_4').html(res.user_117.bathroom);
                    $('#content_user_2_result_4').html(res.content_user_2.bathroom);
                    $('#TD122_result_4').html(res.user_122.bathroom);
                    $('#TD140_result_4').html(res.user_140.bathroom);
                    $('#TD141_result_4').html(res.user_141.bathroom);
                    $('#content_user_3_result_4').html(res.content_user_3.bathroom);
                    $('#ind_1_room_4').css("backgroundColor", res.content_user_indicator_1.bathroom);
                    $('#ind_2_room_4').css("backgroundColor", res.content_user_indicator_2.bathroom);
                    $('#ind_3_room_4').css("backgroundColor", res.content_user_indicator_3.bathroom);

                    $('#TD101_result_5').text(res.user_101.rest_room);
                    $('#TD108_result_5').text(res.user_108.rest_room);
                    $('#TD109_result_5').text(res.user_109.rest_room);
                    $('#TD03_result_5').text(res.user_03.rest_room);
                    $('#TD59_result_5').text(res.user_59.rest_room);
                    $('#TD54_result_5').text(res.user_54.rest_room);
                    $('#TD145_result_5').text(res.user_145.rest_room);
                    $('#content_user_1_result_5').html(res.content_user_1.rest_room);
                    $('#TD05_result_5').html(res.user_05.rest_room);
                    $('#TD115_result_5').html(res.user_115.rest_room);
                    $('#TD118_result_5').html(res.user_118.rest_room);
                    $('#TD117_result_5').html(res.user_117.rest_room);
                    $('#content_user_2_result_5').html(res.content_user_2.rest_room);
                    $('#TD122_result_5').html(res.user_122.rest_room);
                    $('#TD140_result_5').html(res.user_140.rest_room);
                    $('#TD141_result_5').html(res.user_141.rest_room);
                    $('#content_user_3_result_5').html(res.content_user_3.rest_room);
                    $('#ind_1_room_5').css("backgroundColor", res.content_user_indicator_1.rest_room);
                    $('#ind_2_room_5').css("backgroundColor", res.content_user_indicator_2.rest_room);
                    $('#ind_3_room_5').css("backgroundColor", res.content_user_indicator_3.rest_room);

                    $('#TD101_result_6').text(res.user_101.hall);
                    $('#TD108_result_6').text(res.user_108.hall);
                    $('#TD109_result_6').text(res.user_109.hall);
                    $('#TD03_result_6').text(res.user_03.hall);
                    $('#TD59_result_6').text(res.user_59.hall);
                    $('#TD54_result_6').text(res.user_54.hall);
                    $('#TD145_result_6').text(res.user_145.hall);
                    $('#content_user_1_result_6').html(res.content_user_1.hall);
                    $('#TD05_result_6').html(res.user_05.hall);
                    $('#TD115_result_6').html(res.user_115.hall);
                    $('#TD118_result_6').html(res.user_118.hall);
                    $('#TD117_result_6').html(res.user_117.hall);
                    $('#content_user_2_result_6').html(res.content_user_2.hall);
                    $('#TD122_result_6').html(res.user_122.hall);
                    $('#TD140_result_6').html(res.user_140.hall);
                    $('#TD141_result_6').html(res.user_141.hall);
                    $('#content_user_3_result_6').html(res.content_user_3.hall);
                    $('#ind_1_room_6').css("backgroundColor", res.content_user_indicator_1.hall);
                    $('#ind_2_room_6').css("backgroundColor", res.content_user_indicator_2.hall);
                    $('#ind_3_room_6').css("backgroundColor", res.content_user_indicator_3.hall);

                    $('#TD101_result_7').text(res.user_101.vestibule);
                    $('#TD108_result_7').text(res.user_108.vestibule);
                    $('#TD109_result_7').text(res.user_109.vestibule);
                    $('#TD03_result_7').text(res.user_03.vestibule);
                    $('#TD59_result_7').text(res.user_59.vestibule);
                    $('#TD54_result_7').text(res.user_54.vestibule);
                    $('#TD145_result_7').text(res.user_145.vestibule);
                    $('#content_user_1_result_7').html(res.content_user_1.vestibule);
                    $('#TD05_result_7').html(res.user_05.vestibule);
                    $('#TD115_result_7').html(res.user_115.vestibule);
                    $('#TD118_result_7').html(res.user_118.vestibule);
                    $('#TD117_result_7').html(res.user_117.vestibule);
                    $('#content_user_2_result_7').html(res.content_user_2.vestibule);
                    $('#TD122_result_7').html(res.user_122.vestibule);
                    $('#TD140_result_7').html(res.user_140.vestibule);
                    $('#TD141_result_7').html(res.user_141.vestibule);
                    $('#content_user_3_result_7').html(res.content_user_3.vestibule);
                    $('#ind_1_room_7').css("backgroundColor", res.content_user_indicator_1.vestibule);
                    $('#ind_2_room_7').css("backgroundColor", res.content_user_indicator_2.vestibule);
                    $('#ind_3_room_7').css("backgroundColor", res.content_user_indicator_3.vestibule);

                    $('#TD101_result_8').text(res.user_101.pantry);
                    $('#TD108_result_8').text(res.user_108.pantry);
                    $('#TD109_result_8').text(res.user_109.pantry);
                    $('#TD03_result_8').text(res.user_03.pantry);
                    $('#TD59_result_8').text(res.user_59.pantry);
                    $('#TD54_result_8').text(res.user_54.pantry);
                    $('#TD145_result_8').text(res.user_145.pantry);
                    $('#content_user_1_result_8').html(res.content_user_1.pantry);
                    $('#TD05_result_8').html(res.user_05.pantry);
                    $('#TD115_result_8').html(res.user_115.pantry);
                    $('#TD118_result_8').html(res.user_118.pantry);
                    $('#TD117_result_8').html(res.user_117.pantry);
                    $('#content_user_2_result_8').html(res.content_user_2.pantry);
                    $('#TD122_result_8').html(res.user_122.pantry);
                    $('#TD140_result_8').html(res.user_140.pantry);
                    $('#TD141_result_8').html(res.user_141.pantry);
                    $('#content_user_3_result_8').html(res.content_user_3.pantry);
                    $('#ind_1_room_8').css("backgroundColor", res.content_user_indicator_1.pantry);
                    $('#ind_2_room_8').css("backgroundColor", res.content_user_indicator_2.pantry);
                    $('#ind_3_room_8').css("backgroundColor", res.content_user_indicator_3.pantry);

// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
                    termoCalc();

                }
            });
        // ==========================================

        function termoCalc (){
            var tab_num = $('#tab_num').val();

            var TD44 = +($("#AllTbody").find(".td44").text());
            var TD05 =  +($("#AllTbody").find(".td05").text());
            var TD37 =  +($("#AllTbody").find(".td37").text());
            var TD39 =  +($("#AllTbody").find(".td39").text());

            //?
            for (var i_ = 1; i_ <= 8; i_++) {
                var element_tr_1 = $('#calk-tbody_1');
                var element_tr_2 = $('#calk-tbody_2');
                var element_tr_3 = $('#calk-tbody_3');
                var element_tr_4 = $('#calk-tbody_4');
                var element_tr_5 = $('#calk-tbody_5');
                var element_tr_6 = $('#calk-tbody_6');
                var element_tr_7 = $('#calk-tbody_7');
                var element_tr_8 = $('#calk-tbody_8');
                if (element_tr_1.length != 0) {
                    var num_isset = 1;
                    break;
                } else if (element_tr_2.length != 0) {
                    var num_isset = 2;
                    break;
                } else if (element_tr_3.length != 0) {
                    var num_isset = 3;
                    break;
                } else if (element_tr_4.length != 0) {
                    var num_isset = 4;
                    break;
                } else if (element_tr_5.length != 0) {
                    var num_isset = 5;
                    break;
                } else if (element_tr_6.length != 0) {
                    var num_isset = 6;
                    break;
                } else if (element_tr_7.length != 0) {
                    var num_isset = 7;
                    break;
                } else if (element_tr_8.length != 0) {
                    var num_isset = 8;
                    break;
                }
            }
                //?

            var TD117 =  +($('#AllTD_'+num_isset+' .TD117').text());
            var TD145 =  +($('#AllTD_'+num_isset+' .TD145').text());
            var formData = new FormData();
            if(tab_num == 2) {
                formData.append('TD176', $('#all_wall_volume').val());
            } else if(tab_num == 3){
                formData.append('TD178', $('#roof_square').val());
            }else if(tab_num == 4){
                formData.append('TD180', $('#floor_square').val());
            }

            formData.append('td05', TD05 );
            formData.append('td37', TD37 );
            formData.append('td39', TD39 );
            formData.append('td44', TD44 );
            formData.append('td117', TD117 );
            formData.append('td145', TD145 );
            formData.append('cookie_name', "data"+tab_num);
            $.ajax({
                url: '/writeCookie',
                type: 'POST',
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                    calculateEstimate();

                }
            });



            // estimate DATA
            function calculateEstimate() {
                var estimate_id_arr = $('#dataTbody .id');
                var lay_material = "";

                for (let i = 0; i < estimate_id_arr.length; i++) {
                    var estimate_id = +$(estimate_id_arr[i]).text();
                    var tmp_lay_material = $(estimate_id_arr[i]).parent().parent().attr("class");
                    if (tmp_lay_material != undefined)
                        lay_material = tmp_lay_material;

                    if (lay_material == 'tab-blocks') {
                        formData_estimate.append('estimate[blocks][' + i + ']', estimate_id);
                    } else if (lay_material == 'tab-frame') {
                        formData_estimate.append('estimate[frame][' + i + ']', estimate_id);
                    } else {
                        formData_estimate.append('estimate[another][' + i + ']', estimate_id);
                    }

                }

                    formData_estimate.append('estimate[tab_num]', tab_num);
                    formData_estimate.append('estimate[coefficient_name]', $("#selectCoefficient").children("option:selected").html());

                if (tab_num == 2) {
                    formData_estimate.append('estimate[volume]', $('#all_wall_volume').val());
                    formData_estimate.append('estimate[coefficient][2]', $("#selectCoefficient").val());
                } else if (tab_num == 3) {
                    formData_estimate.append('estimate[volume]', $('#roof_square').val());
                    formData_estimate.append('estimate[coefficient][3]', $("#selectCoefficient").val());
                } else if (tab_num == 4) {
                    formData_estimate.append('estimate[volume]', $('#floor_square').val());
                    formData_estimate.append('estimate[coefficient][4]', $("#selectCoefficient").val());
                }

                $.ajax({
                    url: '/calc_estimate',
                    type: 'POST',
                    data: formData_estimate,
                    cache: false,
                    processData: false,
                    contentType: false,
                    beforeSend: function () {

                    },
                    success: function (result) {
                        $('#for-estimate').html(result);
                        $('#for-estimate').show();
                        writeLayerAndCookie();
                    }
                });
                // END estimate DATA
            }
        }


// SAVING DATA

        function writeLayerAndCookie(){
            formData_data.append('cookie_name', "tab_"+tab_num);
            $.ajax({
                url: '/writeCookie',
                type: 'POST',
                data: formData_data,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                }
            });

            var num_isset_b = false;
            for (var i_ = 1; i_ <= 8; i_++) {
                if ($('#calk-tbody_'+i_).length != 0) {
                    if(!num_isset_b)
                        num_isset_b = i_;

                    let elem;
                    switch(i_) {
                        case 1:
                            elem = "living";
                            break;
                        case 2:
                            elem = "kitchen";
                            break;
                        case 3:
                            elem = "toilet";
                            break;
                        case 4:
                            elem = "bathroom";
                            break;
                        case 5:
                            elem = "rest_room";
                            break;
                        case 6:
                            elem = "hall";
                            break;
                        case 7:
                            elem = "vestibule";
                            break;
                        case 8:
                            elem = "pantry";
                            break;
                        default:
                            elem = "---not_found---";
                    }

                    var text_tab = $('#calk-tbody_'+i_).html();
                    $("#for-compare").html(text_tab);
                    $("#for-compare .td_CTM03").remove();
                    $("#for-compare .td_CTM04").remove();
                    $("#for-compare .td_CTM05").remove();
                    $("#for-compare .td_CTM09").remove();

                    // for(let i = 11; i<36; i++){
                    //     $("#for-compare .td_CTM"+i).remove();
                    // }
                    $("#for-compare .td_CTM12").remove();
                    $("#for-compare .td_CTM13").remove();
                    $("#for-compare .td_CTM14").remove();
                    $("#for-compare .td_CTM15").remove();
                    $("#for-compare .td_CTM16").remove();
                    $("#for-compare .td_CTM17").remove();
                    $("#for-compare .td_CTM18").remove();
                    $("#for-compare .td_CTM19").remove();
                    $("#for-compare .td_CTM20").remove();
                    $("#for-compare .td_CTM21").remove();
                    $("#for-compare .td_CTM22").remove();
                    $("#for-compare .td_CTM23").remove();
                    $("#for-compare .td_CTM24").remove();
                    $("#for-compare .td_CTM25").remove();
                    $("#for-compare .td_CTM26").remove();
                    $("#for-compare .td_CTM27").remove();
                    $("#for-compare .td_CTM28").remove();
                    $("#for-compare .td_CTM29").remove();
                    $("#for-compare .td_CTM30").remove();
                    $("#for-compare .td_CTM31").remove();
                    $("#for-compare .td_CTM32").remove();
                    $("#for-compare .td_CTM33").remove();
                    $("#for-compare .td_CTM34").remove();
                    $("#for-compare .td_CTM35").remove();

                    formData_data.append('data['+elem+']', $("#for-compare").html());
                    formData_data.append('data['+elem+'_data]', $('.for-data-compare-'+i_).html());

                }
                $("#for-compare").html("");

            }


            // var canvas_base = document.getElementById('c'+num_isset_b);
            var canvas_base = document.querySelector('#c'+num_isset_b);
            // var canvas_base = $('#c'+num_isset_b);
            var dataURL = canvas_base.toDataURL();

            formData_data.append('data[tr_0-td_CTM37]',  $('#calk-tbody_'+num_isset_b+' .tr_0 .td_CTM37').text());
            formData_data.append('data[tr_x-td_CTM37]',  $('#calk-tbody_'+num_isset_b+' .tr_x .td_CTM37').text());
            formData_data.append('data[image]',  dataURL);

            text_tab = $('#calk-tbody_'+num_isset_b).html();
            $("#for-compare").html(text_tab);

            $("#for-compare .td_CTM02").remove();
            $("#for-compare .td_CTM03").remove();
            $("#for-compare .td_CTM04").remove();
            $("#for-compare .td_CTM05").remove();
            $("#for-compare .td_CTM07").remove();
            $("#for-compare .td_CTM09").remove();
            for(let i = 11; i<43; i++){
                if(i == 19)
                    continue;
                $("#for-compare .td_CTM"+i).remove();
            }
            $("#for-compare .tr_0").remove();
            $("#for-compare .tr_x").remove();
            $("#for-compare .tr_last").remove();
            formData_data.append('data[construct_send]', $("#for-compare").html());
            $("#for-compare").html("");

            if(tab_num == 2){
                formData_data.append('data[all_wall_volume]',  $('#all_wall_volume').val());
            }else if(tab_num == 3){
                formData_data.append('data[roof_square]',  $('#roof_square').val());
            }else if(tab_num == 4){
                formData_data.append('data[floor_square]',  $('#floor_square').val());
            }
            formData_data.append('data[estimate]', $("#for-estimate").html());

            $.ajax({
                url: '/writeLayer',
                type: 'POST',
                data: formData_data,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                },
                success: function (result) {
                    console.log(result);
                    $('#waiting').css('display', 'none');

                    if(
                        typeof runAutoLoadData != 'undefined'
                         && typeof logged != 'undefined'
                         && typeof tab_num != 'undefined'
                         ) {
                            if(tab_num == 2) {
                                let secretRoof = $('<iframe  src="/roof?runAutoLoadData=3" height="0" width="0"></iframe>');
                                $('body').append(secretRoof);
                                secretRoof.hide();
                            }
                            else if(tab_num == 3) {
                                let secretFloor = $('<iframe  src="/floor?runAutoLoadData=4" height="0" width="0"></iframe>');
                                $('body').append(secretFloor);
                                secretFloor.hide();
                            }
                            else if(tab_num == 4) {
                                let secretWindow = $('<iframe  src="/window?runAutoLoadData=5" height="0" width="0"></iframe>');
                                $('body').append(secretWindow);
                                secretWindow.hide();
                            }
                        }

                }
            });

        }
// END SAVING DATA
    }

});
