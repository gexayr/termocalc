    $(document).ready(function () {
        var formDataForMat = new FormData();
            formDataForMat.append('col', 'COL20');
            $.ajax({
                url: '/get-staticThickness-mat',
                type: 'POST',
                data: formDataForMat,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    var myMap = new Map();
                    let array = JSON.parse(result);
                    for (let kv of array) {
                        for ( var key in kv ) {
                                myMap.set(+key, +kv[key]);
                        }
                    }
                    set.newMap = myMap
                }
            });
    });

var set = {

    ajaxMethod: 'POST',
    newMap: [],

    ChangeRegion: function() {

        var formData = new FormData();
        formData.append('region', $('#selectRegion').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-cities',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log();
                $("#selectCity").html(result);

            }
        });

    },

    ChangeCity: function() {

        var formData = new FormData();
        formData.append('city', $('#selectCity').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-city-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#cityTbody").html(result);

            }
        });
        set.allTbody();
    },

    AnswerFloorBuilding: function() {
        $('.answer_floor_building').fadeIn();
        if($('#one_floor_building').val() == 'yes'){
            $('.for_multistory').fadeOut();
        }else if($('#one_floor_building').val() == 'no'){
            $('.for_multistory').fadeIn();
        }else{
            // $('.answer_floor_building').fadeOut();
        }
    },
    ChangeBuilding: function() {
        
        var formData = new FormData();
        formData.append('building', $('#selectBuilding').val());
        $.ajax({
            url: '/get-building-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#buildingTbody").html(result);

            }
        });
        set.allTbody();
    },

    allTbody: function() {

        if($('#selectCity').val() == '' || $('#selectBuilding').val() == ''){
            return false
        }

        var formData = new FormData();
        formData.append('city', $('#selectCity').val());
        formData.append('building', $('#selectBuilding').val());
        // formData.append('room', $('#selectRoom').val());

        $.ajax({
            url: '/get-cities-buildings',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#AllTbody").html(result);
                $("#conclusion_content").css("display", "block");

                $("#TD01_info").val($(result).find('.td01').text());
                $("#TD02_info").val($(result).find('.td02').text());
                $("#TD04_info").val($(result).find('.td04').text());
                $("#TD37_info").val($(result).find('.td37').text());
                $("#TD38_info").val($(result).find('.td38').text());
                $("#TD39_info").val($(result).find('.td39').text());
                $("#TD43_info").val($(result).find('.td43').text());
                $("#TD40_info").val($(result).find('.td40').text());
                $("#TD41_info").val($(result).find('.td41').text());
                $("#TD42_info").val($(result).find('.td42').text());
                $("#TD45_info").val($(result).find('.td45').text());
                $("#TD46_info").val($(result).find('.td46').text());
                $("#TD47_info").val($(result).find('.td47').text());
                $("#TD48_info").val($(result).find('.td48').text());
                $("#TD49_info").val($(result).find('.td49').text());
                $("#TD50_info").val($(result).find('.td50').text());
                $("#TD51_info").val($(result).find('.td51').text());
                $("#TD52_info").val($(result).find('.td52').text());
                $("#TD53_info").val($(result).find('.td53').text());

            }
        });
    },


    ChangeRoom: function() {

        var formData = new FormData();
        formData.append('room', $('#selectRoom').val());
        // console.log($('#selectRegion').val());
        $.ajax({
            url: '/get-room-params',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#roomTbody").html(result);

            }
        });
    },


    ChangeCoefficient: function() {

        var formData = new FormData();
        formData.append('coefficient', $('#selectCoefficient').val());
        // console.log($('#selectMat_1').val());
        $.ajax({
            url: '/get-coefficient',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#change_coefficient").val(result);

            }
        });

    },
    
    ChangeCoefficient_: function () {

        var result = $("#selectCoefficient").val();
        console.log(result);
        $("#change_coefficient").val(result);


        var tab_num = $('#tab_num').val();

        // if( tab_num == 3 ){
        //
        //
        //     var tables = $('#dataTbody');
        //     var table = tables.find('.table');
        //     var l = table.length; // total items
        //     var mineral = false;
        //     for (var i = 0; i < l; i++) {
        //         var element = table[i];
        //         if ($(element).find('.COL3').text().trim() === 'Минеральные') {
        //             mineral = i;
        //         }
        //     }
        //
        //     if (mineral != false) {
        //         if (($("#selectCoefficient option:selected").text()).trim() == '1. Скатная кровля') {
        //             $('#change_coefficient').val(0.9);
        //             +$('#selectCoefficient').val(0.9)
        //         }
        //     }
        // }


    },

// ###############################################################################################
    ChangeCategory: function(element, type) {
        var id = +$(element).val();
        var formData = new FormData();
        formData.append('material_id', id);
        formData.append('type', type);
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];
        $.ajax({
            // url: '/get-mat_cat',
            url: '/',
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                console.log('loading');
            }
        });
    },
// ###############################################################################################
//     ChangeMaterialCatNew: (element, type) => {
//         if(type == 'homogeneous'){
//             $("#homogeneous_modal").click();
//         }else if(type == 'thermal'){
//             $("#thermal_modal").click();
//         }else if(type == 'holder'){
//             $("#holder_modal").click();
//         }
//     },
    //
    // HideChildren: function(){
    //     alert('this');
    //     // console.log(this);
    //     // console.log($(this));
    //     // this.children().hide();
    //     return false;
    // },


    clickSelect: function(element, type) {
        var numb = ($(element).parent().parent().attr('class'));
        var result = numb.split('_');
        numb = result[1];
        $("#numb_clicked_input").val(numb);
        $("#type_clicked_input").val(type);
        var type_con = type;
        if(type == "blocks"){
            type_con = "seam";
            if($(element).parent().hasClass( "second_select" )){
                type_con = "blocks";
            }
        }else if(type == "frame"){
            type_con = "filling";
            if($(element).parent().hasClass( "second_select" )){
                type_con = "frame";
            }
        }

        $("#type_con_clicked_input").val(type_con);

        let title;
        switch(type_con) {
            case "homogeneous":
                title = "Oднородный";
                break;
            case "thermal":
                title = "Теплоизоляции";
                break;
            case "holder":
                title = "Несущий";
                break;
            case "frame":
                title = "Заполнение каркаса";
                break;
            case "filling":
                title = "Каркас";
                break;
            case "blocks":
                title = "Заполнение швов кладки";
                break;
            case "seam":
                title = "Блоки";
                break;
            default:
                title = "материалы";
        }

        $("#material-modal-title").html(title);
        $("#modal-material-button").click();
        //
        // console.log(numb);
        // console.log(type);
        // console.log(type_con);

        var url = "/loader-materials";
        var formData = new FormData();
        formData.append('type', type_con);

        $.ajax({
            url: url,
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
                // button.addClass('loading');
            },
            success: function(result){
                // console.log(result);
                $("#modal-material-body").html(result);
            }
        });
    },

    clickMaterial: function(id) {
        $("#myModalMaterialClose").click();
        var numb = $("#numb_clicked_input").val();
        var type = $("#type_clicked_input").val();
        var type_con = $("#type_con_clicked_input").val();

        var thickness = $("#ol li[data-id=" + numb + "]").find('.thickness')[0];
        var element = $("#ol li[data-id=" + numb + "]").find("input[name=mat_cat]")[0];
        var section = 1;
        if(type_con == "blocks" || type_con == "frame"){
            element = $("#ol li[data-id=" + numb + "]").find("input[name=mat_cat]")[1];
            section = 2;
        }
        $(thickness).val('');
        $(thickness).prop('disabled', false);

        for (let kv of this.newMap) {
            if(id === kv[0]){
                $(thickness).val(kv[1]);
                $(thickness).prop('disabled', true);
            }
        }

        var formData = new FormData();
        formData.append('material_id', id);
        formData.append('type', type);
        var url = '/get-material-by-id';
        if(id > 10000)url = '/get-self-material-by-id';
        $.ajax({
            url: url,
            type: this.ajaxMethod,
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function(){
            },
            success: function(result){
                $(element).val(result);
            }
        });

        if(type == "blocks" || type == "frame"){
            var url = '/get-mat_cat_blocks';
            if(id > 10000)url = '/get-mat_cat_blocks_self';
            $.ajax({
                url: url,
                type: this.ajaxMethod,
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    if(section == 1){
                        $('[data-tab="'+numb+'"]').find(".table_first").html(result);
                    }else{
                        $('[data-tab="'+numb+'"]').find(".table_second").html(result);
                    }

                }
            });

        } else {
            var url = '/get-mat_cat';
            if(id > 10000)url = '/get-mat_cat_self';
            $.ajax({
                url: url,
                type: this.ajaxMethod,
                data: formData,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function(){
                },
                success: function(result){
                    $('[data-tab="'+numb+'"]').html(result);
                }
            });
        }

    },




//     ChangeMaterialCat: function(element, type) {
//
//         var id = +$(element).val();
//
//         var map = new Map([
//             [102,1.3],
//             [103,6],
//             [104,10],
//             [105,10],
//             [106,12.5],
//             [107,2],
//             [108,4],
//             [109,1],
//             [110,1],
//             [111,2],
//             [112,1],
//             [113,2],
//             [114,0.4],
//             [115,0.16],
//             [116,1.5],
//             [117,1.9],
//             [118,3],
//             [124, 10],
//             [125, 20],
//             [126, 30],
//             [127, 50],
//             [128, 100],
//             [129, 150],
//             [130, 200],
//             [131, 300],
//             [261, 0.2],
//             [262, 10],
//             [263, 12],
//             [264, 15],
//             [265, 0.3],
//             [266, 0.4],
//         ]);
//         var thickness = ($(element).parent().parent().find('.thickness'))[0];
//         $(thickness).val('');
//         $(thickness).prop('disabled', false);
//         for (let kv of map) {
//             if(id === kv[0]){
//                 $(thickness).val(kv[1]);
//                 $(thickness).prop('disabled', true);
//             }
//         }
//
//
//         var formData = new FormData();
//         formData.append('material_id', id);
//         formData.append('type', type);
//
//         var numb = ($(element).parent().parent().attr('class'));
//         var result = numb.split('_');
//         numb = result[1];
//
//         var url = '/get-mat_cat';
//         if(id > 10000)url = '/get-mat_cat_self';
//
//
//         $.ajax({
//             url: url,
//             type: this.ajaxMethod,
//             data: formData,
//             cache: false,
//             processData: false,
//             contentType: false,
//             beforeSend: function(){
//                 // button.addClass('loading');
//             },
//             success: function(result){
//
//                 $('[data-tab="'+numb+'"]').html(result);
//
//             }
//         });
//
//     },
//
//     ChangeMaterialCatBlocks: function(element, type) {
//
//         var id = +$(element).val();
//
//         var section = 1;
//         var formData = new FormData();
//         formData.append('material_id', $(element).val());
//         formData.append('type', type);
//         var numb = ($(element).parent().parent().attr('class'));
//         var result = numb.split('_');
//         numb = result[1];
//         var select = $(element).parent();
// // console.log(numb);
//         if(select.hasClass( "second_select" )){
//             section = 2;
//         }
//
//         var url = '/get-mat_cat_blocks';
//         if(id > 10000)url = '/get-mat_cat_blocks_self';
//         $.ajax({
//             url: url,
//             type: this.ajaxMethod,
//             data: formData,
//             cache: false,
//             processData: false,
//             contentType: false,
//             beforeSend: function(){
//                 // button.addClass('loading');
//             },
//             success: function(result){
//                 // console.log(result);
//                 // console.log(section);
//                 // console.log(numb);
//                 if(section == 1){
//                     $('[data-tab="'+numb+'"]').find(".table_first").html(result);
//                 }else{
//                     $('[data-tab="'+numb+'"]').find(".table_second").html(result);
//                 }
//
//             }
//         });
//
//     },

    ChangeRegStatus: function (element) {
        $("#other-input").fadeOut();
            if($(element).val() == "other"){
                $("#other-input").fadeIn();
            }
    },

    ChangeWindow: function() {
        console.log('ChangeWindow');

        $('#window_type_distance').removeAttr('disabled');
        $('#window_type_filling').removeAttr('disabled');
        $('#term_resistance').prop('disabled', true);

        var chamber = $('#window_type_chamber').val();
        var html = '';
        var html1 = '';
        console.log(chamber);
        if(chamber == 1){
            html = "<option value='12'>- 12 мм </option>" +
                "<option value='16'>- 16 мм </option>" +
                "<option value='20'>- 20 мм </option>"
            html1 = "" +
                "<option value='1'>  - из стекла без покрытий с заполнением воздухом</option>" +
                "<option value='2'>  - из стекла без покрытий с заполнением аргоном</option>" +
                "<option value='3'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='4'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='5'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением криптоном</option>"
            $('#term_resistance').val(0.34);

        }else if( chamber == 2){
            html = "<option value='10'>- 10 + 10 мм</option>" +
                "<option value='14'>- 14 + 14 мм</option>" +
                "<option value='18'>- 18 + 18  мм</option>"
            html1 = "" +
                "<option value='1'> - из стекла без покрытий с заполнением воздухом</option>" +
                "<option value='2'> - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='3'> - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='4'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='5'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='6'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением криптоном</option>"

            $('#term_resistance').val(0.46);

        }else{
            $('#window_type_distance').prop('disabled', true);
            $('#window_type_filling').prop('disabled', true);
            $('#term_resistance').removeAttr('disabled');

        }
        $('#window_type_distance').html(html);
        $('#window_type_filling').html(html1);

    },

    ChangeWindowDistance: function() {
        var chamber = $('#window_type_chamber').val();
        var distance = $('#window_type_distance').val();
        var filling = $('#window_type_filling').val();
        if(chamber == ''){
            return false;
        }else if(chamber == 1){
            if(distance == 12){
                if(filling == 1){
                    $('#term_resistance').val(0.34);

                }else if(filling == 2){
                    $('#term_resistance').val(0.36);

                }else if(filling == 3){
                    $('#term_resistance').val(0.59);

                }else if(filling == 4){
                    $('#term_resistance').val(0.76);

                }else if(filling == 5){
                    $('#term_resistance').val(0.86);

                }
            }else if(distance == 16){
                if(filling == 1){
                    $('#term_resistance').val(0.35);

                }else if(filling == 2){
                    $('#term_resistance').val(0.37);

                }else if(filling == 3){
                    $('#term_resistance').val(0.65);

                }else if(filling == 4){
                    $('#term_resistance').val(0.81);

                }else if(filling == 5){
                    $('#term_resistance').val(0.84);

                }
            }else if(distance == 20){
                if(filling == 1){
                    $('#term_resistance').val(0.35);

                }else if(filling == 2){
                    $('#term_resistance').val(0.37);

                }else if(filling == 3){
                    $('#term_resistance').val(0.64);

                }else if(filling == 4){
                    $('#term_resistance').val(0.79);

                }else if(filling == 5){
                    $('#term_resistance').val(0.82);

                }
            }
        }else if(chamber == 2){
            if(distance == 10){
                if(filling == 1){
                    $('#term_resistance').val(0.46);

                }else if(filling == 2){
                    $('#term_resistance').val(0.64);

                }else if(filling == 3){
                    $('#term_resistance').val(0.78);

                }else if(filling == 4){
                    $('#term_resistance').val(0.82);

                }else if(filling == 5){
                    $('#term_resistance').val(1.1);

                }else if(filling == 6){
                    $('#term_resistance').val(1.73);

                }
            }else if(distance == 14){
                if(filling == 1){
                    $('#term_resistance').val(0.5);

                }else if(filling == 2){
                    $('#term_resistance').val(0.78);

                }else if(filling == 3){
                    $('#term_resistance').val(0.95);

                }else if(filling == 4){
                    $('#term_resistance').val(1.06);

                }else if(filling == 5){
                    $('#term_resistance').val(1.4);

                }else if(filling == 6){
                    $('#term_resistance').val(1.71);

                }
            }else if(distance == 28){
                if(filling == 1){
                    $('#term_resistance').val(0.53);

                }else if(filling == 2){
                    $('#term_resistance').val(0.9);

                }else if(filling == 3){
                    $('#term_resistance').val(1.05);

                }else if(filling == 4){
                    $('#term_resistance').val(1.27);

                }else if(filling == 5){
                    $('#term_resistance').val(1.55);

                }else if(filling == 6){
                    $('#term_resistance').val(1.67);

                }
            }
        }
    },
    ChangeWindowFilling: function() {
        var chamber = $('#window_type_chamber').val();
        var distance = $('#window_type_distance').val();
        var filling = $('#window_type_filling').val();
        if(chamber == ''){
            return false;
        }else if(chamber == 1){
            if(distance == 12){
                if(filling == 1){
                    $('#term_resistance').val(0.34);

                }else if(filling == 2){
                    $('#term_resistance').val(0.36);

                }else if(filling == 3){
                    $('#term_resistance').val(0.59);

                }else if(filling == 4){
                    $('#term_resistance').val(0.76);

                }else if(filling == 5){
                    $('#term_resistance').val(0.86);

                }
            }else if(distance == 16){
                if(filling == 1){
                    $('#term_resistance').val(0.35);

                }else if(filling == 2){
                    $('#term_resistance').val(0.37);

                }else if(filling == 3){
                    $('#term_resistance').val(0.65);

                }else if(filling == 4){
                    $('#term_resistance').val(0.81);

                }else if(filling == 5){
                    $('#term_resistance').val(0.84);

                }
            }else if(distance == 20){
                if(filling == 1){
                    $('#term_resistance').val(0.35);

                }else if(filling == 2){
                    $('#term_resistance').val(0.37);

                }else if(filling == 3){
                    $('#term_resistance').val(0.64);

                }else if(filling == 4){
                    $('#term_resistance').val(0.79);

                }else if(filling == 5){
                    $('#term_resistance').val(0.82);

                }
            }
        }else if(chamber == 2){
            if(distance == 10){
                if(filling == 1){
                    $('#term_resistance').val(0.46);

                }else if(filling == 2){
                    $('#term_resistance').val(0.64);

                }else if(filling == 3){
                    $('#term_resistance').val(0.78);

                }else if(filling == 4){
                    $('#term_resistance').val(0.82);

                }else if(filling == 5){
                    $('#term_resistance').val(1.1);

                }else if(filling == 6){
                    $('#term_resistance').val(1.73);

                }
            }else if(distance == 14){
                if(filling == 1){
                    $('#term_resistance').val(0.5);

                }else if(filling == 2){
                    $('#term_resistance').val(0.78);

                }else if(filling == 3){
                    $('#term_resistance').val(0.95);

                }else if(filling == 4){
                    $('#term_resistance').val(1.06);

                }else if(filling == 5){
                    $('#term_resistance').val(1.4);

                }else if(filling == 6){
                    $('#term_resistance').val(1.71);

                }
            }else if(distance == 18){
                if(filling == 1){
                    $('#term_resistance').val(0.53);

                }else if(filling == 2){
                    $('#term_resistance').val(0.9);

                }else if(filling == 3){
                    $('#term_resistance').val(1.05);

                }else if(filling == 4){
                    $('#term_resistance').val(1.27);

                }else if(filling == 5){
                    $('#term_resistance').val(1.55);

                }else if(filling == 6){
                    $('#term_resistance').val(1.67);

                }
            }
        }
    },


    ChangeLantern: function() {
        console.log('ChangeLantern');

        $('#lantern_type_distance').removeAttr('disabled');
        $('#lantern_type_filling').removeAttr('disabled');
        $('#lantern_term').prop('disabled', true);

        var chamber = $('#lantern_type_chamber').val();
        var html = '';
        var html1 = '';
        console.log(chamber);
        if(chamber == 1){
            html = "<option value='12'>- 12 мм </option>" +
                "<option value='16'>- 16 мм </option>" +
                "<option value='20'>- 20 мм </option>"
            html1 = "" +
                "<option value='1'>  - из стекла без покрытий с заполнением воздухом</option>" +
                "<option value='2'>  - из стекла без покрытий с заполнением аргоном</option>" +
                "<option value='3'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='4'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='5'>  - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением криптоном</option>"
            $('#lantern_term').val(0.34);

        }else if( chamber == 2){
            html = "<option value='10'>- 10 + 10 мм</option>" +
                "<option value='14'>- 14 + 14 мм</option>" +
                "<option value='18'>- 18 + 18  мм</option>"
            html1 = "" +
                "<option value='1'> - из стекла без покрытий с заполнением воздухом</option>" +
                "<option value='2'> - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='3'> - с одним стеклом с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='4'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением воздухом</option>" +
                "<option value='5'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением аргоном</option>" +
                "<option value='6'> - с двумя стеклами с низкоэмиссионным мягким покрытием с заполнением криптоном</option>"
            $('#lantern_term').val(0.46);

        }else{
            $('#lantern_type_distance').prop('disabled', true);
            $('#lantern_type_filling').prop('disabled', true);
            $('#lantern_term').removeAttr('disabled');

        }

        $('#lantern_type_distance').html(html);
        $('#lantern_type_filling').html(html1);
    },


    ChangeLanternDistance: function() {
        var chamber = $('#lantern_type_chamber').val();
        var distance = $('#lantern_type_distance').val();
        var filling = $('#lantern_type_filling').val();
        if(chamber == ''){
            return false;
        }else if(chamber == 1){
            if(distance == 12){
                if(filling == 1){
                    $('#lantern_term').val(0.34);

                }else if(filling == 2){
                    $('#lantern_term').val(0.36);

                }else if(filling == 3){
                    $('#lantern_term').val(0.59);

                }else if(filling == 4){
                    $('#lantern_term').val(0.76);

                }else if(filling == 5){
                    $('#lantern_term').val(0.86);

                }
            }else if(distance == 16){
                if(filling == 1){
                    $('#lantern_term').val(0.35);

                }else if(filling == 2){
                    $('#lantern_term').val(0.37);

                }else if(filling == 3){
                    $('#lantern_term').val(0.65);

                }else if(filling == 4){
                    $('#lantern_term').val(0.81);

                }else if(filling == 5){
                    $('#lantern_term').val(0.84);

                }
            }else if(distance == 20){
                if(filling == 1){
                    $('#lantern_term').val(0.35);

                }else if(filling == 2){
                    $('#lantern_term').val(0.37);

                }else if(filling == 3){
                    $('#lantern_term').val(0.64);

                }else if(filling == 4){
                    $('#lantern_term').val(0.79);

                }else if(filling == 5){
                    $('#lantern_term').val(0.82);

                }
            }
        }else if(chamber == 2){
            if(distance == 10){
                if(filling == 1){
                    $('#lantern_term').val(0.46);

                }else if(filling == 2){
                    $('#lantern_term').val(0.64);

                }else if(filling == 3){
                    $('#lantern_term').val(0.78);

                }else if(filling == 4){
                    $('#lantern_term').val(0.82);

                }else if(filling == 5){
                    $('#lantern_term').val(1.1);

                }else if(filling == 6){
                    $('#lantern_term').val(1.73);

                }
            }else if(distance == 14){
                if(filling == 1){
                    $('#lantern_term').val(0.5);

                }else if(filling == 2){
                    $('#lantern_term').val(0.78);

                }else if(filling == 3){
                    $('#lantern_term').val(0.95);

                }else if(filling == 4){
                    $('#lantern_term').val(1.06);

                }else if(filling == 5){
                    $('#lantern_term').val(1.4);

                }else if(filling == 6){
                    $('#lantern_term').val(1.71);

                }
            }else if(distance == 18){
                if(filling == 1){
                    $('#lantern_term').val(0.53);

                }else if(filling == 2){
                    $('#lantern_term').val(0.9);

                }else if(filling == 3){
                    $('#lantern_term').val(1.05);

                }else if(filling == 4){
                    $('#lantern_term').val(1.27);

                }else if(filling == 5){
                    $('#lantern_term').val(1.55);

                }else if(filling == 6){
                    $('#lantern_term').val(1.67);

                }
            }
        }
    },
    ChangeLanternFilling: function() {
        var chamber = +$('#lantern_type_chamber').val();
        var distance = +$('#lantern_type_distance').val();
        var filling = +$('#lantern_type_filling').val();
        if(chamber == ''){
            return false;
        }else if(chamber == 1){
            if(distance == 12){
                if(filling == 1){
                    $('#lantern_term').val(0.34);

                }else if(filling == 2){
                    $('#lantern_term').val(0.36);

                }else if(filling == 3){
                    $('#lantern_term').val(0.59);

                }else if(filling == 4){
                    $('#lantern_term').val(0.76);

                }else if(filling == 5){
                    $('#lantern_term').val(0.86);

                }
            }else if(distance == 16){
                if(filling == 1){
                    $('#lantern_term').val(0.35);

                }else if(filling == 2){
                    $('#lantern_term').val(0.37);

                }else if(filling == 3){
                    $('#lantern_term').val(0.65);

                }else if(filling == 4){
                    $('#lantern_term').val(0.81);

                }else if(filling == 5){
                    $('#lantern_term').val(0.84);

                }
            }else if(distance == 20){
                if(filling == 1){
                    $('#lantern_term').val(0.35);

                }else if(filling == 2){
                    $('#lantern_term').val(0.37);

                }else if(filling == 3){
                    $('#lantern_term').val(0.64);

                }else if(filling == 4){
                    $('#lantern_term').val(0.79);

                }else if(filling == 5){
                    $('#lantern_term').val(0.82);

                }
            }
        }else if(chamber == 2){
            if(distance == 10){
                if(filling == 1){
                    $('#lantern_term').val(0.46);

                }else if(filling == 2){
                    $('#lantern_term').val(0.64);

                }else if(filling == 3){
                    $('#lantern_term').val(0.78);

                }else if(filling == 4){
                    $('#lantern_term').val(0.82);

                }else if(filling == 5){
                    $('#lantern_term').val(1.1);

                }else if(filling == 6){
                    $('#lantern_term').val(1.73);

                }
            }else if(distance == 14){
                if(filling == 1){
                    $('#lantern_term').val(0.5);

                }else if(filling == 2){
                    $('#lantern_term').val(0.78);

                }else if(filling == 3){
                    $('#lantern_term').val(0.95);

                }else if(filling == 4){
                    $('#lantern_term').val(1.06);

                }else if(filling == 5){
                    $('#lantern_term').val(1.4);

                }else if(filling == 6){
                    $('#lantern_term').val(1.71);

                }
            }else if(distance == 18){
                if(filling == 1){
                    $('#lantern_term').val(0.53);

                }else if(filling == 2){
                    $('#lantern_term').val(0.9);

                }else if(filling == 3){
                    $('#lantern_term').val(1.05);

                }else if(filling == 4){
                    $('#lantern_term').val(1.27);

                }else if(filling == 5){
                    $('#lantern_term').val(1.55);

                }else if(filling == 6){
                    $('#lantern_term').val(1.67);

                }
            }
        }
    },

};