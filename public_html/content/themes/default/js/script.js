//
// $(".up").click(function(){
//     var pdiv = $(this).parent('li');
//     pdiv.insertBefore(pdiv.prev());
//     return false
// });
// $(".down").click(function(){
//     var pdiv = $(this).parent('li');
//     pdiv.insertAfter(pdiv.next());
//     return false
// });
//
//
// $(".edit").click(function(){
//     var edit = $(this).parent().find('.edit-block');
//     edit.slideToggle("slow");
// });
//
// $(".remove").click(function(){
//     if (!confirm("are you sure?")) {
//         return false
//     }
//     var remove = $(this).parent();
//     remove.remove();
// });

var ol = $('#ol');
var ol_tab = $('#tab-ol');

$(document).ready(function () {
    $('.menu-toggle').click(function () {
        $('.menu-toggle').toggleClass('active')
        $('ul').toggleClass('active')
    });
});

function upField(field) {
    var pdiv = $(field).parent('li');
    pdiv.insertBefore(pdiv.prev());

    var num = pdiv.attr('data-id');
    var tab_ptab = $('[data-tab="' + num + '"]');
    tab_ptab.insertBefore(tab_ptab.prev());

    // +++++++++++++++++++

    // setTimeout(function(){
    var lists = ol.find('li');
    var l = lists.length; // total items

    for (var i = 0; i <= l; i++) {
        var j = (i + 1);
        var element = lists[i];
        $(element).attr('data-id', j);
        $(element).removeClass();
        $(element).addClass('col-md-12 margin-li li_' + j);
        $(element).find('.form-control-lg').data('id', j);
        // console.log(lists[i]);
    }
    // }, 1);


    // setTimeout(function(){
    var lists = ol_tab.find('li');
    var l = lists.length; // total items

    for (var i = 0; i <= l; i++) {
        var j = (i + 1);
        var element = lists[i];
        var par = $(element).parent();
        $(element).parent().attr('data-tab', j);
        $(par).removeClass();
        $(par).addClass('table_' + j);
        // $(element).find('.form-control-lg').data('id', j);
    }
    // }, 1);

    // +++++++++++++++++++
    return false
}

function downField(field) {
    var pdiv = $(field).parent('li');
    pdiv.insertAfter(pdiv.next());
    // console.log(pdiv);

    var num = pdiv.attr('data-id');
    var tab_ptab = $('[data-tab="' + num + '"]');
    tab_ptab.insertAfter(tab_ptab.next());

    // +++++++++++++++++++

    // setTimeout(function(){
    var lists = ol.find('li');
    var l = lists.length; // total items

    for (var i = 0; i <= l; i++) {
        var j = (i + 1);
        var element = lists[i];
        $(element).attr('data-id', j);
        $(element).removeClass();
        $(element).addClass('col-md-12 margin-li li_' + j);
        $(element).find('.form-control-lg').data('id', j);
    }
    // }, 1);


    // setTimeout(function(){
    var lists = ol_tab.find('li');
    var l = lists.length; // total items

    for (var i = 0; i <= l; i++) {
        var j = (i + 1);
        var element = lists[i];
        var par = $(element).parent();
        $(element).parent().attr('data-tab', j);
        $(par).removeClass();
        $(par).addClass('table_' + j);
        // $(element).find('.form-control-lg').data('id', j);
    }
    // }, 1);


    // +++++++++++++++++++
    return false
}

function editField(field) {
    var edit = $(field).parent().find('.edit-block');
    edit.slideToggle("slow");
    var edit_in = $(field).parent().find('.inline-block');
    edit_in.css("display", "inline-block");
}

function removeField(field) {
    if (!confirm("Вы удаляете слой материала из конструкции.\n" +
        "Вы уверены, удалить?")) {
        return false
    }
    var remove = $(field).parent();
    var num = remove.attr('data-id');
    var tab_rem = $('[data-tab="' + num + '"]');
    // console.log(tab_rem);

    remove.remove();
    tab_rem.remove();

    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
        }
    }, 400);


    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
        }
    }, 600);

    // +++++++++++++++++++
}


$(".homogeneous").click(function () {
    var add = $(this).parent().parent().parent().find('.add-fields');
    var tab = $('#dataTbody');
    var mat = 28;

    // ==============
    var type = "homogeneous";


    if (add.hasClass("block_1")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_1');
    }
    if (add.hasClass("block_2")) {
        var lenght = $('.block_1').find('li').length + 1 + 1;
        var tab = $('.table_block_2');

    }
    if (add.hasClass("block_3")) {
        var lenght = $('.block_1').find('li').length + 1 + $('.block_3').find('li').length + 1;
        var tab = $('.table_block_3');

    }
    // ==============

    $.ajax({
        url: '/loader-homogeneous',
        method: 'GET',
        data: {"count": lenght, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {
        // console.log(data);

        add.append(data);

    });
    $.ajax({
        url: '/table-homogeneous_thermal',
        method: 'GET',
        data: {"count": lenght, "mat": mat, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        tab.append(data);

    });

    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 500);


    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
        }
    }, 500);
    // +++++++++++++++++++
});


$(".thermal").click(function () {
    var add = $(this).parent().parent().parent().find('.add-fields');
    var tab = $('#dataTbody');
    var mat = 1;

    var type = "thermal";

    // ==============

    if (add.hasClass("block_1")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_1');

    }
    if (add.hasClass("block_2")) {
        var lenght = $('.block_1').find('li').length + 1 + 1;
        var tab = $('.table_block_2');
    }
    if (add.hasClass("block_3")) {
        var lenght = $('.block_1').find('li').length + 1 + $('.block_3').find('li').length + 1;
        var tab = $('.table_block_3');
    }

    // ==============
    $.ajax({
        url: '/loader-thermal',
        method: 'GET',
        data: {"count": lenght, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        add.append(data);

    });
    $.ajax({
        url: '/table-homogeneous_thermal',
        method: 'GET',
        data: {"count": lenght, "mat": mat, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        tab.append(data);

    });

    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 500);


    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 500);

    // +++++++++++++++++++

});

$(".blocks").click(function () {
//     var block = $(this).parent().parent().find('.opt-block');
// console.log(block);
//     block.slideToggle("slow");

    var mat = 30;
    var mat2 = 85;
    var add = $(this).parent().parent().parent().find('.add-fields');
    var isset = 'append';
    if ($(this).parent().hasClass('holder_button')) {
        isset = 'html';
    }

    // ==============
    var type = "blocks";

    if (add.hasClass("block_1")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_1');

    }
    if (add.hasClass("block_2")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_2');
    }
    if (add.hasClass("block_3")) {
        var lenght = $('.block_1').find('li').length + 1 + $('.block_3').find('li').length + 1;
        var tab = $('.table_block_3');
    }

    // ==============
    $.ajax({
        url: '/loader-blocks-block',
        method: 'GET',
        data: {"isset": isset, "count": lenght, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        if (isset == 'append') {
            add.append(data);
        } else {
            add.html(data);
            $('.hide-holder').show();
        }

    });

    $.ajax({
        url: '/table-block_filling',
        method: 'GET',
        data: {"count": lenght, "mat": mat, "mat2": mat2, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {
        if (isset == 'append') {
            tab.append(data);
        } else {
            tab.html(data);
        }
    });

    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 500);

    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
        }
    }, 500);
    // +++++++++++++++++++
});

$(".frame").click(function () {
    // var block = $(this).parent().parent().find('.opt-frame');
    // console.log(block);
    // block.slideToggle("slow");


    var mat = 28;
    var mat2 = 1;
    var add = $(this).parent().parent().parent().find('.add-fields');

    var isset = 'append';
    if ($(this).parent().hasClass('holder_button')) {
        isset = 'html';
    }

    var type = "frame";
    // ==============

    if (add.hasClass("block_1")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_1');

    }
    if (add.hasClass("block_2")) {
        var lenght = $('.block_1').find('li').length + 1;
        var tab = $('.table_block_2');
    }
    if (add.hasClass("block_3")) {
        var lenght = $('.block_1').find('li').length + 1 + $('.block_3').find('li').length + 1;
        var tab = $('.table_block_3');
    }

    // ==============

    $.ajax({
        url: '/loader-frame-filling',
        method: 'GET',
        data: {"isset": isset, "count": lenght, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        if (isset == 'append') {
            add.append(data);
        } else {
            add.html(data);
            $('.hide-holder').show();
        }

    });


    $.ajax({
        url: '/table-block_filling',
        method: 'GET',
        data: {"count": lenght, "mat": mat, "mat2": mat2, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        if (isset == 'append') {
            tab.append(data);
        } else {
            tab.html(data);
        }

    });
    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 500);

    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
        }
    }, 500);
    // +++++++++++++++++++
});


$(".holder").click(function () {
    var add = $(this).parent().parent().parent().find('.add-fields');
    var lenght = $('.block_1').find('li').length + 1;
    // console.log(lenght);

    var tab = $('.table_block_2');
    var mat = 28;
    var type = "holder";

    $.ajax({
        url: '/loader-holder',
        method: 'GET',
        data: {"count": lenght, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        add.html(data);

    });

    $.ajax({
        url: '/table-homogeneous_thermal',
        method: 'GET',
        data: {"count": lenght, "mat": mat, "type": type},
        beforeSend: function () {
        }
    }).done(function (data) {

        tab.html(data);

    });

    // +++++++++++++++++++

    setTimeout(function () {
        var lists = ol.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            $(element).attr('data-id', j);
            $(element).removeClass();
            $(element).addClass('col-md-12 margin-li li_' + j);
            $(element).find('.form-control-lg').data('id', j);
            // console.log(lists[i]);
        }
    }, 400);


    setTimeout(function () {
        var lists = ol_tab.find('li');
        var l = lists.length; // total items

        for (var i = 0; i <= l; i++) {
            var j = (i + 1);
            var element = lists[i];
            var par = $(element).parent();
            $(element).parent().attr('data-tab', j);
            $(par).removeClass();
            $(par).addClass('table_' + j);
            // $(element).find('.form-control-lg').data('id', j);
        }
    }, 500);
    // +++++++++++++++++++
});

$('#reg_form').submit(function () {

});


$('#reg_user').click(function () {

    if ($('#exampleCheck1').is(':checked') == false) {
        $('#must_check').fadeIn();
        setTimeout(function () {
            $('#must_check').fadeOut();
        }, 2000);
        return false;
    }
    if (document.getElementById('sPass').value != document.getElementById('sRePass').value) {
        window.alert("Пароли не совпадают");
        return false;
    }
    if ($('#sPass').val().length < 4) {
        alert("Пароль не может быть короче 4 символов");
        return false;
    }
    return true;
});


function send_city(auto = '') {
    // console.log(auto);

    var formData = new FormData();
    if (($('#selectCity').val() == '') || ($('#selectBuilding').val() == '') || ($('#one_floor_building').val() == '')) {
        alert('Данные не заполнены')
        return false
    }
    set.allTbody();

    $('#core-data-conclusion-admin').fadeIn();
    setTimeout(function () {

        formData.append('TD170', $('#living_floors').val());
        formData.append('TD171', $('#one_floor_building').val());
        formData.append('TD172', $('#people_count').val());
        formData.append('TD181', $('#floor_height').val());
        formData.append('TD201', $('#living_room_wall').val());
        formData.append('TD216', $('#living_room_roof').val());
        formData.append('TD200', $('#living_room_floor').val());
        formData.append('TD280', $('#living_room_volume_air').val());
        if ($('#living_room_wall').val() == '')
            formData.append('living_room_w', 'skip');

        if ($('#living_room_roof').val() == '')
            formData.append('living_room_r', 'skip');

        if ($('#living_room_floor').val() == '')
            formData.append('living_room_b', 'skip');

        formData.append('TD203', $('#kitchen_wall').val());
        formData.append('TD217', $('#kitchen_roof').val());
        formData.append('TD202', $('#kitchen_floor').val());
        formData.append('TD281', $('#kitchen_volume_air').val());
        formData.append('TD288', $('#kitchen_count').val());
        if ($('#kitchen_wall').val() == '')
            formData.append('kitchen_w', 'skip');

        if ($('#kitchen_roof').val() == '')
            formData.append('kitchen_r', 'skip');

        if ($('#kitchen_floor').val() == '')
            formData.append('kitchen_b', 'skip');

        formData.append('TD205', $('#toilet_wall').val());
        formData.append('TD218', $('#toilet_roof').val());
        formData.append('TD204', $('#toilet_floor').val());
        formData.append('TD282', $('#toilet_volume_air').val());
        formData.append('TD289', $('#toilet_count').val());
        if ($('#toilet_wall').val() == '')
            formData.append('toilet_w', 'skip');

        if ($('#toilet_roof').val() == '')
            formData.append('toilet_r', 'skip');

        if ($('#toilet_floor').val() == '')
            formData.append('toilet_b', 'skip');

        formData.append('TD207', $('#bathroom_wall').val());
        formData.append('TD219', $('#bathroom_roof').val());
        formData.append('TD206', $('#bathroom_floor').val());
        formData.append('TD283', $('#bathroom_volume_air').val());
        formData.append('TD290', $('#bathroom_count').val());
        if ($('#bathroom_wall').val() == '')
            formData.append('bathroom_w', 'skip');

        if ($('#bathroom_roof').val() == '')
            formData.append('bathroom_r', 'skip');

        if ($('#bathroom_floor').val() == '')
            formData.append('bathroom_b', 'skip');

        if ($('#one_floor_building').val() == 'no') {
            formData.append('TD208', $('#rest_room_floor').val());
            formData.append('TD209', $('#rest_room_wall').val());
            formData.append('TD220', $('#rest_room_roof').val());
            formData.append('TD284', $('#rest_room_volume_air').val());
            if ($('#rest_room_wall').val() == '')
                formData.append('rest_room_w', 'skip');

            if ($('#rest_room_roof').val() == '')
                formData.append('rest_room_r', 'skip');

            if ($('#rest_room_floor').val() == '')
                formData.append('rest_room_b', 'skip');

            formData.append('TD210', $('#hall_floor').val());
            formData.append('TD211', $('#hall_wall').val());
            formData.append('TD221', $('#hall_roof').val());
            formData.append('TD285', $('#hall_volume_air').val());
            if ($('#hall_wall').val() == '')
                formData.append('hall_w', 'skip');

            if ($('#hall_roof').val() == '')
                formData.append('hall_r', 'skip');

            if ($('#hall_floor').val() == '')
                formData.append('hall_b', 'skip');

            formData.append('TD212', $('#vestibule_floor').val());
            formData.append('TD213', $('#vestibule_wall').val());
            formData.append('TD223', $('#vestibule_roof').val());
            formData.append('TD286', $('#vestibule_volume_air').val());
            if ($('#vestibule_wall').val() == '')
                formData.append('vestibule_w', 'skip');

            if ($('#vestibule_roof').val() == '')
                formData.append('vestibule_r', 'skip');

            if ($('#vestibule_floor').val() == '')
                formData.append('vestibule_b', 'skip');


            formData.append('TD214', $('#pantry_floor').val());
            formData.append('TD215', $('#pantry_wall').val());
            formData.append('TD225', $('#pantry_roof').val());
            formData.append('TD287', $('#pantry_volume_air').val());
            if ($('#pantry_wall').val() == '')
                formData.append('pantry_w', 'skip');

            if ($('#pantry_roof').val() == '')
                formData.append('pantry_r', 'skip');

            if ($('#pantry_floor').val() == '')
                formData.append('pantry_b', 'skip');

        }

        formData.append('TD05', $("#AllTbody").find(".td05").text());
        formData.append('TD37', $("#AllTbody").find(".td37").text());
        formData.append('TD38', $("#AllTbody").find(".td38").text());
        formData.append('TD39', $("#AllTbody").find(".td39").text());

        formData.append('TD09', $("#AllTbody").find(".td09").text());
        formData.append('TD13', $("#AllTbody").find(".td13").text());
        formData.append('TD17', $("#AllTbody").find(".td17").text());
        formData.append('TD21', $("#AllTbody").find(".td21").text());
        formData.append('TD25', $("#AllTbody").find(".td25").text());
        formData.append('TD29', $("#AllTbody").find(".td29").text());
        formData.append('TD33', $("#AllTbody").find(".td33").text());

        formData.append('TD57', $("#AllTbody").find(".td57").text());
        formData.append('TD58', $("#AllTbody").find(".td58").text());

        formData.append('TD44', $("#AllTbody").find(".td44").text());
        formData.append('TD72', $("#AllTbody").find(".td72").text());
        formData.append('TD73', $("#AllTbody").find(".td73").text());
        formData.append('TD74', $("#AllTbody").find(".td74").text());
        formData.append('TD75', $("#AllTbody").find(".td75").text());
        formData.append('TD76', $("#AllTbody").find(".td76").text());
        formData.append('TD77', $("#AllTbody").find(".td77").text());
        formData.append('TD78', $("#AllTbody").find(".td78").text());

        formData.append('region', $("#selectRegion").val());
        formData.append('city', $("#selectCity").val());
        formData.append('building', $("#selectBuilding").val());

        formData.append('cookie_name', 'data1');
        $.ajax({
            url: '/writeCookie',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
            }
        });




        formData.append('data[TD05]', $("#AllTbody").find(".td05").text());
        formData.append('data[TD37]', $("#AllTbody").find(".td37").text());
        formData.append('data[TD38]', $("#AllTbody").find(".td38").text());
        formData.append('data[TD39]', $("#AllTbody").find(".td39").text());
        formData.append('data[TD09]', $("#AllTbody").find(".td09").text());
        formData.append('data[TD13]', $("#AllTbody").find(".td13").text());
        formData.append('data[TD17]', $("#AllTbody").find(".td17").text());
        formData.append('data[TD21]', $("#AllTbody").find(".td21").text());
        formData.append('data[TD25]', $("#AllTbody").find(".td25").text());
        formData.append('data[TD29]', $("#AllTbody").find(".td29").text());
        formData.append('data[TD33]', $("#AllTbody").find(".td33").text());
        formData.append('data[TD57]', $("#AllTbody").find(".td57").text());
        formData.append('data[TD58]', $("#AllTbody").find(".td58").text());
        formData.append('data[TD44]', $("#AllTbody").find(".td44").text());
        formData.append('data[TD72]', $("#AllTbody").find(".td72").text());
        formData.append('data[TD73]', $("#AllTbody").find(".td73").text());
        formData.append('data[TD74]', $("#AllTbody").find(".td74").text());
        formData.append('data[TD75]', $("#AllTbody").find(".td75").text());
        formData.append('data[TD76]', $("#AllTbody").find(".td76").text());
        formData.append('data[TD77]', $("#AllTbody").find(".td77").text());
        formData.append('data[TD78]', $("#AllTbody").find(".td78").text());

        formData.append('data[TD04]', $("#AllTbody").find(".td04").text());
        formData.append('data[TD06]', $("#AllTbody").find(".td06").text());
        formData.append('data[TD07]', $("#AllTbody").find(".td07").text());
        formData.append('data[TD08]', $("#AllTbody").find(".td08").text());
        formData.append('data[TD03]', $("#AllTbody").find(".td03").text());
        formData.append('data[TD40]', $("#AllTbody").find(".td40").text());
        formData.append('data[TD41]', $("#AllTbody").find(".td41").text());
        formData.append('data[TD42]', $("#AllTbody").find(".td42").text());
        formData.append('data[TD43]', $("#AllTbody").find(".td43").text());
        formData.append('data[TD45]', $("#AllTbody").find(".td45").text());
        formData.append('data[TD46]', $("#AllTbody").find(".td46").text());
        formData.append('data[TD47]', $("#AllTbody").find(".td47").text());
        formData.append('data[TD48]', $("#AllTbody").find(".td48").text());
        formData.append('data[TD49]', $("#AllTbody").find(".td49").text());
        formData.append('data[TD50]', $("#AllTbody").find(".td50").text());
        formData.append('data[TD51]', $("#AllTbody").find(".td51").text());
        formData.append('data[TD52]', $("#AllTbody").find(".td52").text());
        formData.append('data[TD53]', $("#AllTbody").find(".td53").text());
        formData.append('data[TD54]', $("#AllTbody").find(".td54").text());
        formData.append('data[TD55]', $("#AllTbody").find(".td55").text());
        formData.append('data[TD56]', $("#AllTbody").find(".td56").text());
        formData.append('data[TD59]', $("#AllTbody").find(".td59").text());
        formData.append('data[TD60]', $("#AllTbody").find(".td60").text());
        formData.append('data[TD61]', $("#AllTbody").find(".td61").text());
        formData.append('data[TD10]', $("#AllTbody").find(".td10").text());
        formData.append('data[TD11]', $("#AllTbody").find(".td11").text());
        formData.append('data[TD12]', $("#AllTbody").find(".td12").text());
        formData.append('data[TD82]', $("#AllTbody").find(".td82").text());
        formData.append('data[TD82_1]', $("#AllTbody").find(".td82_1").text());
        formData.append('data[TD82_2]', $("#AllTbody").find(".td82_2").text());
        formData.append('data[TD14]', $("#AllTbody").find(".td14").text());
        formData.append('data[TD15]', $("#AllTbody").find(".td15").text());
        formData.append('data[TD16]', $("#AllTbody").find(".td16").text());
        formData.append('data[TD83]', $("#AllTbody").find(".td83").text());
        formData.append('data[TD83_1]', $("#AllTbody").find(".td83_1").text());
        formData.append('data[TD83_2]', $("#AllTbody").find(".td83_2").text());
        formData.append('data[TD18]', $("#AllTbody").find(".td18").text());
        formData.append('data[TD19]', $("#AllTbody").find(".td19").text());
        formData.append('data[TD20]', $("#AllTbody").find(".td20").text());
        formData.append('data[TD84]', $("#AllTbody").find(".td84").text());
        formData.append('data[TD84_1]', $("#AllTbody").find(".td84_1").text());
        formData.append('data[TD84_2]', $("#AllTbody").find(".td84_2").text());
        formData.append('data[TD22]', $("#AllTbody").find(".td22").text());
        formData.append('data[TD23]', $("#AllTbody").find(".td23").text());
        formData.append('data[TD24]', $("#AllTbody").find(".td24").text());
        formData.append('data[TD85]', $("#AllTbody").find(".td85").text());
        formData.append('data[TD85_1]', $("#AllTbody").find(".td85_1").text());
        formData.append('data[TD85_2]', $("#AllTbody").find(".td85_2").text());
        formData.append('data[TD26]', $("#AllTbody").find(".td26").text());
        formData.append('data[TD27]', $("#AllTbody").find(".td27").text());
        formData.append('data[TD28]', $("#AllTbody").find(".td28").text());
        formData.append('data[TD86]', $("#AllTbody").find(".td86").text());
        formData.append('data[TD86_1]', $("#AllTbody").find(".td86_1").text());
        formData.append('data[TD86_2]', $("#AllTbody").find(".td86_2").text());
        formData.append('data[TD30]', $("#AllTbody").find(".td30").text());
        formData.append('data[TD31]', $("#AllTbody").find(".td31").text());
        formData.append('data[TD32]', $("#AllTbody").find(".td32").text());
        formData.append('data[TD87]', $("#AllTbody").find(".td87").text());
        formData.append('data[TD87_1]', $("#AllTbody").find(".td87_1").text());
        formData.append('data[TD87_2]', $("#AllTbody").find(".td87_2").text());
        formData.append('data[TD34]', $("#AllTbody").find(".td34").text());
        formData.append('data[TD35]', $("#AllTbody").find(".td35").text());
        formData.append('data[TD36]', $("#AllTbody").find(".td36").text());
        formData.append('data[TD88]', $("#AllTbody").find(".td88").text());
        formData.append('data[TD88_1]', $("#AllTbody").find(".td88_1").text());
        formData.append('data[TD88_2]', $("#AllTbody").find(".td88_2").text());

        formData.append('cookie_name', "tab_1");
        $.ajax({
            url: '/writeLayer',
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
                console.log(result);
                if(
                typeof runAutoLoadData != 'undefined'
                 && typeof logged != 'undefined'
                 ) {
                     $('.header-menu').addClass('deactivate_link');
                     $('.passing-test-content-sequence').addClass('deactivate-block');

                    $('.deactivate-block .blank p a').click(function(){
                        let disableMessage = "<div class='info-text_disabled_message'> Подождите, идёт загрузка данных в<br> фоновом режиме </div>";
                        $(this).parent().append(disableMessage);
                        setTimeout(function(){
                            $('.info-text_disabled_message').remove()
                        }, 4000);
                        return false;
                    });

                    $('.deactivate_link').click(function(){
                         let disableMessage = "<div class='info-text_disabled_message-header'> Подождите, идёт загрузка данных в<br> фоновом режиме </div>";
                         $(this).parent().append(disableMessage);
                         setTimeout(function(){
                             $('.info-text_disabled_message-header').remove()
                         }, 4000);
                         return false;
                     });

                    let secretWalls = $('<iframe  src="/walls?runAutoLoadData=2"></iframe>');
                    $('body').append(secretWalls);
                    secretWalls.hide()

                    var myVar = setInterval(myTimer, 1000);
                    let num = 0;
                    function myTimer() {
                        console.log(num)
                        console.log(localStorage.getItem("activate_buttons"))
                        if(num == 40 || localStorage.getItem("activate_buttons") == 'activate') {
                            localStorage.clear();
                            clearInterval(myVar);
                            $('.deactivate_link, .deactivate-block .blank p a').unbind();
                            $('.header-menu').removeClass('deactivate_link');
                            $('.passing-test-content-sequence').removeClass('deactivate-block');
                            if(num > 39) {
                                alert('что то пошло не так. данные не до конца загрузились. пройдитесь по вкладкам.')
                            }

                        }
                        num++;
                    }

                }
            }
        });

        var formData_cookie = new FormData();
        formData_cookie.append('city', $("#selectCity").val());
        formData_cookie.append('building', $("#selectBuilding").val());
        $.ajax({
            url: '/writeCookie_index',
            type: 'POST',
            data: formData_cookie,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
            }
        });
    }, 500);

}

// $('#send').click(function(){});

$('#send').click(send_city);


function send_window(auto = '') {


    if ($('.alert-danger-city').length != 0) {
        alert('Ошибка!')
        return false
    }

    $('#admin_block').css('display', 'block');
    var formData = new FormData();

    formData.append('TD182', $('#window_square').val());
    formData.append('TD183', $('#term_resistance').val());
    formData.append('TD184', $('#window_type').val());
    formData.append('TD186', $('#lantern_square').val());
    formData.append('TD188', $('#lantern_term').val());

    $('.win_basic_data').css('display', 'block');

    if ($('#window_square').val() != '' || $('#term_resistance').val() != '' && $('#window_type').val() != '') {
        $('.content_block_wind').css('display', 'block');
    }

    if ($('#lantern_square').val() != '' || $('#lantern_term').val() != '') {
        $('.content_block_lantern').css('display', 'block');
    }

    formData.append('cookie_name', 'data5');
    $.ajax({
        url: '/calc_window',
        type: 'POST',
        data: formData,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {
        },
        success: function (result) {
            var isAccept = 'true';

            if (result == 'error') {
                alert('Ошибка! Не заполнены данные предыдущих вкладок ')
                return false;
            }
            var content = '';
            var obj = JSON.parse(result)
            if ($('#term_resistance').val() != '') {

                if (obj.TD145WI >= obj.TD57) {
                    content = "<p class='indicator' style='background-color: green;width: 50px;'></p>" +
                        " <span class='content_3_2'>Фактическое теплосопротивление конструкции (" + obj.TD145WI + ") больше, чем нормируемое (" + obj.TD57 + ")" +
                        " (м2·°С)/Вт, значит конструкции окон (дверей) <span class='green'>СООТВЕТСТВУЮТ</span> нормативным требованиям. </span>";
                    $("#content_block .content_info_3_2").html(content);

                } else {
                    content = "<p class='indicator' style='background-color: red;width: 50px;'></p>" +
                        "<span class='content_3_2'> Фактическое теплосопротивление конструкции (" + obj.TD145WI + ") меньше, чем нормируемое" +
                        "  (" + obj.TD57 + ") (м2·°С)/Вт, значит конструкции окон (дверей) <span class='red'>НЕ СООТВЕТСТВУЕТ</span> нормативным требованиям. " +
                        "Совет: Необходимо подобрать оконные (дверные) конструкции с повышенными теплозащитными характеристиками. </span>";
                    $("#content_block .content_info_3_2").html(content);
                    isAccept = 'false';

                }
                if (obj.TD115WI <= obj.TD117) {
                    content = "<p class='indicator' style='background-color: red;width: 50px;'></p>" +
                        "<span class='content_3_3'> Температура внутренней поверхности окон (и дверей)  (" + obj.TD115WI + ") °С " +
                        "меньше температуры точки росы" +
                        " в помещении (" + obj.TD117 + ") °С, поэтому будет образовываться конденсат." +
                        " Такая конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ</span> " +
                        "нормативным требованиям. Совет: необходимо применить окна (и двери) с повышенными теплозащитными характеристиками. </span>";
                    $("#content_block .content_info_3_3").html(content);
                    isAccept = 'false';

                } else {
                    content = "<p class='indicator' style='background-color: green;width: 50px;'></p>" +
                        "<span class='content_3_3'> Температура внутренней поверхности окон (и дверей)  (" + obj.TD115WI + ")  °С больше температуры точки росы" +
                        " в помещении (" + obj.TD117 + ") °С, поэтому образования конденсата не будет." +
                        " Такая конструкция <span class='green'>СООТВЕТСТВУЕТ</span> " +
                        "нормативным требованиям. </span>";
                    $("#content_block .content_info_3_3").html(content);

                }
                $('.schedule_block_w').fadeIn();
                $('#schedule_TD05w').text(" Температура помещения \n" + obj.TD05 + " ");
                $('#schedule_TD37w').text(obj.TD37 + "  температура на улице");
                $('#schedule_TD115w').text(" " + obj.TD115WI + "  Температура на внутренней поверхности окна");
            }
            if ($('#lantern_term').val() != '') {

                if (obj.TD145La >= obj.TD58) {
                    content = "<p class='indicator' style='background-color: green;width: 50px;'></p>" +
                        "<span class='content_3_5'> Фактическое теплосопротивление конструкции (" + obj.TD145La + ") (м2·°С)/Вт больше, чем нормируемое" +
                        "(" + obj.TD58 + ") (м2·°С)/Вт, значит конструкции фонарей <span class='green'>СООТВЕТСТВУЮТ</span> нормативным требованиям. </span>";
                    $("#content_block .content_info_3_5").html(content);

                } else {
                    content = "<p class='indicator' style='background-color: red;width: 50px;'></p>" +
                        "<span class='content_3_5'> Фактическое теплосопротивление конструкции (" + obj.TD145La + ") (м2·°С)/Вт меньше," +
                        " чем нормируемое (" + obj.TD58 + ")" +
                        "(м2·°С)/Вт, значит конструкции фонарей <span class='red'>НЕ СООТВЕТСТВУЕТ</span> нормативным требованиям. Совет: Необходимо подобрать" +
                        "конструкции фонарей (мансардных окон) с повышенными теплозащитными характеристиками. </span>";
                    $("#content_block .content_info_3_5").html(content);
                    isAccept = 'false';

                }

                if (obj.TD115La <= obj.TD117) {
                    content = "<p class='indicator' style='background-color: red;width: 50px;'></p>" +
                        "<span class='content_3_6'> Температура внутренней поверхности фонарей (мансардных окон)" +
                        "  (" + obj.TD115La + ") °С  меньше температуры точки росы в помещении (" + obj.TD117 + ") °С, поэтомубудет образовываться " +
                        "конденсат. Такая конструкция <span class='red'>НЕ СООТВЕТСТВУЕТ</span> нормативным требованиям. Совет: необходимоприменить" +
                        " фонари (мансардные окна) с повышенными теплозащитными характеристиками. </span>";
                    $("#content_block .content_info_3_6").html(content);
                    isAccept = 'false';

                } else {
                    content = "<p class='indicator' style='background-color: green;width: 50px;'></p>" +
                        "<span class='content_3_6'> Температура внутренней поверхности фонарей (мансардных окон)  " +
                        "(" + obj.TD115La + ") °С  больше температуры" +
                        " точки росы в помещении (" + obj.TD117 + ") °С, поэтому образования конденсата не будет." +
                        " Такая конструкция <span class='green'>СООТВЕТСТВУЕТ</span>" +
                        "нормативным требованиям. </span>";
                    $("#content_block .content_info_3_6").html(content);
                }
                $('#schedule_TD05l').text(" Температура помещения \n" + obj.TD05 + " ");
                $('#schedule_TD37l').text(obj.TD37 + "  Температура на улице");
                $('#schedule_TD115l').text(" " + obj.TD115La + " Температура на внутренней поверхности окна");
                $('.schedule_block_l').fadeIn();
            }

            $('#content_block').fadeIn();
            $('#admin_block').fadeIn();


            $('#input_TD182').val($('#window_square').val());
            $('#input_TD183').val($('#term_resistance').val());
            $('#input_TD184').val($('#window_type').val());
            $('#input_TD186').val($('#lantern_square').val());
            $('#input_TD188').val($('#lantern_term').val());

            $('#input_TD145WI').val(obj.TD145WI);
            $('#input_TD57').val(obj.TD57);
            $('#input_TD115WI').val(obj.TD115WI);
            $('#input_TD117').val(obj.TD117);
            $('#input_TD145La').val(obj.TD145La);
            $('#input_TD58').val(obj.TD58);
            $('#input_TD115La').val(obj.TD115La);
            $('#input_TD117La').val(obj.TD117);

            write_accepted_5(isAccept);

            var formData_estimate = new FormData();
            formData_estimate.append('estimate[volume][win]', $('#window_square').val());
            formData_estimate.append('estimate[volume][lan]', $('#lantern_square').val());
            $.ajax({
                url: '/wind_estimate',
                type: 'POST',
                data: formData_estimate,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {

                },
                success: function (result) {
                saveLayersInfo();
                    // console.log(result);
                    $('#for-estimate').html(result);
                    $('#for-estimate').show();
                }
            });

        }
    });
    function saveLayersInfo(obj) {
        var formData_data = new FormData();
        formData_data.append('TD182', $('#window_square').val());
        formData_data.append('TD183', $('#term_resistance').val());
        formData_data.append('TD184', $('#window_type').val());
        formData_data.append('TD186', $('#lantern_square').val());
        formData_data.append('TD188', $('#lantern_term').val());

        // formData_data.append('window_type_chamber', $('#window_type_chamber').val());
        // formData_data.append('window_type_distance', $('#window_type_distance').val());
        // formData_data.append('window_type_filling', $('#window_type_filling').val());

        formData_data.append('cookie_name', "tab_5");

        formData_data.append('data[TD182]', $('#window_square').val());
        formData_data.append('data[TD183]', $('#term_resistance').val());
        formData_data.append('data[TD184]', $('#window_type').val());
        formData_data.append('data[TD186]', $('#lantern_square').val());
        formData_data.append('data[TD188]', $('#lantern_term').val());
        formData_data.append('TD145WI', $('#input_TD145WI').val());
        formData_data.append('TD145La', $('#input_TD145La').val());

        setTimeout(function(){
            formData_data.append('data[content_block]', $('#content_block').html());
            formData_data.append('data[estimate]', $("#for-estimate").html());

            $.ajax({
                url: '/writeLayer',
                type: 'POST',
                data: formData_data,
                cache: false,
                processData: false,
                contentType: false,
                beforeSend: function () {
                    if (auto != 'auto') {
                        $('#waiting').css('display', 'block');
                    }
                },
                success: function (result) {
                    console.log(result);
                    $('#waiting').css('display', 'none');
                    if(
                        typeof runAutoLoadData != 'undefined'
                        && typeof logged != 'undefined'
                    ) {
                        let secretHeating = $('<iframe  src="/heating?runAutoLoadData=6"></iframe>', { css: { 'display': 'none' }});
                        $('body').append(secretHeating);
                        secretHeating.hide();
                    }
                }
            });
        }, 700);

        $.ajax({
            url: '/writeCookie',
            type: 'POST',
            data: formData_data,
            cache: false,
            processData: false,
            contentType: false,
            beforeSend: function () {

            },
            success: function (result) {
            }
        });
    }
}


$('#send_wind').click(send_window);

function write_accepted_5(status = 'false') {
    var formData_accept = new FormData();
    formData_accept.append('cookie_name', 'is_accepted_5');
    formData_accept.append('cookie_data', status);

    $.ajax({
        url: '/writeCookieSimple',
        type: 'POST',
        data: formData_accept,
        cache: false,
        processData: false,
        contentType: false,
        beforeSend: function () {

        },
        success: function (result) {
        }
    });
}

// =============================================================================================
var td05_temp = $('#AllTbody').find('.td05').text();
var td37_temp = $('#AllTbody').find('.td37').text();
var td38_temp = $('#AllTbody').find('.td38').text();

$('#inside_temp').html(td05_temp + "C<sup>о</sup>");
$('#outside_temp_td37').html(td37_temp + "C<sup>о</sup>");
$('#outside_temp_td38').html(td38_temp + "C<sup>о</sup>");



function proverka(val) {
    var reg = [/^\D+/, /[^.,\d]+/g, /[\.,]+/, /(\d+\.\d{2}).*$/],
        ch = val.replace(reg[0], '').replace(reg[1], '').replace(reg[2], '.').replace(reg[3], '$1');
    return ch;
};

$(window).scroll(function () {
    if ($(this).scrollTop() > 100) {
        if ($('#upbutton').is(':hidden')) {
            $('#upbutton').css({opacity: 1}).fadeIn('slow');
        }
    } else {
        $('#upbutton').stop(true, false).fadeOut('fast');
    }
});
$('#upbutton').click(function () {
    $('html, body').stop().animate({scrollTop: 0}, 300);
});


var map = new Map([
    [1, 'Выберите регион РФ, в котором или рядом с которым находится ваш объект. '],
    [2, 'Выберите ближайший к вашему объекту город из списка городов области.'],
    [3, 'Выберите подходящий тип здания.'],
    [4, 'Ответьте ДА, если у вас коттедж – одноквартирное жилое здание. Нет, если другие типы жилых зданий (многоквартирные).'],
    [5, 'Поставьте цифру, сколько этажей у вас в здании.'],
    [6, 'Укажите цифрой предполагаемой количество постоянно находящихся (проживающих) в здании людей.'],
    [7, ''],
    [8, 'Укажите площадь помещений (в квадратных метрах) по полу  (включая площадь опирания на пол внутренних стен) ' +
    'каждого помещения.  Полом считается только отапливаемый этаж (первый этаж, если подвал неотапливаемый) ' +
    'или подвал, если он отапливаемый. Если в списке нет названия иных помещений, они считаются условно «Жилыми».' +
    'Подсчитывается (суммируется) и вносится для расчёта общая площадь всех помещений одного типа. Указываются' +
    ' площади только тех помещений, которые находятся на нижней плоскости (полу) тёплого контура здания. На верхних ' +
    'этажах, если пол помещений находится внутри тёплого контура здания, площадь полов не засчитывается.\n' +
    'Если данный тип помещения не имеет такого условия, то поле оставьте пустым.'],
    [9, 'Укажите внутреннюю площадь (в квадратных метрах) наружных стен (фасада),  включая площадь примыкания внутренних ' +
    'стен к наружным, а также включая площадь откосов окон и дверей. \n' +
    'Если помещение не имеет наружной стены тёплого контура здания (выходящую на улицу), то такая площадь не засчитывается.\n' +
    'Если в списке нет названия иных помещений, они считаются условно «Жилыми». Подсчитывается (суммируется) и вносится для ' +
    'расчёта общая площадь всех помещений одного типа.\n' +
    'Если данный тип помещения не имеет такого условия, то поле оставьте пустым.'],
    [10, 'Укажите площадь потолка (в квадратных метрах) в помещениях, включая площадь примыкания внутренних стен к потолку,' +
    ' которые выше имеют кровлю или неотапливаемый чердак. \n' +
    'Если в списке нет названия иных помещений, они считаются условно «Жилыми». Подсчитывается (суммируется) и вносится ' +
    'для расчёта общая площадь всех помещений одного типа.\n' +
    'Если данный тип помещения не имеет такого условия, то поле оставьте пустым.\n'],
    [11, 'Укажите внутренние объёмы (в кубических метрах воздуха) помещений в соответствии с их типом. Подсчитывается ' +
    '(суммируется) и вносится для расчёта общий объём всех помещений одного типа. Если в списке нет названия иных помещений, ' +
    'они считаются условно «Жилыми». '],
    [12, 'Внесите количество помещений одного указанного типа, которые имеются в здании. Ставится цифра, целое число.' +
    ' К примеру, в здании 3 туалета и 2 кухни, соответственно проставляются цифры 2 и 3 в окошках напротив типов этих помещений.'],
    [13, 'В расчёте участвуют только те помещения, которые входят в тёплый контур здания. Этот контур необходимо условно' +
    ' представлять целыми плоскостями (без вычета каких-либо участков, за исключением окон и дверей – их площадь указывается' +
    ' в соответствующей вкладке).'],
    [14, 'Введите количество квадратных метров наружной площади всего фасада здания, с учётом откосов окон и дверей.'],
    [15, "С помощью выпадающего меню выберите материал, который будет применён в конструкции стены. Здесь слои материалов " +
    "располагаются сверху-вниз «из тёплого помещения наружу, на улицу». Для выбора и правильного расчёта материалы разделены на" +
    " группы. В группе «Добавить слой материала» находятся все материалы для расчёта. В группе «Добавить слой теплоизоляции» - " +
    "утеплители, даже не очень эффективные, в том числе воздушные прослойки и ячеистые бетоны.<br>" +
    "Если у вас в стене есть слой – каркасная конструкция или кладка из кирпича или блоков, то необходимо нажать" +
    " на соответствующий тип и там выбрать соответствующую пару материалов с указанием размеров кладки или каркаса.<br><br>" +
    "В блоке, выделенном серым цветом, сделайте выбор материалов для <b>несущей стены</b> конструкции. "],
    [16, "Выберите тип конструкции стены – технологию, по которой строится стена и выполняется её отделка. Если нет " +
    "определённой технологии, стена просто штукатурится и красится, или такой отделки нет в меню, тогда выберите первые " +
    "два пункта выпадающего меню в зависимости от наличия утеплителя."],
    [17, "С помощью выпадающего меню выберите материал, который будет применён в кровельной конструкции. Здесь слои " +
    "материалов располагаются сверху-вниз «снаружи с улицы в тёплое помещение». Для выбора и правильного расчёта " +
    "материалы разделены на группы. В группе «Добавить слой материала» находятся все материалы для расчёта. В группе " +
    "«Добавить слой теплоизоляции» - утеплители, даже не очень эффективные, в том числе воздушные прослойки и ячеистые бетоны.<br>" +
    "Если у вас в кровле есть слой – каркасная конструкция (как правило это стропиловка) или кладка из кирпича или " +
    "блоков, то необходимо нажать на соответствующий тип и там выбрать соответствующую пару материалов с указанием " +
    "размеров кладки или каркаса. <br>" +
    "В блоке, выделенном серым цветом, сделайте выбор материалов для <b>несущей конструкции кровли.</b> "],
    [18, "Введите количество квадратных метров наружной площади всей кровли здания."],
    [19, "Выберите первый пункт, если потолок гладкий или с выступающими ребрами при отношении высоты h ребер" +
    " к расстоянию a, между гранями соседних ребер h / a ≤ 0,3 <br>" +
    "Если потолок с выступающими ребрами при отношении h / a ≥ 0,3, тогда выберите второй пункт."],
    [20, "Выберите тип конструкции кровли или перекрытия – технологию, по которой строится кровля (перекрытие " +
    "последнего этажа здания)  и выполняется отделка. "],
    [21, "С помощью выпадающего меню выберите материал, который будет применён в конструкции пола. Здесь слои материалов " +
    "располагаются сверху-вниз «из тёплого помещения наружу (условно) на улицу (или в грунт)». Для выбора и правильного " +
    "расчёта материалы разделены на группы. В группе «Добавить слой материала» находятся все материалы для расчёта." +
    " В группе «Добавить слой теплоизоляции» - утеплители, даже не очень эффективные, в том числе воздушные прослойки " +
    "и ячеистые бетоны.<br><br>" +
    "Если у вас в полах есть слой – каркасная конструкция или кладка из кирпича или блоков, то необходимо нажать на " +
    "соответствующий тип и там выбрать соответствующую пару материалов с указанием размеров кладки или каркаса. <br>" +
    "В блоке, выделенном серым цветом, сделайте выбор материалов для <b>несущей конструкции пола.</b> "],
    [22, "Введите цифру -  количество квадратных метров наружной площади всего пола."],
    [23, "Выберите тип пола – расположение конструкции и технологию, по которой строится и выполняется отделка пола. "],
    [24, "Введите количество квадратных метров всех окон и дверей (балконных, входных)."],
    [25, "Введите значение теплосопротивления оконной конструкции. Эти данные необходимо получить от производителя окон. <br>" +
    "В случае, если коэффициенты теплосопротивления окон производитель не предоставил, то ниже в выпадающих меню выберите" +
    " параметры ваших окон по внешним признакам. После выбора в этом окне коэффициент просчитывается автоматически."],
    [26, "Выберите тип оконного переплёта. Обычные современные пластиковые или деревянные окна со стеклопакетом – " +
    "это одинарный переплёт. <br>" +
    "Двойной и тройной переплёт – это окна, с советских времён с так называемыми «двойной» или «тройной рамой»"],
    [27, "Введите количество квадратных метров всех фонарей (мансардных окон)."],
    [28, "Введите значение теплосопротивления оконной конструкции. Эти данные необходимо получить от производителя" +
    " фонарей (мансардных окон). <br>" +
    "В случае, если коэффициенты теплосопротивления производитель не предоставил, то ниже в выпадающих меню выберите" +
    " параметры ваших фонарей по внешним признакам. После выбора в этом окне коэффициент просчитывается автоматически."],
    [29, "Выберите тип топлива, на котором работает система отопления здания"],
    [30, "Выберите тип оборудования для приготовления пищи и нагрева воды. Если плита и водонагреватель вместе или" +
    " хотя бы какой-то из этих приборов – газовый, то выберите «на газе». В других случаях – «на электричестве»."],
    [31, "Выберите тип вентиляции – естественная, если организован приток и отток естественным (гравитационным)" +
    " способом. И «механическая» - если система вентиляции сделана принудительной."],
    [32, "Выберите наличие рекуператора. Если он есть, то в паспорте производитель указывает коэффициент эффективности " +
    "рекуператора и в выпадающем меню следует выбрать указанный в паспорте изделия коэффициент."],
    [33, "33"],
    [34, "34"],
    [35, "35"],
    [36, "36"],
    [37, "37"],
    [38, "38"],
    [39, "39"],
    [40, "40"],
    [41, "41"],
    [42, "42"],
    [43, "43"],
    [44, "44"],
    [45, "45"],
    [46, "46"],
    [47, ""],
    [48, ""],
    [49, ""],
    [50, ""],

]);


$('.help-info').click(function () {
    for (let kv of map) {
        if ($(this).data("id") === kv[0]) {
            $('#modal-body').html(kv[1]);
        }
    }
});

function clearContent() {
    $('#modal-body').html('');
}

console.log($("#previewVideo").find('iframe').attr('src'));
let url = $("#previewVideo").find('iframe').attr('src');
$("#previewModal").on('hide.bs.modal', function(){
    $("#previewVideo").find('iframe').attr('src', '');
});
$("#previewModal").on('show.bs.modal', function(){
    $("#previewVideo").find('iframe').attr('src', url);
});

$('#dont-show-checkbox').change(function(){
    if($(this).is(':checked')) {
        $.ajax({
                url: '/show-preview-video',
                method: 'GET',
                data: {"show": "not-show"},
                beforeSend: function () {
                    $('#waiting').css('display', 'block');
                }
            }).done(function (data) {
            console.log(data)
                $('#waiting').css('display', 'none');
            });
    } else {
     $.ajax({
        url: '/show-preview-video',
        method: 'GET',
        data: {"show": 1},
        beforeSend: function () {
            $('#waiting').css('display', 'block');
        }
        }).done(function (data) {
                        console.log(data)

            $('#waiting').css('display', 'none');
        });
    }
})