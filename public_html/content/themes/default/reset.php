<?php $this->theme->header(); ?>
<?php

$message = '';
if (!empty($_GET['reset']) && isset($_GET['reset'])) {
    $reset = $_GET['reset'];

    $pieces = explode("+", $reset);

    if (!isset($pieces[0]) || !isset($pieces[2])) {
        echo "
<div class='container activity-message'>
    <div class='row'>
        <div class='activity'>
            <div class='text-center alert alert-danger'>
                Вы хотите взломать нас? о_O
            </div>
        </div>
    </div>
</div>
  ";
        $this->theme->footer();
        die();
    }
    if ($pieces[0] == $pieces[2]) {

        $time = time();
        $time_get = $pieces[2];

        $interval = $time - $time_get;
        if ($interval >= 1800) {
            echo "
<div class='container activity-message'>
    <div class='row'>
        <div class='activity'>
            <div class='text-center alert alert-info'>
               Этот запрос уже не действителен. 30 минут истекли, попробуйте снова
            </div>
        </div>
    </div>
</div>
  ";
            $this->theme->footer();
            die();
        }

        $secret = $pieces[1];

        $params['col'] = 'secret';
        $params['val'] = $secret;
        $user = User::getUserBy($params);
    } else {
        echo "
<div class='container activity-message'>
    <div class='row'>
        <div class='activity'>
            <div class='text-center alert alert-danger'>
                Вы хотите взломать нас? о_O
            </div>
        </div>
    </div>
</div>
  ";
        $this->theme->footer();
        die();

    }
    if (!empty($user)) {
        $user = $user[0];
        $id = $user->id;
        $user = get_object_vars($user);
    } else {
        echo "
<div class='container activity-message'>
    <div class='row'>
        <div class='activity'>
            <div class='text-center alert alert-danger'>
                Что-то пошло не так. :(
            </div>
        </div>
    </div>
</div>
  ";
        $this->theme->footer();
        die();
    }

    echo '
        <div class="container col-md-6 col-md-offset-3" style="height: 239px;">
            <div class="row">
                <div class="activity">
                    <div class="col-md-12">
                        <div class="well well-sm bs-well">
                            <fieldset>
                                <form class="form-horizontal" method="POST" action="/update/pass" onsubmit="return passConfirm()">
            
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <input type="hidden" name="key" value="' . $user['secret'] . '">
                                            <input type="password" class="form-control" id="sPass" name="password" value=""
                                                   placeholder="Новый пароль"/>
                                        </div>
                                    </div>
                                    <div class="col-xs-12">
                                        <div class="form-group">
                                            <input type="password" class="form-control" id="sRePass" name="confirm-password"
                                                   value=""
                                                   placeholder="Введите пароль снова"/>
                                        </div>
                                    </div>
                                    <div class="text-center col-xs-12">
                                        <button type="submit" class="btn btn-primary">
                                            Сменить
                                        </button>
                                    </div>
                            </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        ';
    $this->theme->footer();
    die();
} else {
    echo "        
<div class='container activity-message'>
    <div class='row'>
        <div class='activity'>
            <div class='text-center alert alert-danger'>
                Что-то пошло не так. :(
            </div>
        </div>
    </div>
</div>
  ";
    $this->theme->footer();
    die();
}

?>

