<?php $this->theme->header(); ?>
<?php
require  "part/login.php";
require  "part/if_not_login.php";


$city_cookie = User::getCookie('city');
$build_cookie = User::getCookie('building');

$error = '';
if($city_cookie == null){
    $error .= " вы не выбрали город <br> ";
}
if($build_cookie == null){
    $error .= " вы не выбрали тип здания <br>";
}

$arr_tab = [
  1=>'Ввод начальных данных',
  2=>'Конструкция стен',
  3=>'Конструкция кровли',
  4=>'Конструкция пола',
  5=>'Окна - Фонари',
];

for($i=1;$i<=5;$i++){
    $dat = "data$i";
    $$dat = User::getCookie("data$i");
    if($$dat == null){
        $error .= " вкладка '$arr_tab[$i]' не заполнена <br>";
    }
}


require "part/calc_title.php";

$tab_cookie = User::getCookie('tab_6');

if ($tab_cookie != null) {
    $data = (json_decode($tab_cookie));
}

//debug($data);

?>

<!-- Main Content -->
<section class="pt-l-ad">
    <div class="container">
<!--    <div class="" style="display: flex">-->
        <?php $this->theme->sidebar(); ?>

        <div class="inner-surface">
            <?php if(isset($error) && $error != ''):?>
                <div class="alert alert-danger alert-dismissible">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Ошибка! </strong> <?=$error;?>
                </div>
            <?php endif; ?>

            <div class="additional-data windows" style="width: 100%">
                <div class="additional-data-title">
                    <div class="passing-test-img">
                        <img src="<?= Theme::getUrl() ?>/img/edit.png" alt="Прохождение">
                    </div>
                    <h3>Ведите данные по отоплению и вентиляции:</h3>
                </div>

                <form action="#" class="additional-data-form">
                    <div class="resident" style="position:relative;">
                        <p>тип энергии для отопления</p>
                        <div class="help-info" data-id="29"
                             style="position: absolute; left: 175px; top: 8px;"
                             data-toggle="modal" data-target="#myModalHelp"></div>
                        <div class="input-label-info">
                            <div class="width_90">
                                <select name="" id="energy_type" class="form-control">
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- природный газ') echo "selected"; ?>> - природный газ</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- сжиженный газ') echo "selected"; ?>> - сжиженный газ</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- дизтопливо') echo "selected"; ?>> - дизтопливо</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- электричество') echo "selected"; ?>> - электричество</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- дрова') echo "selected"; ?>> - дрова</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- уголь каменный') echo "selected"; ?>> - уголь каменный</option>
                                    <option <?php if ($tab_cookie != null && $data->TD173 == '- пеллеты') echo "selected"; ?>> - пеллеты</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="resident" style="position: relative;">
                        <p>Тип оборудования нагрева (плита,
                            водонагреватель…)</p>
                        <div class="help-info" data-id="30"
                             style="position: absolute; left: 335px; top: 8px;"
                             data-toggle="modal" data-target="#myModalHelp"></div>
                        <div class="input-label-info">
                            <div class="width_90">
                                <select name="" id="heater_type" class="form-control">
                                    <option value="100" <?php if ($tab_cookie != null && $data->TD174 == '100') echo "selected"; ?>> - на газе</option>
                                    <option value="60" <?php if ($tab_cookie != null && $data->TD174 == '60') echo "selected"; ?>> - на электричестве</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="resident" style="position: relative;">
                        <p>Тип вентиляции (приток и удаление
                            воздуха)</p>
                        <div class="help-info" data-id="31"
                             style="position: absolute; left: 285px; top: 8px;"
                             data-toggle="modal" data-target="#myModalHelp"></div>
                        <div class="input-label-info">
                            <div class="width_90">
                                <select name="" id="vent_type" class="form-control">
                                    <option <?php if ($tab_cookie != null && $data->TD175 == '- с естественным') echo "selected"; ?>> - с естественным</option>
                                    <option <?php if ($tab_cookie != null && $data->TD175 == '- с механическим') echo "selected"; ?>> - с механическим</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="individual" style="position: relative;">
                        <p>Наличие рекуператора</p>
                        <div class="help-info" data-id="32"
                             style="position: absolute; left: 150px; top: 8px;"
                             data-toggle="modal" data-target="#myModalHelp"></div>
                        <div class="width_90">
<!--                            <input type="range" min="40" max="90" step="5" class="form-control" id="recuperator" value="100">-->
                            <select name="" class="form-control" id="recuperator">
                                <option value="100" <?php if ($tab_cookie != null && $data->TD190 == '100') echo "selected"; ?>> НЕТ</option>
                                <option value="90" disabled <?php if ($tab_cookie != null && $data->TD190 == '90') echo "selected"; ?>> ДА</option>
                            </select>
                        </div>
                    </div>

                    <div class="individual" id="recuperator_isset">
                        <p>Коэффициент эффективности рекуператора" (внести паспортные данные прибора)</p>
                        <div class="width_90">
                            <select name="" id="recuperator_value" class="form-control">
                                <option>90</option>
                                <option>85</option>
                                <option>80</option>
                                <option>75</option>
                                <option>70</option>
                                <option>65</option>
                                <option>60</option>
                                <option>55</option>
                                <option>50</option>
                                <option>45</option>
                                <option>40</option>
                            </select>
                        </div>
                    </div>
                    <br>
                    <br>
                    <div class="btn_block button_block_heating">
                        <div class="btn-input">
                            <a href="javascript: void(0)" id="calculate_heating"><p>Ввод</p></a>
                            <p>Если ввод данных завершён, нажмите кнопку ВВОД</p>
                        </div>
                        <div class="btn-clear">
                            <a id="clear" href="/reset_cookie?tab=6"><p>ОЧИСТИТЬ ВКЛАДКУ</p></a>
                            <p>Очистка результатов в данной вкладке</p>
                        </div>
                        <div class="btn-clear-all">
                            <a  href="/reset_cookie?tab=all"><p>ОЧИСТИТЬ ВЕСЬ РАСЧЁТ</p></a>
                            <p>Очистка всего расчёта</p>
                        </div>
                    </div>

                </form>
            </div>


            <div class="core-data-conclusion-admin heating_basic_data">
                <!-- Основные данные -->
                <div class="core-data" style="display: block">
                    <div class="core-data-title">
                        <div class="core-data-img">
                            <img src="<?= Theme::getUrl() ?>/img/data.png" alt="Нормативы">
                        </div>
                        <h3>Результаты расчёта тепловых поступлений и потерь:</h3>
                    </div>
                    <div class="core-data-content" id="table_heating">

                        <table class="table margin-top">
                            <thead>
                            <tr>
                                <th>Зоны тепловых потерь здания:</th>
                                <th>Одномоментные теплопотери, Вт:</th>
                                <th>Теплопотери за год, кВт/час</th>
                                <th>Структура тепловых поступлений, в % к общим</th>
                            </tr>
                            </thead>
                            <tbody id="term_out">
<!--                            <tr>-->
<!--                                <td>Конструкции стен</td>-->
<!--                                <td></td>-->
<!--                                <td>теплопотери, Вт</td>-->
<!--                                <td>теплопотери за год кВт/ч</td>-->
<!--                                <td>в % к общим</td>-->
<!--                            </tr>-->
                            <!-- ==============================================-->
                            <tr>
                                <td>Конструкции стен</td>
                                <td class="TD230"></td>
                                <td class="TD231"></td>
                                <td class="TD321"></td>
                            </tr>
                            <tr>
                                <td>Конструкции кровли</td>
                                <td class="TD240"></td>
                                <td class="TD241"></td>
                                <td class="TD322"></td>
                            </tr>
                            <tr>
                                <td>Конструкции полов</td>
                                <td class="TD250"></td>
                                <td class="TD251"></td>
                                <td class="TD323"></td>
                            </tr>
                            <tr>
                                <td>Окна</td>
                                <td class="TD260"></td>
                                <td class="TD261"></td>
                                <td class="TD324"></td>
                            </tr>
                            <tr>
                                <td>Фонари (мансардные окна)</td>
                                <td class="TD270"></td>
                                <td class="TD271"></td>
                                <td class="TD325"></td>
                            </tr>
                            <tr>
                                <td>Вентиляция здания</td>
                                <td class="TD310"></td>
                                <td class="TD315"></td>
                                <td class="TD326"></td>
                            </tr>

                            </tbody>
                        </table>


                        <table class="table margin-top">
                            <thead id="term_out_result">
                            <tr>
                                <th><b>Всего теплопотери:</b></th>
                                <th style="font-size: 12px; color: grey">одномоментные <span class="TD312" style="font-size: 16px; color: black"></span> Вт</th>
                                <th style="font-size: 12px; color: grey">всего за год <span class="TD314" style="font-size: 16px; color: black"></span> Квт/ч</th>
                            </tr>
                            </thead>
                        </table>



                        <table class="table margin-top">
                            <thead>
                                <tr>
                                    <th>Тепловые поступления в здание:</th>
                                    <th>Одномоментные поступления, Вт:</th>
                                    <th>Поступления за год, кВт/час</th>
                                    <th>Структура тепловых поступлений, в % к общим</th>
                                </tr>
                            </thead>
                            <tbody id="term_in">
                                <tr>
                                    <td>Экономия тепловой энергии рекуператором</td>
                                    <td class="TD311"></td>
                                    <td class="TD316"></td>
                                    <td class="TD327"></td>
                                </tr>
                                <tr>
                                    <td>Бытовые тепловыделения</td>
                                    <td class="TD309"></td>
                                    <td class="TD317"></td>
                                    <td class="TD328"></td>
                                </tr>
                                <tr>
                                    <td>Поступления от солнечного нагрева</td>
                                    <td class="TD306"></td>
                                    <td class="TD318"></td>
                                    <td class="TD329"></td>
                                </tr>
                                <tr>
                                    <td>Поступления от ориентирования по сторонам света</td>
                                    <td class="TD307"></td>
                                    <td class="TD319"></td>
                                    <td class="TD340"></td>
                                </tr>
                            <!--==============================================-->
                            </tbody>
                        </table>




                        <table class="table margin-top">
                            <thead id="term_in_result">
                            <tr>
                                <th><b>Всего тепловые поступления составляют:</b></th>
                                <th style="font-size: 12px; color: grey">одномоментные <span class="TD341" style="font-size: 16px; color: black"></span> Вт</th>
                                <th style="font-size: 12px; color: grey">всего за год <span class="TD342" style="font-size: 16px; color: black"></span> Квт/ч</th>
                            </tr>
                            </thead>
                        </table>

                        <div id="all_res_com">
                            <h3>Итого:</h3>
                            <div class="data">
                               <table>
                                   <tr>
                                       <td style="width:400px"><p class="data-name">Одномоментные потери здания:</p></td>
                                       <td style="width:120px"><p class="data-value"><span class="TD313"></span></p></td>
                                       <td style="width:120px"><p class="data-value"><span>Вт</span></p></td>
                                   </tr>
                               </table>
                            </div>
                            <div class="data">
                               <table>
                                   <tr>
                                       <td style="width:400px"><p class="data-name">Потери тепла за годовой период составляют:</p></td>
                                       <td style="width:120px"><p class="data-value"><span class="TD320"></span></p></td>
                                       <td style="width:120px"><p class="data-value"><span>кВт/ч</span></p></td>
                                   </tr>
                               </table>
                            </div>
                        </div>


                    </div>

                    <div class="schedule_block" style="display: none">
                        <h3 style="background-color: aliceblue; padding: 12px">Структура теплопотерь здания</h3>
                        <img src="<?= Theme::getUrl() ?>/img/grafik2.png" id="texture_heating" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/grafik2_.png" id="texture_heating_" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/arrow.png" id="img_arrow" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/arrow_w.png" id="img_arrow_w" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/arrow_wi.png" id="img_arrow_wi" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/arrow_f.png" id="img_arrow_f" class="hidden" alt="">
                        <img src="<?= Theme::getUrl() ?>/img/arrow_l.png" id="img_arrow_l" class="hidden" alt="">
                        <canvas class="convas_heating" id="convas_heating" width="500" height="450"></canvas>
                    </div>

                </div>
            </div>

            <?php if($login == 'wdvs.ru@gmail.com'){ ?>
            <!-- Админ. область -->
                            <div class="admin"">
                                <div class="admin-title">
                                    <div class="admin-img">
                                        <img src="<?= Theme::getUrl() ?>/img/group.png" alt="Администратор">
                                    </div>
                                    <h3>Администраторская область</h3>
                                </div>
                                <div class="admin-content">
                                    <table class="table margin-top">
                                        <thead>
                                        <tr>
                                            <th>test data:</th>
                                            <th class="">test data</th>
                                            <th class="">test data</th>
                                        </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
        <?php } ?>

            <div class="data">
                <table border="1" cellpadding="3" style="display: ;" id="for-estimate">

                </table>
            </div>


        <div class="data" style="display: block;">
            <p class="heating-note">Нажав на кнопку «Записать расчёт» вы вносите выполненный настоящий расчёт в
                «Личный кабинет», откуда можете впоследствии редактировать.
                При сохранении дайте название, которое будет вам понятно, чтобы потом можно было во вкладке «Сравнение»
                сравнить сделанные вами до 5 вариантов расчётов по основным показателям и затем выбрать из них самый для
                вас подходящий.</p>
            <div class="green-line">
                <div class="btn_block" id="save_calculation" style="display: none">
                    <div class="btn-clear">
                        <a href="#" data-toggle="modal" data-target="#myModalSave"><p>ЗАПИСАТЬ РАСЧЁТ</p></a>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


</section>

<?php
$link_name='';
$links_data = Data::getLinks();
?>
<div class="modal fade in" id="myModalSave" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="close_modal_saving_modal">×</button>
                <h4 class="modal-title" style="text-align: center"></h4>
            </div>
            <div class="modal-body" id="modal-body">
                <div align="center">
                    <label for="changeLinkName">ваши записи</label>
                    <select class="form-control width_90" id="changeLinkName">
                        <?php echo(count($links_data)<6) ? '<option selected value="-">Новая запись </option>':'';
                        foreach ($links_data as $links_datum) {
                            if($links_data[0]->id == $links_datum->id )
                                continue;
                            if($links_datum->name == '-')
                                $links_datum->name = $links_datum->layer_link;
                            echo "<option value='$links_datum->id'>$links_datum->name</option>";
                        }
                        ?>
                    </select>
                    <input type="hidden" id="layerDataId" value="<?=(count($links_data)<5) ? '-': $links_data[0]->id?>">
                    <br>
                    <label for="save_calc_name">имя</label>
                    <input type="text" class="form-control width_90" id="save_calc_name" placeholder="введите имя расчета"
                           value="">
                    <div class="red" style="display: none"> запись сохранена  </div>
                    <button class="btn" id="save_calculation_button" style="margin-top: 10px">Сохранить</button>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
if ($tab_cookie != null) {

    echo "<script>
            $(document).ready(function() {
                $('#calculate_heating').click();
            });
        </script>";
}
?>

<?php $this->theme->footer(); ?>
