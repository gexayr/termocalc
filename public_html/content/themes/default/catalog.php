<?php $this->theme->header(); ?>
<?php $posts = User::getPostsByParams('-');
$postcategories = User::getPostCategories();

?>

    <div class="container">
        <div class="row">
            <div class="passing-test-content">
                <div class="post-block">
                    <?php foreach ($postcategories as $item): ?>
                            <div class="blank-help">
                                <a class="catalog-menu-item" href="/catalog/<?= $item->lat_name ?>">
                                    <span><?= $item->name ?></span>
                                </a>
                            </div>
                        <?php
                        $item_posts = User::getPostsByParams($item->id);
                        if ($item_posts != NULL) {
                            foreach ($item_posts as $post) { ?>
                                <div class="blank-sub">
                                    <a class="catalog-menu-item" href="/catalog/<?= $item->lat_name ?>/<?= $post->id ?>">
                                        - <span><?= $post->title ?></span></a>
                                </div>
                            <?php }
                        }
                        ?>
                    <?php endforeach; ?>
                    <div class="activity post-block">
                        <div style="margin-top: 25px">
                            <?php
                            foreach ($posts as $post) {
                                if(isset($post)) {
                                    ?>
                                    <h2 align="center"><?= $post->title ?></h2>
                                    <div class="post_content">
                                        <?= $post->content ?>
                                    </div>
                                    <div class="post_date" align="right">
                                        <?= $post->date ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php $this->theme->footer(); ?>