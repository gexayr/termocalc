<?php $this->theme->header(); ?>
<?php
require  "part/data_params.php";
require  "part/login.php";
//
?>
<?php Theme::block('part/calc_title') ?>

<link rel="stylesheet" href="<?= Theme::getUrl() ?>/css/inner-surface.css">

<section class="passing-test-inner-surface">
    <div class="container">

        <!-- Этапы теста -->
        <?php $this->theme->sidebar(); ?>
        <!-- Внутренняя поверхность -->
        <div class="inner-surface">
            <?php if (isset($error)): ?>
                <div class="alert alert-danger-city alert-danger alert-dismissible" style="text-align: center">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Ошибка!</strong> <?= $error; ?>
                </div>
            <?php endif; ?>
            <input type="hidden" id="tab_num" value="2">
<div class="title_block" style="position: relative;padding: 8px">
    <h3>Подбор слоёв материалов в конструкции наружных стен:</h3>
        <div class="help-info" data-id="15" data-toggle="modal" data-target="#myModalHelp"></div>
</div>
            <div class="inner-surface-title">
                <h2>Внутренняя поверхность (помещение)</h2>
                <p id="inside_temp">21C<sup>о</sup></p>

            </div>
            <ol id="ol">
                <?php
                    $tab_cookie = User::getCookie('tab_2');
//                                                debug(json_decode($tab_cookie));
                if ($tab_cookie == null) {
                    ?>
                    <!-- Материалы до несущей стены -->
                    <div class="carrier-materials">
                        <h4>Материалы до несущей стены</h4>
                        <div class="add-fields block_1 carrier-materials-form">
                            <li class="col-md-12 margin-li li_1" data-id="1">
                                <div>
                                    <input class="" name="mat_cat" onclick = "set.clickSelect(this, 'homogeneous')"
                                           value="Гранит, гнейс и базальт">
                                </div>
                                <input type="" class="thickness" value="" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                <a class="up btn btn-primary" onclick="upField(this)" style="margin-left: 10px"><i
                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                <a class="down btn btn-primary" onclick="downField(this)"
                                   style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-down"></i></a>
                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                   style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>
                            </li>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                    материала</a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                    теплоизоляции</a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                    блоков </a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                    конструкцию </a>
                            </div>
                        </div>
                    </div>
                    <div class="bearing-wall bearing-wall-form">
                        <h3 style="padding: 15px">Несущая стена</h3>
                        <div class="add-fields block_2">
                            <h4 style="padding-top: 8px">тип слоя: Однослойная конструкция</h4>
                            <li class="col-md-12 margin-li li_2" data-id="2">
                                <div>
                                    <input class="" name="mat_cat" onclick = "set.clickSelect(this, 'holder')"
                                            value="Гранит, гнейс и базальт">
                                </div>
                                <input type="number" class="thickness" value="" placeholder="мм" oninput="this.value=proverka(this.value)">
                            </li>
                        </div>
                        <div class="controls hide-holder">
                            <div class="holder_button">
                                <a href="javascript:void(0)" class="add-field holder">+ Добавить однослойный материал</a>
                            </div>
                        </div>
                        <div class="controls">
                            <div class="holder_button">
                                <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                    блоков </a>
                            </div>
                        </div>
                        <div class="controls">
                            <div class="holder_button">
                                <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                    конструкцию </a>
                            </div>
                        </div>
                    </div>
                    <div class="after-bearing-wall carrier-materials-form">
                        <h4>Материалы после несущей стены</h4>
                        <div class="add-fields block_3">
                            <li class="col-md-12 margin-li li_3" data-id="3">
                                <div>
                                    <input name="mat_cat" onclick = "set.clickSelect(this, 'homogeneous')"
                                    value="Гранит, гнейс и базальт">
                                </div>
                                <input type="" class="thickness" value="" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                <a class="up btn btn-primary" onclick="upField(this)" style="margin-left: 10px"><i
                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                <a class="down btn btn-primary" onclick="downField(this)"
                                   style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-down"></i></a>
                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                   style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>
                            </li>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                    материала</a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                    теплоизоляции</a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                    блоков </a>
                            </div>
                        </div>
                        <div class="controls">
                            <div>
                                <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                    конструкцию </a>
                            </div>
                        </div>
                        <!-- Наружная температура -->
                        <div class="ambient-temperature">
                            <h2>Наружная температура</h2>
                            <div class="least-average-temperature">
                                <div class="least-temperature">
                                    <div class="least-temperature-desc">
                                        <p>Температура наиболее холодной <br>
                                            пятидневки (обеспеч. 0,92)</p>
                                    </div>
                                    <p class="temp" id="outside_temp_td37">21C<sup>о</sup></p>
                                </div>
                                <div class="average-temperature">
                                    <div class="average-temperature-desc">
                                        <p>Средняя температура <br>
                                            отопительного периода</p>
                                    </div>
                                    <p class="temp" id="outside_temp_td38">21C<sup>о</sup></p>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
                else { ?>
                <?php
                    $data = (json_decode($tab_cookie));
                    if(isset($data->volume)){
                        echo "<script>
                                $( document ).ready(function() {
                                    $('#all_wall_volume').val($data->volume)
                                });</script>";
                    }

                    $lay_block_count = 1;
                    foreach ($data->layer as $lay_block => $data_block) {


                        if($lay_block_count == 1){
                            if (!array_key_exists('table_block_1', $data->layer)) {

                                ?>
                                <div class="carrier-materials">
                                    <h4>Материалы до несущей стены</h4>
                                    <div class="add-fields block_1 carrier-materials-form">

                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                                материала</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                                теплоизоляции</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                                блоков </a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                                конструкцию </a>
                                        </div>
                                    </div>
                                </div>

                                <?php

                            }
                        }
                        if ($lay_block == "table_block_1") {
                            ?>
                            <div class="carrier-materials">
                                <h4>Материалы до несущей стены</h4>
                                <div class="add-fields block_1 carrier-materials-form">
                                    <?php
                                    foreach ($data_block as $lay_numb => $data_layer) {
                                        $type_arr = explode("-", $data_layer->type);
                                        $type_mat = $type_arr[1];
                                        if ($type_mat == 'blocks') {
                                            ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>"
                                                style="display: block">
                                                <h4>тип слоя: Кладка из блоков</h4>
                                                <div class="first_select" style="display: inline-flex">

                                                    <?php
                                                        foreach ($material_seam as $item) {
                                                            if ($item->id == $data_layer->val[0])
                                                                $material_item = $item;
                                                        }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="text" class="thickness" value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-down"></i></a>
                                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-remove"></i></a>
                                                <div class="second_select" style="margin-top: 10px;">

                                                    <?php
                                                    foreach ($material_block as $item) {
                                                        if ($item->id == $data_layer->val[1])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <div class="data-input-block block-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                    <div class="col-sm-4">
                                                        <div class="">
                                                            <label for="">Размер блока</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">ширина</label>
                                                            <input type="text" class="block-w" value="<?= $data_layer->params[0]; ?>" placeholder="мм" oninput="this.value=proverka(this.value)">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">высота</label>
                                                            <input type="text" class="block-h" value="<?= $data_layer->params[1]; ?>" placeholder="мм" oninput="this.value=proverka(this.value)">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="">
                                                            <label for="">Толщина шва </label>
                                                        </div>
                                                        <div class="">
                                                            <label style="opacity: 0;display: block">Толщина шва </label>
                                                            <input type="text" class="block-seam" value="<?= $data_layer->params[2]; ?>" placeholder="мм" oninput="this.value=proverka(this.value)">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                            <?php
                                        }
                                        elseif ($type_mat == 'frame') {
                                            ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>"
                                                style="display: block">
                                                <h4>тип слоя: Каркасная конструкция</h4>
                                                <div class="first_select" style="display: inline-flex">


                                    <?php
                                    foreach ($material_filling as $item) {
                                        if ($item->id == $data_layer->val[0])
                                            $material_item = $item;
                                    }
                                    ?>

                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="text" class="thickness" placeholder="мм" value="<?= $data_layer->thickness; ?>"  oninput="this.value=proverka(this.value)">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-down"></i>
                                                        <a class="remove btn btn-primary" onclick="removeField(this)"
                                                           style="display: inline-flex; margin-left: 10px"><i
                                                                    class="glyphicon glyphicon-remove"></i></a>
                                                        <div class="second_select" style="margin-top: 10px;">

                                                        <?php
                                                        foreach ($material_frame as $item) {
                                                            if ($item->id == $data_layer->val[1])
                                                                $material_item = $item;
                                                        }
                                                        ?>

                                                        <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                               value="<?=$material_item->name?>">

                                                    </div>
                                                    <div class="data-input-block frame-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                        <div class="col-md-6">
                                                            <div>
                                                                <label for="">расстояние между стойками</label>
                                                            </div>
                                                            <div class="">
                                                                <input type="text" class="frame-between" value="<?= $data_layer->params[0]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-5">
                                                            <div>
                                                                <label for="">ширина стоек каркаса </label>
                                                            </div>
                                                            <div>
                                                                <input type="text" class="frame-width" value="<?= $data_layer->params[1]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                            </div>
                                                        </div>
                                                    </div>
                                            </li>
                                            <?php
                                        }
                                        else {
                                            $material_type = "material_$type_mat";
                                            $material_type = $$material_type;
                                            ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>" data-id="<?= $lay_numb; ?>">
                                                <div>


                                                    <?php
                                                    foreach ($material_type as $item) {
                                                        if ($item->id == $data_layer->val)
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">


                                                </div>
                                                <input type="" class="thickness" value="<?= $data_layer->thickness ?>"  oninput="this.value=proverka(this.value)" placeholder="мм">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-down"></i></a>
                                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                                   style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                            материала</a>
                                    </div>
                                </div>

                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                            теплоизоляции</a>
                                    </div>
                                </div>

                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                            блоков </a>
                                    </div>
                                </div>

                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                            конструкцию </a>
                                    </div>
                                </div>
                            </div>
                        <?php }
                        elseif ($lay_block == "table_block_2") {
                            ?>
                            <div class="bearing-wall bearing-wall-form">
                                <h3 style="padding: 15px">Несущая стена</h3>
                                <div class="add-fields block_2">
                                    <?php
                                    foreach ($data_block as $lay_numb => $data_layer) {
                                        $type_arr = explode("-", $data_layer->type);
                                        $type_mat = $type_arr[1];
                                        if ($type_mat == 'blocks') {
                                            ?>
                                            <h4>тип слоя: Кладка из блоков</h4>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>" style="display: block">

                                                <div class="first_select" style="display: inline-flex">


                                                    <?php
                                                    foreach ($material_seam as $item) {
                                                        if ($item->id == $data_layer->val[0])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                <input type="text" class="thickness" value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">

                                                <div class="second_select" style="margin-top: 10px;">

                                                    <?php
                                                    foreach ($material_block as $item) {
                                                        if ($item->id == $data_layer->val[1])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <div class="data-input-block block-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                    <div class="col-sm-4">
                                                        <div class="">
                                                            <label for="">Размер блока</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">ширина</label>
                                                            <input type="text" class="block-w" value="<?= $data_layer->params[0]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">высота</label>
                                                            <input type="text" class="block-h" value="<?= $data_layer->params[1]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="">
                                                            <label for="">Толщина шва </label>
                                                        </div>
                                                        <div class="">
                                                            <label style="opacity: 0; display: block">Толщина шва </label>
                                                            <input type="text" class="block-seam" value="<?= $data_layer->params[2]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php }
                                        elseif ($type_mat == 'frame') { ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>" style="display: block">
                                                <h4>тип слоя: Каркасная конструкция</h4>
                                                <div class="first_select" style="display: inline-flex">

                                                    <?php
                                                    foreach ($material_filling as $item) {
                                                        if ($item->id == $data_layer->val[0])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="text" class="thickness"
                                                       value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">

                                                <div class="second_select" style="margin-top: 10px;">
                                                    <?php
                                                    foreach ($material_frame as $item) {
                                                        if ($item->id == $data_layer->val[1])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <div class="data-input-block frame-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                    <div class="col-md-6">
                                                        <div>
                                                            <label for="">расстояние между стойками</label>
                                                        </div>
                                                        <div class="">
                                                            <input type="text" class="frame-between" value="<?= $data_layer->params[0]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div>
                                                            <label for="">ширина стоек каркаса </label>
                                                        </div>
                                                        <div>
                                                            <input type="text" class="frame-width" value="<?= $data_layer->params[1]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } else {
                                            ?>
                                            <h4 style="padding-top: 8px">тип слоя: Однослойная конструкция</h4>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>" data-id="<?= $lay_numb; ?>">
                                                <div>
                                                    <?php
                                                    foreach ($material_holder as $item) {
                                                        if ($item->id == $data_layer->val)
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">


                                                </div>
                                                <input type="" class="thickness"  oninput="this.value=proverka(this.value)" value="<?= $data_layer->thickness ?>" placeholder="мм">
<!--                                                <a class="remove btn btn-primary" onclick="removeField(this)" style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>-->
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="controls hide-holder">
                                    <div class="holder_button">
                                        <a href="javascript:void(0)" class="add-field holder">+ Добавить однослойный материал</a>
                                    </div>
                                </div>
                                <div class="controls">
                                    <div class="holder_button">
                                        <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                            блоков </a>
                                    </div>
                                </div>
                                <div class="controls">
                                    <div class="holder_button">
                                        <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                            конструкцию </a>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        elseif ($lay_block == "table_block_3") {
                            ?>
                            <div class="after-bearing-wall carrier-materials-form">
                                <h4>Материалы после несущей стены</h4>
                                <div class="add-fields block_3">
                                    <?php
                                    foreach ($data_block as $lay_numb => $data_layer) {
                                        $type_arr = explode("-", $data_layer->type);
                                        $type_mat = $type_arr[1];
                                        if ($type_mat == 'blocks') {
                                            ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>" style="display: block">
                                                <h4>тип слоя: Кладка из блоков</h4>
                                                <div class="first_select" style="display: inline-flex">
                                                    <?php
                                                    foreach ($material_seam as $item) {
                                                        if ($item->id == $data_layer->val[0])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="text" class="thickness"
                                                       value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-down"></i></a>
                                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-remove"></i></a>
                                                <div class="second_select" style="margin-top: 10px;">
                                                    <?php
                                                    foreach ($material_block as $item) {
                                                        if ($item->id == $data_layer->val[1])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <div class="data-input-block block-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                    <div class="col-sm-4">
                                                        <div class="">
                                                            <label for="">Размер блока</label>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">ширина</label>
                                                            <input type="text" class="block-w" value="<?= $data_layer->params[0]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <label style="margin-left: 25px">высота</label>
                                                            <input type="text" class="block-h" value="<?= $data_layer->params[1]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <div class="">
                                                            <label for="">Толщина шва </label>
                                                        </div>
                                                        <div class="">
                                                            <label style="opacity: 0; display: block">Толщина шва </label>
                                                            <input type="text" class="block-seam" value="<?= $data_layer->params[2]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } elseif ($type_mat == 'frame') { ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>"
                                                data-id="<?= $lay_numb; ?>" style="display: block">
                                                <h4>тип слоя: Каркасная конструкция</h4>
                                                <div class="first_select" style="display: inline-flex">
                                                    <?php
                                                    foreach ($material_filling as $item) {
                                                        if ($item->id == $data_layer->val[0])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="text" class="thickness"
                                                       value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-arrow-down"></i></a>
                                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                                   style="display: inline-flex; margin-left: 10px"><i
                                                            class="glyphicon glyphicon-remove"></i></a>
                                                <div class="second_select" style="margin-top: 10px;">
                                                    <?php
                                                    foreach ($material_frame as $item) {
                                                        if ($item->id == $data_layer->val[1])
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <div class="data-input-block frame-input col-sm-12" style="text-align: center; margin-top: 22px; margin-bottom: 15px">
                                                    <div class="col-md-6">
                                                        <div>
                                                            <label for="">расстояние между стойками</label>
                                                        </div>
                                                        <div class="">
                                                            <input type="text" class="frame-between" value="<?= $data_layer->params[0]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-5">
                                                        <div>
                                                            <label for="">ширина стоек каркаса </label>
                                                        </div>
                                                        <div>
                                                            <input type="text" class="frame-width" value="<?= $data_layer->params[1]; ?>" oninput="this.value=proverka(this.value)" placeholder="мм">
                                                        </div>
                                                    </div>
                                                </div>
                                            </li>
                                        <?php } else {
                                            $material_type = "material_$type_mat";
                                            $material_type = $$material_type; ?>
                                            <li class="col-md-12 margin-li li_<?= $lay_numb; ?>" data-id="<?= $lay_numb; ?>">
                                                <div>
                                                    <?php
                                                    foreach ($material_type as $item) {
                                                        if ($item->id == $data_layer->val)
                                                            $material_item = $item;
                                                    }
                                                    ?>

                                                    <input name="mat_cat" onclick="set.clickSelect(this, '<?=$type_mat; ?>')"
                                                           value="<?=$material_item->name?>">

                                                </div>
                                                <input type="" class="thickness"
                                                       value="<?= $data_layer->thickness; ?>" placeholder="мм"  oninput="this.value=proverka(this.value)">
                                                <a class="up btn btn-primary" onclick="upField(this)"
                                                   style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-up"></i></a>
                                                <a class="down btn btn-primary" onclick="downField(this)"
                                                   style="margin-left: 10px"><i class="glyphicon glyphicon-arrow-down"></i></a>
                                                <a class="remove btn btn-primary" onclick="removeField(this)"
                                                   style="margin-left: 10px"><i class="glyphicon glyphicon-remove"></i></a>
                                            </li>
                                            <?php
                                        }
                                    }
                                    ?>
                                </div>
                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                            материала</a>
                                    </div>
                                </div>
                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                            теплоизоляции</a>
                                    </div>
                                </div>
                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                            блоков </a>
                                    </div>
                                </div>
                                <div class="controls">
                                    <div>
                                        <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                            конструкцию </a>
                                    </div>
                                </div>
                                <!-- Наружная температура -->
                                <div class="ambient-temperature">
                                    <h2>Наружная температура</h2>
                                    <div class="least-average-temperature">
                                        <div class="least-temperature">
                                            <div class="least-temperature-desc">
                                                <p>Температура наиболее холодной <br>
                                                    пятидневки (обеспеч. 0,92)</p>
                                            </div>
                                            <p class="temp" id="outside_temp_td37">21C<sup>о</sup></p>
                                        </div>
                                        <div class="average-temperature">
                                            <div class="average-temperature-desc">
                                                <p>Средняя температура <br>
                                                    отопительного периода</p>
                                            </div>
                                            <p class="temp" id="outside_temp_td38">21C<sup>о</sup></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        if ($lay_block_count == 2 ) {
                            if (!array_key_exists('table_block_3', $data->layer)) {
                                ?>
                                <div class="after-bearing-wall carrier-materials-form">
                                    <h4>Материалы после несущей стены</h4>
                                    <div class="add-fields block_3">

                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                                материала</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                                теплоизоляции</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                                блоков </a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                                конструкцию </a>
                                        </div>
                                    </div>
                                    <!-- Наружная температура -->
                                    <div class="ambient-temperature">
                                        <h2>Наружная температура</h2>
                                        <div class="least-average-temperature">
                                            <div class="least-temperature">
                                                <div class="least-temperature-desc">
                                                    <p>Температура наиболее холодной <br>
                                                        пятидневки (обеспеч. 0,92)</p>
                                                </div>
                                                <p class="temp" id="outside_temp_td37">-35C<sup>о</sup></p>
                                            </div>
                                            <div class="average-temperature">
                                                <div class="average-temperature-desc">
                                                    <p>Средняя температура <br>
                                                        отопительного периода</p>
                                                </div>
                                                <p class="temp" id="outside_temp_td38">-6.4C<sup>о</sup></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        } elseif ($lay_block_count == 1 && count((array)$data->layer) == 1){
                            if (!array_key_exists('table_block_3', $data->layer)) {
                                ?>
                                <div class="after-bearing-wall carrier-materials-form">
                                    <h4>Материалы после несущей стены</h4>
                                    <div class="add-fields block_3">

                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field homogeneous">+ Добавить слой
                                                материала</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field thermal">+ Добавить слой
                                                теплоизоляции</a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field blocks">+ Добавить кладку из кирпича или 
                                                блоков </a>
                                        </div>
                                    </div>
                                    <div class="controls">
                                        <div>
                                            <a href="javascript:void(0)" class="add-field frame">+ Добавить каркасную
                                                конструкцию </a>
                                        </div>
                                    </div>
                                    <!-- Наружная температура -->
                                    <div class="ambient-temperature">
                                        <h2>Наружная температура</h2>
                                        <div class="least-average-temperature">
                                            <div class="least-temperature">
                                                <div class="least-temperature-desc">
                                                    <p>Температура наиболее холодной <br>
                                                        пятидневки (обеспеч. 0,92)</p>
                                                </div>
                                                <p class="temp" id="outside_temp_td37">-35C<sup>о</sup></p>
                                            </div>
                                            <div class="average-temperature">
                                                <div class="average-temperature-desc">
                                                    <p>Средняя температура <br>
                                                        отопительного периода</p>
                                                </div>
                                                <p class="temp" id="outside_temp_td38">-6.4C<sup>о</sup></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <?php

                            }
                        }
                        $lay_block_count ++;
                    }

                }
                ?>
                <!-- Тип конструкции стены -->
                <div class="type-wall">
                    <form action="javascript: void(0)" class="type-wall-form">
                        <div class="type-wall-construction">
                            <div class="title" style="position: relative">
                                <h4>Тип конструкции стены</h4>
                                <div class="help-info" data-id="16" data-toggle="modal" data-target="#myModalHelp"></div>
                            </div>
                            <select name="type-wall" id="selectCoefficient" onchange="set.ChangeCoefficient_()">
                                <option value="1" <?php  if ($tab_cookie != null){ if($data->selectCoefficient == "1"){echo  "selected";} };?>>1. Стена без применения утеплителей</option>
                                <option value="0.98" <?php  if ($tab_cookie != null){ if($data->selectCoefficient == "0.98"){echo  "selected";} };?>>2. Стена с утеплением отдельных элементов ("мостиков холода") </option>
                                <option value="0.75" <?php  if ($tab_cookie != null){ if($data->selectCoefficient == "0.75"){echo  "selected";} };?>>3. Стена с навесным фасадом</option>
                                <option value="0.87" <?php  if ($tab_cookie != null){ if($data->selectCoefficient == "0.87"){echo  "selected";} };?>>4. Трёхслойная стена (кладка с утеплением)</option>
                                <option value="0.92" <?php  if ($tab_cookie != null){ if($data->selectCoefficient == "0.92"){echo  "selected";} };?>>5. Стена с "мокрым фасадом" (СФТК)</option>
                            </select>
                        </div>
                        <div class="type-wall-coeff" style="display: none">
                            <div class="title">
                                <h4>Коэффицент</h4>
                            </div>
                            <input type="number"  id="change_coefficient" disabled value="<?php echo ($tab_cookie != null) ? $data->selectCoefficient: '1';?>">
                        </div>

                        <div class="type-wall-coeff">
                            <div class="title" style="position: relative;">
                                <h4>Введите общую площадь стен</h4>
                                <div class="help-info" data-id="14" style="position: absolute; right: -36px; top: -5px;"
                                     data-toggle="modal" data-target="#myModalHelp"></div>
                            </div>
                            <input type="text" id="all_wall_volume"
                                   oninput="this.value=proverka(this.value)" placeholder="Введите общую наружную площадь стен">
                        </div>
<!--                        <div class="width_90 form-group-sm">-->
<!--                            <input type="text" class="form-control" id="all_wall_volume"-->
<!--                                   oninput="this.value=proverka(this.value)" placeholder="Введите общую наружную площадь стен">-->
<!--                        </div>-->
                    </form>
                </div>
            </ol>
            <div class="margin-top" id="Data"  style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                echo 'block';
            }else{
                echo 'none';
            }
            ?>">
                <ol id="tab-ol">
                    <div class="row">
                        <div class="col project-title">
                            <h3>
                                <span id="data"></span>
                            </h3>
                        </div>
                    </div>
                    <?php
                    require  "part/in_tab.php";
                    ?>
                </ol>
            </div>
            <div class="row_">
                <div class="btn_block">
                    <div class="btn-input">
                        <a href="javascript: void(0)" id="calculate"><p>Ввод</p></a>
                        <p>Если ввод данных завершён, нажмите кнопку ВВОД</p>
                    </div>
                    <div class="btn-clear">
                        <a id="clear" href="/reset_cookie?tab=2"><p>ОЧИСТИТЬ ВКЛАДКУ</p></a>
                        <p>Очистка результатов в данной вкладке</p>
                    </div>
                    <div class="btn-clear-all">
                        <a  href="/reset_cookie?tab=all"><p>ОЧИСТИТЬ ВЕСЬ РАСЧЁТ</p></a>
                        <p>Очистка всего расчёта</p>
                    </div>
                </div>
                <?php Theme::block('part/for_graph') ?>
                <div class="row_ calc">
                    <?php
                    $tab_cookie = User::getCookie('data1');
                    $data = (json_decode($tab_cookie));
                    //debug_die($data);
                    foreach ($rooms as $room){
                        if ($tab_cookie != null) {
                            $data = (json_decode($tab_cookie));
                            if (isset($data->living_room_w) && $room->name == 'Жилая комната'){
                                continue;
                            }
                            if (isset($data->kitchen_w) && $room->name == 'Кухня'){
                                continue;
                            }
                            if (isset($data->toilet_w) && $room->name == 'Туалет'){
                                continue;
                            }
                            if (isset($data->bathroom_w) && $room->name == 'Ванная или Совмещённый санузел'){
                                continue;
                            }
                            if (isset($data->rest_room_w) && $room->name == 'Помещения для отдыха и учебных занятий') {
                                continue;
                            }
                            if (isset($data->hall_w) && $room->name == 'Межквартирный коридор') {
                                continue;
                            }
                            if (isset($data->vestibule_w) && $room->name == 'Вестибюль, лестничная клетка') {
                                continue;
                            }
                            if (isset($data->pantry_w) && $room->name == 'Кладовые') {
                                continue;
                            }

                            if(isset($data->TD171) && $data->TD171 == 'yes'  && $room->name == 'Помещения для отдыха и учебных занятий'){
                                continue;
                            }
                            if(isset($data->TD171) && $data->TD171 == 'yes'  && $room->name == 'Межквартирный коридор'){
                                continue;
                            }
                            if(isset($data->TD171) && $data->TD171 == 'yes'  && $room->name == 'Вестибюль, лестничная клетка'){
                                continue;
                            }
                            if(isset($data->TD171) && $data->TD171 == 'yes'  && $room->name == 'Кладовые'){
                                continue;
                            }
                        }
                        ?>



                        <div class="core-data">
                            <div class="core-data-title">
                                <div class="core-data-img">
                                    <img src="<?= Theme::getUrl() ?>/img/data.png" alt="Результат расчёта">
                                </div>
                                <h3>Результат расчёта по помещению "<?=$room->name?>":</h3>
                            </div>
                            <div class="core-data-content">
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name">Внутренняя температура помещения :</p>
                                    <p class="data-value">
                                        <span> °С</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" id="TD05_calc_<?=$room->id?>" disabled="disabled" value=''>
                                    </p>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name">Максимальная влажность помещения :</p>
                                    <p class="data-value">
                                        <span> %</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" id="TD06_calc_<?=$room->id?>" disabled="disabled" value=''>
                                    </p>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name">Влажностный режим помещения :</p>
                                    <p class="data-value">
                                        <span></span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" id="TD07_calc_<?=$room->id?>" disabled="disabled" value=''>
                                    </p>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name">Условия эксплуатации конструкций :</p>
                                    <p class="data-value">
                                        <span></span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" id="TD08_calc_<?=$room->id?>" disabled="disabled" value=''>
                                    </p>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name">Градусо-сутки отопительного периода :</p>
                                    <p class="data-value">
                                        <span> ГСОП</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" id="TD44_calc_<?=$room->id?>" disabled="disabled" value=''>
                                    </p>
                                </div>

                                <div class="schedule_block">
                                    <canvas class="c1" id="c<?=$room->id?>" width="820" height="500"></canvas>
                                </div>

                                <div class="for-data-compare for-data-compare-<?=$room->id?>">
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name"><b>Плоскость максимального увлажнения в конструкции:</b><br>
                                        <span id="span_pmu_<?=$room->id?>">
                                        </span>
                                    </p>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="indicator" id="ind_1_room_<?=$room->id?>"></p>
                                    <p class=""><b>Соответствие фактического теплосопротивления данной стеновой
                                            конструкции нормативным требованиям:</b><br>
                                    </p>

                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="data-name"> В соответствии с ГОСТ Р 54851-2011:</p>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Применён коэффициент теплотехнической однородности типа выбранной стеновой конструкции</p></td>
                                            <td style="width:120px"><p class="data-value"><span></span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD101_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Применён коэффициент теплотехнической однородности особенностей здания</p></td>
                                            <td style="width:120px"><p class="data-value"><span></span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD108_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Коэффициент продуваемой минеральной ваты</p></td>
                                            <td style="width:120px"><p class="data-value"><span></span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD109_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                             <td style="width:400px"><p class="data-name">Применён региональный коэффициент</p></td>
                                             <td style="width:120px"><p class="data-value"><span></span></p></td>
                                             <td style="width:120px"><p class="data-value"><span id="TD03_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Понижающий коэффициент при превышении удельного расхода отопления стен</p></td>
                                            <td style="width:120px"><p class="data-value"><span></span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD59_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                   <table>
                                       <tr>
                                           <td style="width:400px"><p class="data-name">ТРЕБУЕМОЕ теплосопротивление стеновой конструкции:</p></td>
                                           <td style="width:120px"><p class="data-value"><span>(м·°С)/Вт</span></p></td>
                                           <td style="width:120px"><p class="data-value"><span id="TD54_result_<?=$room->id?>"></span></p></td>
                                       </tr>
                                   </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Приведённое теплосопротивление конструкции:</p></td>
                                            <td style="width:120px"><p class="data-value"><span>(м·°С)/Вт</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD145_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="" style="background-color: aliceblue;padding: 0 0 15px 15px" id="content_user_1_result_<?=$room->id?>">
                                    </p>
                                </div>

                                <br>
                                <br>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="indicator" id="ind_2_room_<?=$room->id?>"></p>
                                    <p class=""><b>Соответствие данной стеновой конструкции нормативу по санитарно-гигиеническому
                                            требованию:</b><br>
                                    </p>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Температура помещения:</p></td>
                                            <td style="width:120px"><p class="data-value"><span>°С</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD05_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Температура внутренней поверхности стены:</p></td>
                                            <td style="width:120px"><p class="data-value"><span>°С</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD115_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Требуемая разница между температур. не более:</p></td>
                                            <td style="width:120px"><p class="data-value"><span>°С</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD118_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Температура росы (справочно):</p></td>
                                            <td style="width:120px"><p class="data-value"><span>°С</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD117_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="" style="background-color: aliceblue; padding: 0 0 15px 15px" id="content_user_2_result_<?=$room->id?>">
                                    </p>
                                </div>




                                <br>
                                <br>
                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="indicator" id="ind_3_room_<?=$room->id?>"></p>
                                    <p class=""><b>Соответствие нормативу по защите данной стеновой конструкции от переувлажнения:</b><br>
                                    </p>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Сопротивление паропроницанию от внутренней поверхности до плоскости максимального увлажнения:</p></td>
                                            <td style="width:120px"><p class="data-value"><span>(м·ч·Па)/мг</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD122_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Требуемое сопротивление паропроницанию (из условия недопустимости накопления влаги в ограждающей конструкции за годовой период):</p></td>
                                            <td style="width:120px"><p class="data-value"><span>(м·ч·Па)/мг</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD140_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <table>
                                        <tr>
                                            <td style="width:400px"><p class="data-name">Требуемого сопротивления паропроницанию, (из условия ограничения влаги в ограждающей конструкции за период с отрицательными средними месячными температурами наружного воздуха):</p></td>
                                            <td style="width:120px"><p class="data-value"><span>(м·ч·Па)/мг</span></p></td>
                                            <td style="width:120px"><p class="data-value"><span id="TD141_result_<?=$room->id?>"></span></p></td>
                                        </tr>
                                    </table>
                                </div>

                                <div class="data" style="display: flex;
    justify-content: space-between;
    width: 785px;
    margin: 0 auto;
    padding-top: 20px;
    padding-bottom: 10px;
    border-bottom: 1px solid #f0f0f0;">
                                    <p class="" style="background-color: aliceblue; padding:  0 0 15px 15px" id="content_user_3_result_<?=$room->id?>">
                                    </p>
                                </div>

                            </div>

                            </div>
                            <hr>
                        </div>

                    <div class=""   style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                        echo 'block';
                    }else{
                        echo 'none';
                    }
                    ?>">
                        <div class="margin-top table-calculate">
                            <span class="padding-20 display-block calc-span"><h3>Справочник по применяемым материалам</h3></span>
                            <table class="table" style="
                                                        width: 900px;
                                                        overflow-x: auto;
                                                        display: block;
                                                    ">
                                <thead>
                                <tr>
                                    <th colspan="2" rowspan="3"></th>
                                    <th colspan="3">Характеристики материалов в сухом состоянии</th>
                                    <th colspan="4">Расчетные характеристики материалов при условиях эксплуатации конструкций А и Б</th>
                                    <th rowspan="3">Предельно допустимое приращение влаги, %</th>
                                    <th rowspan="3">КОД обозначения что материал НЕСУЩИЙ</th>
                                    <th rowspan="3">КОД обозначения что материал ТЕПЛОИЗОЛЯЦИ</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">Фиксированная толщина слоя в мм</th>
                                    <th rowspan="3">Сопротивление паропроницанию , м ·ч·Па/мг</th>
                                    <th rowspan="3">Источник данных</th>
                                    <th rowspan="3">весённая клиентом толщина слоя в мм</th>
                                    <th rowspan="3">весённая клиентом толщина слоя переводится в метры (после ввода умножается на
                                        0,001)
                                    </th>
                                    <th colspan="4">Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</th>
                                    <th rowspan="3">Шаг обрешётки каркаса, мм</th>
                                    <th rowspan="3">Ширина стоек каркаса, мм</th>
                                    <th rowspan="3">Размер блока кладки длина, мм</th>
                                    <th rowspan="3">Размер блока кладки высота, мм</th>
                                    <th rowspan="3">Толщина шва кладки, мм</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">резерв1</th>
                                    <th rowspan="3">R сопротивление паропроницанию</th>
                                    <th rowspan="3">R теплосопр.слоя</th>
                                    <th rowspan="3">координата х график толщины (м)</th>
                                    <th rowspan="3">t слоёв при пятидневке</th>
                                    <th rowspan="3">температура слоя при средней темп. Периода отрицательных температур</th>
                                    <th rowspan="3">вычисление комплекса tmy</th>
                                    <th rowspan="3">температура в слое</th>
                                </tr>
                                <tr>
                                    <th rowspan="2">плотность кг/куб.м.</th>
                                    <th rowspan="2">удельная теплоёмкость кДж/(кг·°С)</th>
                                    <th rowspan="2">теплопроводность Вт/(м·°С)</th>
                                    <th rowspan="2">влажность %</th>
                                    <th rowspan="2">теплопроводность Вт/(м·°С)</th>
                                    <th rowspan="2">теплоусвоение (при 24 ч) Вт/(м ·°С)</th>
                                    <th rowspan="2">паропроницаемость мг/(м·ч·Па)</th>
                                    <th colspan="2">горизонтальной при потоке теплоты снизу вверх и вертикальной, при температуре
                                        воздуха в прослойке
                                    </th>
                                    <th colspan="2">горизонтальной при потоке теплоты сверху вниз, при температуре воздуха в прослойке
                                    </th>
                                </tr>
                                <tr>
                                    <th>положительной</th>
                                    <th>отрицательной</th>
                                    <th>положительной</th>
                                    <th>отрицательной</th>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td>A | B</td>
                                    <td>A | B</td>
                                    <td>A | B</td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>CTM 01</td>
                                    <td>CTM 02</td>
                                    <td>CTM 03</td>
                                    <td>CTM 04</td>
                                    <td>CTM05 | CTM06</td>
                                    <td>CTM07 | CTM08</td>
                                    <td>CTM09 | CTM10</td>
                                    <td>CTM 11</td>
                                    <td>CTM 12</td>
                                    <td>CTM 13</td>
                                    <td>CTM 14</td>
                                    <td>CTM 15</td>
                                    <td>CTM 16</td>
                                    <td>CTM 17</td>
                                    <td>CTM 18</td>
                                    <td>CTM 19</td>
                                    <td>CTM 20</td>
                                    <td>CTM 21</td>
                                    <td>CTM 22</td>
                                    <td>CTM 23</td>
                                    <td>CTM 24</td>
                                    <td>CTM 25</td>
                                    <td>CTM 26</td>
                                    <td>CTM 27</td>
                                    <td>CTM 28</td>
                                    <td>CTM 29</td>
                                    <td>CTM 30</td>
                                    <td>CTM 31</td>
                                    <td>CTM 32</td>
                                    <td>CTM 33</td>
                                    <td>CTM 34</td>
                                    <td>CTM 35</td>
                                    <td>CTM 36</td>
                                    <td>CTM 37</td>
                                    <td>CTM 38</td>
                                    <td>CTM 39</td>
                                    <td>CTM 40</td>
                                    <td>CTM 41</td>
                                    <td>CTM 42</td>
                                </tr>
                                </thead>
                                <tbody id="calk-tbody_<?=$room->id?>">
                                </tbody>
                            </table>
                        </div>
                        <div id="notice_block_<?=$room->id?>"  style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                            echo 'block';
                        }else{
                            echo 'none';
                        }
                        ?>"></div>
                        <table class="table margin-top"  style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                            echo 'block';
                        }else{
                            echo 'none';
                        }
                        ?>">
                            <thead>
                            <tr>
                                <th>TD</th>
                                <th>Name</th>
                                <th style="text-align: center">Value</th>
                            </tr>
                            </thead>
                            <tbody id="AllTD_<?=$room->id?>">

                            </tbody>
                        </table>
                        <div id="result_block_<?=$room->id?>"></div>
                    </div>
                    <?php } ?>
                </div>
                <table class="table margin-top td_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th style="text-align: center">Value</th>
                    </tr>
                    </thead>
                    <tbody id="AllTbody">
                    <?php
                    print_r($alltd);
                    ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="data">
        <table border="1" cellpadding="3" style="display: none;" id="for-estimate">

        </table>
    </div>

    <table style="display: none;" id="for-compare">

    </table>
</section>
<input type="hidden" id="numb_clicked_input">
<input type="hidden" id="type_clicked_input">
<input type="hidden" id="type_con_clicked_input">
<!--<script>
$(document).ready(function() {
        set.staticThickness();
});
</script>-->
<?php
//require  "part/modal_material.php";
?>
<?php $this->theme->footer(); ?>
