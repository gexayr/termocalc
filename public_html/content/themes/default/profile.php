<?php $this->theme->header(); ?>
<link rel="stylesheet" href="<?= Theme::getUrl() ?>/css/profile.css">
<?php
$the_cookie = User::getCookie('auth-Login');
$secret = User::getCookie('auth-Key');
$params['col'] = 'secret';
$params['val'] = $secret;
$user = User::getUserBy($params);

$selfCreates = User::getSelfCreates();
$selfMaterial=null;
?>
<div class="container" style="min-height: 800px">
    <div class="row">
        <?php
if($user == null){
    echo "
        <div class='alert alert-danger alert-dismissible text-center'>
            Вы должны зарегистрироваться
        </div>
    </div>
</div>
        ";
    $this->theme->footer();
    die;
}
    $user = $user[0];
    if($user->email == $the_cookie){
        $cookie = 'login';
    }
    if($user->verified != 1){
        echo "
        <div class='alert alert-danger alert-dismissible text-center'>
            Вы не подтвердили свой емайл
        </div>
    </div>
</div>
        ";
        $this->theme->footer();
        die;
    }
        $regions = Data::getRegions();
        $buildings = Data::getBuildings();
        $cities = Data::getCitiesByReg($user->region_id);

        ?>

<div class="col-md-12">
    <ul class="nav nav-tabs">
        <li  class="active"><a data-toggle="tab" href="#menu1">Ссылки</a></li>
        <li><a data-toggle="tab" href="#menu2">Профиль</a></li>
        <li><a data-toggle="tab" href="#menu3">Свои материалы</a></li>
    </ul>
    <div class="tab-content">
        <div id="menu1" class="tab-pane fade in active">
            <?php
            $links_data = Data::getLinks();
            if(!empty($links_data)){
                echo "<h3 align='center' class='saving-data-title'>Сохранённые записи </h3>";
                ?>
                <p class="note_profile red">*Внимание! Есть особенность возврата запомненного расчёта обратно в калькулятор.
                    После того, как вы кликнете на одну из ссылок запомненных расчётов, для того, чтобы данные полностью
                    загрузились обратно в калькулятор надо просто пройти снова заново по вкладкам. Нужно лишь кликнуть
                    слева в меню на все вкладки начиная с верхней. Как пройдёте по всем вкладкам, все данные корректно
                    загрузятся.</p>
                <table id="t01">
                <?php
                foreach ($links_data as $item) {
                    ?>
                    <tr>
                        <td>
                            <?php if($links_data[0]->id != $item->id || $item->compare == 1) { ?>
                            <input class="form-control not-focus checkbox-for-compare" data-id="<?=$item->id?>"
                                   <?=($item->compare == 1)?'checked':'';?> type="checkbox">
                           <?php } ?>
                        </td>
                        <td class="link-name">
                            <span>
                    <?php if ($item->name == '-') {
                        ?>
                            <a href='<?= $_SERVER['REQUEST_SCHEME'] ?>://<?= $_SERVER['SERVER_NAME'] ?>/?params=<?= $item->layer_link ?>'
                            data-id='<?= $item->id ?>'><?= $_SERVER['SERVER_NAME'] ?>/?params=<?= $item->layer_link ?></a>
                    <?php } else { ?>
                            <a href='<?= $_SERVER['REQUEST_SCHEME'] ?>://<?= $_SERVER['SERVER_NAME'] ?>/?params=<?= $item->layer_link ?>'
                            data-id='<?= $item->id ?>'><?= $item->name ?> </a>
                    <?php } ?>
                            </span>
                            <a href="javascript:void(0)" onclick="copyLink(this)" data-toggle="tooltip"
                               title="Скопировать ссылку на расчёт в буфер"
                               class="copy-link">
                               <div class="info-text" style="
                            display: none;
                            position: absolute;
                            margin-top: -22px;
                            margin-left: -44px;"> скопировано  </div>
                                <i class='fa fa-files-o'></i>
                                <span class="hidden">
                                    <?= $_SERVER['REQUEST_SCHEME'] ?>://<?= $_SERVER['SERVER_NAME'] ?>/?params=<?= $item->layer_link ?>
                                </span>
                            </a>
                        </td>
                        <td>
                            <span><?= $item->date ?></span>
                        </td>
                        <td>
                            <?php if($links_data[0]->id != $item->id) { ?>

                            <a href='#' data-toggle='modal' title='редактирование названия расчёта'
                                data-target='#myModalSave' onclick='editDataLayer(<?= $item->id ?>, "<?= $item->name ?>")'>
                                <i class='fa fa-edit'></i>
                            </a>&nbsp;&nbsp;
                            <a href="#" title='удалить расчёт' onclick='removeDataLayer(<?= $item->id ?>, <?= $user->id ?>, this)'>
                                <i class='fa fa-times'></i></a>
                            </a>
                            <?php } ?>
                        </td>
                    </tr>
                <?php
                    }
                ?>
                </table>
                <button id="send-to-compare" class="log_a_p">Перенести в таблицу сравнения</button>
            <?php
                } else {
                    echo "<h3 align='center' class='saving-data-title'>У Вас пока нет сохраненных записей</h3>";
                }
                ?>
        </div>
        <div id="menu2" class="tab-pane fade">
            <div>
                        <?php if(isset($success)):?>
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong></strong>  <?=$success;?>
                            </div>
                        <?php endif; ?>

                        <?php if(isset($error)):?>
                            <div class="alert alert-danger alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                <strong></strong> <?=$error;?>
                            </div>
                        <?php endif; ?>
                        <!--                        ===========================================-->
                        <?php
                        $other_status = false;
                        $status_array = array(
                            'Архитектор или инженер-конструктор',
                            'Самостоятельный застройщик для себя (коттеджа, квартиры и т.п.)',
                            'Заказчик строительных услуг (бизнес-сектор)',
                            'Частный заказчик строительных услуг (найм подрядчиков для строительства)',
                            'Исполнитель стройработ (частный подрядчик)',
                            'Исполнитель работ (подрядная организация)',
                            'Торговля стройтоварами и услугами',
                        );
                        if (!in_array($user->status, $status_array)){
                            $other_status = true;
                        }
                        ?>

                        <section>
                            <div class="container profile_container">
                                <div class="profile">
                                    <h1 align="center">Профиль</h1>
                                    <div align="center">
                                        <a href="/forum/index.php?account/account-details">Изменить </a>
                                    </div>


                                    <table style="width: 500px">
                                        <tbody>
                                            <tr>
                                                <td>Фамилия</td>
                                                <td><?= $user->surname ?></td>
                                            </tr>
                                            <tr>
                                                <td>Имя</td>
                                                <td><?= $user->name ?></td>
                                            </tr>
                                            <tr>
                                                <td>E-mail</td>
                                                <td><?= $user->email ?></td>
                                            </tr>
                                            <tr>
                                                <td>Статус</td>
                                                <td><?= $user->status ?></td>
                                            </tr>
                                            <tr>
                                                <td>Телефон</td>
                                                <td><?= $user->phone ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </section>

                </div>
        </div>
        <div id="menu3" class="tab-pane fade">

            <ul class="nav nav-tabs">
                <li  class="active pull-right"><a data-toggle="tab" href="#submenu1">Создать свой материал </a></li>
                <li class="pull-right"><a data-toggle="tab" href="#submenu2">Список созданных материалов</a></li>
            </ul>
            <div class="tab-content">
                <div id="submenu1" class="tab-pane fade in active">
                    <div class="well">
                        <form class="material-form" action="/self_material/add/" method="POST">
                    <?php require "part/self_create.php"; ?>

                            <div class="btn-input" style="margin: 0 auto; padding-bottom: 20px">
                                <input  type="submit" class="a_p log_a_p" value="Добавить">
                            </div>
                        </form>
                    </div>

                </div>
                <div id="submenu2" class="tab-pane fade">
                    <?php   if(!empty($selfCreates)){ ?>
                    <table id="t02">
                        <?php
                        foreach ($selfCreates as $item) { ?>
                            <tr>
                                <td>
                                    <span><?= $item->name ?></span>
                                </td>
                                <td>
                                    <a href='/self_materials/edit/<?= $item->id ?>'>
                                        <i class='fa fa-edit'></i>
                                    </a>&nbsp;&nbsp;
                                    <a href="#" onclick='removeSelfCreated(<?= $item->id ?>, <?= $user->id ?>, this)'>
                                        <i class='fa fa-times'></i></a>
                                    </a>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <?php } else{ ?>
                        <h3 align='center' class='saving-data-title'>У Вас пока нет сохраненных материалов</h3>
                    <?php } ?>
                </div>
            </div>


        </div>
    </div>
</div>
            </div>
        </div>

<div class="modal fade in" id="myModalSave" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" id="close_modal_saving_modal">×</button>
                <h4 class="modal-title" style="text-align: center"></h4>
            </div>
            <div class="modal-body" id="modal-body">
                <div align="center">
                    <input type="text" class="form-control width_90" id="edit_calc_name" placeholder="введите имя расчета ">
                    <input type="hidden" id="edit_calc_id">
                    <div class="red" style="display: none"></div>
                    <button class="btn" id="edit_calculation_button" style="margin-top: 10px">Сохранить</button>
                </div>
            </div>
        </div>

    </div>
</div>
<?php $time = time(); ?>
<script src="<?= Theme::getUrl()?>/js/profile.js?v=<?=$time?>" type="text/javascript"></script>

<?php $this->theme->footer(); ?>

