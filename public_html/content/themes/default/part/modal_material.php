<?php

$par_homogeneous['col'] = 'COL18';
$par_homogeneous['val'] = 'Т';
$par_homogeneous['operator'] = '!=';
$material_homogeneous = Data::getMaterialsByParamsNull($par_homogeneous);

$par_thermal['col'] = 'COL18';
$par_thermal['val'] = 'Т';
$par_thermal['operator'] = '=';
$material_thermal = Data::getMaterialsByParams($par_thermal);


$par_holder['col'] = 'COL17';
$par_holder['val'] = 'H';
$par_holder['operator'] = '=';
$material_holder = Data::getMaterialsByParams($par_holder);

?>
<div class="hidden">
<a data-toggle="modal" id="homogeneous_modal" class="material-prop" data-type="frame" data-target="#myModalMaterial_homogeneous">open homo</a>
<a data-toggle="modal" id="thermal_modal" class="material-prop" data-type="frame" data-target="#myModalMaterial_thermal">open thermal</a>
<a data-toggle="modal" id="holder_modal" class="material-prop" data-type="frame" data-target="#myModalMaterial_holder">open holder</a>
</div>
<!-- Modal homogeneous-->
<div class="modal fade" id="myModalMaterial_homogeneous" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--                <button type="button" class="close" data-dismiss="modal" onclick="clearContent()">&times;</button>-->
            </div>
            <div class="modal-body" style="display: inline-block; width: 90%;">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <?php
                        $material_category = array();
                        foreach ($material_homogeneous as $item) {
                            $material_category[] = $item->category;
                        }
                        $material_category = array_flip($material_category);
                        $material_category = array_flip($material_category);
                        foreach ($material_category as $key=>$item_category) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_h_<?=$key?>"><?=$item_category?></a>
                                    </h4>
                                </div>
                                <div id="collapse_h_<?=$key?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <?php foreach ($material_homogeneous as $item) { ?>

                                                <?php
                                                if($item->category == $item_category){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#"><?=$item->name?></a>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal thermal-->
<div class="modal fade" id="myModalMaterial_thermal" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--                <button type="button" class="close" data-dismiss="modal" onclick="clearContent()">&times;</button>-->
            </div>
            <div class="modal-body" style="display: inline-block; width: 90%;">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <?php
                        $material_category = array();
                        foreach ($material_thermal as $item) {
                            $material_category[] = $item->category;
                        }
                        $material_category = array_flip($material_category);
                        $material_category = array_flip($material_category);
                        foreach ($material_category as $key=>$item_category) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_the_<?=$key?>"><?=$item_category?></a>
                                    </h4>
                                </div>
                                <div id="collapse_the_<?=$key?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <?php foreach ($material_thermal as $item) { ?>
                                                <?php
                                                if($item->category == $item_category){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#"><?=$item->name?></a>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal holder-->
<div class="modal fade" id="myModalMaterial_holder" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <!--                <button type="button" class="close" data-dismiss="modal" onclick="clearContent()">&times;</button>-->
            </div>
            <div class="modal-body" style="display: inline-block; width: 90%;">
                <div class="col-sm-12">
                    <div class="panel-group" id="accordion">
                        <?php
                        $material_category = array();
                        foreach ($material_holder as $item) {
                            $material_category[] = $item->category;
                        }
                        $material_category = array_flip($material_category);
                        $material_category = array_flip($material_category);
                        foreach ($material_category as $key=>$item_category) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" data-parent="#accordion" href="#collapse_ho_<?=$key?>"><?=$item_category?></a>
                                    </h4>
                                </div>
                                <div id="collapse_ho_<?=$key?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <table class="table">
                                            <?php foreach ($material_holder as $item) { ?>
                                                <?php
                                                if($item->category == $item_category){
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <a href="#"><?=$item->name?></a>
                                                        </td>
                                                    </tr>
                                                <?php }
                                            } ?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>