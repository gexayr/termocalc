<?php
$colName = [
    'COL6' => "Плотность кг/куб.м.",
    'COL7' => "Удельная теплоёмкость кДж/(кг·°С)",
    'COL8' => "Теплопроводность Вт/(м·°С)",
    'COL9' => "Влажность % A",
    'COL10' => "Влажность % B",
    'COL11' => "Теплопроводность Вт/(м·°С) A",
    'COL12' => "Теплопроводность Вт/(м·°С) B",
    'COL13' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) A",
    'COL14' => "Теплоусвоение (при 24 ч) Вт/(м ·°С) B",
    'COL15' => "Паропроницаемость мг/(м·ч·Па)",
    'COL16' => "Предельно допустимое приращение влаги, %",
    'COL20' => "Фиксированная толщина слоя в мм",
    'COL21' => "Сопротивление паропроницанию , м ·ч·Па/мг",
    'COL25' => "Положительной",
    'COL26' => "Отрицательной",
    'COL27' => "Положительной",
    'COL28' => "Отрицательной",
];
?>
<!--oninput="this.value=proverka(this.value)"-->
        <div class="form-group">
            <label>Название</label>
            <input type="text" name="name" class="form-control width_90" id="name" required value="<?=($selfMaterial)?$selfMaterial->name:"";?>">
        </div>
        <?php
        for($i=6;$i<=21;$i++){
            if(
                $i == 17 ||
                $i == 18 ||
                $i == 19 ||
                $i == 22 ||
                $i == 23 ||
                $i == 24
            )continue;
            $res = "COL$i";
            $colLabel = $colName["COL$i"];
            ?>
            <div class="form-group">
                <?php if($i == 25){
                    echo "<span>Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</span><br>";
                    echo "<span>горизонтальной при потоке теплоты снизу вверх и вертикальной, при температуре воздуха в прослойке</span><br>";
                }elseif($i == 27){
                    echo "<span>Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</span><br>";
                    echo "<span>горизонтальной при потоке теплоты сверху вниз, при температуре воздуха в прослойке</span><br>";
                } ?>
                <label for="<?=$res?>"><?=$colLabel?></label>
                <input type="text" name="<?=$res?>" class="form-control width_90" id="<?=$res?>"
                       value="<?=($selfMaterial)?$selfMaterial->$res:"";?>" oninput="this.value=proverka(this.value)">
            </div>
        <?php } ?>


<div class="form-group">
    <?php if($i == 25){
        echo "<span>Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</span><br>";
        echo "<span>горизонтальной при потоке теплоты снизу вверх и вертикальной, при температуре воздуха в прослойке</span><br>";
    }elseif($i == 27){
        echo "<span>Термическое сопротивление замкнутой воздушной прослойки,м2 · °С/Вт</span><br>";
        echo "<span>горизонтальной при потоке теплоты сверху вниз, при температуре воздуха в прослойке</span><br>";
    } ?>
    <label for="COL30">Стоимость</label>
    <input type="text" name="COL30" class="form-control width_90" id="COL30"
           value="<?=($selfMaterial)?$selfMaterial->COL30:"";?>" oninput="this.value=proverka(this.value)">
</div>