<?php

                            if ($tab_cookie == null) {

                                echo '<div id="dataTbody">';
                                echo '<div class="table_block_1">';
                                include_once __DIR__ . '/../loader/table_block_1.php';
                                echo '</div>';
                                echo '<div class="table_block_2">';
                                include_once __DIR__ . '/../loader/table_block_2.php';
                                echo '</div>';
                                echo '<div class="table_block_3">';
                                include_once __DIR__ . '/../loader/table_block_3.php';
                                //        require __DIR__ . "/loader/table_block_3.php";
                                echo '</div>';
                                echo '</div>';
                            }else {
                                $html_1 = '';
                                $html_2 = '';
                                $html_3 = '';

                            foreach ($data->layer as $lay_block => $data_block) {
                                if ($lay_block == "table_block_1") {
                                    foreach ($data_block as $lay_numb => $data_layer) {
                                        $type_arr = explode("-", $data_layer->type);
                                        $type_mat = $type_arr[1];


                                    if ($type_mat == 'homogeneous' || $type_mat == 'thermal') {

                                    //========================================================================================
                                    $materials = '';
                                    $result = Data::getMaterial($data_layer->val);
                                    if($data_layer->val > 10000)$result = User::getSelfCreateData($data_layer->val);
                                        if (!isset($result)) {
                                        die();
                                    }
                                    $materials .= '<div class="table_' . $lay_numb . '" data-tab="' . $lay_numb . '"><li>
                        <table class="table"><tbody class="tab-' . $type_mat . '"><tr>';
                                    foreach ($result as $key => $value) {
                                        $style = '';
                                        if ($key == 'id') {
                                            $style = "style='width: 60px;'";
                                        }
                                        $materials .= '
                            <th scope="row" ' . $style . '>
                    ' . $key . '
                </th>';
                                    }
                                    $materials .= '</tr><tr>';

                                    foreach ($result as $key => $value) {
                                        $materials .= '  <td class="' . $key . '">
                                ' . $value . '
                            </td>';
                                    }
                                    $materials .= '</tr></tbody></table></li></div>';

                                    $html_1 .= $materials;
//========================================================================================
                                        }elseif ($type_mat == 'blocks' || $type_mat == 'frame'){

                                        $materials = '';
                                        $result = Data::getMaterial($data_layer->val[0]);
                                        $result2 = Data::getMaterial($data_layer->val[1]);
                                        if($data_layer->val[0] > 10000)$result = User::getSelfCreateData($data_layer->val[0]);
                                        if($data_layer->val[1] > 10000)$result2 = User::getSelfCreateData($data_layer->val[1]);

                                        if(!isset($result)){
                                            die();
                                        }
                                        $materials.= '<div class="table_'.$lay_numb.'" data-tab="'.$lay_numb.'"><li>
                                <table class="table table_first"><tbody class="tab-'.$type_mat.'"><tr>';
                                        foreach ($result as $key=>$value){
                                            $style = '';
                                            if($key == 'id'){ $style = "style='width: 60px;'";}
                                            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                        }
                                        $materials.= '</tr><tr>';

                                        foreach ($result as $key=>$value){
                                            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                        }

                                        $materials.= '</tr></tbody></table>';

                                        $materials.= '<table class="table table_second"><tbody><tr>';
                                        foreach ($result2 as $key=>$value){
                                            $style = '';
                                            if($key == 'id'){ $style = "style='width: 60px;'";}
                                            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                        }
                                        $materials.= '</tr><tr>';

                                        foreach ($result2 as $key=>$value){
                                            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                        }

                                        $materials.= '</tr></tbody></table></li></div>';

                                        $html_1 .= $materials;
                                        }
                                    }
                                } elseif ($lay_block == "table_block_2") {
                                    foreach ($data_block as $lay_numb => $data_layer) {

                                    $type_arr = explode("-", $data_layer->type);
                                    $type_mat = $type_arr[1];


                                    if ($type_mat == 'holder' || $type_mat == 'homogeneous') {
                                        //========================================================================================
                                        $materials = '';
                                        $result = Data::getMaterial($data_layer->val);
                                        if($data_layer->val > 10000)$result = User::getSelfCreateData($data_layer->val);
                                        if (!isset($result)) {
                                            die();
                                        }
                                        $materials .= '<div class="table_' . $lay_numb . '" data-tab="' . $lay_numb . '"><li>
                        <table class="table"><tbody class="tab-' . $type_mat . '"><tr>';
                                        foreach ($result as $key => $value) {
                                            $style = '';
                                            if ($key == 'id') {
                                                $style = "style='width: 60px;'";
                                            }
                                            $materials .= '
                            <th scope="row" ' . $style . '>
                    ' . $key . '
                </th>';
                                        }
                                        $materials .= '</tr><tr>';

                                        foreach ($result as $key => $value) {
                                            $materials .= '  <td class="' . $key . '">
                                ' . $value . '
                            </td>';
                                        }
                                        $materials .= '</tr></tbody></table></li></div>';

                                        $html_2 .= $materials;
//========================================================================================
                                        }elseif ($type_mat == 'blocks' || $type_mat == 'frame'){


                                    $materials = '';
                                    $result = Data::getMaterial($data_layer->val[0]);
                                    $result2 = Data::getMaterial($data_layer->val[1]);
                                    if($data_layer->val[0] > 10000)$result = User::getSelfCreateData($data_layer->val[0]);
                                    if($data_layer->val[1] > 10000)$result2 = User::getSelfCreateData($data_layer->val[1]);
                                    if(!isset($result)){
                                        die();
                                    }
                                    $materials.= '<div class="table_'.$lay_numb.'" data-tab="'.$lay_numb.'"><li>
                                <table class="table table_first"><tbody class="tab-'.$type_mat.'"><tr>';
                                    foreach ($result as $key=>$value){
                                        $style = '';
                                        if($key == 'id'){ $style = "style='width: 60px;'";}
                                        $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                    }
                                    $materials.= '</tr><tr>';

                                    foreach ($result as $key=>$value){
                                        $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                    }

                                    $materials.= '</tr></tbody></table>';

                                    $materials.= '<table class="table table_second"><tbody><tr>';
                                    foreach ($result2 as $key=>$value){
                                        $style = '';
                                        if($key == 'id'){ $style = "style='width: 60px;'";}
                                        $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                    }
                                    $materials.= '</tr><tr>';

                                    foreach ($result2 as $key=>$value){
                                        $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                    }

                                    $materials.= '</tr></tbody></table></li></div>';

                                    $html_2 .= $materials;
                                        }
                                    }
                                } elseif ($lay_block == "table_block_3") {
                                    foreach ($data_block as $lay_numb => $data_layer) {

                                    $type_arr = explode("-", $data_layer->type);
                                    $type_mat = $type_arr[1];


                                    if ($type_mat == 'homogeneous' || $type_mat == 'thermal') {

//========================================================================================
                                        $materials = '';
                                        $result = Data::getMaterial($data_layer->val);
                                        if($data_layer->val > 10000)$result = User::getSelfCreateData($data_layer->val);
                                        if (!isset($result)) {
                                            die();
                                        }
                                        $materials .= '<div class="table_' . $lay_numb . '" data-tab="' . $lay_numb . '"><li>
                        <table class="table"><tbody class="tab-' . $type_mat . '"><tr>';
                                        foreach ($result as $key => $value) {
                                            $style = '';
                                            if ($key == 'id') {
                                                $style = "style='width: 60px;'";
                                            }
                                            $materials .= '
                            <th scope="row" ' . $style . '>
                    ' . $key . '
                </th>';
                                        }
                                        $materials .= '</tr><tr>';

                                        foreach ($result as $key => $value) {
                                            $materials .= '  <td class="' . $key . '">
                                ' . $value . '
                            </td>';
                                        }
                                        $materials .= '</tr></tbody></table></li></div>';

                                        $html_3 .= $materials;
//========================================================================================

                                        }elseif ($type_mat == 'blocks' || $type_mat == 'frame'){

                                        $materials = '';
                                        $result = Data::getMaterial($data_layer->val[0]);
                                        $result2 = Data::getMaterial($data_layer->val[1]);
                                        if($data_layer->val[0] > 10000)$result = User::getSelfCreateData($data_layer->val[0]);
                                        if($data_layer->val[1] > 10000)$result2 = User::getSelfCreateData($data_layer->val[1]);
                                        if(!isset($result)){
                                            die();
                                        }
                                        $materials.= '<div class="table_'.$lay_numb.'" data-tab="'.$lay_numb.'"><li>
                                <table class="table table_first"><tbody class="tab-'.$type_mat.'"><tr>';
                                        foreach ($result as $key=>$value){
                                            $style = '';
                                            if($key == 'id'){ $style = "style='width: 60px;'";}
                                            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                        }
                                        $materials.= '</tr><tr>';

                                        foreach ($result as $key=>$value){
                                            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                        }

                                        $materials.= '</tr></tbody></table>';

                                        $materials.= '<table class="table table_second"><tbody><tr>';
                                        foreach ($result2 as $key=>$value){
                                            $style = '';
                                            if($key == 'id'){ $style = "style='width: 60px;'";}
                                            $materials.='
                            <th scope="row" '.$style.'>
                                '.$key.'
                            </th>';
                                        }
                                        $materials.= '</tr><tr>';

                                        foreach ($result2 as $key=>$value){
                                            $materials.='  <td class="'.$key.'">
                                '.$value.'
                            </td>';
                                        }

                                        $materials.= '</tr></tbody></table></li></div>';

                                        $html_3 .= $materials;
                                        }
                                    }
                                }
                            }
                            
                                echo '<div id="dataTbody">';
                                echo '<div class="table_block_1">';
                                echo $html_1;
                                echo '</div>';
                                echo '<div class="table_block_2">';
                                echo $html_2;
                                echo '</div>';
                                echo '<div class="table_block_3">';
                                echo $html_3;
                                echo '</div>';
                                echo '</div>';

                                echo "<script>
$(document).ready(function() { 
        $('#calculate').click();
});
                                    </script>";
                            }
                            ?>
