<?php $this->theme->header(); ?>
<?php

require  "part/login.php";
require  "part/if_not_login.php";

$city_cookie = User::getCookie('city');
$build_cookie = User::getCookie('building');

$error = '';
if($city_cookie == null){
    $error .= " вы не выбрали город, ";
}
if($build_cookie == null){
    $error .= " вы не выбрали тип здания ";
}
$tab_cookie = User::getCookie('tab_5');

if ($tab_cookie != null) {
    $data = (json_decode($tab_cookie));
}

//debug($data);
require "part/calc_title.php";

?>

<!-- Main Content -->
<section class="pt-l-ad">
    <div class="container">
<!--        <div class="" style="display: flex">-->
            <?php $this->theme->sidebar(); ?>
            <div class="inner-surface windows">


                <?php if(isset($error) && $error != ''):?>
                    <div class="alert alert-danger alert-dismissible alert-danger-city">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        <strong>Ошибка!</strong> <?=$error;?>
                    </div>
                <?php endif; ?>

                <div class="additional-data" style="width: 100%">
                    <div class="additional-data-title">
                        <div class="passing-test-img">
                            <img src="<?= Theme::getUrl() ?>/img/edit.png" alt="Прохождение">
                        </div>
                        <h3>Введите данные:</h3>
                    </div>

                    <form action="#" class="additional-data-form">
                        <h2>Окна </h2>
                        <div class="individual_win">
                            <p>Площадь окон (+ входных и балконных дверей)<span class="red">*</span></p>
                            <div class="help-info" data-id="24"
                                 data-toggle="modal" data-target="#myModalHelp"></div>
                            <div class="input-label-info">
                                <input type="text" class="form-control" id="window_square" placeholder="кв.м."
                                       oninput="this.value=proverka(this.value)" value="<?php if ($tab_cookie != null) echo $data->TD182; ?>">
                            </div>
                        </div>
                        <div class="individual_win">
                            <p>Теплосопротивление ( R ) оконной конструкции<span class="red">*</span></p>
                            <div class="help-info" data-id="25"
                                 data-toggle="modal" data-target="#myModalHelp"></div>
                            <div class="input-label-info">
                                <input type="text" class="form-control" id="term_resistance" placeholder="м²·С°/Вт"
                                       oninput="this.value=proverka(this.value)" value="<?php if ($tab_cookie != null) echo $data->TD183; ?>">
                            </div>
                        </div>
                        <div class="resident">
                            <p>Тип оконного переплёта</p>
                            <div class="label-info_block">
                                <div class="help-info" data-id="26"
                                     data-toggle="modal" data-target="#myModalHelp"></div>
                            </div>
                            <div class="input-label-info">
                                <div style="width: 95%">
                                    <select name="" id="window_type" class="form-control">
                                        <option value="1" <?php if ($tab_cookie != null && $data->TD184 == '1') echo "selected"; ?>> - одинарный, спаренный или открытый</option>
                                        <option value="0.8" <?php if ($tab_cookie != null && $data->TD184 == '0.8') echo "selected"; ?>> - двойной</option>
                                        <option value="0.7" <?php if ($tab_cookie != null && $data->TD184 == '0.7') echo "selected"; ?>> - тройной</option>
                                    </select>
                                </div>
                                <!--                            <div class="resident-info">-->
                                <!--                                <div class="info">-->
                                <!--                                    <p>Нам нужно знать ширину вашего дома, для точного-->
                                <!--                                        подсчёта данных</p>-->
                                <!--                                </div>-->
                                <!--                            </div>-->
                            </div>
                        </div>
                        <br>

                        <h5 style="margin-top: 25px">* В случае отсутствия данных о теплосопротивлении окон от производителя можно подобрать</h5>
                        <h5>такие ориентировочные данные самостоятельно, выбрав тип стеклопакета:</h5>
                        <div class="resident">
                            <p>количество камер в стеклопакете:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="window_type_chamber" class="form-control" onchange="set.ChangeWindow()">
                                        <option value="">- есть данные из аккредитованной лаборатории</option>
                                        <option value="1">- однокамерный стеклопакет</option>
                                        <option value="2">- двухкамерный стеклопакет</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="resident">
                            <p>расстояние между стёклами в стеклопакете:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="window_type_distance" class="form-control" disabled  onchange="set.ChangeWindowDistance()">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="resident">
                            <p>тип заполнения и покрытия стёкол:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="window_type_filling" class="form-control" disabled  onchange="set.ChangeWindowFilling()">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <br>
                        <br>
                        <br>
                        <h2>ФОНАРИ (мансардные окна)</h2>
                        <div class="individual_win">
                            <p>Площадь фонарей</p>
                            <div class="help-info" data-id="27"
                                 data-toggle="modal" data-target="#myModalHelp"></div>
                            <div class="input-label-info">
                                <input type="text" class="form-control" id="lantern_square" placeholder="кв.м."
                                       oninput="this.value=proverka(this.value)" value="<?php if ($tab_cookie != null) echo $data->TD186; ?>">
                            </div>
                        </div>

                        <div class="individual_win">
                            <p>Теплосопротивление <span class="ind_span">( R ) фонаря</span></p>
                            <div class="help-info" data-id="28"
                                 data-toggle="modal" data-target="#myModalHelp"></div>
                            <div class="input-label-info">
                                <input type="text" class="form-control" id="lantern_term" placeholder="м²·С°/Вт"
                                       oninput="this.value=proverka(this.value)" value="<?php if ($tab_cookie != null) echo $data->TD188; ?>">
                            </div>
                        </div>
                        <h5>* В случае отсутствия данных о теплосопротивлении окон от производителя можно подобрать</h5>
                        <h5>такие ориентировочные данные самостоятельно, выбрав тип стеклопакета:</h5>
                        <div class="resident">
                            <p>количество камер в стеклопакете:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="lantern_type_chamber" class="form-control" onchange="set.ChangeLantern()">
                                        <option value="">- есть данные из аккредитованной лаборатории</option>
                                        <option value="1">- однокамерный стеклопакет</option>
                                        <option value="2">- двухкамерный стеклопакет</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="resident">
                            <p>расстояние между стёклами в стеклопакете:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="lantern_type_distance" class="form-control" disabled  onchange="set.ChangeLanternDistance()">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="resident">
                            <p>тип заполнения и покрытия стёкол:</p>
                            <div class="input-label-info">
                                <div class="width_90">
                                    <select name="" id="lantern_type_filling" class="form-control" disabled  onchange="set.ChangeLanternFilling()">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <!--                    <button id="send_wind" class="btn btn-primary pull-right margin-top">Send</button>-->
                        <div class="btn_block win_btn_block">
                            <div class="btn-input">
                                <a href="javascript: void(0)" id="send_wind"><p>Ввод</p></a>
                                <p>Если ввод данных завершён, нажмите кнопку ВВОД</p>
                            </div>
                            <div class="btn-clear">
                                <a id="clear" href="/reset_cookie?tab=5"><p>ОЧИСТИТЬ ВКЛАДКУ</p></a>
                                <p>Очистка результатов в данной вкладке</p>
                            </div>
                            <div class="btn-clear-all">
                                <a  href="/reset_cookie?tab=all"><p>ОЧИСТИТЬ ВЕСЬ РАСЧЁТ</p></a>
                                <p>Очистка всего расчёта</p>
                            </div>
                        </div>
                    </form>
                </div>

                <?php
                if ($tab_cookie != null) {

                    echo "<script>
            $(document).ready(function() {
//                $('#send_wind').click();
send_window('auto');
            });
        </script>";
                }
                ?>

                <div class="schedule_block_row" style="display: none">
                    <br>
                    <div class="schedule_block_w">
                        <div class="pull-left" id="schedule_TD37w"></div>

                        <!--                        <div class="col-sm-12">-->
                        <!--                            <img src="--><?//= Theme::getUrl()?><!--/img/graf_ok_pic.png" class="img-fluid" alt="Responsive image">-->
                        <!--                        </div>-->
                        <div class="pull-right" id="schedule_TD05w"></div>
                        <br>
                        <div class="pull-right" id="schedule_TD115w"></div>
                    </div>
                    <hr>

                    <div class="schedule_block_l">
                        <div class="pull-left" id="schedule_TD37l"></div>

                        <!--                        <div class="col-sm-12">-->
                        <!--                            <img src="--><?//= Theme::getUrl()?><!--/img/graf_fo_pic.png" class="img-fluid" alt="Responsive image">-->
                        <!--                        </div>-->
                        <div class="pull-right" id="schedule_TD115l"></div>
                        <br>&nbsp
                        <div class="pull-right" id="schedule_TD05l"></div>

                    </div>
                </div>
                <br>
                <br>
                <div class="core-data-conclusion-admin">
                    <!-- Основные данные -->
                    <div class="win_basic_data">
                        <div class="core-data-title">
                            <div class="core-data-img">
                                <img src="<?= Theme::getUrl() ?>/img/data.png" alt="Нормативы">
                            </div>
                            <h3>Результаты расчёта:</h3>
                        </div>

                        <div class="core-data-content">

                            <div id="content_block" style="padding: 15px; max-width: 800px">
                                <div class="content_block_wind" style="display: none">
                                    <h4 style="margin: 11px 0px 8px 0px">Окна (и двери)</h4>
                                    <p>1. Соответствие фактического теплосопротивления данной конструкции нормативным требованиям:</p>
                                    <p class="content_info_3_2 win_content_info" style="padding: 8px"></p>

                                    <p>2. Соответствие температурного перепада данной конструкции нормативным требованиям:</p>
                                    <p class="content_info_3_3 win_content_info"  style="padding: 8px"></p>
                                </div>
                                <br>
                                <div class="content_block_lantern" style="display: none">
                                    <h4 style="margin: 11px 0px 8px 0px">Фонари (мансардные окна)</h4>
                                    <p>1. Соответствие фактического теплосопротивления данной конструкции нормативным требованиям:</p>
                                    <p class="content_info_3_5 win_content_info" style="padding: 8px"></p>

                                    <p>2. Соответствие температурного перепада данной конструкции нормативным требованиям:</p>
                                    <p class="content_info_3_6 win_content_info" style="padding: 8px"></p>
                                </div>
                            </div>

                        </div>
                    </div>

                    <!-- Админ. область -->
                    <div class="admin"
                         style="display: <?php if($login == 'wdvs.ru@gmail.com'){
                             echo 'block';
                         }else{
                             echo 'none';
                         }
                         ?>">
                        <div class="admin-title">
                            <div class="admin-img">
                                <img src="<?= Theme::getUrl() ?>/img/group.png" alt="Администратор">
                            </div>
                            <h3>Администраторская область</h3>
                        </div>
                        <hr>

                        <div class="admin-content admin-content_windows" id="admin_block">
                            <div class="core-data-content core-data-content_windows">
                                <div class="data">
                                    <p class="data-name">TD182</p>
                                    <p class="data-value data-value_desc">
                                        <span>Площадь окон</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD182">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD183</p>
                                    <p class="data-value data-value_desc">
                                        <span>Теплосопротивление</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD183">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD184</p>
                                    <p class="data-value data-value_desc">
                                        <span>Тип оконного переплёта</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD184">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD186</p>
                                    <p class="data-value data-value_desc">
                                        <span>Площадь фонарей</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD186">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD188</p>
                                    <p class="data-value data-value_desc">
                                        <span>Теплосопротивление фонаря</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD188">
                                    </p>
                                </div>

                                <div class="data">
                                    <p class="data-name">TD145WI</p>
                                    <p class="data-value data-value_desc">
                                        <span>Фактическое теплосопротивление окон</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD145WI">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD57</p>
                                    <p class="data-value data-value_desc">
                                        <span>Нормируемое теплосопротивление окон</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD57">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD115WI</p>
                                    <p class="data-value data-value_desc">
                                        <span>Температура внутренней поверхности окон</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD115WI">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD117</p>
                                    <p class="data-value data-value_desc">
                                        <span>Температура точки росы в помещении</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD117">
                                    </p>
                                </div>

                                <div class="data">
                                    <p class="data-name">TD145La</p>
                                    <p class="data-value data-value_desc">
                                        <span>Фактическое теплосопротивление фонарей</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD145La">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD58</p>
                                    <p class="data-value data-value_desc">
                                        <span>Нормируемое теплосопротивление фонарей</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD58">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD115La</p>
                                    <p class="data-value data-value_desc">
                                        <span>Температура внутренней поверхности фонарей</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD115La">
                                    </p>
                                </div>
                                <div class="data">
                                    <p class="data-name">TD117</p>
                                    <p class="data-value data-value_desc">
                                        <span>Температура точки росы в помещении</span>
                                    </p>
                                    <p class="data-value">
                                        <input type="text" class="form-control" id="input_TD117La">
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>



                <div class="data">
                    <table border="1" cellpadding="3" style="display: ;" id="for-estimate">

                    </table>
                </div>

            </div>


<!--        </div>-->
    </div>
</section>

<?php $this->theme->footer(); ?>
